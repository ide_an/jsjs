/**
 * @fileOverview コード生成関連
 * @author ide
 */
/**
 * @namespace code generator related container
 */
var codegen = {};
(function(){
  var REQUIRE_STATEMENT_VALUE = false;
  /**
   * @class ラベル。
   */
  var Label = function(id){
    this.id = id;
    this.address = -1;
    this.unresolved = [];
  };
  Label.prototype.add = function(pc,arg_at){
    this.unresolved.push([pc,arg_at]);
  };
  /**
   * @class コード生成まとめ役
   */
  var CodeGenerator = codegen.CodeGenerator = function(){
    this.init();
  };
  CodeGenerator.prototype.init = function(){
    this.ast_visitor = new AstVisitor();
    this.instruction_builder = new InstructionBuilder();
    this.symbol_solver = new SymbolSolver();
    this.exit_manager = new ExitManager();
    this.ast_visitor.init(this.instruction_builder,this.symbol_solver,this.exit_manager);
  }
  CodeGenerator.prototype.compile = function(ast){
    this.ast_visitor.visit(ast);
    return this.instruction_builder.instructions;
  }
  /**
   * @class コード生成
   *
   */
  var InstructionBuilder = codegen.InstructionBuilder = function(){
    this.init();
  };
  InstructionBuilder.prototype.init = function(){
    this.instructions = [];
    this.pc = 0;
    this.label_id = 0;
    this.named_label_for_break = {
      key:[],
      value: []
    };
    this.named_label_for_continue = {
      key:[],
      value: []
    };
    this.label_for_break = []
    this.label_for_continue = []
  };
  InstructionBuilder.prototype.getCurrentAddress = function(){
    return this.pc;
  };
  InstructionBuilder.prototype.newLabel = function(){
    this.label_id++;
    return new Label(this.label_id);
  };
  InstructionBuilder.prototype.setLabel = function(label){
    label.address = this.pc;
  };
  InstructionBuilder.prototype.resolveLabel = function(label){
    var pos;
    while(label.unresolved.length>0){
      pos = label.unresolved.pop(); 
      //console.log(pos);
      this.instructions[pos[0]].operand[pos[1]] = label.address;
    }
  };
  //// named label
  InstructionBuilder.prototype.registerNamedLabelForBreak = function(name,label){
    if(this.named_label_for_break.key.indexOf(name)!== -1){
      throw new parse.ParseError("重複したラベル:"+name);
    }
    this.named_label_for_break.key.push(name);
    this.named_label_for_break.value.push(label);
  };
  InstructionBuilder.prototype.registerNamedLabelForContinue = function(name,label){
    if(this.named_label_for_continue.key.indexOf(name)!== -1){
      throw new parse.ParseError("重複したラベル:"+name);
    }
    this.named_label_for_continue.key.push(name);
    this.named_label_for_continue.value.push(label);
  };
  InstructionBuilder.prototype.getNamedLabelForBreak = function(name){
    var i =this.named_label_for_break.key.indexOf(name); 
    if(i<0){
      throw new parse.ParseError("未定義ラベル:"+name);
    }
    return this.named_label_for_break.value[i];
  };
  InstructionBuilder.prototype.getNamedLabelForContinue = function(name){
    var i =this.named_label_for_continue.key.indexOf(name); 
    if(i<0){
      throw new parse.ParseError("未定義ラベル:"+name);
    }
    return this.named_label_for_continue.value[i];
  };
  InstructionBuilder.prototype.removeNamedLabelForBreak = function(name){
    var i =this.named_label_for_break.key.indexOf(name); 
    if(i<0){
      throw new Error("実装エラー未定義ラベル:"+name);
    }
    this.named_label_for_break.key.splice(i,1);
    this.named_label_for_break.value.splice(i,1);
  };
  InstructionBuilder.prototype.removeNamedLabelForContinue = function(name){
    var i =this.named_label_for_continue.key.indexOf(name); 
    if(i<0){
      throw new Error("実装エラー未定義ラベル:"+name);
    }
    this.named_label_for_continue.key.splice(i,1);
    this.named_label_for_continue.value.splice(i,1);
  };
  //// unnamed label
  InstructionBuilder.prototype.registerLabelForBreak = function(label){
    this.label_for_break.push(label);
  };
  InstructionBuilder.prototype.registerLabelForContinue = function(label){
    this.label_for_continue.push(label);
  };
  InstructionBuilder.prototype.getLabelForBreak = function(){
    if(this.label_for_break.length<1){
      throw new parse.ParseError("break in non breakable statement");
    }
    return this.label_for_break[this.label_for_break.length-1];
  };
  InstructionBuilder.prototype.getLabelForContinue = function(){
    if(this.label_for_continue.length<1){
      throw new parse.ParseError("break in non breakable statement");
    }
    return this.label_for_continue[this.label_for_continue.length-1];
  };
  InstructionBuilder.prototype.removeLabelForBreak = function(){
    this.label_for_break.pop();
  };
  InstructionBuilder.prototype.removeLabelForContinue = function(){
    this.label_for_continue.pop();
  };
  //
  InstructionBuilder.prototype.addInstruction = function(instr){
    var args = [];
    var label;
    for(var i=1;i<arguments.length;i++){
      args[i-1] = arguments[i];
      if(arguments[i] instanceof Label){
        arguments[i].add(this.pc,i-1);
      }
    }
    this.instructions.push(new vm.Instruction(instr,args));
    this.pc++;
  };
  //
  // VM instructions
  //
  // calcuration
  ///unary
  InstructionBuilder.prototype.add_1 = function(){
    this.addInstruction(vm.opecode.add_1);
  };
  InstructionBuilder.prototype.sub_1 = function(){
    this.addInstruction(vm.opecode.sub_1);
  };
  InstructionBuilder.prototype.mul_plus = function(){
    this.addInstruction(vm.opecode.mul_plus);
  };
  InstructionBuilder.prototype.mul_minus = function(){
    this.addInstruction(vm.opecode.mul_minus);
  };
  InstructionBuilder.prototype.comp = function(){
    this.addInstruction(vm.opecode.comp);
  };
  InstructionBuilder.prototype.not = function(){
    this.addInstruction(vm.opecode.not);
  };
  InstructionBuilder.prototype.inc = function(scope_depth,var_name){
    this.addInstruction(vm.opecode.inc,scope_depth,var_name);
  };
  InstructionBuilder.prototype.dec = function(scope_depth,var_name){
    this.addInstruction(vm.opecode.dec,scope_depth,var_name);
  };
  InstructionBuilder.prototype.typeof_ = function(){
    this.addInstruction(vm.opecode.typeof_);
  };
  InstructionBuilder.prototype.get_iterator = function(){
    this.addInstruction(vm.opecode.get_iterator);
  };
  InstructionBuilder.prototype.has_next = function(){
    this.addInstruction(vm.opecode.has_next);
  };
  InstructionBuilder.prototype.get_next = function(){
    this.addInstruction(vm.opecode.get_next);
  };
  ///binary
  InstructionBuilder.prototype.add = function(){
    this.addInstruction(vm.opecode.add);
  };
  InstructionBuilder.prototype.sub = function(){
    this.addInstruction(vm.opecode.sub);
  };
  InstructionBuilder.prototype.mul = function(){
    this.addInstruction(vm.opecode.mul);
  };
  InstructionBuilder.prototype.mod = function(){
    this.addInstruction(vm.opecode.mod);
  };
  InstructionBuilder.prototype.div = function(){
    this.addInstruction(vm.opecode.div);
  };
  InstructionBuilder.prototype.bit_shl = function(){
    this.addInstruction(vm.opecode.bit_shl);
  };
  InstructionBuilder.prototype.bit_shr = function(){
    this.addInstruction(vm.opecode.bit_shr);
  };
  InstructionBuilder.prototype.bit_shr_arith = function(){
    this.addInstruction(vm.opecode.bit_shr_arith);
  };
  InstructionBuilder.prototype.bit_and = function(){
    this.addInstruction(vm.opecode.bit_and);
  };
  InstructionBuilder.prototype.bit_or = function(){
    this.addInstruction(vm.opecode.bit_or);
  };
  InstructionBuilder.prototype.bit_xor = function(){
    this.addInstruction(vm.opecode.bit_xor);
  };
  InstructionBuilder.prototype.lt = function(){
    this.addInstruction(vm.opecode.lt);
  };
  InstructionBuilder.prototype.gt = function(){
    this.addInstruction(vm.opecode.gt);
  };
  InstructionBuilder.prototype.leq = function(){
    this.addInstruction(vm.opecode.leq);
  };
  InstructionBuilder.prototype.geq = function(){
    this.addInstruction(vm.opecode.geq);
  };
  InstructionBuilder.prototype.eq = function(){
    this.addInstruction(vm.opecode.eq);
  };
  InstructionBuilder.prototype.neq = function(){
    this.addInstruction(vm.opecode.neq);
  };
  InstructionBuilder.prototype.eqs = function(){
    this.addInstruction(vm.opecode.eqs);
  };
  InstructionBuilder.prototype.neqs = function(){
    this.addInstruction(vm.opecode.neqs);
  };
  InstructionBuilder.prototype.in_ = function(){
    this.addInstruction(vm.opecode.in_);
  };
  InstructionBuilder.prototype.instanceof_ = function(){
    this.addInstruction(vm.opecode.instanceof_);
  };
  // stack operation
  InstructionBuilder.prototype.pop = function(){
    this.addInstruction(vm.opecode.pop);
  };
  InstructionBuilder.prototype.dup = function(){
    this.addInstruction(vm.opecode.dup);
  };
  InstructionBuilder.prototype.dup2 = function(){
    this.addInstruction(vm.opecode.dup2);
  };
  InstructionBuilder.prototype.rot3 = function(){
    this.addInstruction(vm.opecode.rot3);
  };
  // load/store
  InstructionBuilder.prototype.loadi = function(immidiate){
    this.addInstruction(vm.opecode.loadi,immidiate);
  };
  InstructionBuilder.prototype.load = function(scope_depth,var_name){
    this.addInstruction(vm.opecode.load,scope_depth,var_name);
  };
  InstructionBuilder.prototype.store = function(scope_depth,var_name){
    this.addInstruction(vm.opecode.store,scope_depth,var_name);
  };
  InstructionBuilder.prototype.load_this = function(){
    this.addInstruction(vm.opecode.load_this);
  };
  InstructionBuilder.prototype.store_this = function(){
    this.addInstruction(vm.opecode.store_this);
  };//たぶｎいらない
  // object operation
  InstructionBuilder.prototype.get_prop = function(){
    this.addInstruction(vm.opecode.get_prop);
  };
  InstructionBuilder.prototype.set_prop = function(){
    this.addInstruction(vm.opecode.set_prop);
  };
  InstructionBuilder.prototype.del_prop = function(){
    this.addInstruction(vm.opecode.del_prop);
  };
  InstructionBuilder.prototype.inc_prop = function(){
    this.addInstruction(vm.opecode.inc_prop);
  };
  InstructionBuilder.prototype.dec_prop = function(){
    this.addInstruction(vm.opecode.dec_prop);
  };
  // jump/branch
  InstructionBuilder.prototype.jump = function(label_id){
    this.addInstruction(vm.opecode.jump,label_id);
  };
  InstructionBuilder.prototype.jump_if_true = function(label_id){
    this.addInstruction(vm.opecode.jump_if_true,label_id);
  };
  InstructionBuilder.prototype.jump_if_false = function(label_id){
    this.addInstruction(vm.opecode.jump_if_false,label_id);
  };
  InstructionBuilder.prototype.jump_and_pop_if_equal = function(label_id){
    this.addInstruction(vm.opecode.jump_and_pop_if_equal,label_id);
  };
  InstructionBuilder.prototype.jump_subroutine = function(label_id){
    this.addInstruction(vm.opecode.jump_subroutine,label_id);
  };
  InstructionBuilder.prototype.return_subroutine = function(){
    this.addInstruction(vm.opecode.return_subroutine);
  };
  // function call
  InstructionBuilder.prototype.call = function(arglen){
    this.addInstruction(vm.opecode.call,arglen);
  };
  InstructionBuilder.prototype.call_new = function(arglen){
    this.addInstruction(vm.opecode.call_new,arglen);
  };
  InstructionBuilder.prototype.call_method = function(arglen){
    this.addInstruction(vm.opecode.call_method,arglen);
  };
  InstructionBuilder.prototype.returns = function(){
    this.addInstruction(vm.opecode.returns);
  };
  InstructionBuilder.prototype.generate_func = function(func_declare){
    this.addInstruction(vm.opecode.generate_func,func_declare);
  };
  //other
  InstructionBuilder.prototype.exit = function(){
    this.addInstruction(vm.opecode.exit);
  };
  InstructionBuilder.prototype.add_scope = function(scope){
    var args = [vm.opecode.add_scope].concat(scope);
    this.addInstruction.apply(this,args);
  };
  InstructionBuilder.prototype.add_scope_from_object = function(){
    this.addInstruction(vm.opecode.add_scope_from_object);
  };
  InstructionBuilder.prototype.add_global_scope = function(name){
    this.addInstruction(vm.opecode.add_global_scope,name);
  };
  InstructionBuilder.prototype.remove_scope = function(){
    this.addInstruction(vm.opecode.remove_scope);
  };
  //例外処理
  InstructionBuilder.prototype.throw_ = function(){
    this.addInstruction(vm.opecode.throw_);
  };
  InstructionBuilder.prototype.add_exception_handler = function(label){
    this.addInstruction(vm.opecode.add_exception_handler,label);
  };
  InstructionBuilder.prototype.remove_exception_handler = function(){
    this.addInstruction(vm.opecode.remove_exception_handler);
  };

  /**
   * @class 識別子の名前解決クラス
   */
  var SymbolSolver = codegen.SymbolSolver = function(){
    this.scopes = []
  };
  SymbolSolver.prototype.lookUp = function(identifier_name){
    var depth = 0;
    for(var i=this.scopes.length-1;i>-1;i--){
      //console.log(this.scopes);
      if(this.scopes[i].lastIndexOf(identifier_name)!==-1){
        return depth;
      }
      depth++;
    }
    return -1;
  };
  SymbolSolver.prototype.addScope = function(names){
    this.scopes.push(names);
  };
  SymbolSolver.prototype.removeScope = function(){
    this.scopes.pop();
  };

  /**
   * @class breakやcontinue,throwでブロックを抜けるときの処理を管理するクラス
   */
  var ExitManager = codegen.ExitManager = function(){
    this.scopes = [];
  };
  ExitManager.prototype.addTask = function(task,labels,accept_no_label){
    task = task || null;
    labels = labels || [];
    accept_no_label = accept_no_label || false;
    this.scopes.push({task:task,labels:labels,accept_no_label:accept_no_label});
  };
  ExitManager.prototype.removeTask = function(){
    this.scopes.pop();
  };
  /**
   * breakで抜けるときの処理
   * @param {String} [label=""] labelラベルでブレークする
   */
  ExitManager.prototype.onBreak = function(label){
    label = label || null;
    var scope;
    var len = this.scopes.length;
    if(label===null){
      console.log("ok",this.scopes.join());
      for(var i=len-1;i>-1;i--){
        console.log(i);
        scope = this.scopes[i];
        if(scope.accept_no_label){
          if(scope.task){
            scope.task(false);
          }
          break;
        }
      }
    }else{
      for(var i=len-1;i>-1;i--){
        scope = this.scopes[i];
        if(scope.task){
          scope.task(false);
        }
        if(scope.labels.indexOf(label)>-1){
          break;
        }
      }
    }
  };
  /**
   * continueで抜けるときの処理
   * @param {String} [label=""] labelラベルでコンティニューする
   */
  ExitManager.prototype.onContinue = function(label){
    label = label || null;
    var scope;
    var len = this.scopes.length;
    if(label===null){
    }else{
      for(var i=len-1;i>-1;i--){
        scope = this.scopes[i];
        if(scope.labels.indexOf(label)>-1){
          break;
        }
        if(scope.task){
          scope.task(false);
        }
      }
    }
  };
  /**
   * return で抜けるときの処理
   */
  ExitManager.prototype.onReturn = function(){
    //すべてのブロックを抜けるはず
    var scope;
    var len = this.scopes.length;
    for(var i=len-1;i>-1;i--){
      scope = this.scopes[i];
      if(scope.task){
        scope.task(true);
      }
    }
  };
  /**
   * throwで抜けるときの処理
   */
  ExitManager.prototype.onThrow = function(){
    //TODO: implement
    var len = this.scopes.length;
    if(len > 0){
      var scope = this.scopes[len-1];
      if(scope.task){
        scope.task(false);
      }
    }
  };

  /**
   * @class ASTをたどるクラス
   */
  var AstVisitor = codegen.AstVisitor = function(){};
  AstVisitor.prototype.init = function(instruction_builder,symbol_solver,exit_manager){
    /**
     * @type codegen.InstructionBuilder
     */
    this.ib = instruction_builder;
    /**
     * @type codegen.SymbolSolver
     */
    this.ss = symbol_solver;
    /**
     * @type codegen.ExitManager
     */
    this.em = exit_manager;

    this.func_declarations = [];
  };
  AstVisitor.prototype.removeOldValueBeforeStatement = function(){
    if(REQUIRE_STATEMENT_VALUE){
      this.ib.pop();
    }
  };
  AstVisitor.prototype.visit = function(node,reference_required){
    reference_required = reference_required || false;
    if(node instanceof ast.Arguments){
      return this.visitArguments(node);
    }
    if(node instanceof ast.ArrayLiteral){
      return this.visitArrayLiteral(node);
    }
    if(node instanceof ast.Assignment){
      return this.visitAssignment(node);
    }
    if(node instanceof ast.BinaryOperation){
      return this.visitBinaryOperation(node);
    }
    if(node instanceof ast.BlockStatement){
      return this.visitBlockStatement(node);
    }
    if(node instanceof ast.BreakStatement){
      return this.visitBreakStatement(node);
    }
    if(node instanceof ast.Call){
      return this.visitCall(node);
    }
    if(node instanceof ast.CallNew){
      return this.visitCallNew(node);
    }
    if(node instanceof ast.CaseClause){
      return this.visitCaseClause(node);
    }
    if(node instanceof ast.Conditional){
      return this.visitConditional(node);
    }
    if(node instanceof ast.ContinueStatement){
      return this.visitContinueStatement(node);
    }
    if(node instanceof ast.DoWhileStatement){
      return this.visitDoWhileStatement(node);
    }
    if(node instanceof ast.EmptyStatement){
      return this.visitEmptyStatement(node);
    }
    if(node instanceof ast.ExpressionStatement){
      return this.visitExpressionStatement(node);
    }
    if(node instanceof ast.ForInStatement){
      return this.visitForInStatement(node);
    }
    if(node instanceof ast.ForStatement){
      return this.visitForStatement(node);
    }
    if(node instanceof ast.FunctionExpression){
      return this.visitFunctionExpression(node);
    }
    if(node instanceof ast.Identifier){
      return this.visitIdentifier(node);
    }
    if(node instanceof ast.IfStatement){
      return this.visitIfStatement(node);
    }
    if(node instanceof ast.Literal){
      return this.visitLiteral(node);
    }
    if(node instanceof ast.ObjectLiteral){
      return this.visitObjectLiteral(node);
    }
    if(node instanceof ast.PostfixOperation){
      return this.visitPostfixOperation(node);
    }
    if(node instanceof ast.Program){
      return this.visitProgram(node);
    }
    if(node instanceof ast.PropertyAccessor){
      return this.visitPropertyAccessor(node,reference_required);
    }
    if(node instanceof ast.RegExpLiteral){
      return this.visitRegExpLiteral(node);
    }
    if(node instanceof ast.ReturnStatement){
      return this.visitReturnStatement(node);
    }
    if(node instanceof ast.SwitchStatement){
      return this.visitSwitchStatement(node);
    }
    if(node instanceof ast.This){
      return this.visitThis(node);
    }
    if(node instanceof ast.ThrowStatement){
      return this.visitThrowStatement(node);
    }
    if(node instanceof ast.TryCatchStatement){
      return this.visitTryCatchStatement(node);
    }
    if(node instanceof ast.TryFinallyStatement){
      return this.visitTryFinallyStatement(node);
    }
    if(node instanceof ast.UnaryOperation){
      return this.visitUnaryOperation(node);
    }
    if(node instanceof ast.WhileStatement){
      return this.visitWhileStatement(node);
    }
    if(node instanceof ast.WithStatement){
      return this.visitWithStatement(node);
    }
  };

//  ast . Arguments
  AstVisitor.prototype.visitArguments = function(/** ast.Node */node){
    var i = 0;
    while(i<node.args.length){
      this.visit(node.args[i]);
      i++;
    }
  };
//  ast . ArrayLiteral
  AstVisitor.prototype.visitArrayLiteral = function(/** ast.Node */node){
    var i = 0;
    this.ib.loadi(new jsobjects.JSArray());
    while(i<node.values.length){
      this.ib.dup();
      this.ib.loadi(new jsobjects.JSNumber(i));
      this.visit(node.values[i]);
      this.ib.set_prop();
      i++;
    }
  };
//  ast . Assignment
  AstVisitor.prototype.visitAssignment = function(/** ast.Node */node){
    var var_name,scope_depth;
    // reference　生成
    if(node.target instanceof ast.PropertyAccessor){
      this.visit(node.target,true);
      this.ib.dup2();
    }else{
      var_name = node.target.name;
      scope_depth = this.ss.lookUp(var_name);
    }
    if(node.isCompound()){
      if(node.target instanceof ast.PropertyAccessor){
        this.ib.dup2();
        this.ib.get_prop();
      }else if(node.target instanceof ast.Identifier){
        this.ib.load(scope_depth,var_name);
      }
      //右辺値生成
      this.visit(node.value);
      this.executeBinaryOperation(node.getBinaryOp());
    }else{
      this.visit(node.value);
    }
    if(node.target instanceof ast.PropertyAccessor){
      this.ib.set_prop();
      this.ib.get_prop();//戻り値として返すため
    }else if(node.target instanceof ast.Identifier){
      this.ib.dup();//戻り値として返すため
      this.ib.store(scope_depth,var_name);
    }else{
      this.visit(node.expression);
      throw new Error("reference Error");
    }
  };
//  ast . BinaryOperation
  AstVisitor.prototype.executeBinaryOperation = function(/** tokenize.TOKEN_TYPE */operator){
    switch(operator){
      case tokenize.TOKEN_TYPE.P_ADD: this.ib.add();break; //generate add instruction
      case tokenize.TOKEN_TYPE.P_SUB: this.ib.sub();break;
      case tokenize.TOKEN_TYPE.P_MUL: this.ib.mul();break;
      case tokenize.TOKEN_TYPE.P_MOD: this.ib.mod();break;
      case tokenize.TOKEN_TYPE.P_DIV: this.ib.div();break;
      case tokenize.TOKEN_TYPE.P_SHL: this.ib.bit_shl();break;
      case tokenize.TOKEN_TYPE.P_SHR: this.ib.bit_shr();break;
      case tokenize.TOKEN_TYPE.P_SHR_ARITH: this.ib.bit_shr_arith();break;
      case tokenize.TOKEN_TYPE.P_AND: this.ib.bit_and();break;
      case tokenize.TOKEN_TYPE.P_OR: this.ib.bit_or();break;
      case tokenize.TOKEN_TYPE.P_XOR: this.ib.bit_xor();break;
      //case tokenize.TOKEN_TYPE.P_AND_LOGIC: 
      //  this.ib.and_logic();
      //  break;
      //case tokenize.TOKEN_TYPE.P_OR_LOGIC: 
      //  this.ib.or_logic();
      //  break;
      case tokenize.TOKEN_TYPE.P_LT: this.ib.lt(); break;
      case tokenize.TOKEN_TYPE.P_GT: this.ib.gt();break;
      case tokenize.TOKEN_TYPE.P_LEQ: this.ib.leq();break;
      case tokenize.TOKEN_TYPE.P_GEQ: this.ib.geq();break;
      case tokenize.TOKEN_TYPE.P_EQUAL: this.ib.eq();break;
      case tokenize.TOKEN_TYPE.P_NOT_EQUAL: this.ib.neq();break;
      case tokenize.TOKEN_TYPE.P_EQAUL_STRICT: this.ib.eqs();break;
      case tokenize.TOKEN_TYPE.P_NOT_EQUAL_STRICT: this.ib.neqs();break;
      case tokenize.TOKEN_TYPE.P_COMMA: break;
      case tokenize.TOKEN_TYPE.K_IN: this.ib.in_();break;
      case tokenize.TOKEN_TYPE.K_INSTANCEOF: this.ib.instanceof_();break;
      default:
        throw new Error("コンパイルできない二項演算："+operator);
    }
  };
  AstVisitor.prototype.visitBinaryOperation = function(/** ast.Node */node){
    var jump_to;
    var operator = node.operator;
    if(operator !== tokenize.TOKEN_TYPE.P_AND_LOGIC &&
       operator !== tokenize.TOKEN_TYPE.P_OR_LOGIC &&
       operator !== tokenize.TOKEN_TYPE.P_COMMA){
      this.visit(node.left);
      this.visit(node.right);
      this.executeBinaryOperation(operator);
    }else if(operator === tokenize.TOKEN_TYPE.P_COMMA){
      this.visit(node.left);
      this.ib.pop();
      this.visit(node.right);
    }else if(operator === tokenize.TOKEN_TYPE.P_AND_LOGIC){
      jump_to = this.ib.newLabel();
      this.visit(node.left);
      this.ib.dup();
      this.ib.jump_if_false(jump_to);
      this.ib.pop();
      this.visit(node.right);
      this.ib.setLabel(jump_to);
      this.ib.resolveLabel(jump_to);
    }else if(operator === tokenize.TOKEN_TYPE.P_OR_LOGIC){
      jump_to = this.ib.newLabel();
      this.visit(node.left);
      this.ib.dup();
      this.ib.jump_if_true(jump_to);
      this.ib.pop();
      this.visit(node.right);
      this.ib.setLabel(jump_to);
      this.ib.resolveLabel(jump_to);
    }
  };
  var registerNamedBreakLabel = function(visitor,labelnames){
    var labels = [];
    for(var i=0;i<labelnames.length;i++){
      labels[i] = visitor.ib.newLabel();
      visitor.ib.registerNamedLabelForBreak(labelnames[i],labels[i]);
      //ラベル名とidの組を登録,breakのみのジャンプ先
    }
    return labels;
  };
  var removeNamedBreakLabel = function(visitor,labelnames,labels){
    for(var i=0;i<labelnames.length;i++){
      visitor.ib.setLabel(labels[i]);
      visitor.ib.resolveLabel(labels[i]);
      visitor.ib.removeNamedLabelForBreak(labelnames[i]);
      //ラベル名とidの組を削除
    }
  };
  var registerNamedContinueLabel = function(visitor,labelnames){
    var labels = [];
    for(var i=0;i<labelnames.length;i++){
      labels[i] = visitor.ib.newLabel();
      visitor.ib.registerNamedLabelForContinue(labelnames[i],labels[i]);
      //ラベル名とidの組を登録,Continueのみのジャンプ先
    }
    return labels;
  };
  var removeNamedContinueLabel = function(visitor,labelnames,labels){
    for(var i=0;i<labelnames.length;i++){
      visitor.ib.setLabel(labels[i]);
      visitor.ib.resolveLabel(labels[i]);
      visitor.ib.removeNamedLabelForContinue(labelnames[i]);
      //ラベル名とidの組を削除
    }
  };
//  ast . BlockStatement
  AstVisitor.prototype.visitBlockStatement = function(/** ast.Node */node){
    var labels_for_break = registerNamedBreakLabel(this,node.label);

    this.em.addTask(null,node.label);

    //前の文の値は消さない。
    for(var i=0;i<node.statements.length;i++){
      this.visit(node.statements[i]);
    }

    this.em.removeTask();

    removeNamedBreakLabel(this,node.label,labels_for_break);
  };
//  ast . BreakStatement
  AstVisitor.prototype.visitBreakStatement = function(/** ast.Node */node){
    //TODO: 例外処理
    var jump_to;
    this.em.onBreak(node.label);
    if(node.label!==null){
      jump_to = this.ib.getNamedLabelForBreak(node.label);
      //ラベル名からid取得。
      //該当IDがなければエラー 
    }else{
      jump_to = this.ib.getLabelForBreak();
      //ブレーク先を格納したスタックからid取得
    }
    this.ib.jump(jump_to);
  };
//  ast . Call
  AstVisitor.prototype.visitCall = function(/** ast.Node */node){
    var arglen = node.args.args.length;
    if(node.callee instanceof ast.PropertyAccessor){//call_method
      this.visit(node.callee.base);
      this.ib.dup();
      this.visit(node.callee.property);
      this.ib.get_prop();
      this.visit(node.args);
      this.ib.call_method(arglen);
    }else{//call
      this.visit(node.callee);
      this.visit(node.args);
      this.ib.call(arglen);
    }
  };
//  ast . CallNew
  AstVisitor.prototype.visitCallNew = function(/** ast.Node */node){
    this.visit(node.callee);
    this.visit(node.args);
    var arglen = node.args.args.length;
    this.ib.call_new(arglen);
  };
//  ast . CaseClause
  AstVisitor.prototype.visitCaseClause = function(/** ast.Node */node,label_id){//visitSwitchStatementから直接呼ぶ。
      this.ib.setLabel(label_id);
      this.ib.resolveLabel(label_id);
      for(var i=0;i<node.statements.length;i++){
        this.visit(node.statements[i]);
      }
  };
//  ast . Conditional
  AstVisitor.prototype.visitConditional = function(/** ast.Node */node){
    var jump_false = this.ib.newLabel();
    var jump_true = this.ib.newLabel();
    this.visit(node.condition);
    this.ib.jump_if_false(jump_false);
    this.visit(node.then);
    this.ib.jump(jump_true);
    this.ib.setLabel(jump_false);
    this.ib.resolveLabel(jump_false);
    this.visit(node.else_);
    this.ib.setLabel(jump_true);
    this.ib.resolveLabel(jump_true);
  };
//  ast . ContinueStatement
  AstVisitor.prototype.visitContinueStatement = function(/** ast.Node */node){
    //TODO: 例外処理
    var jump_to;
    this.em.onContinue(node.label);
    if(node.label!==null){
      jump_to = this.ib.getNamedLabelForContinue(node.label);
      //ラベル名からid取得。
      //該当IDがなければエラー 
    }else{
      jump_to = this.ib.getLabelForContinue();
      //コンティニュー先を格納したスタックからid取得
    }
    this.ib.jump(jump_to);
  };
//  ast . DoWhileStatement
  AstVisitor.prototype.visitDoWhileStatement = function(/** ast.Node */node){
    // label_next:
    //     statements
    //     ...
    // label_continue:
    //     expression
    //     jump_if_true label_next
    // label_end
    var label_next = this.ib.newLabel();
    var label_continue = this.ib.newLabel();
    var label_end = this.ib.newLabel();
    var labels_for_break = registerNamedBreakLabel(this,node.label);
    var labels_for_continue = registerNamedContinueLabel(this,node.label);

    this.em.addTask(null,node.label,true);

    this.ib.registerLabelForBreak(label_end);
    this.ib.registerLabelForContinue(label_continue);
    // break,continueそれぞれのジャンプ先スタックに追加
    
    if(REQUIRE_STATEMENT_VALUE){
      this.removeOldValueBeforeStatement();//前の式の値を捨てとく
      this.ib.loadi(new jsobjects.JSUndefined());
    }

    this.ib.setLabel(label_next);
    //
    //前の文の値は消さない。
    this.visit(node.statement);

    this.em.removeTask();

    removeNamedContinueLabel(this,node.label,labels_for_continue);
    this.ib.setLabel(label_continue);
    this.ib.resolveLabel(label_continue);

    this.visit(node.expression);
    this.ib.jump_if_true(label_next);
    this.ib.resolveLabel(label_next);

    removeNamedBreakLabel(this,node.label,labels_for_break);
    this.ib.setLabel(label_end);
    this.ib.resolveLabel(label_end);

    this.ib.removeLabelForBreak();
    this.ib.removeLabelForContinue();
    // break,continueそれぞれのジャンプ先スタックをpop

  };
//  ast . EmptyStatement
  AstVisitor.prototype.visitEmptyStatement = function(/** ast.Node */node){
    //からっぽ～
  };
//  ast . ExpressionStatement
  AstVisitor.prototype.visitExpressionStatement = function(/** ast.Node */node){
    this.removeOldValueBeforeStatement(); //前に積んであった値を捨てる。
    this.visit(node.expression);
    if(!REQUIRE_STATEMENT_VALUE){
      this.ib.pop();
    }
  };
//  ast . ForInStatement
  AstVisitor.prototype.visitForInStatement = function(/** ast.Node */node){
    //  enumerable評価
    //  get_iterator
    //  jump continue
    //next:
    //         ;キー取得
    //  dup 
    //  get_next
    //  左辺式 評価
    //  左辺式 に 次のキーを代入
    //  stmt 評価
    //continue:
    //         ;次のキーがあるかどうか
    //  dup 
    //  has_next
    //  jump_if_true next
    //on_exit: ;jump_subroutineで呼び出す？  
    //  pop    ;イテレータを成仏
    //end:

    var label_continue = this.ib.newLabel();
    var label_end = this.ib.newLabel();
    var label_next = this.ib.newLabel();
    var labels_for_break = registerNamedBreakLabel(this,node.label);
    var labels_for_continue = registerNamedContinueLabel(this,node.label);

    this.ib.registerLabelForContinue(label_continue);
    this.ib.registerLabelForBreak(label_end);
    var that = this;
    this.em.addTask(function(){//ブロックを抜ける場合の処理
      //後始末としてスタックに積んでるイテレータをpopする
      console.log("for-in exit do");
      that.ib.pop();
    },node.label,true);

    this.visit(node.enumerable); //obj評価
    this.ib.get_iterator(); //イテレータ取得
    this.ib.jump(label_continue);

    this.ib.setLabel(label_next);

    this.ib.dup();
    this.ib.get_next();//キー取得
    //左辺式評価
    if(node.key instanceof ast.PropertyAccessor){
      this.visit(node.key,true);
      this.ib.rot3(); //キーの値をスタックトップに持ってくる
      this.ib.set_prop();//キー代入
    }else{//node.key instanceof ast.Identifier
      var var_name = node.key.name;
      var scope_depth = this.ss.lookUp(var_name);
      this.ib.store(scope_depth,var_name);//キー代入
    }

    this.visit(node.statement);

    removeNamedContinueLabel(this,node.label,labels_for_continue);
    this.ib.setLabel(label_continue);
    this.ib.resolveLabel(label_continue);

    this.ib.dup();
    this.ib.has_next();//次のキー取得
    this.ib.jump_if_true(label_next); //キーがあれば継続
    this.ib.resolveLabel(label_next);

    //後始末としてスタックに積んでるイテレータをpopする
    this.ib.pop();

    removeNamedBreakLabel(this,node.label,labels_for_break);
    this.ib.setLabel(label_end);
    this.ib.resolveLabel(label_end);

    this.em.removeTask();

    this.ib.removeLabelForBreak();
    this.ib.removeLabelForContinue();
    // break,continueそれぞれのジャンプ先スタックをpop
  };
//  ast . ForStatement
  AstVisitor.prototype.visitForStatement = function(/** ast.Node */node){
    //    init
    //    jump continue
    //  next:
    //    statement
    //    next_do
    //  continue:
    //    condition
    //    jump_if_true next
    //  break:
    var label_next = this.ib.newLabel();
    var label_continue = this.ib.newLabel();
    var label_end = this.ib.newLabel();
    var labels_for_break = registerNamedBreakLabel(this,node.label);
    var labels_for_continue = registerNamedContinueLabel(this,node.label);

    //前の文の値を処理する。
    if(REQUIRE_STATEMENT_VALUE){
      this.removeOldValueBeforeStatement();//前の式の値を捨てとく
      this.ib.loadi(new jsobjects.JSUndefined());
    }
    this.ib.registerLabelForBreak(label_end);
    this.ib.registerLabelForContinue(label_continue);
    //break,continueそれぞれのジャンプ先スタックに追加

    this.em.addTask(null,node.label,true);

    if(node.init){
      this.visit(node.init);
      this.ib.pop();//値を捨てる。
    }
    this.ib.jump(label_continue);
    this.ib.setLabel(label_next);
    this.visit(node.statement);
    if(node.next){
      this.visit(node.next);
      this.ib.pop();//値を捨てる。
    }

    removeNamedContinueLabel(this,node.label,labels_for_continue);
    this.ib.setLabel(label_continue);
    this.ib.resolveLabel(label_continue);

    if(node.condition){
      this.visit(node.condition);
      this.ib.jump_if_true(label_next);
    }else{
      this.ib.jump(label_next);
    }
    this.ib.resolveLabel(label_next);

    removeNamedBreakLabel(this,node.label,labels_for_break);
    this.ib.setLabel(label_end);
    this.ib.resolveLabel(label_end);

    this.em.removeTask();

    this.ib.removeLabelForBreak();
    this.ib.removeLabelForContinue();
    // break,continueそれぞれのジャンプ先スタックをpop
  };
//  ast . FunctionExpression
  AstVisitor.prototype.visitFunctionExpression = function(/** ast.Node */node){
    var declaration = new jsobjects.JSFunctionDeclaration(-1,node.argumentNames,node.name);
    this.func_declarations.push({
      node:node,
      declaration:declaration
    });
    this.ib.generate_func(declaration);
  };
  AstVisitor.prototype.visitFunctionDeclaration = function(node){
    var scope = ["arguments"];
    if(node.name){
      scope.unshift(node.name)
    }
    this.ss.addScope(scope);
    scope = node.argumentNames;
    this.ss.addScope(scope);
    scope=[];
    for(var i=0;i<node.functionDeclares.length;i++){
      scope.push(node.functionDeclares[i].name);
    }
    scope = scope.concat(node.variableNames);
    //console.log(scope,node.variableNames);
    this.ss.addScope(scope);
    this.ib.add_scope(scope);
    for(var i=0;i<node.functionDeclares.length;i++){
      this.visit(node.functionDeclares[i].func);
      this.ib.store(i,node.functionDeclares[i].name);
    }
    for(var i=0;i<node.statements.length;i++){
      this.visit(node.statements[i]);
    }
    this.ib.loadi(new jsobjects.JSUndefined());//ダメ押しのreturn ;
    this.ib.returns();
    while(this.func_declarations.length>0){
      var f = this.func_declarations.pop();
      f.declaration.address = this.ib.getCurrentAddress();
      this.visitFunctionDeclaration(f.node);
    }
    this.ss.removeScope();
  };
//  ast . Identifier
  AstVisitor.prototype.visitIdentifier = function(/** ast.Node */node){
    var var_name,scope_depth;
    var_name = node.name;
    scope_depth = this.ss.lookUp(var_name);
    this.ib.load(scope_depth,var_name);
  };
//  ast . IfStatement
  AstVisitor.prototype.visitIfStatement = function(/** ast.Node */node){
    //    condition
    //    jump_if_false end
    //    then_do
    //    jump end
    //  end:
    //        or
    //    condition
    //    jump_if_false else
    //    then_do
    //    jump end
    //  else:
    //    else_do
    //  end:
    var label_else = this.ib.newLabel(),label_end = this.ib.newLabel();
    var labels_for_break = registerNamedBreakLabel(this,node.label);

    this.em.addTask(null,node.label);

    if(REQUIRE_STATEMENT_VALUE){
      this.removeOldValueBeforeStatement();//前の式の値を捨てとく
      this.ib.loadi(new jsobjects.JSUndefined());
    }

    this.visit(node.condition);
    if(node.elseStatement === null){
      this.ib.jump_if_false(label_end);
      this.visit(node.thenStatement);
    }else{
      this.ib.jump_if_false(label_else);
      this.visit(node.thenStatement);
      this.ib.jump(label_end);

      this.ib.setLabel(label_else);
      this.ib.resolveLabel(label_else);
      this.visit(node.elseStatement);
    }

    this.em.removeTask();

    removeNamedBreakLabel(this,node.label,labels_for_break);
    this.ib.setLabel(label_end);
    this.ib.resolveLabel(label_end);
  };
//  ast . Literal
  AstVisitor.prototype.visitLiteral = function(/** ast.Node */node){
    this.ib.loadi(node.value);
  };
//  ast . ObjectLiteral
  AstVisitor.prototype.visitObjectLiteral = function(/** ast.Node */node){
    var i = 0;
    // load Object
    //this.ib.call_new(0);
    this.ib.loadi(new jsobjects.JSObject());
    while(i<node.values.length){
      this.ib.dup();
      this.ib.loadi(node.keys[i].value);//
      this.visit(node.values[i]);
      this.ib.set_prop();
      i++;
    }
  };
//  ast . PostfixOperation
  AstVisitor.prototype.visitPostfixOperation = function(/** ast.Node */node){
    var op = node.operator;
    var var_name,scope_depth;
    switch(op){
      case tokenize.TOKEN_TYPE.P_INC://++
        if(node.expression instanceof ast.Identifier){
          var_name = node.expression.name;
          scope_depth = this.ss.lookUp(var_name);
          this.ib.inc(scope_depth,var_name)
        }else if(node.expression instanceof ast.PropertyAccessor){
          this.visit(node.expression,true);
          this.ib.inc_prop();
        }else{
          this.visit(node.expression);
          throw new Error("reference Error");
        }
        break;
      case tokenize.TOKEN_TYPE.P_DEC://--
        if(node.expression instanceof ast.Identifier){
          var_name = node.expression.name;
          scope_depth = this.ss.lookUp(var_name);
          this.ib.dec(scope_depth,var_name)
        }else if(node.expression instanceof ast.PropertyAccessor){
          this.visit(node.expression,true);
          this.ib.dec_prop();
        }else{
          this.visit(node.expression);
          throw new Error("reference Error");
        }
        break;
    }
  }
//  ast . Program
  AstVisitor.prototype.visitProgram = function(/** ast.Node */node,/** Boolean */reference_required){
    var scope = [];
    for(var i=0;i<node.functionDeclares.length;i++){
      scope.push(node.functionDeclares[i].name);
    }
    scope = scope.concat(node.variableNames);
    //console.log(scope,node.variableNames);
    this.ss.addScope(scope);
    for(var i=0;i<scope.length;i++){
      this.ib.add_global_scope(scope[i]);
    }
    for(var i=0;i<node.functionDeclares.length;i++){
      this.visit(node.functionDeclares[i].func);
      this.ib.store(i,node.functionDeclares[i].name);
    }
    for(var i=0;i<node.statements.length;i++){
      this.visit(node.statements[i]);
    }
    this.ib.exit();
    while(this.func_declarations.length>0){
      var f = this.func_declarations.pop();
      f.declaration.address = this.ib.getCurrentAddress();
      this.visitFunctionDeclaration(f.node);
    }
  };
//  ast . PropertyAccessor
  AstVisitor.prototype.visitPropertyAccessor = function(/** ast.Node */node,/** Boolean */reference_required){
    this.visit(node.base);
    this.visit(node.property);
    if(!reference_required){
      this.ib.get_prop();
    }
  };
//  ast . RegExpLiteral
  AstVisitor.prototype.visitRegExpLiteral = function(/** ast.Node */node,/** Boolean */reference_required){
    //TODO: implement
    //this.ib.loadi(new jsobjects.JSRegExp());
  };
//  ast . ReturnStatement
  AstVisitor.prototype.visitReturnStatement = function(/** ast.Node */node,/** Boolean */reference_required){
    //TODO: 例外処理（finallyの場合）
    if(REQUIRE_STATEMENT_VALUE){
      this.removeOldValueBeforeStatement();//前の式の値を捨てとく
      this.ib.loadi(new jsobjects.JSUndefined());
    }

    if(node.expression !== null){
      this.visit(node.expression);
    }else{
      this.ib.loadi(new jsobjects.JSUndefined());
    }

    this.em.onReturn();
    this.ib.returns();
  };
//  ast . SwitchStatement
  AstVisitor.prototype.visitSwitchStatement = function(/** ast.Node */node,/** Boolean */reference_required){
    //  exp
    //  dup
    //  exp1
    //  jump_if_eq l1
    //  dup
    //  exp2
    //  jump_if_eq l2
    //  jump def d
    var default_label_index = -1;
    var case_labels = [];
    var label_end = this.ib.newLabel();
    var labels_for_break = registerNamedBreakLabel(this,node.label);
    this.ib.registerLabelForBreak(label_end);
    // breakジャンプ先スタックに追加

    this.em.addTask(null,node.label,true);

    this.visit(node.expression);
    for(var i=0;i<node.caseClauses.length;i++){
      case_labels[i] = this.ib.newLabel();
      if(node.caseClauses[i].label !== null){//case exp:...
        this.ib.dup();
        this.visit(node.caseClauses[i].label);
        this.ib.jump_and_pop_if_equal(case_labels[i]);
      }else{//default:...
        default_label_index = i;
      }
    }
    if(default_label_index>-1){
      this.ib.pop();
      this.ib.jump(case_labels[default_label_index]);
    }
    for(var i=0;i<node.caseClauses.length;i++){
      this.visitCaseClause(node.caseClauses[i],case_labels[i]);
    }

    removeNamedBreakLabel(this,node.label,labels_for_break);
    this.ib.setLabel(label_end);
    this.ib.resolveLabel(label_end);

    this.em.removeTask();

    this.ib.removeLabelForBreak();
    // breakジャンプ先スタックをpop
  };
//  ast . This
  AstVisitor.prototype.visitThis = function(/** ast.Node */node,/** Boolean */reference_required){
    if(reference_required){
      this.ib.store_this();
    }else{
      this.ib.load_this();
    }
    //そもそもthisは左辺値になれない？
  };
//  ast . ThrowStatement
  AstVisitor.prototype.visitThrowStatement = function(/** ast.Node */node){
    //  exp
    //  ;on throw task //finallyするときとか
    //  throw_
    this.visit(node.expression);
    this.em.onThrow();
    this.ib.throw_();
  };
//  ast . TryCatchStatement
  AstVisitor.prototype.visitTryCatchStatement = function(/** ast.Node */node){
    //try:
    //  stmt
    //  jump end
    //catch:
    //  add_scope [e]
    //  store 0 e
    //  stmt2
    //  remove_scope
    //end:

    var label_end = this.ib.newLabel();
    var label_catch = this.ib.newLabel();
    
    //begin try
    this.ib.add_exception_handler(label_catch);
    var that = this;
    var on_exit_try = function(){
      // tryブロックから抜けるときの処理
      that.ib.remove_exception_handler();
    };
    this.em.addTask(on_exit_try,[]);

    this.visit(node.tryBlock);
    on_exit_try();
    this.ib.jump(label_end);

    this.em.removeTask();
    //end try
    //begin catch
    this.ib.setLabel(label_catch);
    this.ib.resolveLabel(label_catch);
    var scope = [node.exception.name];//catch(e)
    this.ss.addScope(scope);
    this.ib.add_scope(scope);
    this.ib.store(0,scope[0]);//e <= Exception
    on_exit_try();

    this.em.addTask(function(){
      that.ib.remove_scope();
    },[]);

    this.visit(node.catchBlock);

    this.ib.remove_scope();
    this.ss.removeScope();

    this.em.removeTask();
    //end catch
    this.ib.setLabel(label_end);
    this.ib.resolveLabel(label_end);

  };
//  ast . TryFinallyStatement
  AstVisitor.prototype.visitTryFinallyStatement = function(/** ast.Node */node){
    //try:
    //  add_scope ["@return","@shouldpop"]
    //  stmt
    //  jump_subroutine finally
    //  jump end
    //catch:
    //  loadi true
    //  store 0 "@shouldpop"
    //  jump_subroutine finally
    //  throw_
    //finally:
    //  store 0 "@return";戻り番地格納
    //  stmt2
    //  load 0 "@return";戻り番地吐いてジャンプ
    //  remove_scope 
    //  return_subroutine
    //end:
    // ; tryをexitするときはjump_subroutine finallyさせる
    // ; finallyをexitするときもremove_scopeする
    // ; finallyをexitするときは@shouldpopの値を見て真ならpopする。(積んでる戻り値や例外オブジェクトを成仏させる)

    //TODO: implement

    //TODO:finallyでbreakした場合などアキュムレータの掃除をどうする？？？？
    //return ないし throwで抜ける場合はスタックに余計なものを積んでる(戻り値、例外オブジェクト
    //これらのケースでは変数@shouldpopに真値を入れる？
    
    //jump_subroutineの戻り番地はスタックに積む
    //変数@returnに格納する <-新たにスコープを生成する

    // finally は　外道

    var label_end = this.ib.newLabel();
    var label_catch = this.ib.newLabel();
    var label_finally = this.ib.newLabel();

    var that = this;
    var scope = ["@return","@shouldpop"];

    //begin try
    this.ss.addScope(scope);
    this.ib.add_scope(scope);
    this.ib.loadi(new jsobjects.JSBoolean(false));
    this.ib.store(0,scope[1]);// @shouldpop <= true

    this.em.addTask(function(is_return){
      //TODO: return でもどる場合は@shouldpop <= true
      if(is_return){
        that.ib.loadi(new jsobjects.JSBoolean(true));
        that.ib.store(0,scope[1]);// @shouldpop <= true
      }
      that.ib.jump_subroutine(label_finally);
    },[]);
    this.visit(node.tryBlock);
    this.ib.jump_subroutine(label_finally);
    this.ib.jump(label_end);
    this.em.removeTask();
    //end try
    //begin catch
    this.ib.setLabel(label_catch);
    this.ib.resolveLabel(label_catch);
    this.ib.loadi(new jsobjects.JSBoolean(true));
    this.ib.store(0,scope[1]);// @shouldpop <= true
    this.ib.jump_subroutine(label_finally);
    this.ib.throw_();// throw e;スタックトップに例外オブジェクトがあるはず
    //end catch
    //begin finally
    this.ib.setLabel(label_finally);
    this.ib.resolveLabel(label_finally);

    this.em.addTask(function(){
      //スコープを成仏。@shouldpop==trueならスタックのpop
      var lable_dont_pop = that.ib.newLabel();
      //TODO:スコープ解決は必要？
      that.ib.load(0,scope[1]);
      that.ib.jump_if_false(lable_dont_pop);
      that.ib.pop();
      that.ib.setLabel(lable_dont_pop);
      that.ib.resolveLabel(lable_dont_pop);
      that.ib.remove_scope();
    },[]);
    this.ib.store(0,scope[0]);//戻り番地格納

    this.visit(node.finallyBlock);

    this.ib.load(0,scope[0]);//戻り番地
    this.ib.remove_scope();
    this.ib.return_subroutine();
    this.em.removeTask();
    //end finally
    this.ib.setLabel(label_end);
    this.ib.resolveLabel(label_end);
    this.ss.removeScope();
  };
//  ast . UnaryOperation
  AstVisitor.prototype.visitUnaryOperation = function(/** ast.Node */node){
    var op = node.operator;
    var var_name,scope_depth;
    switch(op){
      case tokenize.TOKEN_TYPE.K_DELETE:
        if(node.expression instanceof ast.Identifier){
          var_name = node.expression.name;
          scope_depth = this.ss.lookUp(var_name);
          this.ib.del_var(scope_depth,var_name)
        }else if(node.expression instanceof ast.PropertyAccessor){
          this.visit(node.expression,true);
          this.ib.del_prop();
        }else{
          this.visit(node.expression);
          this.ib.pop();
          this.ib.loadi(new jsobjects.JSBoolean(true));
        }
        break;
      case tokenize.TOKEN_TYPE.K_VOID:
        this.visit(node.expression);
        this.ib.pop();
        this.ib.loadi(new jsobjects.JSUndefined());
        break;
      case tokenize.TOKEN_TYPE.K_TYPEOF:
        this.visit(node.expression);
        this.ib.typeof_();
        break;
      case tokenize.TOKEN_TYPE.P_INC:
        if(node.expression instanceof ast.Identifier){
          var_name = node.expression.name;
          scope_depth = this.ss.lookUp(var_name);
          this.ib.inc(scope_depth,var_name)
          this.ib.add_1();
        }else if(node.expression instanceof ast.PropertyAccessor){
          this.visit(node.expression,true);
          this.ib.inc_prop();
          this.ib.add_1();
        }else{
          this.visit(node.expression);
          throw new Error("reference Error");
        }
        break;
      case tokenize.TOKEN_TYPE.P_DEC:
        if(node.expression instanceof ast.Identifier){
          var_name = node.expression.name;
          scope_depth = this.ss.lookUp(var_name);
          this.ib.dec(scope_depth,var_name)
          this.ib.sub_1();
        }else if(node.expression instanceof ast.PropertyAccessor){
          this.visit(node.expression,true);
          this.ib.dec_prop();
          this.ib.sub_1();
        }else{
          this.visit(node.expression);
          throw new Error("reference Error");
        }
        break;
      case tokenize.TOKEN_TYPE.P_ADD:
        this.visit(node.expression);
        this.ib.mul_plus();
        break;
      case tokenize.TOKEN_TYPE.P_SUB:
        this.visit(node.expression);
        this.ib.mul_minus();
        break;
      case tokenize.TOKEN_TYPE.P_COMP:
        this.visit(node.expression);
        this.ib.comp();
        break;
      case tokenize.TOKEN_TYPE.P_NOT:
        this.visit(node.expression);
        this.ib.not();
        break;
    }
  };
//  ast . WhileStatement
  AstVisitor.prototype.visitWhileStatement = function(/** ast.Node */node){
    //     jump label_continue
    // label_next:
    //     statements
    //     ...
    // label_continue:
    //     expression
    //     jump_if_true label_next
    // label_end
    var label_next = this.ib.newLabel();
    var label_continue = this.ib.newLabel();
    var label_end = this.ib.newLabel();
    var labels_for_break = registerNamedBreakLabel(this,node.label);
    var labels_for_continue = registerNamedContinueLabel(this,node.label);
    this.ib.registerLabelForBreak(label_end);
    this.ib.registerLabelForContinue(label_continue);
    // break,continueそれぞれのジャンプ先スタックに追加

    this.em.addTask(null,node.label,true);

    if(REQUIRE_STATEMENT_VALUE){
      this.removeOldValueBeforeStatement();//前の式の値を捨てとく
      this.ib.loadi(new jsobjects.JSUndefined());
    }

    this.ib.jump(label_continue);
    this.ib.setLabel(label_next);
    //
    //前の文の値は消さない。
    this.visit(node.statement);

    removeNamedContinueLabel(this,node.label,labels_for_continue);
    this.ib.setLabel(label_continue);
    this.ib.resolveLabel(label_continue);

    this.visit(node.expression);
    this.ib.jump_if_true(label_next);
    this.ib.resolveLabel(label_next);

    removeNamedBreakLabel(this,node.label,labels_for_break);
    this.ib.setLabel(label_end);
    this.ib.resolveLabel(label_end);

    this.em.removeTask();

    this.ib.removeLabelForBreak();
    this.ib.removeLabelForContinue();
    // break,continueそれぞれのジャンプ先スタックをpop
  };
//  ast . WithStatement
  AstVisitor.prototype.visitWithStatement = function(/** ast.Node */node){
    var labels_for_break = registerNamedBreakLabel(this,node.label);

    var that =this;
    this.em.addTask(function(){
      that.ib.remove_scope();
    },node.label);

    this.visit(node.expression);
    this.ib.add_scope_from_object();
    this.visit(node.statement);
    this.ib.remove_scope();

    this.em.removeTask();

    removeNamedBreakLabel(this,node.label,labels_for_break);
  };
})();
