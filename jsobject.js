/**
 * @namespace JavaScript objects container
 */
var jsobjects = {};
(function(){
  /**
   * @name jsobjects.JSObject
   * @class JavaScript Object
   */
  var JSObject = jsobjects.JSObject = function(parent){
    this.keys = [];
    this.values = [];
    this.proto = parent||null;
  };
  /** @memberOf jsobjects.JSObject# */
  JSObject.prototype.isNumber = function(){
    return false;
  };
  /** @memberOf jsobjects.JSObject# */
  JSObject.prototype.isString = function(){
    return false;
  };
  /** @memberOf jsobjects.JSObject# */
  JSObject.prototype.isBoolean= function(){
    return false;
  };
  /** @memberOf jsobjects.JSObject# */
  JSObject.prototype.isNull= function(){
    return false;
  };
  /** @memberOf jsobjects.JSObject# */
  JSObject.prototype.isUndefined = function(){
    return false;
  };
  /** @memberOf jsobjects.JSObject# */
  JSObject.prototype.isTrue = function(){
    return true;
  };
  /** @memberOf jsobjects.JSObject# */
  JSObject.prototype.getNumber = function(){
    return NaN;
  };
  /** @memberOf jsobjects.JSObject# */
  JSObject.prototype.getString = function(){
    return "object Object";
  };
  /** @memberOf jsobjects.JSObject# */
  JSObject.prototype.getBoolean = function(){
    return true;//TODO: implement 
  };
  /** @memberOf jsobjects.JSObject# */
  JSObject.prototype.getObject = function(){
    return this;
  };
  /** @memberOf jsobjects.JSObject# */
  JSObject.prototype.getTypeof = function(){
    return "object";
  };
  /** @memberOf jsobjects.JSObject# */
  JSObject.prototype.getInt32 = function(){
    var i = this.getNumber();
    if(isNaN(i) || !isFinite(i) || i===0){
      return 0;
    }
    return ~~i;//きたねぇ
  };
  /** 
   * @memberOf jsobjects.JSObject# 
   * @param {String} key 取得したいプロパティ名
   * @return {jsobjects.JSObject} プロパティの値
   */
  JSObject.prototype.getProperty = function(key){
    var i = this.keys.indexOf(key);
    if(i>-1){
      return this.values[i];
    }
    if(this.proto!==null){
      return this.proto.getProperty(key);
    }
    return new jsobjects.JSUndefined();
  };
  /** 
   * @memberOf jsobjects.JSObject# 
   * @param {String} key setしたいプロパティ名
   * @param {jsobjects.JSObject} value setしたい値
   */
  JSObject.prototype.setProperty = function(key,value){
    var i = this.keys.indexOf(key);
    if(i>-1){
      this.values[i] = value;
    }else{
      this.keys.push(key);
      this.values.push(value);
    }
  };
  /** @memberOf jsobjects.JSObject# */
  JSObject.prototype.hasProperty = function(key)/** Boolean */{
    if(this.keys.indexOf(key)>-1){
      return true;
    }
    if(this.proto!==null){
      return this.proto.hasProperty(key);
    }
    return false;
  };
  /** @memberOf jsobjects.JSObject# */
  JSObject.prototype.isInstanceof = function(obj)/** Boolean */{
    if(this.proto==obj.getProperty("prototype")){
      return true;
    }
    if(this.proto!==null){
      return this.proto.isInstanceof(obj);
    }
    return false;
  };
  /** @memberOf jsobjects.JSObject# */
  JSObject.prototype.deleteProperty = function(key)/** Boolean */{
    var i = this.keys.indexOf(key);
    if(i===-1){
      return true;
    }else{
      //TODO: DontDelete属性
      this.keys.splice(i,1);
      this.values.splice(i,1);
      return true;
    }
  };
  /** @memberOf jsobjects.JSObject# */
  JSObject.prototype.getIterator = function()/** jsobjects.JSIterator */{
    //イテレータを返す。
    //イテレータは1.次のキーがあるかどうか 2.次のキーを返す メソッドを持つ
    return new JSIterator(this);
  };
  //TODO: implement

  var JSIterator = jsobjects.JSIterator = function(/* jsobjects.JSObject */obj){
    this.obj = obj;
    //this.keys = function _(o){return o.proto ? o.keys.concat(o.proto) : o.keys.slice(0);}(this.obj);
    this.keys = obj.keys.slice(0);
    var o = obj;
    while(o.proto){
      o = o.proto;
      this.keys.concat(o.keys);
    }
    this.current_key_index = -1;
  };
  JSIterator.prototype.hasNext = function(){
    this.current_key_index++;
    while(this.current_key_index < this.keys.length){
      if(this.obj.hasProperty(this.keys[this.current_key_index])){
        return true;
      }
      this.current_key_index++;
    }
    return false;
  };
  JSIterator.prototype.getNext = function(){
    return this.keys[this.current_key_index];
  };

  /**
   * @name jsobjects.JSPrimitive
   * @class JavaScript primitive object
   * @extends jsobjects.JSObject
   */
  var JSPrimitive = jsobjects.JSPrimitive = function(){};
  JSPrimitive.prototype = new JSObject();
  //TODO: implement

  /**
   * @name jsobjects.JSNumber
   * @class JavaScript number object
   * @extends jsobjects.JSPrimitive
   * @param {Number} value number value
   */
  var JSNumber = jsobjects.JSNumber = function(value){
    this.value = value;
  };
  JSNumber.prototype = new JSPrimitive();
  /** 
   * @memberOf jsobjects.JSNumber# 
   */
  JSNumber.prototype.isNumber = function(){
    return true;
  };
  /**
   * @memberOf jsobjects.JSNumber#
   */
  JSNumber.prototype.isTrue = function(){
    return !!this.value;//きたねぇ
  };
  /**
   * @memberOf jsobjects.JSNumber#
   */
  JSNumber.prototype.getNumber = function(){
    return this.value;
  };
  /**
   * @memberOf jsobjects.JSNumber#
   */
  JSNumber.prototype.getString = function(){
    return this.value+"";
  };
  /** @memberOf jsobjects.JSNumber# */
  JSNumber.prototype.getObject = function(){
    return new jsobjects.JSNumberObject(this.value);//TODO:implement JSNumberObject
  };
  /** @memberOf jsobjects.JSNumber# */
  JSNumber.prototype.getTypeof = function(){
    return "number";
  };
  //TODO: implement

  /**
   * @name jsobjects.JSBoolean
   * @class JavaScript number object
   * @extends jsobjects.JSPrimitive
   * @param {Boolean} value boolean value
   */
  var JSBoolean = jsobjects.JSBoolean = function(value){
    this.value = value;
  };
  JSBoolean.prototype = new JSPrimitive();
  /**
   * @memberOf jsobjects.JSBoolean#
   */
  JSBoolean.prototype.getNumber = function(){
    return this.value ? 1 : 0;
  };
  /**
   * @memberOf jsobjects.JSBoolean#
   */
  JSBoolean.prototype.getString = function(){
    return this.value ? "true" : "false";
  };
  /**
   * @memberOf jsobjects.JSBoolean#
   */
  JSBoolean.prototype.isBoolean = function(){
    return true;
  };
  /**
   * @memberOf jsobjects.JSBoolean#
   */
  JSBoolean.prototype.isTrue = function(){
    return this.value;
  };
  /**
   * @memberOf jsobjects.JSBoolean#
   */
  JSBoolean.prototype.getBoolean = function(){
    return this.value;
  };
  /**
   * @memberOf jsobjects.JSBoolean#
   */
  JSBoolean.prototype.getTypeof = function(){
    return "boolean";
  };
  //TODO: implement

  /**
   * @name jsobjects.JSString
   * @class JavaScript number object
   * @extends jsobjects.JSPrimitive
   * @param {String} value string value
   */
  var JSString = jsobjects.JSString = function(value){
    this.value = value;
  };
  JSString.prototype = new JSPrimitive();
  /**
   * @memberOf jsobjects.JSString#
   */
  JSString.prototype.isString = function(){
    return true;
  };
  /**
   * @memberOf jsobjects.JSString#
   */
  JSString.prototype.getNumber = function(){
    return 1*this.value;
  };
  /**
   * @memberOf jsobjects.JSString#
   */
  JSString.prototype.getString = function(){
    return this.value;
  };
  /**
   * @memberOf jsobjects.JSString#
   */
  JSString.prototype.getTypeof = function(){
    return "string";
  };
  //TODO: implement

  /**
   * @name jsobjects.JSNull
   * @class JavaScript null 
   * @extends jsobjects.JSPrimitive
   */
  var JSNull = jsobjects.JSNull = function(){
  };
  JSNull.prototype = new JSPrimitive();
  /**
   * @memberOf jsobjects.JSNull#
   */
  JSNull.prototype.isNull = function(){
    return true;
  };

  /**
   * @name jsobjects.JSUndefined
   * @class JavaScript undefined
   * @extends jsobjects.JSPrimitive
   */
  var JSUndefined = jsobjects.JSUndefined = function(){
  };
  JSUndefined.prototype = new JSPrimitive();
  /**
   * @memberOf jsobjects.JSUndefined#
   */
  JSUndefined.prototype.isUndefined = function(){
    return true;
  };
  JSUndefined.prototype.isTrue = function(){
    return false;
  };
  JSUndefined.prototype.getTypeof = function(){
    return "undefined";
  };
  JSUndefined.prototype.getString = function(){
    return "undefined";
  };
  //TODO: implement
  /**
   * 関数オブジェクト
   * @class JSFunction
   * @extends jsobjects.JSObject
   */
  var JSFunction = jsobjects.JSFunction = function(/** jsobjects.JSFunctionDeclaration */ declaration,/** vm.Scope */scope){
    //this.address = address;
    this.declaration = declaration;
    this.scope = scope;
    this.values = [];
    this.keys = [];
  };
  var jsfunction_parent = new JSObject();
  jsfunction_parent.setProperty("prototype",new JSNull());//
  JSFunction.prototype = jsfunction_parent;
  /**
   * @memberOf jsobjects.JSFunction#
   */
  JSFunction.prototype.getTypeof = function(){
    return "function";
  };

  /**
   * @class JSNativeFunction
   * @extends jsobjects.JSFunction
   */
  var JSNativeFunction = jsobjects.JSNativeFunction = function(/** Function */func,/** String */name){
    this.func = func;
    this.name = name;
    this.values = [];
    this.keys = [];
  };
  JSNativeFunction.prototype = new JSFunction(null,null);

  /**
   * 関数定義オブジェクト
   */
  var JSFunctionDeclaration = jsobjects.JSFunctionDeclaration = function(/** Number */address,/** String[] */args,/** String*/func_name){
    this.address = address;
    this.args = args;
    this.func_name  = func_name || null;
    this.values = [];
    this.keys = [];
  };
  /**
   * argumentsオブジェクト
   * @param {jsobjects.JSObject[]} args 引数配列
   * @param {jsobjects.JSFunction} callee 呼出された関数
   * @extends jsobjects.JSObject
   */
  var JSArguments = jsobjects.JSArguments = function(args,callee){
    this.keys = ["length","callee"];
    this.values = [new jsobjects.JSNumber(args.length),callee];
    for(var i=0;i<args.length;i++){
      this.keys[i+2] = i+"";
      this.values[i+2] = args[i];
    }
  };
  JSArguments.prototype = new JSObject();

  /**
   * @class Array Object
   * @extends jsobjects.Object
   */
  var JSArray = jsobjects.JSArray = function(){
    this.keys = ["length"];
    this.values = [new jsobjects.JSNumber(0)];
  };
  JSArray.prototype = new JSObject();
  JSArray.prototype.setProperty = (function(){
    var _setProperty = JSArray.prototype.setProperty;
    return function(key,value){
      var i = 1*key;
      if(!isNaN(i) && i>=this.values[0].getNumber()){//添字がlength以上
        this.values[0] = new jsobjects.JSNumber(i+1);
      }else if(key==="length"){//length更新
        if(value.isNumber() && value.getNumber()<this.values[0].getNumber()){//lengthが短く設定されたとき
          var new_len = value.getNumber(); 
          var j=0;
          var k = 0;
          while(j<this.values.length){
            k = 1*this.keys[j];
            if(!isNaN(k) && k>=new_len){
              this.keys.splice(j,1);
              this.values.splice(j,1);
            }else{
              j++;
            }
          }
        }
      }
      return _setProperty.call(this,key,value);
    };
  })();
})();
