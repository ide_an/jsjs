/**
 * @fileOverview Array#indexOf未実装処理系のための互換実装
 * @author ide
 */
if(typeof Array.prototype.indexOf !== "function"){
  //添字オフセットは0からに固定（どうせ使わん）
  Array.prototype.indexOf = function(val){
    for(var i=0;i<this.length;i++){
      if(this[i] === val){
        return i;
      }
    }
    return -1;
  };
}

if(typeof Array.prototype.lastIndexOf !== "function"){
  //添字オフセットは0からに固定（どうせ使わん）
  Array.prototype.lastIndexOf = function(val){
    for(var i=this.length-1;i>-1;i--){
      if(this[i] === val){
        return i;
      }
    }
    return -1;
  };
}
