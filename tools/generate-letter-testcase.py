import re
pat = re.compile(r"^([0-9A-F]{4});(\w*).*;Zs;.*$")
f =open("../../python/UnicodeData-2.1.2.txt")
type = ""
res = []
for line in f:
  g = pat.match(line)
  if g and type != g.group(2):
    type = g.group(2)
    res.append('"\\u'+g.group(1)+'"')

print(",".join(res))
