import re

patterns = {
    "Lu":re.compile(r"^.*;Lu.*$"),
    "Ll":re.compile(r"^.*;Ll.*$"),
    "Lt":re.compile(r"^.*;Lt.*$"),
    "Lm":re.compile(r"^.*;Lm.*$"),
    "Nl":re.compile(r"^.*;Nl.*$"),
    "Nd":re.compile(r"^.*;Nd.*$"),
    "Pc":re.compile(r"^.*;Pc.*$"),
    "Mn":re.compile(r"^.*;Mn.*$"),
    "Mc":re.compile(r"^.*;Mc.*$"),
    "Zs":re.compile(r"^.*;Zs.*$"),
    "Lo":re.compile(r"^.*;Lo.*$")
    }
codePattern = re.compile(r"^([0-9a-fA-F]{4})")
codes = {
    "Lu":[],
    "Ll":[],
    "Lt":[],
    "Lm":[],
    "Nl":[],
    "Nd":[],
    "Pc":[],
    "Mn":[],
    "Mc":[],
    "Zs":[],
    "Lo":[]

    }

f = open("./UnicodeData-2.1.2.txt","r")
for line in f:
  for key,pattern in patterns.items():
    g = pattern.match(line)
    if g:
      unicode = codePattern.match(line).group(1)
      codes[key].append('"\\u'+unicode+'"')
      break
f.close()

for category,code in codes.items():
  i = 0;
  res = ""
  for x in code:
    i += 1
    res += x
    if i!=len(code):
      res += ","
    if i%10==0:
      res += "\n"
  print("/** @constant unicode letter table of "+category+" */\nletter.unicode"+category + "= [\n" + res + "\n];")
