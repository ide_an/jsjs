/**
 * @fileOverview 構文抽象木　関連
 * @author ide
 */
/**
 * @namespace AST utility container
 */
var ast = {};
(function(){
  /**
   * @class AST node basis
   */
  ast.Node = function(){
  };
  /**
   * @class AST Program Node
   * @param {String[]} variableNames グローバル変数名配列、順番は宣言順
   * @param {Object[]} functionDeclares グローバル関数配列、順番は宣言順. {name: "Identifier", func: FunctionExpression }
   * @param {ast.Statement[]} statements 構成文
   * @extends ast.Node
   */
  ast.Program = function(variableNames,functionDeclares,statements){
    this.variableNames = variableNames;
    this.functionDeclares = functionDeclares;
    this.statements = statements;
  };
  ast.Program.prototype = new ast.Node();

  /**
   * @class AST Statement node
   * @extends ast.Node
   */
  ast.Statement = function(){
  };
  ast.Statement.prototype = new ast.Node();
  /**
   * @class AST Expression node
   * @extends ast.Node
   */
  ast.Expression = function(){
  };
  ast.Expression.prototype = new ast.Node();
  ast.Expression.prototype.isValidLeftHandSide = function()/** Boolean */{
    return false;
  };
  /**
   * @class AST labelable Statement node
   * @extends ast.Statement
   */
  ast.Labelable = function(){
    this.label = [];
  };
  ast.Labelable.prototype = new ast.Statement();
  /**
   * @class AST Iteration Statement node
   * @extends ast.Labelable
   */
  ast.Iteration = function(){
  };
  ast.Iteration.prototype = new ast.Labelable();

  /**
   * @class AST new呼出ノード
   * @extends ast.Expression
   * @param {ast.Expression} callee 呼ばれる関数オブジェクト
   * @param {ast.Arguments} args 引数
   */
  ast.CallNew = function(callee,args){
    /**
     * @type {ast.Expression}
     */
    this.callee = callee;
    /**
     * @type {ast.Arguments}
     */
    this.args = args;
  };
  ast.CallNew.prototype = new ast.Expression();

  /**
   * @class AST 関数呼出ノード
   * @extends ast.Expression
   * @param {ast.Expression} callee 呼ばれる関数オブジェクト
   * @param {ast.Arguments} args 引数
   */
  ast.Call = function(callee,args){
    /**
     * @type {ast.Expression}
     */
    this.callee = callee;
    /**
     * @type {ast.Arguments}
     */
    this.args = args;
  };
  ast.Call.prototype = new ast.Expression();

  /**
   * @class AST arguments node. 
   * @extends ast.Expression
   * @param {ast.Expression[]} args 引数を構成する式ノードの配列.前から順番に格納
   */
  ast.Arguments = function(args){
    /**
     * @type {ast.Expression[]}
     */
    this.args = args;
  };
  ast.Arguments.prototype = new ast.Expression();

  /**
   * @class AST property accessor node. base[property]
   * @extends ast.Expression
   * @param {ast.Expression} base expression which has property
   * @param {ast.Expression} property expression returns property name
   */
  ast.PropertyAccessor = function(base,property){
    /**
     * @type {ast.Expression}
     */
    this.base = base;
    /**
     * @type {ast.Expression}
     */
    this.property =property 
  };
  ast.PropertyAccessor.prototype = new ast.Expression();
  ast.PropertyAccessor.prototype.isValidLeftHandSide = function(){
    return true;
  };

  /**
   * @class AST postfix operation node.後置単項演算. 後置++ -- 
   * @extends ast.Expression
   * @param {tokenize.TOKEN_TYPE} operator 演算子
   * @param {ast.Expression} expression 式
   */
  ast.PostfixOperation = function(operator,expression){
    /**
     * @type {tokenize.TOKEN_TYPE}
     */
    this.operator =operator 
    /**
     * @type {ast.Expression}
     */
    this.expression = expression;
  };
  ast.PostfixOperation.prototype = new ast.Expression();

  /**
   * @class AST unary operation node.前置単項演算. 前置++ -- + - delete typeof void
   * @extends ast.Expression
   * @param {tokenize.TOKEN_TYPE} operator 演算子
   * @param {ast.Expression} expression 式
   */
  ast.UnaryOperation = function(operator,expression){
    /**
     * @type {tokenize.TOKEN_TYPE}
     */
    this.operator =operator 
    /**
     * @type {ast.Expression}
     */
    this.expression = expression;
  };
  ast.UnaryOperation.prototype = new ast.Expression();

  /**
   * @class AST binary operation node.二項演算.+-*%/instanceof in |^&||&&
   * @extends ast.Expression
   * @param {tokenize.TOKEN_TYPE} operator 演算子
   * @param {ast.Expression} left 左辺式
   * @param {ast.Expression} right 右辺式
   */
  ast.BinaryOperation = function(operator,left,right){
    /**
     * @type {tokenize.TOKEN_TYPE}
     */
    this.operator =operator 
    /**
     * @type {ast.Expression}
     */
    this.left = left;
    /**
     * @type {ast.Expression}
     */
    this.right = right;
  };
  ast.BinaryOperation.prototype = new ast.Expression();

  /**
   * @class AST Conditional operation node.三項演算子
   * @extends ast.Expression
   * @param {ast.Expression} condExp 条件式
   * @param {ast.Expression} thenExp 真のときの評価式
   * @param {ast.Expression} elseExp 偽のときの評価式
   */
  ast.Conditional = function(condExp,thenExp,elseExp){
    /**
     * @type {ast.Expression}
     */
    this.condition = condExp
    /**
     * @type {ast.Expression}
     */
    this.then = thenExp;
    /**
     * @type {ast.Expression}
     */
    this.else_ = elseExp;
  };
  ast.Conditional.prototype = new ast.Expression();

  /**
   * @class AST Assignment node 代入文
   * @extends ast.Expression
   * @param {tokenize.TOKEN_TYPE} operator 演算子
   * @param {ast.Expression} target
   * @param {ast.Expression} value
   */
  ast.Assignment = function(operator,target,value){
    /**
     * @type {tokenize.TOKEN_TYPE}
     */
    this.operator =operator 
    /**
     * @type {ast.Expression}
     */
    this.target = target;
    /**
     * @type {ast.Expression}
     */
    this.value = value;
  };
  ast.Assignment.prototype = new ast.Expression();
  ast.Assignment.prototype.isCompound = function(){
    return this.operator!==tokenize.TOKEN_TYPE.P_ASSIGN;
  }
  ast.Assignment.prototype.getBinaryOp = function(){
    var op = this.operator;
    switch(op){
      case tokenize.TOKEN_TYPE.P_ASSIGN_ADD: return tokenize.TOKEN_TYPE.P_ADD;
      case tokenize.TOKEN_TYPE.P_ASSIGN_SUB: return tokenize.TOKEN_TYPE.P_SUB;
      case tokenize.TOKEN_TYPE.P_ASSIGN_MUL: return tokenize.TOKEN_TYPE.P_MUL;
      case tokenize.TOKEN_TYPE.P_ASSIGN_MOD: return tokenize.TOKEN_TYPE.P_MOD;
      case tokenize.TOKEN_TYPE.P_ASSIGN_DIV: return tokenize.TOKEN_TYPE.P_DIV;
      case tokenize.TOKEN_TYPE.P_ASSIGN_SHL: return tokenize.TOKEN_TYPE.P_SHL;
      case tokenize.TOKEN_TYPE.P_ASSIGN_SHR: return tokenize.TOKEN_TYPE.P_SHR;
      case tokenize.TOKEN_TYPE.P_ASSIGN_SHR_ARITH: return tokenize.TOKEN_TYPE.P_SHR_ARITH;
      case tokenize.TOKEN_TYPE.P_ASSIGN_AND: return tokenize.TOKEN_TYPE.P_AND;
      case tokenize.TOKEN_TYPE.P_ASSIGN_OR: return tokenize.TOKEN_TYPE.P_OR;
      case tokenize.TOKEN_TYPE.P_ASSIGN_XOR: return tokenize.TOKEN_TYPE.P_XOR;
      default:
        throw new Error("二項演算にできない代入演算子:"+op);
    }
  }

  /**
   * @class AST thisノード
   * @extends ast.Expression
   */
  ast.This = function(){
  };
  ast.This.prototype = new ast.Expression();

  /**
   * @class AST Identifier　識別子ノード
   * @extends ast.Expression
   * @param {String} name 識別子の名前
   */
  ast.Identifier = function(name){
    /**
     * @type {String}
     */
    this.name = name;
  };
  ast.Identifier.prototype = new ast.Expression();
  ast.Identifier.prototype.isValidLeftHandSide = function(){
    return true;
  };

  /**
   * @class AST literal node リテラル
   * @extends ast.Expression
   * @param {jsobjects.JSObject} value リテラル値のJSオブジェクト
   */
  ast.Literal = function(value){
    this.value = value;
  };
  ast.Literal.prototype = new ast.Expression();

  /**
   * @class AST complex literal node 
   * @extends ast.Expression
   */
  ast.ComplexLiteral = function(){};
  ast.ComplexLiteral.prototype = new ast.Expression();

  /**
   * @class AST arrayliteral node 配列リテラルノード
   * @extends ast.ComplexLiteral
   * @param {ast.Expression[]} values 配列要素の式の配列
   */
  ast.ArrayLiteral = function(values){
    this.values = values;
  };
  ast.ArrayLiteral.prototype = new ast.ComplexLiteral();

  /**
   * @class AST ObjectLiteral node 配列リテラルノード
   * @extends ast.ComplexLiteral
   * @param {jsobjects.JSString[]} keys 添字の式の配列
   * @param {ast.Expression[]} values 値の式の配列
   */
  ast.ObjectLiteral = function(keys,values){
    this.keys = keys;
    this.values = values;
  };
  ast.ObjectLiteral.prototype = new ast.ComplexLiteral();

  /**
   * @class AST RegExpLiteral node 配列リテラルノード
   * @extends ast.ComplexLiteral
   * @param {jsobjects.JSString} pattern 正規表現パターン
   * @param {jsobjects.JSString} flags 正規表現フラグ
   */
  ast.RegExpLiteral = function(pattern,flags){
    this.pattern = pattern;
    this.flags = flags;
  };
  ast.RegExpLiteral.prototype = new ast.ComplexLiteral();

  /**
   * @class AST 関数式
   * @extends ast.ComplexLiteral
   * @param {String} name 関数内部識別名
   * @param {String[]} argumentNames 引数名配列、順番は宣言順
   * @param {String[]} variableNames ローカル変数名配列、順番は宣言順
   * @param {Object[]} functionDeclares ローカル関数配列、順番は宣言順. {name: "Identifier", func: FunctionExpression }
   * @param {ast.Statement[]} statements 関数ボディ構成文
   */
  ast.FunctionExpression = function(name,argumentNames,variableNames,functionDeclares,statements){
    this.name = name;
    this.argumentNames = argumentNames;
    this.variableNames = variableNames;
    this.functionDeclares = functionDeclares;
    this.statements = statements;
  };
  ast.FunctionExpression.prototype = new ast.Expression();
  /**
   * @class AST 式文　ノード
   * @extends ast.Statement
   * @param {ast.Expression} expression 式
   */
  ast.ExpressionStatement = function(expression){
    this.expression = expression;
  };
  ast.ExpressionStatement.prototype = new ast.Statement();

  /**
   * @class AST if文　ノード
   * @extends ast.Labelable
   * @param {String[]} labelset ラベルセットを構成するラベル群
   * @param {ast.Expression} condition 条件式
   * @param {ast.Statement} then 真のときの実行文
   * @param {ast.Statement} else_ 偽のときの実行文
   */
  ast.IfStatement = function(labelset,condition,then,else_){
    this.condition = condition;
    this.thenStatement = then;
    this.elseStatement = else_;
    this.label = labelset;
  };
  ast.IfStatement.prototype = new ast.Statement();

  /**
   * @class AST block文　ノード
   * @extends ast.Labelable
   * @param {String[]} labelset ラベルセットを構成するラベル群
   * @param {ast.Statement[]} statements 実行文配列
   */
  ast.BlockStatement = function(labelset,statements){
    this.statements = statements;
    this.label = labelset;
  };
  ast.BlockStatement.prototype = new ast.Statement();

  /**
   * @class AST 空文　ノード
   * @extends ast.Statement
   */
  ast.EmptyStatement = function(){
  };
  ast.EmptyStatement.prototype = new ast.Statement();

  /**
   * @class AST continue文　ノード
   * @extends ast.Statement
   * @param {String} label ラベル名
   */
  ast.ContinueStatement = function(label){
    this.label = label;
  };
  ast.ContinueStatement.prototype = new ast.Statement();

  /**
   * @class AST break文　ノード
   * @extends ast.Statement
   * @param {String} label ラベル名
   */
  ast.BreakStatement = function(label){
    this.label = label;
  };
  ast.BreakStatement.prototype = new ast.Statement();

  /**
   * @class AST return文　ノード
   * @extends ast.Statement
   * @param {ast.Expression} result 戻り値式
   */
  ast.ReturnStatement = function(result){
    this.expression = result;
  };
  ast.ReturnStatement.prototype = new ast.Statement();

  /**
   * @class AST throw文　ノード
   * @extends ast.Statement
   * @param {ast.Expression} exception 例外式 
   */
  ast.ThrowStatement = function(exception){
    this.expression = exception;
  };
  ast.ThrowStatement.prototype = new ast.Statement();

  /**
   * @class AST WithStatement文　ノード
   * @extends ast.Labelable
   * @param {String[]} labelset ラベルセットを構成するラベル群
   * @param {ast.Expression} scope_top スコープ連鎖に追加される式
   * @param {ast.Statement} target_statement 対象文
   */
  ast.WithStatement = function(labelset,scope_top,target_statement){
    this.label = labelset;
    this.expression = scope_top;
    this.statement = target_statement;
  };
  ast.WithStatement.prototype = new ast.Statement();

  /**
   * @class AST try-catch文　ノード
   * @extends ast.Statement
   * @param {ast.Identifier} exception 例外識別子
   * @param {ast.BlockStatement} tryBlock tryブロック
   * @param {ast.BlockStatement} catchBlock catchブロック
   */
  ast.TryCatchStatement = function(exception,tryBlock,catchBlock){
    this.exception = exception;
    this.tryBlock = tryBlock;
    this.catchBlock = catchBlock;
  };
  ast.TryCatchStatement.prototype = new ast.Statement();

  /**
   * @class AST try-finally文　ノード
   * @extends ast.Statement
   * @param {ast.BlockStatement} tryBlock tryブロック
   * @param {ast.BlockStatement} finallyBlock finallyブロック
   */
  ast.TryFinallyStatement = function(tryBlock,finallyBlock){
    this.tryBlock = tryBlock;
    this.finallyBlock = finallyBlock;
  };
  ast.TryFinallyStatement.prototype = new ast.Statement();

  /**
   * @class AST switch文　ノード
   * @extends ast.Labelable
   * @param {String[]} labelset ラベルセットを構成するラベル群
   * @param {ast.Expression} condition 条件式
   * @param {ast.CaseClause[]} caseClauses case配列
   */
  ast.SwitchStatement = function(labelset,condition,caseClauses){
    this.label = labelset;
    this.expression = condition;
    this.caseClauses = caseClauses;
  };
  ast.SwitchStatement.prototype = new ast.Statement();

  /**
   * @class AST CaseClause　ノード
   * @extends ast.Statement
   * @param {ast.Expression} label 適合条件式
   * @param {ast.Statement[]} statements 実行文配列
   */
  ast.CaseClause = function(label,statements){
    this.label = label;
    this.statements = statements;
  };
  ast.CaseClause.prototype = new ast.Statement();

  /**
   * @class AST WhileStatement文　ノード
   * @extends ast.Iteration
   * @param {String[]} labelset ラベルセットを構成するラベル群
   * @param {ast.Expression} condition 条件式
   * @param {ast.Statement} statement 対象文
   */
  ast.WhileStatement = function(labelset,condition,statement){
    this.label = labelset;
    this.expression = condition;
    this.statement = statement;
  };
  ast.WhileStatement.prototype = new ast.Iteration();

  /**
   * @class AST DoWhileStatement文　ノード
   * @extends ast.Iteration
   * @param {String[]} labelset ラベルセットを構成するラベル群
   * @param {ast.Expression} condition 条件式
   * @param {ast.Statement} statement 対象文
   */
  ast.DoWhileStatement = function(labelset,condition,statement){
    this.label = labelset;
    this.expression = condition;
    this.statement = statement;
  };
  ast.DoWhileStatement.prototype = new ast.Iteration();

  /**
   * @class AST ForStatement文　ノード
   * @extends ast.Iteration
   * @param {String[]} labelset ラベルセットを構成するラベル群
   * @param {ast.Expression} init 初期化式
   * @param {ast.Expression} condition 条件式
   * @param {ast.Expression} next ステップ実行式
   * @param {ast.Statement} statement 対象文
   */
  ast.ForStatement = function(labelset,init,condition,next,statement){
    this.label = labelset;
    this.init = init;
    this.condition = condition;
    this.next = next;
    this.statement = statement;
  };
  ast.ForStatement.prototype = new ast.Iteration();

  /**
   * @class AST ForInStatement文　ノード
   * @extends ast.Iteration
   * @param {String[]} labelset ラベルセットを構成するラベル群
   * @param {ast.Expression} key キーを突っ込む式 
   * @param {ast.Expression} enumerable eachする式
   * @param {ast.Statement} statement 対象文
   */
  ast.ForInStatement = function(labelset,key,enumerable,statement){
    this.label = labelset;
    this.key = key;
    this.enumerable = enumerable;
    this.statement = statement;
  };
  ast.ForInStatement.prototype = new ast.Iteration();

})();
