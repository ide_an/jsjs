/**
 * @fileOverview VM関連
 * @author ide
 */
/**
 * @namespace virtual machine related container
 */
var vm = {};
(function(){
  vm.opecode2str = [
    "exit",
    "add_scope",// [$name1,$name2,..]current_call_stack_top.scopeを親とする$name1,$name2..をキーに持つスコープを追加
    "remove_scope",// current_call_stack_top.scopeの親を現在のスコープにする
    "add_scope_from_object",//pop $obj,current_call_stack_top.scopeを親とする$objによるObjectiveScopeを追加
    "add_global_scope",//$key, 変数名$keyのグローバル変数を追加。つまりグローバルオブジェクトのプロパティとして追加。
    //スタック操作
    "pop",//
    "dup",//
    "dup2",//
    "rot3",//スタック上位三つをローテート.3番目のエントリーがトップに来る
    //load|store
    "loadi",//$value push $value
    //結局変数アクセスはスコープの深さ抜きで名前だけでアクセスすべき？
    //evalへの対応,with文・・・
    "load",//$scope $name $scopeの$nameキーの変数の値をpush
    "store",//$scope $name pop $a, $scopeの$nameキーの変数に$aを代入
    "load_this",//
    "store_this",//
    //プロパティアクセス
    "get_prop",//pop $x,pop $a,push $a[$x]
    "set_prop",//pop $v,pop $x,pop $a,$a[$x]=$v,
    //破壊的インクリメント|デクリメント
    "inc",//$scope $name  push $x,$x=$x+1, 
    "inc_prop",//pop $x, pop $a, push $a[$x], $a[$x]=$a[$x]+1
    "dec",//$scope $name  push $x,$x=$x-1,
    "dec_prop",//pop $x, pop $a, push $a[$x], $a[$x]=$a[$x]-1
    //ジャンプ、ブランチ
    "jump",//$address
    "jump_if_true",//$address, pop $x, jump $address if $x is true
    "jump_if_false",//$address pop $x, jump $address if $x is false
    "jump_and_pop_if_equal",//$address pop $v, pop $e, (jump $address, pop ) if $v===$e
    "jump_subroutine",//$address push 戻り番地, jump $address
    "return_subroutine",//pop $address, jump $address
    //コール
    "call",//$argnum,　引数が前から順にpushされている $argnum回popして,pop $func,push $func(args)関数コードへのアドレスは$funcがもつ
    "call_new",//$argnum, callと同じだがthisが異なる
    "call_method",//$argnum, callに加えてさらにpopしてそれをthisにする
    "returns",//コールスタックpopしてreturnアドレスに戻る
    "generate_func",//$funcDeclare,関数オブジェクトを生成して実行コードアドレスとスコープを結びつける。生成した関数オブジェクトがスタックにpushされる
    //2項演算
    "in_",//,pop $obj,pop $key,push $key in $obj ? true: false
    "instanceof_",//, pop $parent,pop $child,push $child instanceof $parent
    "add",//pop $right,pop $left,push $left+$right
    "sub",//
    "mul",//
    "div",//
    "mod",//
    "bit_and",//
    "bit_or",//
    "bit_xor",//
    "bit_shl",//
    "bit_shr",//
    "bit_shr_arith",//
    "eq",//
    "neq",//
    "lt",//
    "gt",//
    "leq",//
    "geq",//
    "eqs",//strict
    "neqs",//strict
    //単項演算
    "del_prop",//pop $key,pop $obj, $obj.del($key)
    "del_var",//$scope,$name,対応するキーも同時に削除
    "mul_plus",//pop $a, push 1*$a
    "mul_minus",//pop $a, push -1*$a
    "comp",//pop $a, push ~$a
    "not",//pop $a, push !$a
    "sub_1",//pop $x,push $x-1
    "add_1",//pop $x,push $x+1
    "typeof_",//pop $x ,push new jsobjects.JSString(typeof $x)
    "get_iterator",//pop $x , push $x.getIterator()
    "has_next",//pop $iter,push $iter.hasNext()
    "get_next",//pop $iter,push $iter.getNext()
    //例外処理
    //ちょっと放置の方針で
    "throw_",//例外ハンドラにジャンプ。コールスタックから要らなくなった分pop
    "add_exception_handler",//例外ハンドラを例外ハンドラスタックにpush
    "remove_exception_handler",//例外ハンドラを例外ハンドラスタックにpop
    "<!-- dummy -->"
  ];
  /** @const オペコード */
  vm.opecode = {};
  for(var i=0;i<vm.opecode2str.length;i++){
    vm.opecode[vm.opecode2str[i]] = i;
  }
  var op = vm.opecode;//inner use

  /**
   * @class スコープ
   * @param {Number} size 格納する変数個数
   * @param {vm.Scope} [parent=null] 親スコープ
   */
  vm.Scope = function(keys,parent){
    this.parent = parent||null;
    this.keys = keys;
    this.values = [];
    this.values.length = keys.length;
  };
  /**
   * 該当番地の変数の値を返す。
   * @param {String} key 変数名
   * @return {jsobjects.JSObject}
   */
  vm.Scope.prototype.getVariable = function(key){
    var i = this.keys.lastIndexOf(key);
    if(i<0){
      throw new Error("reference error!!");
    }else if(typeof this.values[i]==="undefined"){
      return new jsobjects.JSUndefined();
    }else{
      return this.values[i];
    }
  };
  /**
   * 該当番地の変数に値を代入する
   * @param {String} key 変数名
   * @param {jsobjects.JSObject} value 代入する値
   */
  vm.Scope.prototype.setVariable = function(key,value){
    var i = this.keys.lastIndexOf(key);
    if(i<0){
      throw new Error("reference error!!");
    }
    this.values[i] = value;
  };
  /**
   * n世代前のスコープを取得する。
   * @param {Number} n n世代前
   * @param {String} name 変数名
   * @return {vm.Scope} 該当する先祖スコープ
   */
  vm.Scope.prototype.getAncestor = function(n,name){
    if(n===0){
      return this;
    }else{
      if(this.parent){
        return this.parent.getAncestor(n-1,name);
      }else{
        throw new Error("スコープ解決できねぇ");
      }
    }
  };
  /**
   * 該当番地の変数を消去する。
   * @param {Number} offset 番地
   */
  vm.Scope.prototype.deleteVariable = function(key){
    //グローバルスコープでない限り変数は消せない。
    return false;
  };

  /**
   * @class オブジェクトから生成されるスコープ。with文とかグローバルオブジェクトとか。
   * @param object
   * @param {vm.Scope} [parent=null] 親スコープ
   * @extends vm.Scope
   */
  vm.ObjectiveScope = function(object,parent){
    this.parent = parent || null;
    this.object = object;
  };
  vm.ObjectiveScope.prototype = new vm.Scope([],null);
  vm.ObjectiveScope.prototype.getVariable = function(key){
    if(this.object.hasProperty(key)){
      return this.object.getProperty(key);
    }
    throw new Error("reference error!! object scope get");
  };
  vm.ObjectiveScope.prototype.setVariable = function(key,value){
    if(this.object.hasProperty(key)){
      this.object.setProperty(key,value);
      return;
    }
    //console.log(this.object.keys.join());
    throw new Error("reference error!!object scope set "+key+"="+value);
  };
  vm.ObjectiveScope.prototype.getAncestor = function(n,name){
    if(this.object.hasProperty(name)){
      return this;
    }else{
      if(this.parent){
        return this.parent.getAncestor(n-1,name);
      }else{
        return this;
      }
    }
  };
  vm.ObjectiveScope.prototype.deleteVariable = function(key){
    return this.object.deleteProperty(key);
  };

  /**
   * @class フレーム
   * 色々未定
   * this,scope,return_address
   */
  vm.CallStackFrame = function(this_,scope,return_address,call_as_new){
    this.this_ = this_;
    this.scope = scope;
    this.return_address = return_address;
    this.call_as_new = call_as_new;
  };

  /**
   * @class 命令記述
   * @param {Number} opecode
   * @param {Number|jsobjects.JSObject[]} operand
   */
  vm.Instruction = function(opecode,operand){
    /** @type {Number} */
    this.opecode = opecode;
    /** @type {Number} */
    this.operand = operand ? operand : [];
  };
  /**
   * @class 例外ハンドラスタックエントリー
   * @param {Number} 例外ハンドラのアドレス
   * @param {Number} エントリー追加時のコールスタックの深さ
   * @param {Number} エントリー追加時のアキュムレータスタックの深さ
   */
  vm.ExceptionHandlerEntry = function(address,call_stack_depth,acc_stack_depth){
    this.address = address;
    this.call_stack_depth = call_stack_depth;
    this.acc_stack_depth = acc_stack_depth;
  };

  /**
   * @class JavaScript virtual machine
   */
  vm.Vm = function(){
    this.init();
  };
  var Vm = vm.Vm;//inner use
  /** @memberOf vm.Vm# */
  Vm.prototype.init = function(){
    this.pc = 0;
    this.global_object = new jsobjects.JSObject();//TODO:グローバルオブジェクトの追加
    /** @type {vm.Instruction[]} */
    this.instructions = [];
    this.accumulatorStack = [];
    this.callStack = [new vm.CallStackFrame(this.global_object,new vm.ObjectiveScope(this.global_object),-1)];
    this.exceptionHandlerStack = [];
  };
  /** @memberOf vm.Vm# */
  Vm.prototype.load = function(instructions){
    this.instructions = instructions;
  };
  /**
   * @memberOf vm.Vm#
   * loadされた命令列の実行
   */
  Vm.prototype.exec = function(){
    if(!this.onStep){
      this.onStep = function(){};
    }
    do{
      this.onStep();
    }while(this.step(this.instructions[this.pc]))
  };
  /**
   * 単一命令の実行
   * @memberOf vm.Vm#
   * @param {vm.Instruction} instruction 実行する命令
   * @return {Boolean} プログラム継続かどうか
   */
  Vm.prototype.step = function(instruction){
    var current_call_stack_top = this.callStack[this.callStack.length-1];
    var self = this;
    var updatePc = function(){self.pc++;};
    switch(instruction.opecode){
      case op.pop:
        this.accumulatorStack.pop();
        updatePc();
        break;
      case op.dup:
        var res = this.accumulatorStack.pop();
        this.accumulatorStack.push(res);
        this.accumulatorStack.push(res);
        updatePc();
        break;
      case op.dup2:
        var res1 = this.accumulatorStack.pop();
        var res2 = this.accumulatorStack.pop();
        this.accumulatorStack.push(res2);
        this.accumulatorStack.push(res1);
        this.accumulatorStack.push(res2);
        this.accumulatorStack.push(res1);
        updatePc();
        break;
      case op.rot3:
        var res1 = this.accumulatorStack.pop();
        var res2 = this.accumulatorStack.pop();
        var res3 = this.accumulatorStack.pop();
        this.accumulatorStack.push(res2);
        this.accumulatorStack.push(res1);
        this.accumulatorStack.push(res3);
        updatePc();
        break;
      case op.add_scope:
        current_call_stack_top.scope = new vm.Scope(instruction.operand,current_call_stack_top.scope);
        updatePc();
        break;
      case op.remove_scope:
        current_call_stack_top.scope = current_call_stack_top.scope.parent;
        updatePc();
        break;
      case op.add_scope_from_object:
        var stack_top = this.accumulatorStack.pop();
        current_call_stack_top.scope = new vm.ObjectiveScope(stack_top,current_call_stack_top.scope);
        updatePc();
        break;
      case op.add_global_scope:
        var name = instruction.operand[0];
        if(!this.global_object.hasProperty(name)){
          this.global_object.setProperty(name,new jsobjects.JSUndefined());
        }
        updatePc();
        break;
      case op.loadi:
        this.accumulatorStack.push(instruction.operand[0]);
        updatePc();
        break;
      case op.load:
        var scope = current_call_stack_top.scope.getAncestor(instruction.operand[0],instruction.operand[1]);
        this.accumulatorStack.push(scope.getVariable(instruction.operand[1]));
        updatePc();
        break;
      case op.store:
        var stack_top = this.accumulatorStack.pop();
        var scope = current_call_stack_top.scope.getAncestor(instruction.operand[0],instruction.operand[1]);
        scope.setVariable(instruction.operand[1],stack_top);
        updatePc();
        break;
      case op.load_this:
        this.accumulatorStack.push(current_call_stack_top.this_);
        updatePc();
        break;
      case op.store_this://実は要らない疑惑
        current_call_stack_top.this_ = this.accumulatorStack.pop();
        updatePc();
        break;
      case op.get_prop:
        var key = this.accumulatorStack.pop();
        var obj = this.accumulatorStack.pop();
        this.accumulatorStack.push(obj.getProperty(key.getString()));
        updatePc();
        break;
      case op.set_prop:
        var val = this.accumulatorStack.pop();
        var key = this.accumulatorStack.pop();
        var obj = this.accumulatorStack.pop();
        obj.setProperty(key.getString(),val);
        updatePc();
        break;
      case op.inc:
        var scope = current_call_stack_top.scope.getAncestor(instruction.operand[0],instruction.operand[1]);
        var val = scope.getVariable(instruction.operand[1]).getNumber();
        this.accumulatorStack.push(new jsobjects.JSNumber(val));
        scope.setVariable(instruction.operand[1],new jsobjects.JSNumber(val+1));
        updatePc();
        break;
      case op.inc_prop:
        var key = this.accumulatorStack.pop();
        var obj = this.accumulatorStack.pop();
        var val = obj.getProperty(key.getString()).getNumber();
        this.accumulatorStack.push(new jsobjects.JSNumber(val));
        obj.setProperty(key.getString(),new jsobjects.JSNumber(val+1));
        updatePc();
        break;
      case op.dec:
        var scope = current_call_stack_top.scope.getAncestor(instruction.operand[0],instruction.operand[1]);
        var val = scope.getVariable(instruction.operand[1]).getNumber();
        this.accumulatorStack.push(new jsobjects.JSNumber(val));
        scope.setVariable(instruction.operand[1],new jsobjects.JSNumber(val-1));
        updatePc();
        break;
      case op.dec_prop:
        var key = this.accumulatorStack.pop();
        var obj = this.accumulatorStack.pop();
        var val = obj.getProperty(key.getString()).getNumber();
        this.accumulatorStack.push(new jsobjects.JSNumber(val));
        obj.setProperty(key.getString(),new jsobjects.JSNumber(val-1));
        updatePc();
        break;
      case op.jump:
        this.pc = instruction.operand[0];
        break;
      case op.jump_if_true:
        var v = this.accumulatorStack.pop();
        if(v.isTrue()){
          this.pc = instruction.operand[0];
        }else{
          updatePc();
        }
        break;
      case op.jump_if_false:
        var v = this.accumulatorStack.pop();
        if(!v.isTrue()){
          this.pc = instruction.operand[0];
        }else{
          updatePc();
        }
        break;
      case op.jump_and_pop_if_equal:
        var a = this.accumulatorStack.pop();
        var b = this.accumulatorStack.pop();
        if((a instanceof jsobjects.JSPrimitive && b instanceof jsobjects.JSPrimitive && a.value===b.value) ||
            a===b){
          this.pc = instruction.operand[0];
          this.accumulatorStack.pop();
        }else{
          updatePc();
        }
        break;
      case op.jump_subroutine://TODO:test
        //戻り先番地をスタックに積んでjump
        this.accumulatorStack.push(this.pc+1);
        this.pc = instruction.operand[0];
        break;
      case op.return_subroutine://TODO: test
        var addr = this.accumulatorStack.pop();
        this.pc = addr;
        break;
      case op.call:
        var n=instruction.operand[0];
        var args = this.accumulatorStack.splice(this.accumulatorStack.length-n,n);
        //console.log(args);
        var func = this.accumulatorStack.pop();
        //console.log(func);
        if(func instanceof jsobjects.JSFunction){
          if(func instanceof jsobjects.JSNativeFunction){
            this.accumulatorStack.push(func.func(this,args));
            updatePc();
          }else{
            var nextFrame = new vm.CallStackFrame(
              this.global_object,
              func.scope,
              this.pc+1,
              false
            );
            //コールスタックを積む
            this.callStack.push(nextFrame);
            if(this.callStack.length>0xff){//暫定
              throw new Error("stack overflow");
            }
            //スコープ生成
            if(func.declaration.func_name!==null){
              nextFrame.scope = new vm.Scope([func.declaration.func_name,"arguments"],nextFrame.scope);//argumentsオブジェクト設定。
              nextFrame.scope.values[0] = func;
              nextFrame.scope.values[1] = new jsobjects.JSArguments(args,func);
            }else{
              nextFrame.scope = new vm.Scope(["arguments"],nextFrame.scope);//argumentsオブジェクト設定。
              nextFrame.scope.values[0] = new jsobjects.JSArguments(args,func);
            }
            nextFrame.scope = new vm.Scope(func.declaration.args,nextFrame.scope);//
            for(var i=0;i<args.length;i++){
              nextFrame.scope.values[i] = args[i];
            }
            //jump
            this.pc = func.declaration.address;
          }
        }else{
          throw new Error("関数じゃねー");
        }
        break;
      case op.call_new:
        var n=instruction.operand[0];
        var args = this.accumulatorStack.splice(this.accumulatorStack.length-n,n);
        //console.log(args);
        var func = this.accumulatorStack.pop();
        //console.log(func);
        if(func instanceof jsobjects.JSFunction){
          if(func instanceof jsobjects.JSNativeFunction){
            this.accumulatorStack.push(func.func(this,args));
            updatePc();
          }else{
            var proto = func.getProperty("prototype");
            var nextFrame = new vm.CallStackFrame(
              new jsobjects.JSObject(proto instanceof jsobjects.JSPrimitive ? new jsobjects.JSObject() : proto),
              func.scope,
              this.pc+1,
              true
            );
            //コールスタックを積む
            this.callStack.push(nextFrame);
            if(this.callStack.length>0xff){//暫定
              throw new Error("stack overflow");
            }
            //スコープ生成
            if(func.declaration.func_name!==null){
              nextFrame.scope = new vm.Scope([func.declaration.func_name,"arguments"],nextFrame.scope);//argumentsオブジェクト設定。
              nextFrame.scope.values[0] = func;
              nextFrame.scope.values[1] = new jsobjects.JSArguments(args,func);
            }else{
              nextFrame.scope = new vm.Scope(["arguments"],nextFrame.scope);//argumentsオブジェクト設定。
              nextFrame.scope.values[0] = new jsobjects.JSArguments(args,func);
            }
            nextFrame.scope = new vm.Scope(func.declaration.args,nextFrame.scope);//
            for(var i=0;i<args.length;i++){
              nextFrame.scope.values[i] = args[i];
            }
            //jump
            this.pc = func.declaration.address;
          }
        }else{
          throw new Error("関数じゃねー");
        }
        break;
      case op.call_method:
        var n=instruction.operand[0];
        var args = this.accumulatorStack.splice(this.accumulatorStack.length-n,n);
        //console.log(args);
        var func = this.accumulatorStack.pop();
        var this_ = this.accumulatorStack.pop();
        //console.log(func);
        if(func instanceof jsobjects.JSFunction){
          if(func instanceof jsobjects.JSNativeFunction){
            this.accumulatorStack.push(func.func(this,args));
            updatePc();
          }else{
            var nextFrame = new vm.CallStackFrame(
              this_,
              func.scope,
              this.pc+1,
              false
            );
            //コールスタックを積む
            this.callStack.push(nextFrame);
            if(this.callStack.length>0xff){//暫定
              throw new Error("stack overflow");
            }
            //スコープ生成
            if(func.declaration.func_name!==null){
              nextFrame.scope = new vm.Scope([func.declaration.func_name,"arguments"],nextFrame.scope);//argumentsオブジェクト設定。
              nextFrame.scope.values[0] = func;
              nextFrame.scope.values[1] = new jsobjects.JSArguments(args,func);
            }else{
              nextFrame.scope = new vm.Scope(["arguments"],nextFrame.scope);//argumentsオブジェクト設定。
              nextFrame.scope.values[0] = new jsobjects.JSArguments(args,func);
            }
            nextFrame.scope = new vm.Scope(func.declaration.args,nextFrame.scope);//
            for(var i=0;i<args.length;i++){
              nextFrame.scope.values[i] = args[i];
            }
            //jump
            this.pc = func.declaration.address;
          }
        }else{
          throw new Error("関数じゃねー");
        }
        break;
      case op.returns:
        var oldFrame = this.callStack.pop();
        this.pc = oldFrame.return_address;
        if(oldFrame.call_as_new && this.accumulatorStack[this.accumulatorStack.length-1] instanceof jsobjects.JSPrimitive){
          this.accumulatorStack.pop();
          this.accumulatorStack.push(oldFrame.this_);
        }
        break;
      case op.generate_func:
        //現在のスコープと関数定義からJSFunctionオブジェクトを生成する。
        var scope = current_call_stack_top.scope;
        var declaration = instruction.operand[0];
        this.accumulatorStack.push(new jsobjects.JSFunction(declaration,scope));
        updatePc();
        break;
      case op.in_:
        var obj = this.accumulatorStack.pop();
        var key = this.accumulatorStack.pop();
        this.accumulatorStack.push(obj.hasProperty(key.getString())?new jsobjects.JSBoolean(true):new jsobjects.JSBoolean(false));
        updatePc();
        break;
      case op.instanceof_:
        var a = this.accumulatorStack.pop();
        var b = this.accumulatorStack.pop();
        //console.log(a.getProperty("prototype"),b,b.isInstanceof(a));
        this.accumulatorStack.push(b.isInstanceof(a)?new jsobjects.JSBoolean(true):new jsobjects.JSBoolean(false));
        updatePc();
        break;
      case op.add:
        var a = this.accumulatorStack.pop();
        var b = this.accumulatorStack.pop();
        var res =  b.value+a.value;
        if(typeof res === "string"){
          this.accumulatorStack.push(new jsobjects.JSString(res));
        }else{
          this.accumulatorStack.push(new jsobjects.JSNumber(res));
        }
        updatePc();
        break;
      case op.sub:
        var a = this.accumulatorStack.pop();
        var b = this.accumulatorStack.pop();
        var res =  b.value-a.value;
        this.accumulatorStack.push(new jsobjects.JSNumber(res));
        updatePc();
        break;
      case op.mul:
        var a = this.accumulatorStack.pop();
        var b = this.accumulatorStack.pop();
        var res =  b.value*a.value;
        this.accumulatorStack.push(new jsobjects.JSNumber(res));
        updatePc();
        break;
      case op.div:
        var a = this.accumulatorStack.pop();
        var b = this.accumulatorStack.pop();
        var res =  b.value/a.value;
        this.accumulatorStack.push(new jsobjects.JSNumber(res));
        updatePc();
        break;
      case op.mod:
        var a = this.accumulatorStack.pop();
        var b = this.accumulatorStack.pop();
        var res =  b.value%a.value;
        this.accumulatorStack.push(new jsobjects.JSNumber(res));
        updatePc();
        break;
      case op.bit_and:
        var a = this.accumulatorStack.pop();
        var b = this.accumulatorStack.pop();
        var res =  b.value&a.value;
        this.accumulatorStack.push(new jsobjects.JSNumber(res));
        updatePc();
        break;
      case op.bit_or:
        var a = this.accumulatorStack.pop();
        var b = this.accumulatorStack.pop();
        var res =  b.value|a.value;
        this.accumulatorStack.push(new jsobjects.JSNumber(res));
        updatePc();
        break;
      case op.bit_xor:
        var a = this.accumulatorStack.pop();
        var b = this.accumulatorStack.pop();
        var res =  b.value^a.value;
        this.accumulatorStack.push(new jsobjects.JSNumber(res));
        updatePc();
        break;
      case op.bit_shl:
        var a = this.accumulatorStack.pop();
        var b = this.accumulatorStack.pop();
        var res =  b.value<<a.value;
        this.accumulatorStack.push(new jsobjects.JSNumber(res));
        updatePc();
        break;
      case op.bit_shr:
        var a = this.accumulatorStack.pop();
        var b = this.accumulatorStack.pop();
        var res =  b.value>>a.value;
        this.accumulatorStack.push(new jsobjects.JSNumber(res));
        updatePc();
        break;
      case op.bit_shr_arith:
        var a = this.accumulatorStack.pop();
        var b = this.accumulatorStack.pop();
        var res =  b.value>>>a.value;
        this.accumulatorStack.push(new jsobjects.JSNumber(res));
        updatePc();
        break;
      case op.eq:
        var a = this.accumulatorStack.pop();
        var b = this.accumulatorStack.pop();
        var res =  b.value==a.value;
        this.accumulatorStack.push(new jsobjects.JSBoolean(res));
        updatePc();
        break;
      case op.neq:
        var a = this.accumulatorStack.pop();
        var b = this.accumulatorStack.pop();
        var res =  b.value!=a.value;
        this.accumulatorStack.push(new jsobjects.JSBoolean(res));
        updatePc();
        break;
      case op.lt:
        var a = this.accumulatorStack.pop();
        var b = this.accumulatorStack.pop();
        var res =  b.value<a.value;
        this.accumulatorStack.push(new jsobjects.JSBoolean(res));
        updatePc();
        break;
      case op.gt:
        var a = this.accumulatorStack.pop();
        var b = this.accumulatorStack.pop();
        var res =  b.value>a.value;
        this.accumulatorStack.push(new jsobjects.JSBoolean(res));
        updatePc();
        break;
      case op.leq:
        var a = this.accumulatorStack.pop();
        var b = this.accumulatorStack.pop();
        var res =  b.value<=a.value;
        this.accumulatorStack.push(new jsobjects.JSBoolean(res));
        updatePc();
        break;
      case op.geq:
        var a = this.accumulatorStack.pop();
        var b = this.accumulatorStack.pop();
        var res =  b.value>=a.value;
        this.accumulatorStack.push(new jsobjects.JSBoolean(res));
        updatePc();
        break;
      case op.eqs:
        var a = this.accumulatorStack.pop();
        var b = this.accumulatorStack.pop();
        var res =  b.value===a.value;
        this.accumulatorStack.push(new jsobjects.JSBoolean(res));
        updatePc();
        break;
      case op.neqs:
        var a = this.accumulatorStack.pop();
        var b = this.accumulatorStack.pop();
        var res =  b.value!==a.value;
        this.accumulatorStack.push(new jsobjects.JSBoolean(res));
        updatePc();
        break;
      case op.del_var:
        var scope = current_call_stack_top.scope.getAncestor(instruction.operand[0],instruction.operand[1]);
        this.accumulatorStack.push(scope.deleteVariable(instruction.operand[1])?new jsobjects.JSBoolean(true):new jsobjects.JSBoolean(false));
        updatePc();
        break;
      case op.del_prop:
        var key = this.accumulatorStack.pop();
        var obj = this.accumulatorStack.pop();
        this.accumulatorStack.push(obj.deleteProperty(key.getString())?new jsobjects.JSBoolean(true):new jsobjects.JSBoolean(false));
        updatePc();
        break;
      case op.mul_plus:
        var a = this.accumulatorStack.pop();
        var res =  1*a.value;
        this.accumulatorStack.push(new jsobjects.JSNumber(res));
        updatePc();
        break;
      case op.mul_minus:
        var a = this.accumulatorStack.pop();
        var res =  -1*a.value;
        this.accumulatorStack.push(new jsobjects.JSNumber(res));
        updatePc();
        break;
      case op.comp:
        var a = this.accumulatorStack.pop();
        var res =  ~a.value;
        this.accumulatorStack.push(new jsobjects.JSNumber(res));
        updatePc();
        break;
      case op.not:
        var a = this.accumulatorStack.pop();
        var res =  !a.value;
        this.accumulatorStack.push(new jsobjects.JSBoolean(res));
        updatePc();
        break;
      case op.sub_1:
        var a = this.accumulatorStack.pop();
        var res =  a.value-1;
        this.accumulatorStack.push(new jsobjects.JSNumber(res));
        updatePc();
        break;
      case op.add_1:
        var a = this.accumulatorStack.pop();
        var res =  a.value+1;
        this.accumulatorStack.push(new jsobjects.JSNumber(res));
        updatePc();
        break;
      case op.typeof_:
        var a = this.accumulatorStack.pop();
        this.accumulatorStack.push(new jsobjects.JSString(a.getTypeof()));
        updatePc();
        break;
      case op.get_iterator:
        var a = this.accumulatorStack.pop();
        this.accumulatorStack.push(a.getIterator());
        updatePc();
        break;
      case op.has_next:
        var a = this.accumulatorStack.pop();
        this.accumulatorStack.push(new jsobjects.JSBoolean(a.hasNext()));
        updatePc();
        break;
      case op.get_next:
        var a = this.accumulatorStack.pop();
        this.accumulatorStack.push(new jsobjects.JSString(a.getNext()));
        updatePc();
        break;
      case op.throw_://TODO: test
        //スタックトップに例外オブジェクトがあるはず
        //例外オブジェクトをpopして、さらに余分に積んでる分もpop、例外オブジェクトをpush
        //余分に積んでるコールスタックをpop
        //飛び先にジャンプ
        var exception = this.accumulatorStack.pop();
        var entry = this.exceptionHandlerStack[this.exceptionHandlerStack.length-1];
        if(this.accumulatorStack.length > entry.acc_stack_depth){
          //余分なアキュムレータスタックを成仏させる
          this.accumulatorStack.length = entry.acc_stack_depth;
        }
        if(this.callStack.length > entry.call_stack_depth){
          //余分なコールスタックを成仏させる
          this.callStack.length = entry.call_stack_depth;
        }
        this.accumulatorStack.push(exception);
        this.pc = entry.address;
        break;
      case op.add_exception_handler://TODO: test
        //アドレスがオペランド。
        //例外ハンドラスタックにオペランドのアドレスとコールスタックの深さ、アキュムレータスタックの深さを積む？
        this.exceptionHandlerStack.push(
          new vm.ExceptionHandlerEntry(
            instruction.operand[0],
            this.callStack.length,
            this.accumulatorStack.length
          ));
        updatePc();
        break;
      case op.remove_exception_handler://TODO: test
        this.exceptionHandlerStack.pop();
        updatePc();
        break;
      case op.exit:
        return false;
        break;
    }
    return true;
  };
})();


