/**
 * @fileOverview 構文解析関連
 * @author ide
 */
/**
 * @namespace parse utility container
 */
var parse = {};
(function(){
  /**
   * @class Parsing Error 
   * @param {String} message error message string
   */
  parse.ParseError = function(message){
    this.type = "parse.ParseError";
    this.message = message;
  };
  /**
   * @class unexpexted token error 
   */
  parse.UnexpectedTokenError = function(/** tokenize.TOKEN_TYPE */tokenType){
    this.message += tokenType;
  };
  parse.UnexpectedTokenError.prototype = new parse.ParseError("Unexpected token:");
  /**
   * @class Javascript Parser
   */
  parse.Parser = function(){
    this._source = "";
    this._tokenizer = new tokenize.Tokenizer();
    this._variables = [[]];
    this._functions = [[]];
  };
  /**
   * initialize Parser
   * @param {String} source source string to parse
   * @param {Number} [startAt=0] start scan at
   */
  parse.Parser.prototype.init = function(source,startAt){
    var start = startAt || 0;
    this._source = source;
    this._tokenizer.init(this._source,start);
    this._variables = [[]];
    this._functions = [[]];
  };

  /**
   * プログラムのパース
   * @return {ast.Program} パースしたノード
   * @throws parse.UnexpectedTokenError
   */
  parse.Parser.prototype.parseProgram = function(){
    var stmts = []
    this.next();
    while(this.current()!==tokenize.TOKEN_TYPE.EOS){
      stmts.push(this.parseStatement([]));
    }
    return new ast.Program(this._variables.pop(),this._functions.pop(),stmts);
  };
  /* expression!!!! */
  /**
   * primary expressionのパース
   * 現在指しているトークンから始める
   * @return {ast.Expression} パースしたノード
   * @throws parse.UnexpectedTokenError
   */
  parse.Parser.prototype.parsePrimaryExpression = function(){
    var next = this.current();
    var res = null;
    switch(next){
      case tokenize.TOKEN_TYPE.K_THIS:{
        res = new ast.This();
        this.next();
        break;
      }
      case tokenize.TOKEN_TYPE.IDENTIFIER:{
        res = this.parseIdentifier();
        break;
      }
      case tokenize.TOKEN_TYPE.STRING_LITERAL:{
        res = new ast.Literal(new jsobjects.JSString(this._tokenizer.getLiteral()));
        this.next();
        break;
      }
      case tokenize.TOKEN_TYPE.NUMERIC_LITERAL:{
        res = new ast.Literal(new jsobjects.JSNumber(parseFloat(this._tokenizer.getLiteral())));
        this.next();
        break;
      }
      case tokenize.TOKEN_TYPE.TRUE_LITERAL:{
        res = new ast.Literal(new jsobjects.JSBoolean(true));
        this.next();
        break;
      }
      case tokenize.TOKEN_TYPE.FALSE_LITERAL:{
        res = new ast.Literal(new jsobjects.JSBoolean(false));
        this.next();
        break;
      }
      case tokenize.TOKEN_TYPE.NULL_LITERAL:{
        res = new ast.Literal(new jsobjects.JSNull());//はたしてnullを個別のインスタンスとして扱う必要はあるのだろうか・・・。
        this.next();
        break;
      }
      case tokenize.TOKEN_TYPE.P_DIV:
        res = this.parseRegularExpression(false);
        break;
      case tokenize.TOKEN_TYPE.P_ASSIGN_DIV:
        res = this.parseRegularExpression(true);
        break;
      case tokenize.TOKEN_TYPE.P_BRACKET_L:{//Aray Literal
        res = this.parseArrayLiteral();
        break;
      }
      case tokenize.TOKEN_TYPE.P_BRACE_L:{//Object Literal
        res = this.parseObjectLiteral();
        break;
      }
      case tokenize.TOKEN_TYPE.P_PAREN_L:{//expression group
        this.next();//consume (
        res = this.parseExpression(true);
        this.expectCurrent(tokenize.TOKEN_TYPE.P_PAREN_R);
        this.next();//consume )
        break;
      }
      default:{
        parse.UnexpectedTokenError(next);
      }
    }
    return res;
  };
  /**
   * Identifierのパース
   * @return {ast.Identifier} パースしたノード
   * @throws parse.UnexpectedTokenError
   */
  parse.Parser.prototype.parseIdentifier = function(){
    this.expectCurrent(tokenize.TOKEN_TYPE.IDENTIFIER);
    var name = this.parseIdentifierName();
    this.next();
    return new ast.Identifier(name.getString());
  };
  /**
   * Arguments式のパース
   * @return {ast.Arguments} パースしたノード
   * @throws parse.UnexpectedTokenError
   */
  parse.Parser.prototype.parseArguments = function(){
    var args = [];
    this.expectCurrent(tokenize.TOKEN_TYPE.P_PAREN_L);
    this.next();
    while(this.current()!==tokenize.TOKEN_TYPE.P_PAREN_R){ 
      args.push( this.parseAssignmentExpression(true) );
      if(this.current()===tokenize.TOKEN_TYPE.P_COMMA){
        this.next();
      }
    }
    this.expectCurrent(tokenize.TOKEN_TYPE.P_PAREN_R);
    this.next();
    return new ast.Arguments(args);
  };
  /**
   * FunctionExpression
   * @return {ast.FunctionExpression} パースしたノード
   * @throws parse.ParseError
   */
  parse.Parser.prototype.parseFunctionExpression = function(){
    var ident = null;
    this.expectCurrent(tokenize.TOKEN_TYPE.K_FUNCTION);
    if(this.next() === tokenize.TOKEN_TYPE.IDENTIFIER){ //consume function
      ident = this.parseIdentifier();
    }
    var exp = this.parseFunctionLiteral(ident?ident.name:null);
    return exp;
  };
  /**
   * LeftHandSideExpression式のパース
   * 現在指しているトークンから始める
   * @return {ast.Expression} パースしたノード
   * @throws parse.UnexpectedTokenError
   */
  parse.Parser.prototype.parseLeftHandSideExpression = function(){
    //  (new)* (FuncExp | PrimaryExp) (Arg | [Exp] | .Id)*
    var new_count = 0;
    var base = null;
    var args = null;
    while(this.current() === tokenize.TOKEN_TYPE.K_NEW){
      new_count++;
      this.next();
    }
    if(this.current() === tokenize.TOKEN_TYPE.K_FUNCTION){
      base = this.parseFunctionExpression();
    }else{
      base = this.parsePrimaryExpression();
    }
    args_or_props:
    while(true){
      var c = this.current();
      switch(c){
        case tokenize.TOKEN_TYPE.P_PERIOD://.Identifier
          this.next();//consume .
          var propName = this.parseIdentifierName();
          base = new ast.PropertyAccessor(
            base,
            new ast.Literal(propName));
          this.next();
          break;
        case tokenize.TOKEN_TYPE.P_BRACKET_L://[Expression]
          this.next();//consume [
          var prop = this.parseExpression(true);
          base = new ast.PropertyAccessor(
            base,
            prop);
          this.next();//consume ]
          break;
        case tokenize.TOKEN_TYPE.P_PAREN_L://Args
          if(new_count>0){
            new_count--;
            args = this.parseArguments(); 
            base = new ast.CallNew(base,args);
          }else{
            args = this.parseArguments(); 
            base = new ast.Call(base,args);
          }
          break;
        default:
          break args_or_props;
      }
    }
    while(new_count>0){
      //var f = function(){return function(){return function(){};};};
      //new new new f //=> Object
      new_count--;
      base = new ast.CallNew(base,
                            new ast.Arguments([]));
    }

    return base;
  };
  /**
   * 後置式のパース
   * 現在指しているトークンから始める
   * @return {ast.Expression} パースしたノード
   * @throws parse.UnexpectedTokenError
   */
  parse.Parser.prototype.parsePostfixExpression = function(){
    var exp = this.parseLeftHandSideExpression();
    if(exp===null){
      console.log(this.current(),this._tokenizer.getLiteral());
      throw new parse.ParseError("おわた.parseLeftHandSideExpression returns null");
      //TODO:エラー処理
    }
    if(!this._tokenizer._hasLineTerminator){
      var c = this.current();
      switch(c){
        case tokenize.TOKEN_TYPE.P_INC://++
        case tokenize.TOKEN_TYPE.P_DEC://--
          exp = new ast.PostfixOperation(c,exp);
          this.next();
          break;
        default:
          break;//TODO:まずいトークンが来た時の対策。
      }
    }
    return exp;
  };

  var isUnaryOp = function(token){
    switch(token){
      case tokenize.TOKEN_TYPE.K_DELETE://fall through
      case tokenize.TOKEN_TYPE.K_VOID://fall through
      case tokenize.TOKEN_TYPE.K_TYPEOF://fall through
      case tokenize.TOKEN_TYPE.P_INC://fall through
      case tokenize.TOKEN_TYPE.P_DEC://fall through
      case tokenize.TOKEN_TYPE.P_ADD://fall through
      case tokenize.TOKEN_TYPE.P_SUB://fall through
      case tokenize.TOKEN_TYPE.P_COMP://fall through
      case tokenize.TOKEN_TYPE.P_NOT:
        return true;
    }
    return false;
  };
  /**
   * 単項演算のパース
   * 現在指しているトークンから始める
   * @return {ast.Expression} パースしたノード
   * @throws parse.UnexpectedTokenError
   */
  parse.Parser.prototype.parseUnaryOperation = function(){
    var next = this.current();
    if(isUnaryOp(next)){
      this.next();
      var exp = this.parseUnaryOperation();
      if(exp instanceof ast.Literal && exp.value.isNumber()){
        switch(next){
          case tokenize.TOKEN_TYPE.P_ADD:
            return exp;
          case tokenize.TOKEN_TYPE.P_SUB:
            return new ast.Literal(new jsobjects.JSNumber(-1*exp.value.getNumber()));
          case tokenize.TOKEN_TYPE.P_COMP:
            return new ast.Literal(new jsobjects.JSNumber(~(exp.value.getNumber())));//double<->int型変換めんどいのでデフォルトの型変換に任せる。
        }
      }
      return new ast.UnaryOperation(next,exp);
    }else{
      return this.parsePostfixExpression();
    }
  };
  var newBinaryOperation = function(op,left,right){
    if(left instanceof ast.Literal && left.value.isNumber() &&
       right instanceof ast.Literal && right.value.isNumber()){//数値な即値のみの演算なら計算しとく。
      var value = 0;
      var generateLiteral = true;
      switch(op){
        case tokenize.TOKEN_TYPE.P_ADD:
          value = left.value.getNumber() + right.value.getNumber();
          break;
        case tokenize.TOKEN_TYPE.P_SUB:
          value = left.value.getNumber() - right.value.getNumber();
          break;
        case tokenize.TOKEN_TYPE.P_MUL:
          value = left.value.getNumber() * right.value.getNumber();
          break;
        case tokenize.TOKEN_TYPE.P_MOD:
          value = left.value.getNumber() % right.value.getNumber();
          break;
        case tokenize.TOKEN_TYPE.P_DIV:
          value = left.value.getNumber() / right.value.getNumber();
          break;
        //ビット演算 めんどいのでHost JavaScriptの型変換に任せる（ぉ
        case tokenize.TOKEN_TYPE.P_AND:
          value = left.value.getNumber() & right.value.getNumber();
          break;
        case tokenize.TOKEN_TYPE.P_OR:
          value = left.value.getNumber() | right.value.getNumber();
          break;
        case tokenize.TOKEN_TYPE.P_XOR:
          value = left.value.getInt32() ^ right.value.getInt32();
          value = left.value.getNumber() ^ right.value.getNumber();
          break;
        case tokenize.TOKEN_TYPE.P_SHL:
          value = left.value.getNumber() << right.value.getNumber();
          break;
        case tokenize.TOKEN_TYPE.P_SHR:
          value = left.value.getNumber() >> right.value.getNumber();
          break;
        case tokenize.TOKEN_TYPE.P_SHR_ARITH:
          value = left.value.getNumber() >>> right.value.getNumber();
          break;
        default:
          generateLiteral = false;
      }
      if(generateLiteral){
        return new ast.Literal(new jsobjects.JSNumber(value));
      }
    }else if(left instanceof ast.Literal && left.value.isString() &&
       right instanceof ast.Literal && right.value.isString()){//文字列な即値のみの演算なら計算しとく。
      var value = 0;
      var generateLiteral = true;
      switch(op){
        case tokenize.TOKEN_TYPE.P_ADD:
          value = left.value.getString() + right.value.getString();
          break;
        default:
          generateLiteral = false;
      }
      if(generateLiteral){
        return new ast.Literal(new jsobjects.JSString(value));
      }
    }
    return new ast.BinaryOperation(op,left,right);
  };
  /**
   * 二項演算のパース
   * 現在指しているトークンから始める
   * @return {ast.Expression} パースしたノード
   * @throws parse.UnexpectedTokenError
   * @param {Boolean} accept_in in演算子を2項演算として許容するかどうか
   */
  parse.Parser.prototype.parseBinaryOperation = function(accept_in){
    var operandStack = [];
    var operatorStack = [];
    var current;
    var operator;
    var precedence;
    while(true){
      current = this.parseUnaryOperation();
      if(!current){
        break;
      }
      operandStack.push(current);
      operator = this.current();
      precedence = this.getPrecedence(operator,accept_in);
      while(operatorStack.length>0 && this.getPrecedence(operatorStack[operatorStack.length-1],accept_in)>precedence){
        var right = operandStack.pop();
        var left = operandStack.pop();
        operandStack.push(newBinaryOperation(operatorStack.pop(),left,right));
      }
      if(precedence===0){
        break;
      }
      operatorStack.push(operator);
      this.next();
    }
    if(operandStack.length!==1){
      throw new Error("なんか二項式解析うまくいってねぇ:"+operandStack);
    }
    return operandStack[0];
  };

  /**
   * 条件式のパース
   * 現在指しているトークンから始める
   * @return {ast.Expression} パースしたノード
   * @throws parse.UnexpectedTokenError
   * @param {Boolean} accept_in in演算子を2項演算として許容するかどうか
   */
  parse.Parser.prototype.parseConditionalExpression = function(accept_in){
    // ConditionalExpression := 
    //    LogicalORExpression
    //    LogicalORExpression ? AssignmentExpression :AssignmentExpression
    var condExp = this.parseBinaryOperation(accept_in);
    if(this.current() === tokenize.TOKEN_TYPE.P_COND){
      this.next();
      var thenExp = this.parseAssignmentExpression(true);
      this.expectCurrent(tokenize.TOKEN_TYPE.P_COLON);
      this.next();
      var elseExp = this.parseAssignmentExpression(accept_in);
      condExp = new ast.Conditional(condExp,thenExp,elseExp);
    }
    return condExp;
  };

  var isAssignOperator = function(/** tokenize.TOKEN_TYPE */token)/** Boolean */{
    switch(token){
      case tokenize.TOKEN_TYPE.P_ASSIGN:
      case tokenize.TOKEN_TYPE.P_ASSIGN_ADD://fall through
      case tokenize.TOKEN_TYPE.P_ASSIGN_SUB://fall through
      case tokenize.TOKEN_TYPE.P_ASSIGN_MUL://fall through
      case tokenize.TOKEN_TYPE.P_ASSIGN_MOD://fall through
      case tokenize.TOKEN_TYPE.P_ASSIGN_DIV://fall through
      case tokenize.TOKEN_TYPE.P_ASSIGN_SHL://fall through
      case tokenize.TOKEN_TYPE.P_ASSIGN_SHR://fall through
      case tokenize.TOKEN_TYPE.P_ASSIGN_SHR_ARITH://fall through
      case tokenize.TOKEN_TYPE.P_ASSIGN_AND://fall through
      case tokenize.TOKEN_TYPE.P_ASSIGN_OR://fall through
      case tokenize.TOKEN_TYPE.P_ASSIGN_XOR:
        return true;
    }
    return false;
  };
  var newAssignment = function(op,left,right){
    return new ast.Assignment(op,left,right);
  };
  /**
   * 代入式のパース
   * 現在指しているトークンから始める
   * @return {ast.Expression} パースしたノード
   * @throws parse.UnexpectedTokenError
   * @param {Boolean} accept_in in演算子を2項演算として許容するかどうか
   */
  parse.Parser.prototype.parseAssignmentExpression = function(accept_in){
    // AssignmentExpression :=
    //    ConditionalExpression
    //    LeftHandSideExpression  ( = | += | -= | *= | %= | /= | <<= | >>= | >>>= | &= | |= | ^= ) AssignmentExpression
    //    ConditionalExpression は LeftHandSideExpression　を含むので　とりあえず parseConditionalExpressionして、その結果がleftHandSideな式かどうか判断する。
    var exp = this.parseConditionalExpression(accept_in);
    var op = this.current();
    if(isAssignOperator(op)){
      if(!exp.isValidLeftHandSide()){
        throw new parse.UnexpectedTokenError(exp);
      }
      this.next();//consume Assignment operator
      var right = this.parseAssignmentExpression(accept_in);
      exp = newAssignment(op,exp,right);
    }
    return exp;
  };
  /**
   * 式のパース
   * 現在指しているトークンから始める
   * @return {ast.Expression} パースしたノード
   * @throws parse.UnexpectedTokenError
   * @param {Boolean} accept_in in演算子を2項演算として許容するかどうか
   */
  parse.Parser.prototype.parseExpression = function(accept_in){
    // Expression := 
    //    (AssignmentExpression ,)* AssignmentExpression
    var exp = this.parseAssignmentExpression(accept_in);
    if(this.current()===tokenize.TOKEN_TYPE.P_COMMA){
      this.next();
      exp = new ast.BinaryOperation(tokenize.TOKEN_TYPE.P_COMMA,
                                    exp,
                                    this.parseExpression(accept_in));
    }
    return exp;
  };

  /**
   * @param has_equal 前に/=トークンが来ているかどうか（すなわち正規表現リテラルの最初が=かどうか）
   * @return {ast.Expression} 正規表現ノード
   */
  parse.Parser.prototype.parseRegularExpression = function(/** Boolean */has_equal){
    this.expectCurrent(has_equal ? tokenize.TOKEN_TYPE.P_ASSIGN_DIV : tokenize.TOKEN_TYPE.P_DIV);
    if(!this._tokenizer.scanRegularExpressionBody(has_equal)){
      throw new parse.ParseError("正規表現パターンのスキャンでこけた");
    }
    var body = this._tokenizer.getLiteral();
    if(!this._tokenizer.scanRegularExpressionFlag()){
      throw new parse.ParseError("正規表現パターンのスキャンでこけた");
    }
    var flags = this._tokenizer.getLiteral();
    this.next();
    return new ast.RegExpLiteral(new jsobjects.JSString(body),
                                 new jsobjects.JSString(flags));
  };
  /**
   * 配列リテラルのパース
   * @return {ast.Expression} パースしたノード
   */
  parse.Parser.prototype.parseArrayLiteral = function(){
    var elements = [];
    var index = 0;
    this.expectCurrent(tokenize.TOKEN_TYPE.P_BRACKET_L);
    this.next();
    while(this.current()!==tokenize.TOKEN_TYPE.P_BRACKET_R){
      if(this.current()===tokenize.TOKEN_TYPE.P_COMMA){
        elements[index] = new ast.Literal(new jsobjects.JSUndefined());
      }else{
        elements[index] = this.parseAssignmentExpression(true);
      }
      index++;
      if(this.current() !== tokenize.TOKEN_TYPE.P_BRACKET_R){
        this.next();
      }
    }
    this.expectCurrent(tokenize.TOKEN_TYPE.P_BRACKET_R);
    this.next();
    return new ast.ArrayLiteral(elements);
  };
  /**
   * 連想配列リテラルのパース
   * @return {ast.Expression} パースしたノード
   */
  parse.Parser.prototype.parseObjectLiteral = function(){
    var keys=[],values=[];
    var c,key,keyname;
    this.expectCurrent(tokenize.TOKEN_TYPE.P_BRACE_L);
    this.next();//consume {
    while(this.current()!==tokenize.TOKEN_TYPE.P_BRACE_R){
      c=this.current(),key = null,keyname ="";
      switch(c){
        case tokenize.TOKEN_TYPE.IDENTIFIER:
          keyname = this.parseIdentifierName();
          this.next();
          break;
        case tokenize.TOKEN_TYPE.STRING_LITERAL:
          keyname = new jsobjects.JSString(this._tokenizer.getLiteral());
          this.next();
          break;
        case tokenize.TOKEN_TYPE.NUMERIC_LITERAL:
          keyname = new jsobjects.JSString(parseFloat(this._tokenizer.getLiteral()) + "");
          this.next();
          break;
        default:
          throw new parse.UnexpectedTokenError(c+this._tokenizer.getLiteral());
      }
      key = new ast.Literal(keyname);
      this.expectCurrent(tokenize.TOKEN_TYPE.P_COLON);
      this.next();//consume :
      var value = this.parseAssignmentExpression(true);
      keys.push(key);
      values.push(value);
      if(this.current()===tokenize.TOKEN_TYPE.P_COMMA){
        this.next();//consume ,
      }
    }
    this.expectCurrent(tokenize.TOKEN_TYPE.P_BRACE_R);
    this.next();//consume }
    return new ast.ObjectLiteral(keys,values);
  };
  /**
   * 関数リテラルのパース
   * @param {String} name 関数内部識別名
   * @throws {parse.ParseError}
   * @return {ast.FunctionExpression} パースした関数リテラル
   */
  parse.Parser.prototype.parseFunctionLiteral = function(name){
    var args = [],statements=[];
    this.expectCurrent(tokenize.TOKEN_TYPE.P_PAREN_L);
    this.next();//consume (
    while(this.current()!==tokenize.TOKEN_TYPE.P_PAREN_R){
      var ident = this.parseIdentifier();
      args.push(ident.name);
      if(this.current()===tokenize.TOKEN_TYPE.P_COMMA){
        this.expectNext(tokenize.TOKEN_TYPE.IDENTIFIER);
      }
    }
    this.expectCurrent(tokenize.TOKEN_TYPE.P_PAREN_R);
    this.expectNext(tokenize.TOKEN_TYPE.P_BRACE_L);//consume )
    this.next();//consume {
    this._variables.push([]);
    this._functions.push([]);
    while(this.current()!==tokenize.TOKEN_TYPE.P_BRACE_R){
      statements.push(this.parseStatement([]));
    }
    this.expectCurrent(tokenize.TOKEN_TYPE.P_BRACE_R);
    this.next();//consume }
    return new ast.FunctionExpression(name,args,this._variables.pop(),this._functions.pop(),statements);
  };

  /* statements!!!! */
  /**
   * Statementのパース
   * @param {String[]} labelset ラベルセットを構成するラベル群
   * @return {ast.Statement} パースしたStatementノード
   * @throws {parse.ParseError}
   */
  parse.Parser.prototype.parseStatement = function(labelset){
    var stmnt = null;
    var c=this.current();
    switch(c){
      case tokenize.TOKEN_TYPE.K_IF:{
        stmnt = this.parseIfStatement(labelset);
        break;
      }
      case tokenize.TOKEN_TYPE.K_CONTINUE:{
        stmnt = this.parseContinueStatement();
        break;
      }
      case tokenize.TOKEN_TYPE.K_BREAK:{
        stmnt = this.parseBreakStatement();
        break;
      }
      case tokenize.TOKEN_TYPE.K_RETURN:{
        stmnt = this.parseReturnStatement();
        break;
      }
      case tokenize.TOKEN_TYPE.K_THROW:{
        stmnt = this.parseThrowStatement();
        break;
      }
      case tokenize.TOKEN_TYPE.K_WITH:{
        stmnt = this.parseWithStatement(labelset);
        break;
      }
      case tokenize.TOKEN_TYPE.K_TRY:{
        stmnt = this.parseTryStatement(labelset);
        break;
      }
      case tokenize.TOKEN_TYPE.K_SWITCH:{
        stmnt = this.parseSwitchStatement(labelset);
        break;
      }
      case tokenize.TOKEN_TYPE.K_DO:{
        stmnt = this.parseDoWhileStatement(labelset);
        break;
      }
      case tokenize.TOKEN_TYPE.K_WHILE:{
        stmnt = this.parseWhileStatement(labelset);
        break;
      }
      case tokenize.TOKEN_TYPE.K_VAR:{
        stmnt = this.parseVariableStatement();
        break;
      }
      case tokenize.TOKEN_TYPE.K_FOR:{
        stmnt = this.parseForStatement(labelset);
        break;
      }
      case tokenize.TOKEN_TYPE.K_FUNCTION:{
        stmnt = this.parseFunctionDeclaration();
        break;
      }
      case tokenize.TOKEN_TYPE.P_BRACE_L:{
        stmnt = this.parseBlockStatement(labelset);
        break;
      }
      case tokenize.TOKEN_TYPE.P_SEMICOLON:{
        stmnt = this.parseEmptyStatement();
        break;
      }
      default:{
        stmnt = this.parseLabelledOrExpressionStatement(labelset);
        break;
      }
    }
    return stmnt;
  };
  /**
   * BlockStatementのパース
   * @param {String[]} labelset ラベルセットを構成するラベル群
   * @return {ast.Statement} パースしたStatementノード
   * @throws {parse.ParseError}
   */
  parse.Parser.prototype.parseBlockStatement = function(labelset){
    var stmts = [];
    this.expectCurrent(tokenize.TOKEN_TYPE.P_BRACE_L);
    this.next();//consume {
    while(this.current() !== tokenize.TOKEN_TYPE.P_BRACE_R){
      var stmt = this.parseStatement([]);
      stmts.push(stmt);
    }
    this.next();//consume }
    return new ast.BlockStatement(labelset,stmts);
  };
  /**
   * VariableDeclaration listのパース
   * @param {Boolean} accept_in 初期化式にinが使えるか否か
   * @return {ast.Statement} パースしたStatementノード
   * @throws {parse.ParseError}
   */
  parse.Parser.prototype.parseVariableDeclarationList = function(accept_in){
    var ident = null,exp=null,expRoot = null;
    this.expectCurrent(tokenize.TOKEN_TYPE.K_VAR);
    this.next();//consume var
    while(true){
      ident = this.parseIdentifier();
      if(this.current() === tokenize.TOKEN_TYPE.P_ASSIGN){//初期化式あり
        this.next();//consume =
        exp = this.parseAssignmentExpression(accept_in);
        exp = new ast.Assignment(tokenize.TOKEN_TYPE.P_ASSIGN,ident,exp);
      }else{//初期化式なし
        exp = ident;
      }
      if(!expRoot){
        expRoot = exp;
      }else{
        expRoot = new ast.BinaryOperation(tokenize.TOKEN_TYPE.P_COMMA,expRoot,exp);
      }
      this.addVariable(ident.name);
      //変数名登録
      if(this.current()===tokenize.TOKEN_TYPE.P_COMMA){
        this.next();//consume ,
      }else{
        break;
      }
    }
    return expRoot;
  };
  /**
   * VariableStatementのパース
   * @return {ast.Statement} パースしたStatementノード
   * @throws {parse.ParseError}
   */
  parse.Parser.prototype.parseVariableStatement = function(){
    this.expectCurrent(tokenize.TOKEN_TYPE.K_VAR);
    var res = this.parseVariableDeclarationList(true);
    this.expectSemicolon();
    res = new ast.ExpressionStatement(res);
    return res;
  };
  /**
   * EmptyStatementのパース
   * @return {ast.Statement} パースしたStatementノード
   * @throws {parse.ParseError}
   */
  parse.Parser.prototype.parseEmptyStatement = function(){
    this.expectCurrent(tokenize.TOKEN_TYPE.P_SEMICOLON);
    this.next();
    return new ast.EmptyStatement();
  };
  /**
   * ExpressionStatement または LabelledStatementのパース
   * @return {ast.Statement} パースしたStatementノード
   * @param {String[]} labelset ラベルセットを構成するラベル群
   * @throws {parse.ParseError}
   */
  parse.Parser.prototype.parseLabelledOrExpressionStatement = function(labelset){
    //  LabelledStatement | ExpressionStatement :=
    //    Identifier : Statement
    //    Expression ;
    if(this.current() === tokenize.TOKEN_TYPE.K_FUNCTION ||
       this.current() === tokenize.TOKEN_TYPE.P_BRACE_L){
      throw new parse.UnexpectedTokenError(this.current());
    }
    var exp = this.parseExpression(true);
    var label = "", newLabel = labelset.slice(0);
    if(exp instanceof ast.Identifier && this.current()===tokenize.TOKEN_TYPE.P_COLON){
      //LabelledStatement
      label = exp.name;
      newLabel.push(label);
      this.next();// consume :
      return this.parseStatement(newLabel);
    }else{
      //ExpressionStatement
      this.expectSemicolon();
      return new ast.ExpressionStatement(exp);
    }
  };
  /**
   * IfStatementのパース
   * @return {ast.Statement} パースしたStatementノード
   * @param {String[]} labelset ラベルセットを構成するラベル群
   * @throws {parse.ParseError}
   */
  parse.Parser.prototype.parseIfStatement = function(labelset){
    this.expectCurrent(tokenize.TOKEN_TYPE.K_IF);
    this.expectNext(tokenize.TOKEN_TYPE.P_PAREN_L);
    this.next();//consume (
    var condtion = this.parseExpression(true);
    this.expectCurrent(tokenize.TOKEN_TYPE.P_PAREN_R);
    this.next();//consume )
    var thenStmt = this.parseStatement([]);
    var elseStmt = null;
    if(this.current() === tokenize.TOKEN_TYPE.K_ELSE){
      this.next();//consume else
      elseStmt = this.parseStatement([]);
    }
    return new ast.IfStatement(labelset,condtion,thenStmt,elseStmt);
  };
  /**
   * do{ }while()のパース
   * @return {ast.Statement} パースしたStatementノード
   * @param {String[]} labelset ラベルセットを構成するラベル群
   * @throws {parse.ParseError}
   */
  parse.Parser.prototype.parseDoWhileStatement = function(labelset){
    // do Statement while(Expression);
    this.expectCurrent(tokenize.TOKEN_TYPE.K_DO);
    this.next();//consume do
    var stmt = this.parseStatement([]);
    this.expectCurrent(tokenize.TOKEN_TYPE.K_WHILE);
    this.expectNext(tokenize.TOKEN_TYPE.P_PAREN_L)
    this.next();//consume (
    var exp = this.parseExpression();
    this.expectCurrent(tokenize.TOKEN_TYPE.P_PAREN_R);
    this.next();//consume )
    this.expectSemicolon();
    return new ast.DoWhileStatement(labelset,exp,stmt);
  };
  /**
   * while文のパース
   * @return {ast.Statement} パースしたStatementノード
   * @param {String[]} labelset ラベルセットを構成するラベル群
   * @throws {parse.ParseError}
   */
  parse.Parser.prototype.parseWhileStatement = function(labelset){
    // while(Expression)Statement
    this.expectCurrent(tokenize.TOKEN_TYPE.K_WHILE);
    this.expectNext(tokenize.TOKEN_TYPE.P_PAREN_L);
    this.next();//consume (
    var exp = this.parseExpression();
    this.expectCurrent(tokenize.TOKEN_TYPE.P_PAREN_R);
    this.next();//consume )
    var stmt = this.parseStatement([]);
    return new ast.WhileStatement(labelset,exp,stmt);
  };
  /**
   * for文のパース
   * @param {String[]} labelset ラベルセットを構成するラベル群
   * @return {ast.Statement} パースしたStatementノード
   * @throws {parse.ParseError}
   */
  parse.Parser.prototype.parseForStatement = function(labelset){
    // for(ExpressionNoIn? ; Expression? ; Expression? )Statement
    // for(VariableDeclarationNoIn? ; Expression? ; Expression? )Statement
    // for(ExpressionNoIn in Expression )Statement
    // for(VariableDeclarationNoIn in Expression )Statement
    var init = null, condition = null, next = null, stmt = null;
    var has_var = false;
    this.expectCurrent(tokenize.TOKEN_TYPE.K_FOR);
    this.expectNext(tokenize.TOKEN_TYPE.P_PAREN_L);
    this.next();//consume (
    if(this.current() === tokenize.TOKEN_TYPE.K_VAR){
      //変数宣言なう
      init = this.parseVariableDeclarationList(false);
      has_var = true;
    }else if(this.current() !== tokenize.TOKEN_TYPE.P_SEMICOLON){
      //初期化式なう
      init = this.parseExpression(false);
    }
    if(this.current() === tokenize.TOKEN_TYPE.K_IN){//for-in
      if(!has_var && !init.isValidLeftHandSide() ||
         has_var && init instanceof ast.BinaryOperation){//複数の変数の宣言は無理
        throw new parse.ParseError("不正なfor in文のLeftHandSideExpression");
      }
      this.next();//consume in
      next = this.parseExpression(true);
      this.expectCurrent(tokenize.TOKEN_TYPE.P_PAREN_R);
      this.next();//consume )
      stmt = this.parseStatement([]);
      //varのときの初期化を一度だけ実行するようにする。
      if(has_var && init instanceof ast.Assignment){
        //for(var x=1 in obj)stmt;
        // |
        // V
        //{
        //  var x=1;
        //  labels:
        //  for(x in obj)stmt;
        //}
        var varStmt = new ast.ExpressionStatement(init);
        var ident =  init.target;//キーを入れる識別子
        var forInStmt = new ast.ForInStatement(labelset,ident,next,stmt);
        return new ast.BlockStatement([],[varStmt,forInStmt]);
      }else{
        return new ast.ForInStatement(labelset,init,next,stmt);
      }
    }else{//for
      this.expectCurrent(tokenize.TOKEN_TYPE.P_SEMICOLON);
      this.next();//consume ;
      if(this.current() !== tokenize.TOKEN_TYPE.P_SEMICOLON){
        condition = this.parseExpression(true);
      }
      this.expectCurrent(tokenize.TOKEN_TYPE.P_SEMICOLON);
      this.next();//consume ;
      if(this.current() !== tokenize.TOKEN_TYPE.P_PAREN_R){
        next = this.parseExpression(true);
      }
      this.expectCurrent(tokenize.TOKEN_TYPE.P_PAREN_R);
      this.next();//consume )
      stmt = this.parseStatement([]);
      return new ast.ForStatement(labelset,init,condition,next,stmt);
    }
  };
  /**
   * ContinueStatementのパース
   * @return {ast.Statement} パースしたStatementノード
   * @throws {parse.ParseError}
   */
  parse.Parser.prototype.parseContinueStatement = function(){
    var label = null;
    this.expectCurrent(tokenize.TOKEN_TYPE.K_CONTINUE);
    this.next();
    if(!this._tokenizer._hasLineTerminator){
      if(this.current()===tokenize.TOKEN_TYPE.IDENTIFIER){
        label = this._tokenizer.getLiteral();
        this.next();
      }
      this.expectSemicolon();
    }
    return new ast.ContinueStatement(label);
  };
  /**
   * BreakStatementのパース
   * @return {ast.Statement} パースしたStatementノード
   * @throws {parse.ParseError}
   */
  parse.Parser.prototype.parseBreakStatement = function(){
    var label = null;
    this.expectCurrent(tokenize.TOKEN_TYPE.K_BREAK);
    this.next();
    if(!this._tokenizer._hasLineTerminator){
      if(this.current()===tokenize.TOKEN_TYPE.IDENTIFIER){
        label = this._tokenizer.getLiteral();
        this.next();
      }
      this.expectSemicolon();
    }
    return new ast.BreakStatement(label);
  };
  /**
   * ReturnStatementのパース
   * @return {ast.Statement} パースしたStatementノード
   * @throws {parse.ParseError}
   */
  parse.Parser.prototype.parseReturnStatement = function(){
    var exp = null;
    this.expectCurrent(tokenize.TOKEN_TYPE.K_RETURN);
    this.next();
    if(!this._tokenizer._hasLineTerminator){
      if(this.current()!==tokenize.TOKEN_TYPE.P_SEMICOLON){
        exp = this.parseExpression(true);
      }
      this.expectSemicolon();
    }
    return new ast.ReturnStatement(exp);
  };
  /**
   * WithStatementのパース
   * @param {String[]} labelset ラベルセットを構成するラベル群
   * @return {ast.Statement} パースしたStatementノード
   * @throws {parse.ParseError}
   */
  parse.Parser.prototype.parseWithStatement = function(labelset){
    this.expectCurrent(tokenize.TOKEN_TYPE.K_WITH);//with
    this.expectNext(tokenize.TOKEN_TYPE.P_PAREN_L);//(
    this.next();//consume (
    var exp = this.parseExpression(true);
    this.expectCurrent(tokenize.TOKEN_TYPE.P_PAREN_R);//)
    this.next();//consume )
    var stmt = this.parseStatement([]);
    return new ast.WithStatement(labelset,exp,stmt);
  };
  /**
   * CaseClauseのパース
   * @return {ast.Statement} パースしたStatementノード
   * @throws {parse.ParseError}
   */
  parse.Parser.prototype.parseCaseClause = function(/** Boolean */already_seen_default){
    var exp = null;
    var statements = [];
    if(this.current() === tokenize.TOKEN_TYPE.K_CASE){
      this.next();//consume case
      exp = this.parseExpression();
    }else{
      this.expectCurrent(tokenize.TOKEN_TYPE.K_DEFAULT);
      if(already_seen_default){
        throw new parse.ParseError("switchのdefaultがダブってる");
        return null;
      }
      this.next();//consume default
    }
    this.expectCurrent(tokenize.TOKEN_TYPE.P_COLON);
    this.next();//consume :
    while(this.current() !== tokenize.TOKEN_TYPE.K_CASE &&
          this.current() !== tokenize.TOKEN_TYPE.K_DEFAULT &&
          this.current() !== tokenize.TOKEN_TYPE.P_BRACE_R ){
      statements.push(this.parseStatement([]));
    }
    return new ast.CaseClause(exp,statements);
  };
  /**
   * SwitchStatementのパース
   * @param {String[]} labelset ラベルセットを構成するラベル群
   * @return {ast.Statement} パースしたStatementノード
   * @throws {parse.ParseError}
   */
  parse.Parser.prototype.parseSwitchStatement = function(labelset){
    var clauses = [],seen_default = false;
    var clause = null;
    this.expectCurrent(tokenize.TOKEN_TYPE.K_SWITCH);
    this.expectNext(tokenize.TOKEN_TYPE.P_PAREN_L);
    this.next();//consume (
    var exp = this.parseExpression();
    this.expectCurrent(tokenize.TOKEN_TYPE.P_PAREN_R);
    this.expectNext(tokenize.TOKEN_TYPE.P_BRACE_L);
    this.next();//consume {
    while(this.current()!==tokenize.TOKEN_TYPE.P_BRACE_R){
      clause = this.parseCaseClause(seen_default);
      if(clause.label===null){
        seen_default = true;
      }
      clauses.push(clause);
    }
    this.expectCurrent(tokenize.TOKEN_TYPE.P_BRACE_R);
    this.next();//consume }
    return new ast.SwitchStatement(labelset,exp,clauses);
  };
  /**
   * ThrowStatementのパース
   * @return {ast.Statement} パースしたStatementノード
   * @throws {parse.ParseError}
   */
  parse.Parser.prototype.parseThrowStatement = function(){
    var exp = null;
    this.expectCurrent(tokenize.TOKEN_TYPE.K_THROW);
    this.next();
    if(!this._tokenizer._hasLineTerminator){
      if(this.current()!==tokenize.TOKEN_TYPE.P_SEMICOLON){
        exp = this.parseExpression(true);
      }
      this.expectSemicolon();
    }
    return new ast.ThrowStatement(exp);
  };
  /**
   * TryStatementのパース
   * @param {String[]} labelset ラベルセットを構成するラベル群
   * @return {ast.Statement} パースしたStatementノード
   * @throws {parse.ParseError}
   */
  parse.Parser.prototype.parseTryStatement = function(labelset){
    this.expectCurrent(tokenize.TOKEN_TYPE.K_TRY);
    this.next();//consume try
    var try_block = this.parseBlockStatement([]);
    var catch_block = null, finally_block = null, exception = null;
    var res = null;
    if(this.current() === tokenize.TOKEN_TYPE.K_CATCH){
      this.next();//consume catch
      this.expectCurrent(tokenize.TOKEN_TYPE.P_PAREN_L);
      this.next();//consume (
      exception = this.parseIdentifier();
      this.expectCurrent(tokenize.TOKEN_TYPE.P_PAREN_R);
      this.next();//consume )
      catch_block = this.parseBlockStatement([]);
    }
    if(this.current() === tokenize.TOKEN_TYPE.K_FINALLY){
      this.next();//consume finally
      finally_block = this.parseBlockStatement([]);
    }
    if(catch_block!==null){
      res = new ast.TryCatchStatement(exception,try_block,catch_block);
    }
    if(finally_block!==null){
      if(res){
        try_block = new ast.BlockStatement([],[res]);
      }
      res = new ast.TryFinallyStatement(try_block,finally_block);
    }
    if(labelset.length > 0){
      res = new ast.BlockStatement(labelset,[res]);
    }
    return res;
  };
  /**
   * function文のパース
   * @return {ast.EmptyStatement} 空だぜ
   * @throws {parse.ParseError}
   */
  parse.Parser.prototype.parseFunctionDeclaration = function(){
    this.expectCurrent(tokenize.TOKEN_TYPE.K_FUNCTION);
    this.expectNext(tokenize.TOKEN_TYPE.IDENTIFIER);
    var ident = this.parseIdentifier();
    var exp = this.parseFunctionLiteral(null);
    this.addFunction(ident.name,exp);
    return new ast.EmptyStatement();
  };
  /**
   * get current token
   * @return {tokenize.TOKEN_TYPE} current token
   */
  parse.Parser.prototype.current = function(){
    return this._tokenizer.current();
  };
  /**
   * get next token
   * @return {tokenize.TOKEN_TYPE} next token
   */
  parse.Parser.prototype.next = function(){
    return this._tokenizer.tokenize();
  };
  /**
   * @throws parse.UnexpectedTokenError
   */
  parse.Parser.prototype.expectSemicolon = function(){
    var current = this.current();
    if(current === tokenize.TOKEN_TYPE.P_SEMICOLON){
      this.next();
      return;
    }
    if(current === tokenize.TOKEN_TYPE.EOS){
      return;
    }
    if(current === tokenize.TOKEN_TYPE.P_BRACE_R){
      return;
    }
    if(this._tokenizer._hasLineTerminator){
      return;
    }
    throw new parse.UnexpectedTokenError("expect ; but "+current);
  };
  /**
   * @throws parse.UnexpectedTokenError
   */
  parse.Parser.prototype.expectCurrent = function(/** tokenize.TOKEN_TYPE */tokenType){
    var current = this.current();
    if(tokenType !== current){
      throw new parse.UnexpectedTokenError("expect "+tokenType+" but "+current);
    }
  };
  /**
   * @throws parse.UnexpectedTokenError
   */
  parse.Parser.prototype.expectNext = function(/** tokenize.TOKEN_TYPE */tokenType){
    var next = this.next();
    if(tokenType !== next){
      throw new parse.UnexpectedTokenError("expect "+tokenType+" but "+next);
    }
  };
  /**
   * @return {Number} 演算子の優先度
   */
  parse.Parser.prototype.getPrecedence = function(/** tokenize.TOKEN_TYPE*/operator,/** Boolean */accept_in){
    switch(operator){
      case tokenize.TOKEN_TYPE.P_OR_LOGIC:
        return 1;
      case tokenize.TOKEN_TYPE.P_AND_LOGIC:
        return 2;
      case tokenize.TOKEN_TYPE.P_OR:
        return 3;
      case tokenize.TOKEN_TYPE.P_XOR:
        return 4;
      case tokenize.TOKEN_TYPE.P_AND:
        return 5;
      case tokenize.TOKEN_TYPE.P_EQUAL://fall through
      case tokenize.TOKEN_TYPE.P_NOT_EQUAL://fall through
      case tokenize.TOKEN_TYPE.P_EQAUL_STRICT://fall through
      case tokenize.TOKEN_TYPE.P_NOT_EQUAL_STRICT:
        return 6;
      case tokenize.TOKEN_TYPE.P_LT://fall through
      case tokenize.TOKEN_TYPE.P_LEQ://fall through
      case tokenize.TOKEN_TYPE.P_GT://fall through
      case tokenize.TOKEN_TYPE.P_GEQ://fall through
      case tokenize.TOKEN_TYPE.K_INSTANCEOF:
        return 7;
      case tokenize.TOKEN_TYPE.K_IN:
        return accept_in ? 7 : 0;
      case tokenize.TOKEN_TYPE.P_SHL://fall through
      case tokenize.TOKEN_TYPE.P_SHR://fall through
      case tokenize.TOKEN_TYPE.P_SHR_ARITH:
        return 8;
      case tokenize.TOKEN_TYPE.P_ADD://fall through
      case tokenize.TOKEN_TYPE.P_SUB:
        return 9;
      case tokenize.TOKEN_TYPE.P_MUL://fall through
      case tokenize.TOKEN_TYPE.P_DIV://fall through
      case tokenize.TOKEN_TYPE.P_MOD:
        return 10;
      default:
        return 0;
    }
  };
  /**
   * 現在パースしているスコープに変数を追加する。
   * @param identifier_name 変数名
   */
  parse.Parser.prototype.addVariable = function(/** String */identifier_name){
    var top_scope = this._variables[this._variables.length-1];
    //重複がないか調べる。
    var i = top_scope.indexOf(identifier_name);
    if(i===-1){
      top_scope.push(identifier_name);
    }
  };
  /**
   * 現在パースしているスコープに関数を追加する。
   * @param {String}identifier_name 関数名
   * @param {ast.FunctionExpression}func 関数
   */
  parse.Parser.prototype.addFunction = function(identifier_name,func){
    var top_scope = this._functions[this._functions.length-1];
    //重複がないか調べる。
    for(var i=0;i<top_scope.length;i++){
      if(top_scope[i].name === identifier_name){
        top_scope[i].func = func;
        return;
      }
    }
    top_scope.push({name:identifier_name,func:func});
  };
  parse.Parser.prototype.parseIdentifierName = function(){
    this.expectCurrent(tokenize.TOKEN_TYPE.IDENTIFIER);
    return new jsobjects.JSString(this._tokenizer.getLiteral());
  };
})();
