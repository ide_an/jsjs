# jsjs

JavaScriptでJavascript処理系を作ろうという試みの残骸。実装は2010年頃。

## 動かす

`exec.html`をブラウザで開くとJS処理系として動かせる。

## 資料
 * スライド http://www.usamimi.info/~ide/diary/20101205_presen/
 * 研究報告書原稿 `docs/report.pdf`
 * その他実装当時のメモが`docs/`以下に転がっている

## 足りてないもの
 * 主にビルトインオブジェクト系の実装が行われてない、というかその辺りの実装に行き詰まって投げたと思う