/**
 * @fileOverview parser.js unit test
 * @author ide
 * @requires qunit.js 
 * @requires jquery.js
*/
jQuery(function(){
  module("parser.js:Parser",{
    setup:function(){
      this.parser = new parse.Parser();
    }
  });
  test("parsePrimaryExpression",function(){
    this.parser.init("this;");
    this.parser.next();
    var res = this.parser.parsePrimaryExpression();
    ok(res instanceof ast.This,"戻り値はthisノード");
    equals(this.parser.current(),tokenize.TOKEN_TYPE.P_SEMICOLON,"current is ;");

    this.parser.init("variable;");
    this.parser.next();
    var res = this.parser.parsePrimaryExpression();
    ok(res instanceof ast.Identifier,"戻り値はIdentifierノード");
    equals(res.name,"variable","識別子名は'variable'");
    equals(this.parser.current(),tokenize.TOKEN_TYPE.P_SEMICOLON,"current is ;");

    this.parser.init("12.0;");
    this.parser.next();
    var res = this.parser.parsePrimaryExpression();
    ok(res instanceof ast.Literal,"戻り値はLiteralノード");
    ok(res.value.isNumber(),"値は数値");
    equals(res.value.getNumber(),12,"値は12");
    equals(this.parser.current(),tokenize.TOKEN_TYPE.P_SEMICOLON,"current is ;");

    this.parser.init("'おｋ？';");
    this.parser.next();
    var res = this.parser.parsePrimaryExpression();
    ok(res instanceof ast.Literal,"戻り値はLiteralノード");
    ok(res.value.isString(),"戻り値は文字列");
    equals(res.value.getString(),'おｋ？',"値は'おｋ？'");
    equals(this.parser.current(),tokenize.TOKEN_TYPE.P_SEMICOLON,"current is ;");

    this.parser.init("null;");
    this.parser.next();
    var res = this.parser.parsePrimaryExpression();
    ok(res instanceof ast.Literal,"戻り値はLiteralノード");
    ok(res.value.isNull(),"値はnull");
    equals(this.parser.current(),tokenize.TOKEN_TYPE.P_SEMICOLON,"current is ;");

    this.parser.init("true;");
    this.parser.next();
    var res = this.parser.parsePrimaryExpression();
    ok(res instanceof ast.Literal,"戻り値はLiteralノード");
    ok(res.value.isBoolean(),"値はboolean");
    equals(res.value.getBoolean(),true,"値はtrue");
    equals(this.parser.current(),tokenize.TOKEN_TYPE.P_SEMICOLON,"current is ;");

    this.parser.init("false;");
    this.parser.next();
    var res = this.parser.parsePrimaryExpression();
    ok(res instanceof ast.Literal,"戻り値はLiteralノード");
    ok(res.value.isBoolean(),"値はboolean");
    equals(res.value.getBoolean(),false,"値はfalse");
    equals(this.parser.current(),tokenize.TOKEN_TYPE.P_SEMICOLON,"current is ;");

    this.parser.init("/.*\\w/gi;");
    this.parser.next();
    var res = this.parser.parsePrimaryExpression();
    ok(res instanceof ast.Expression,"戻り値はExpressionノード");
    equals(this.parser.current(),tokenize.TOKEN_TYPE.P_SEMICOLON,"current is ;");

    this.parser.init("[1,2,3,true,false,undefined,'6'];");
    this.parser.next();
    var res = this.parser.parsePrimaryExpression();
    ok(res instanceof ast.ArrayLiteral,"戻り値はarrayリテラル");
    equals(this.parser.current(),tokenize.TOKEN_TYPE.P_SEMICOLON,"current is ;");

    this.parser.init("{\nis:'this',a:\"object\",its:true};");
    this.parser.next();
    var res = this.parser.parsePrimaryExpression();
    ok(res instanceof ast.Expression,"戻り値はExpressionノード");
    equals(this.parser.current(),tokenize.TOKEN_TYPE.P_SEMICOLON,"current is ;");

    this.parser.init("(++i*3);");
    this.parser.next();
    var res = this.parser.parsePrimaryExpression();
    ok(res instanceof ast.Expression,"(++i*3)戻り値はExpressionノード");
    equals(this.parser.current(),tokenize.TOKEN_TYPE.P_SEMICOLON,"current is ;");
  });
  test("parseObjectLiteral",function(){
    this.parser.init("{};");
    this.parser.next();
    var res = this.parser.parseObjectLiteral();
    ok(res instanceof ast.ObjectLiteral,"戻り値はObjectLiteralノード");
    equals(res.values.length,0,"value length is 0");
    equals(res.keys.length,0,"key length is 0");
    equals(this.parser.current(),tokenize.TOKEN_TYPE.P_SEMICOLON,"current is ;");

    this.parser.init("{a:1};");
    this.parser.next();
    var res = this.parser.parseObjectLiteral();
    ok(res instanceof ast.ObjectLiteral,"戻り値はObjectLiteralノード");
    equals(res.values.length,1,"value length is 1");
    ok(res.values[0] instanceof ast.Literal,"res.aの値はLiteralノード");
    equals(res.keys.length,1,"key length is 1");
    ok(res.keys[0] instanceof ast.Literal,"res.aのキーはLiteralノード");//キーはすべて文字列リテラルとして扱う
    ok(res.keys[0].value.isString(),"res.aのキーは文字列Literalノード");
    equals(res.keys[0].value.getString(),"a","res.aのキーは'a'");
    equals(this.parser.current(),tokenize.TOKEN_TYPE.P_SEMICOLON,"current is ;");

    this.parser.init("{'a':1};");
    this.parser.next();
    var res = this.parser.parseObjectLiteral();
    ok(res instanceof ast.ObjectLiteral,"戻り値はObjectLiteralノード");
    equals(res.values.length,1,"value length is 1");
    ok(res.values[0] instanceof ast.Literal,"res.aの値はLiteralノード");
    equals(res.keys.length,1,"key length is 1");
    ok(res.keys[0] instanceof ast.Literal,"res.aのキーはLiteralノード");//キーはすべて文字列リテラルとして扱う
    ok(res.keys[0].value.isString(),"res.aのキーは文字列Literalノード");
    equals(res.keys[0].value.getString(),"a","res.aのキーは'a'");
    equals(this.parser.current(),tokenize.TOKEN_TYPE.P_SEMICOLON,"current is ;");

    this.parser.init("{1:1};");
    this.parser.next();
    var res = this.parser.parseObjectLiteral();
    ok(res instanceof ast.ObjectLiteral,"戻り値はObjectLiteralノード");
    equals(res.values.length,1,"value length is 1");
    ok(res.values[0] instanceof ast.Literal,"res.aの値はLiteralノード");
    equals(res.keys.length,1,"key length is 1");
    ok(res.keys[0] instanceof ast.Literal,"res.aのキーはLiteralノード");//キーはすべて文字列リテラルとして扱う
    ok(res.keys[0].value.isString(),"res.aのキーは文字列Literalノード");
    equals(res.keys[0].value.getString(),"1","res.aのキーは'1'");
    equals(this.parser.current(),tokenize.TOKEN_TYPE.P_SEMICOLON,"current is ;");

    this.parser.init("{a:1,b:20};");
    this.parser.next();
    var res = this.parser.parseObjectLiteral();
    ok(res instanceof ast.ObjectLiteral,"戻り値はObjectLiteralノード");
    equals(res.values.length,2,"value length is 2");
    ok(res.values[0] instanceof ast.Literal,"res.aの値はLiteralノード");
    ok(res.values[0].value.isNumber(),"res.aの値は数値Literalノード");
    equals(res.values[0].value.getNumber(),1,"res.aの値は1");
    ok(res.values[1] instanceof ast.Literal,"res.bの値はLiteralノード");
    ok(res.values[1].value.isNumber(),"res.bの値は数値Literalノード");
    equals(res.values[1].value.getNumber(),20,"res.bの値は20");
    equals(res.keys.length,2,"key length is 2");
    ok(res.keys[0] instanceof ast.Literal,"res.aのキーはLiteralノード");//キーはすべて文字列リテラルとして扱う
    ok(res.keys[0].value.isString(),"res.aのキーは文字列Literalノード");
    equals(res.keys[0].value.getString(),"a","res.aのキーは'a'");
    ok(res.keys[1] instanceof ast.Literal,"res.aのキーはLiteralノード");//キーはすべて文字列リテラルとして扱う
    ok(res.keys[1].value.isString(),"res.aのキーは文字列Literalノード");
    equals(res.keys[1].value.getString(),"b","res.aのキーは'a'");
    equals(this.parser.current(),tokenize.TOKEN_TYPE.P_SEMICOLON,"current is ;");
  });
  test("parseArrayLiteral",function(){
    this.parser.init("[];");
    this.parser.next();
    var res = this.parser.parseArrayLiteral();
    ok(res instanceof ast.ArrayLiteral,"戻り値はArrayLiteralノード");
    equals(res.values.length,0,"length is 0");
    equals(this.parser.current(),tokenize.TOKEN_TYPE.P_SEMICOLON,"current is ;");

    this.parser.init("[1,2,3,true,false,undefined,'7'];");
    this.parser.next();
    var res = this.parser.parseArrayLiteral();
    ok(res instanceof ast.ArrayLiteral,"戻り値はArrayLiteralノード");
    equals(res.values.length,7,"length is 7");
    equals(this.parser.current(),tokenize.TOKEN_TYPE.P_SEMICOLON,"current is ;");

    this.parser.init("[,2,3,,'6'];");
    this.parser.next();
    var res = this.parser.parseArrayLiteral();
    ok(res instanceof ast.ArrayLiteral,"戻り値はArrayLiteralノード");
    equals(res.values.length,5,"length is 5");
    equals(this.parser.current(),tokenize.TOKEN_TYPE.P_SEMICOLON,"current is ;");

    this.parser.init("[,2,3,[,,],'6'];");
    this.parser.next();
    var res = this.parser.parseArrayLiteral();
    ok(res instanceof ast.ArrayLiteral,"戻り値はArrayLiteralノード");
    equals(res.values.length,5,"length is 5");
    ok(res.values[3] instanceof ast.ArrayLiteral,"戻り値[3]はArrayLiteralノード");
    equals(this.parser.current(),tokenize.TOKEN_TYPE.P_SEMICOLON,"current is ;");
  });
  test("parseRegularExpression",function(){
    this.parser.init("/^(.*)$/ig;");
    this.parser.next();
    var res = this.parser.parseRegularExpression(false);
    ok(res instanceof ast.RegExpLiteral,"戻り値はRegExpLiteralノード");
    ok(res.pattern instanceof jsobjects.JSString,"パターンはJSString");
    equals(res.pattern.getString(),"^(.*)$","パターンは'^(.*)$'");
    ok(res.flags instanceof jsobjects.JSString,"フラグはJSString");
    equals(res.flags.getString(),"ig","フラグは'ig'");
    equals(this.parser.current(),tokenize.TOKEN_TYPE.P_SEMICOLON,"current is ;");

    this.parser.init("/=(.*)$/ig;");
    this.parser.next();
    var res = this.parser.parseRegularExpression(true);
    ok(res instanceof ast.RegExpLiteral,"戻り値はRegExpLiteralノード");
    ok(res.pattern instanceof jsobjects.JSString,"パターンはJSString");
    equals(res.pattern.getString(),"=(.*)$","パターンは'=(.*)$'");
    ok(res.flags instanceof jsobjects.JSString,"フラグはJSString");
    equals(res.flags.getString(),"ig","フラグは'ig'");
    equals(this.parser.current(),tokenize.TOKEN_TYPE.P_SEMICOLON,"current is ;");
  });
  test("parseLeftHandSideExpression",function(){
    this.parser.init("1;");
    this.parser.next();
    var res = this.parser.parseLeftHandSideExpression();
    ok(res instanceof ast.Literal,"戻り値はLiteralノード");
    ok(res.value.isNumber(),"値は数値");
    equals(this.parser.current(),tokenize.TOKEN_TYPE.P_SEMICOLON,"current is ;");

    this.parser.init("i;");
    this.parser.next();
    var res = this.parser.parseLeftHandSideExpression();
    ok(res instanceof ast.Identifier,"戻り値はIdentifierノード");
    equals(res.name,"i","識別子名は'i'");
    equals(this.parser.current(),tokenize.TOKEN_TYPE.P_SEMICOLON,"current is ;");

    this.parser.init("base.identifier;");
    this.parser.next();
    var res = this.parser.parseLeftHandSideExpression();
    ok(res instanceof ast.PropertyAccessor,"base.propertyはPropertyAccessorノード");
    ok(res.base instanceof ast.Expression,"baseが式");
    ok(res.property instanceof ast.Literal,"propertyがリテラル");
    ok(res.property.value.isString(),"propertyが文字列");
    equals(res.property.value.getString(),"identifier","文字列は'identifier'");
    equals(this.parser.current(),tokenize.TOKEN_TYPE.P_SEMICOLON,"current is ;");

    this.parser.init("func();");
    this.parser.next();
    var res = this.parser.parseLeftHandSideExpression();
    ok(res instanceof ast.Call,"戻り値は関数呼び出しノード");
    ok(res.callee instanceof ast.Identifier,"calleeはIdentifierノード");
    equals(res.callee.name,"func","識別子名は'func'");
    ok(res.args instanceof ast.Arguments,"戻り値は引数ノード");
    equals(this.parser.current(),tokenize.TOKEN_TYPE.P_SEMICOLON,"current is ;");

    this.parser.init("new Func();");
    this.parser.next();
    var res = this.parser.parseLeftHandSideExpression();
    ok(res instanceof ast.CallNew,"戻り値はnew呼び出しノード");
    ok(res.callee instanceof ast.Identifier,"calleeはIdentifierノード");
    equals(res.callee.name,"Func","識別子名は'Func'");
    ok(res.args instanceof ast.Arguments,"戻り値は引数ノード");
    equals(this.parser.current(),tokenize.TOKEN_TYPE.P_SEMICOLON,"current is ;");

    this.parser.init("new new Func;");
    this.parser.next();
    var res = this.parser.parseLeftHandSideExpression();
    ok(res instanceof ast.CallNew,"戻り値はnew呼び出しノード");
    ok(res.callee instanceof ast.CallNew,"calleeはCallNewノード");
    ok(res.args instanceof ast.Arguments,"戻り値は引数ノード");
    ok(res.callee.callee instanceof ast.Identifier,"calleeはIdentifierノード");
    equals(res.callee.callee.name,"Func","識別子名は'Func'");
    ok(res.callee.args instanceof ast.Arguments,"戻り値は引数ノード");
    equals(this.parser.current(),tokenize.TOKEN_TYPE.P_SEMICOLON,"current is ;");

    this.parser.init("new new Func();");
    this.parser.next();
    var res = this.parser.parseLeftHandSideExpression();
    ok(res instanceof ast.CallNew,"戻り値はnew呼び出しノード");
    ok(res.callee instanceof ast.CallNew,"calleeはCallNewノード");
    ok(res.args instanceof ast.Arguments,"戻り値は引数ノード");
    ok(res.callee.callee instanceof ast.Identifier,"calleeはIdentifierノード");
    equals(res.callee.callee.name,"Func","識別子名は'Func'");
    ok(res.callee.args instanceof ast.Arguments,"戻り値は引数ノード");
    equals(this.parser.current(),tokenize.TOKEN_TYPE.P_SEMICOLON,"current is ;");

    this.parser.init("base[identifier];");
    this.parser.next();
    var res = this.parser.parseLeftHandSideExpression();
    ok(res instanceof ast.PropertyAccessor,"base.propertyはPropertyAccessorノード");
    ok(res.base instanceof ast.Expression,"baseが式");
    ok(res.property instanceof ast.Identifier,"propertyがIdentifierノード");
    equals(res.property.name,"identifier","識別子名は'identifier'");
    equals(this.parser.current(),tokenize.TOKEN_TYPE.P_SEMICOLON,"current is ;");
    
    this.parser.init("func().x;");
    this.parser.next();
    var res = this.parser.parseLeftHandSideExpression();
    ok(res instanceof ast.PropertyAccessor,"プロパティノード");
    ok(res.base instanceof ast.Call,"戻り値は関数呼び出しノード");
    ok(res.base.callee instanceof ast.Identifier,"calleeはIdentifierノード");
    equals(res.base.callee.name,"func","識別子名は'func'");
    ok(res.base.args instanceof ast.Arguments,"戻り値は引数ノード");
    ok(res.property instanceof ast.Literal,"プロパティはリテラル");
    equals(res.property.value.getString(),"x","'x'");
    equals(this.parser.current(),tokenize.TOKEN_TYPE.P_SEMICOLON,"current is ;");

    this.parser.init("func(1);");
    this.parser.next();
    var res = this.parser.parseLeftHandSideExpression();
    ok(res instanceof ast.Call,"戻り値は関数呼び出しノード");
    ok(res.callee instanceof ast.Identifier,"calleeはIdentifierノード");
    equals(res.callee.name,"func","識別子名は'func'");
    ok(res.args instanceof ast.Arguments,"戻り値は引数ノード");
    equals(res.args.args.length,1,"引数1個");
    equals(this.parser.current(),tokenize.TOKEN_TYPE.P_SEMICOLON,"current is ;");

    this.parser.init("func()\ng();");
    this.parser.next();
    var res = this.parser.parseLeftHandSideExpression();
    ok(res instanceof ast.Call,"戻り値は関数呼び出しノード");
    ok(res.callee instanceof ast.Identifier,"calleeはIdentifierノード");
    equals(res.callee.name,"func","識別子名は'func'");
    ok(res.args instanceof ast.Arguments,"戻り値は引数ノード");
    equals(this.parser.current(),tokenize.TOKEN_TYPE.IDENTIFIER,"current is Identifier");
    ok(this.parser._tokenizer._hasLineTerminator,"\n  before next token");

    this.parser.init("(function _(x){return x+1;})(1);");
    this.parser.next();
    var res = this.parser.parseLeftHandSideExpression();
    ok(res instanceof ast.Call,"戻り値は関数呼び出しノード");
    ok(res.callee instanceof ast.FunctionExpression,"calleeはFunctionExpressionノード");
    equals(res.callee.name,"_","識別子名は'_'");
    ok(res.args instanceof ast.Arguments,"戻り値は引数ノード");
    equals(res.args.args.length,1,"引数1個");
    equals(this.parser.current(),tokenize.TOKEN_TYPE.P_SEMICOLON,"current is ;");
  });
  test("parsePostfixExpression",function(){
    this.parser.init("1;");
    this.parser.next();
    var res = this.parser.parsePostfixExpression();
    ok(res instanceof ast.Literal,"戻り値はLiteralノード");
    ok(res.value.isNumber(),"値は数値");
    equals(this.parser.current(),tokenize.TOKEN_TYPE.P_SEMICOLON,"current is ;");

    this.parser.init("i++;");
    this.parser.next();
    var res = this.parser.parsePostfixExpression();
    ok(res instanceof ast.PostfixOperation,"戻り値はPostfixOperationノード");
    equals(res.operator,tokenize.TOKEN_TYPE.P_INC,"後置インクリメント");
    ok(res.expression instanceof ast.Identifier,"expressionはIdentifierノード");
    equals(res.expression.name,"i","識別子名は'i'");
    equals(this.parser.current(),tokenize.TOKEN_TYPE.P_SEMICOLON,"current is ;");

    this.parser.init("i\n++;");
    this.parser.next();
    var res = this.parser.parsePostfixExpression();
    ok(res instanceof ast.Identifier,"戻り値はIdentifierノード");
    equals(res.name,"i","識別子名は'i'");
    equals(this.parser.current(),tokenize.TOKEN_TYPE.P_INC,"current is ++");
    ok(this.parser._tokenizer._hasLineTerminator,"\n  before next token");
  });
  test("parseUnaryOperation",function(){
    this.parser.init("1;");
    this.parser.next();
    var res = this.parser.parseUnaryOperation();
    ok(res instanceof ast.Literal,"戻り値はLiteralノード");
    ok(res.value.isNumber(),"値は数値");
    equals(this.parser.current(),tokenize.TOKEN_TYPE.P_SEMICOLON,"current is ;");

    this.parser.init("++i;");
    this.parser.next();
    var res = this.parser.parseUnaryOperation();
    ok(res instanceof ast.UnaryOperation,"戻り値はUnaryOperationノード");
    equals(res.operator,tokenize.TOKEN_TYPE.P_INC,"前置インクリメント");
    ok(res.expression instanceof ast.Identifier,"expressionはIdentifierノード");
    equals(res.expression.name,"i","識別子名は'i'");
    equals(this.parser.current(),tokenize.TOKEN_TYPE.P_SEMICOLON,"current is ;");

    this.parser.init("!++i;");
    this.parser.next();
    var res = this.parser.parseUnaryOperation();
    ok(res instanceof ast.UnaryOperation,"戻り値はUnaryOperationノード");
    equals(res.operator,tokenize.TOKEN_TYPE.P_NOT,"前置インクリメント");
    ok(res.expression instanceof ast.UnaryOperation,"expressionはUnaryOperationノード");
    equals(res.expression.operator,tokenize.TOKEN_TYPE.P_INC,"expression.前置インクリメント");
    ok(res.expression.expression instanceof ast.Identifier,"expression.expressionはIdentifierノード");
    equals(res.expression.expression.name,"i","識別子名は'i'");
    equals(this.parser.current(),tokenize.TOKEN_TYPE.P_SEMICOLON,"current is ;");
  });
  test("parseBinaryOperation",function(){
    this.parser.init("1*3;");
    this.parser.next();
    var res = this.parser.parseBinaryOperation(true);
    ok(res instanceof ast.Literal,"戻り値はLiteralノード(定数同士の演算の最適化)");
    ok(res.value.isNumber(),"戻り値は数値");
    equals(res.value.getNumber(),3,"戻り値=3");
    equals(this.parser.current(),tokenize.TOKEN_TYPE.P_SEMICOLON,"current is ;");

    this.parser.init("1*3+4*5;");
    this.parser.next();
    res = this.parser.parseBinaryOperation(true);
    ok(res instanceof ast.Literal,"戻り値はLiteralノード(定数同士の演算の最適化)");
    ok(res.value.isNumber(),"戻り値は数値");
    equals(res.value.getNumber(),23,"戻り値=23");
    equals(this.parser.current(),tokenize.TOKEN_TYPE.P_SEMICOLON,"current is ;");

    this.parser.init("i++*3;");
    this.parser.next();
    var res = this.parser.parseBinaryOperation(true);
    ok(res instanceof ast.BinaryOperation,"戻り値はBinaryOperationノード");
    equals(res.operator,tokenize.TOKEN_TYPE.P_MUL,"オペレータは*");
    ok(res.left instanceof ast.PostfixOperation,"左はPostfixOperation");
    equals(res.left.operator,tokenize.TOKEN_TYPE.P_INC,"左のオペレータは++");
    ok(res.left.expression instanceof ast.Identifier,"左の式はIdentifier");
    equals(res.left.expression.name,"i","左の識別子名は'i'");
    ok(res.right instanceof ast.Literal,"右はリテラル");
    ok(res.right.value.isNumber(),"右は数値");
    equals(res.right.value.getNumber(),3,"右の数値は3");
    equals(this.parser.current(),tokenize.TOKEN_TYPE.P_SEMICOLON,"current is ;");

    this.parser.init("key in keys;");
    this.parser.next();
    res = this.parser.parseBinaryOperation(true);
    ok(res instanceof ast.BinaryOperation,"accept_in=trueなら二項演算");
    ok(res.left instanceof ast.Identifier,"左は識別子");
    equals(res.left.name,"key","識別子名は'key'");
    ok(res.right instanceof ast.Identifier,"右は識別子");
    equals(res.right.name,"keys","識別子名は'keys'");
    equals(this.parser.current(),tokenize.TOKEN_TYPE.P_SEMICOLON,"current is ;");

    this.parser.init("key in keys;");
    this.parser.next();
    res = this.parser.parseBinaryOperation(false);
    ok(res instanceof ast.Identifier,"accept_in=falseなら識別子ノード");
    equals(res.name,"key","識別子名は'key'");
    equals(this.parser.current(),tokenize.TOKEN_TYPE.K_IN,"current is in");
  });
  test("parseConditionalExpression",function(){
    this.parser.init("true;");
    this.parser.next();
    var res = this.parser.parseConditionalExpression(true);
    ok(res instanceof ast.Literal,"true is Literal");
    equals(this.parser.current(),tokenize.TOKEN_TYPE.P_SEMICOLON,"current is ;");

    this.parser.init("true ? false : true;");
    this.parser.next();
    var res = this.parser.parseConditionalExpression(true);
    ok(res instanceof ast.Conditional,"'true ? false : true' is Conditional");
    ok(res.condition instanceof ast.Literal,"condition is Literal");
    ok(res.then instanceof ast.Literal,"then is Literal");
    ok(res.else_ instanceof ast.Literal,"else_ is Literal");
    equals(this.parser.current(),tokenize.TOKEN_TYPE.P_SEMICOLON,"current is ;");

    this.parser.init("i>0 ? f() : o.x ;");
    this.parser.next();
    var res = this.parser.parseConditionalExpression(true);
    ok(res instanceof ast.Conditional,"'true ? false : true' is Conditional");
    ok(res.condition instanceof ast.BinaryOperation,"condition is BinaryOperation");
    equals(res.condition.operator,tokenize.TOKEN_TYPE.P_GT,"condition op is >");
    ok(res.then instanceof ast.Call,"then is Call");
    ok(res.else_ instanceof ast.PropertyAccessor,"else_ is PropertyAccessor");
    equals(this.parser.current(),tokenize.TOKEN_TYPE.P_SEMICOLON,"current is ;");

    this.parser.init("i in f ? f in g : i in g ;");
    this.parser.next();
    var res = this.parser.parseConditionalExpression(true);
    ok(res instanceof ast.Conditional,"'true ? false : true' is Conditional");
    ok(res.condition instanceof ast.BinaryOperation,"condition is BinaryOperation");
    ok(res.then instanceof ast.BinaryOperation,"then is BinaryOperation");
    ok(res.else_ instanceof ast.BinaryOperation,"else_ is BinaryOperation");
    equals(res.condition.operator,tokenize.TOKEN_TYPE.K_IN,"condition op is in");
    equals(res.then.operator,tokenize.TOKEN_TYPE.K_IN,"then op is in");
    equals(res.else_.operator,tokenize.TOKEN_TYPE.K_IN,"else_ op is in");
    equals(this.parser.current(),tokenize.TOKEN_TYPE.P_SEMICOLON,"current is ;");

    this.parser.init("i ? f in g : i in g ");
    this.parser.next();
    var res = this.parser.parseConditionalExpression(false);
    ok(res instanceof ast.Conditional,"'true ? false : true' is Conditional");
    ok(res.condition instanceof ast.Identifier,"condition is Identifier");
    ok(res.then instanceof ast.BinaryOperation,"then is BinaryOperation");
    ok(res.else_ instanceof ast.Identifier,"else_ is Identifier");
    equals(res.then.operator,tokenize.TOKEN_TYPE.K_IN,"then op is in");
    equals(this.parser.current(), tokenize.TOKEN_TYPE.K_IN,"this.current is in")
  });
  test("parseAssignmentExpression",function(){
    //console.log("\nx=20");
    this.parser.init("x = 20;");
    this.parser.next();
    var res = this.parser.parseAssignmentExpression(true);
    ok(res instanceof ast.Assignment,"x=20 is Assignment");
    equals(this.parser.current(), tokenize.TOKEN_TYPE.P_SEMICOLON,"this.current is ;")

    //console.log("\nx=f(x)");
    this.parser.init("x = f(x);");
    this.parser.next();
    var res = this.parser.parseAssignmentExpression(true);
    ok(res instanceof ast.Assignment,"x=f(x) is Assignment");
    ok(res.target instanceof ast.Identifier,"target is Identifier");
    ok(res.value instanceof ast.Call,"value is function call");
    equals(this.parser.current(), tokenize.TOKEN_TYPE.P_SEMICOLON,"this.current is ;")

    //console.log("\nx=f(x)*20");
    this.parser.init("x = f(x)*20;");
    this.parser.next();
    var res = this.parser.parseAssignmentExpression(true);
    ok(res instanceof ast.Assignment,"x=f(x)*20 is Assignment");
    ok(res.target instanceof ast.Identifier,"target is Identifier");
    ok(res.value instanceof ast.BinaryOperation,"value is BinaryOperation");
    equals(this.parser.current(), tokenize.TOKEN_TYPE.P_SEMICOLON,"this.current is ;")

    //console.log("\nx=f(x)*20+g[f(x)](10)");
    this.parser.init("x = f(x)*20+g[f(x)](10);");
    this.parser.next();
    var res = this.parser.parseAssignmentExpression(true);
    ok(res instanceof ast.Assignment,"x=f(x)*20+g[f(x)](10) is Assignment");
    ok(res.target instanceof ast.Identifier,"target is Identifier");
    ok(res.value instanceof ast.BinaryOperation,"value is BinaryOperation");
    equals(res.value.operator,tokenize.TOKEN_TYPE.P_ADD,"op is +");
    ok(res.value.left instanceof ast.BinaryOperation,"op left is BinaryOperation");
    ok(res.value.right instanceof ast.Call,"op right is call");
    equals(this.parser.current(), tokenize.TOKEN_TYPE.P_SEMICOLON,"this.current is ;")

    this.parser.init("x /= f(x);");
    this.parser.next();
    var res = this.parser.parseAssignmentExpression(true);
    ok(res instanceof ast.Assignment,"x=f(x) is Assignment");
    ok(res.target instanceof ast.Identifier,"target is Identifier");
    ok(res.value instanceof ast.Call,"value is function call");
    equals(res.operator,tokenize.TOKEN_TYPE.P_ASSIGN_DIV,"operator is /=");
    equals(this.parser.current(), tokenize.TOKEN_TYPE.P_SEMICOLON,"this.current is ;")
  });
  test("parseExpression",function(){
    this.parser.init("x = f(x)*20+g[f(x)](10);");
    this.parser.next();
    var res = this.parser.parseExpression(true);
    ok(res instanceof ast.Assignment,"this is AssignmentExpression");

    this.parser.init("x = f(x)*20+g[f(x)](10),y=f(x),z='test';");
    this.parser.next();
    var res = this.parser.parseExpression(true);
    ok(res instanceof ast.BinaryOperation,"this is BinaryOperation");
    equals(res.operator,tokenize.TOKEN_TYPE.P_COMMA,"sepalatet by ,");
    ok(res.left instanceof ast.Assignment,"first is AssignmentExpression");
    ok(res.right instanceof ast.BinaryOperation,"this is BinaryOperation");
    equals(res.right.operator,tokenize.TOKEN_TYPE.P_COMMA,"sepalatet by ,");
    ok(res.right.left instanceof ast.Assignment,"second is AssignmentExpression");
    ok(res.right.right instanceof ast.Assignment,"third is AssignmentExpression");
  });
  test("parseStatement",function(){
    //EmptyStatement
    this.parser.init(";");
    this.parser.next();
    var res = this.parser.parseStatement([]);
    ok(res instanceof ast.EmptyStatement, ";はEmptyStatement");
    equals(this.parser.current(),tokenize.TOKEN_TYPE.EOS,"ソースの最後まで到達してる");
    //BlockStatement
    this.parser.init("{\ncall();x=0+1;\n}");
    this.parser.next();
    var res = this.parser.parseStatement([]);
    ok(res instanceof ast.BlockStatement, "this is BlockStatement");
    equals(res.statements.length,2,"文は2個");
    equals(this.parser.current(),tokenize.TOKEN_TYPE.EOS,"ソースの最後まで到達してる");
    //ExpressionStatement
    this.parser.init("x;");
    this.parser.next();
    var res = this.parser.parseStatement([]);
    ok(res instanceof ast.ExpressionStatement, "x;はExpressionStatement");
    equals(this.parser.current(),tokenize.TOKEN_TYPE.EOS,"ソースの最後まで到達してる");
    ok(res.expression instanceof ast.Identifier,"x is Identifier");
    //LabelledStatement
    this.parser.init("label:{}");
    this.parser.next();
    var res = this.parser.parseStatement([]);
    ok(res instanceof ast.BlockStatement, "label:{} is LabelledStatement");
    equals(typeof res.label[0],"string", "label:{} has string label 'label'");
    equals(res.label[0],"label", "label:{} has label 'label'");
    equals(this.parser.current(),tokenize.TOKEN_TYPE.EOS,"ソースの最後まで到達してる");
    //IfStatement
    this.parser.init("if(x)then_do();else else_do();");
    this.parser.next();
    var res = this.parser.parseStatement([]);
    ok(res instanceof ast.IfStatement, "if(x)then_do();else else_do(); is IfStatement");
    ok(res.condition instanceof ast.Expression, "condition is Expression");
    ok(res.thenStatement instanceof ast.Statement, "then is Statement");
    ok(res.elseStatement instanceof ast.Statement, "else is Statement");
    equals(this.parser.current(),tokenize.TOKEN_TYPE.EOS,"ソースの最後まで到達してる");
    //ContinueStatement
    this.parser.init("continue;");
    this.parser.next();
    var res = this.parser.parseStatement([]);
    ok(res instanceof ast.ContinueStatement, "continue; is ContinueStatement");
    equals(res.label,null,"no label");
    equals(this.parser.current(),tokenize.TOKEN_TYPE.EOS,"ソースの最後まで到達してる");
    //BreakStatement
    this.parser.init("break;");
    this.parser.next();
    var res = this.parser.parseStatement([]);
    ok(res instanceof ast.BreakStatement, "break; is BreakStatement");
    equals(res.label,null,"no label");
    equals(this.parser.current(),tokenize.TOKEN_TYPE.EOS,"ソースの最後まで到達してる");
    //ReturnStatement
    this.parser.init("return;");
    this.parser.next();
    var res = this.parser.parseStatement([]);
    ok(res instanceof ast.ReturnStatement, "return; is ReturnStatement");
    equals(res.expression,null,"no expression");
    equals(this.parser.current(),tokenize.TOKEN_TYPE.EOS,"ソースの最後まで到達してる");
    //ThrowStatement
    this.parser.init("throw;");
    this.parser.next();
    var res = this.parser.parseStatement([]);
    ok(res instanceof ast.ThrowStatement, "throw; is ThrowStatement");
    equals(res.expression,null,"no expression");
    equals(this.parser.current(),tokenize.TOKEN_TYPE.EOS,"ソースの最後まで到達してる");
    //WithStatement
    this.parser.init("with(a)do_it();");
    this.parser.next();
    var res = this.parser.parseStatement([]);
    ok(res instanceof ast.WithStatement, "with(a)do_it(); is WithStatement");
    ok(res.expression instanceof ast.Expression,"a is expression");
    ok(res.statement instanceof ast.Statement,"do_it(); is statement");
    equals(this.parser.current(),tokenize.TOKEN_TYPE.EOS,"ソースの最後まで到達してる");
    //TryCatchStatement , TryFinallyStatement
    this.parser.init("try{}catch(e){}");
    this.parser.next();
    var res = this.parser.parseStatement([]);
    ok(res instanceof ast.TryCatchStatement, "try{}catch(e){} is TryCatchStatement");
    ok(res.tryBlock instanceof ast.BlockStatement, "try{} is BlockStatement");
    ok(res.catchBlock instanceof ast.BlockStatement, "catch(e){} is BlockStatement");
    ok(res.exception instanceof ast.Identifier, "e is Identifier");
    equals(this.parser.current(),tokenize.TOKEN_TYPE.EOS,"ソースの最後まで到達してる");

    this.parser.init("try{}finally{}");
    this.parser.next();
    var res = this.parser.parseStatement([]);
    ok(res instanceof ast.TryFinallyStatement, "try{}finally{} is TryFinallyStatement");
    ok(res.tryBlock instanceof ast.BlockStatement, "try{} is BlockStatement");
    ok(res.finallyBlock instanceof ast.BlockStatement, "finally{} is BlockStatement");
    equals(this.parser.current(),tokenize.TOKEN_TYPE.EOS,"ソースの最後まで到達してる");

    this.parser.init("try{}catch(e){}finally{}");
    this.parser.next();
    var res = this.parser.parseStatement([]);
    ok(res instanceof ast.TryFinallyStatement, "try{}catch(e){}finally{} is TryFinallyStatement");
    ok(res.tryBlock instanceof ast.BlockStatement, "try{} is BlockStatement");
    ok(res.finallyBlock instanceof ast.BlockStatement, "finally{} is BlockStatement");
    equals(res.tryBlock.statements.length,1, "try{ } length is 1");
    ok(res.tryBlock.statements[0] instanceof ast.TryCatchStatement, "try{ inner} is TryCatchStatement");
    ok(res.tryBlock.statements[0].tryBlock instanceof ast.BlockStatement, "try{ inner try{}} is BlockStatement");
    ok(res.tryBlock.statements[0].catchBlock instanceof ast.BlockStatement, "try{ inner catch(e){}} is BlockStatement");
    ok(res.tryBlock.statements[0].exception instanceof ast.Identifier, "try{ inner e} is Identifier");
    equals(this.parser.current(),tokenize.TOKEN_TYPE.EOS,"ソースの最後まで到達してる");
    //SwitchStatement
    this.parser.init("switch(i){}");
    this.parser.next();
    var res = this.parser.parseStatement([]);
    ok(res instanceof ast.SwitchStatement, "switch(i){} is SwitchStatement");
    ok(res.expression instanceof ast.Expression, "i is Expression");
    equals(res.caseClauses.length,0,"0 clause");
    equals(this.parser.current(),tokenize.TOKEN_TYPE.EOS,"EOSまで到達してる");
    //WhileStatement
    this.parser.init("while(i);");
    this.parser.next();
    var res = this.parser.parseStatement([]);
    ok(res instanceof ast.WhileStatement, "this is WhileStatement");
    ok(res.expression instanceof ast.Identifier, "i is Identifier");
    ok(res.statement instanceof ast.Statement, "; is Statement");
    equals(this.parser.current(),tokenize.TOKEN_TYPE.EOS,"ソースの最後まで到達してる");
    //DoWhileStatement
    this.parser.init("do{exec();}while(i);");
    this.parser.next();
    var res = this.parser.parseStatement([]);
    ok(res instanceof ast.DoWhileStatement, "this is DoWhileStatement");
    ok(res.expression instanceof ast.Identifier, "i is Identifier");
    ok(res.statement instanceof ast.Statement, "; is Statement");
    equals(this.parser.current(),tokenize.TOKEN_TYPE.EOS,"ソースの最後まで到達してる");
    //VariableStatement
    this.parser.init("var i=0;");
    this.parser.next();
    var res = this.parser.parseStatement([]);
    ok(res instanceof ast.ExpressionStatement, "this is ExpressionStatement");
    equals(this.parser.current(),tokenize.TOKEN_TYPE.EOS,"ソースの最後まで到達してる");
    equals(this.parser._variables[0][0],"i","variable 'i' is registered");
    //ForStatement
    this.parser.init("for(i=0;i<10;i++){i;}");
    this.parser.next();
    var res = this.parser.parseStatement([]);
    ok(res instanceof ast.ForStatement, "this is ForStatement");
    ok(res.init instanceof ast.Expression, "i=0 is Expression");
    ok(res.condition instanceof ast.Expression, "i<10 is Expression");
    ok(res.next instanceof ast.Expression, "i++ is Expression");
    ok(res.statement instanceof ast.Statement, "; is Statement");
    equals(this.parser.current(),tokenize.TOKEN_TYPE.EOS,"ソースの最後まで到達してる");
    //functionDeclares
    this.parser.init("function a(x,y){var v,w=0;while(true){var b=0;}}");
    this.parser.next();//consume function
    var res = this.parser.parseStatement([]);
    ok(res instanceof ast.EmptyStatement, "res is EmptyStatement");
    equals(this.parser._functions[0][0].func.name,null,"has name no name");
    equals(this.parser._functions[0][0].func.argumentNames.length,2,"has 2 args");
    equals(this.parser._functions[0][0].func.variableNames.length,3,"has 3 local variables");
    equals(this.parser._functions[0][0].func.statements.length,2,"has 2 statements");
    equals(this.parser._functions[0][0].func.functionDeclares.length,0,"has no functions");
    equals(this.parser.current(),tokenize.TOKEN_TYPE.EOS,"ソースの最後まで到達してる");
  });
  test("parseBlockStatement",function(){
    this.parser.init("{}");
    this.parser.next();
    var res = this.parser.parseBlockStatement([]);
    ok(res instanceof ast.BlockStatement, "this is BlockStatement");
    equals(res.statements.length,0,"文は0個");
    equals(this.parser.current(),tokenize.TOKEN_TYPE.EOS,"ソースの最後まで到達してる");

    this.parser.init("{\ncall();x=0+1;\n}");
    this.parser.next();
    var res = this.parser.parseBlockStatement([]);
    ok(res instanceof ast.BlockStatement, "this is BlockStatement");
    equals(res.statements.length,2,"文は2個");
    equals(this.parser.current(),tokenize.TOKEN_TYPE.EOS,"ソースの最後まで到達してる");

    this.parser.init("{1\n2}");//cf. ECMA262 3th 7.9.2
    this.parser.next();
    var res = this.parser.parseBlockStatement([]);
    ok(res instanceof ast.BlockStatement, "this is BlockStatement");
    equals(res.statements.length,2,"文は2個");
    equals(this.parser.current(),tokenize.TOKEN_TYPE.EOS,"ソースの最後まで到達してる");

    this.parser.init("{\ncall();x=0+1;\n}");
    this.parser.next();
    var res = this.parser.parseBlockStatement(["label1","label2"]);
    ok(res instanceof ast.BlockStatement, "this is BlockStatement");
    equals(res.statements.length,2,"文は2個");
    equals(res.label[0],"label1", "has label 'label1'");
    equals(res.label[1],"label2", "has label 'label2'");
    equals(this.parser.current(),tokenize.TOKEN_TYPE.EOS,"ソースの最後まで到達してる");
  });
  test("parseVariableDeclarationList",function(){
    this.parser.init("var i=0;");
    this.parser.next();
    var res = this.parser.parseVariableDeclarationList(true);
    ok(res instanceof ast.Expression, "this is Expression");
    equals(this.parser.current(),tokenize.TOKEN_TYPE.P_SEMICOLON,";まで到達してる");
    equals(this.parser._variables[0][0],"i","variable 'i' is registered");

    this.parser.init("var i=x in {x:1};");
    this.parser.next();
    var res = this.parser.parseVariableDeclarationList(true);
    ok(res instanceof ast.Expression, "this is Expression");
    equals(this.parser.current(),tokenize.TOKEN_TYPE.P_SEMICOLON,";まで到達してる");
    equals(this.parser._variables[0][0],"i","variable 'i' is registered");

    this.parser.init("var i=x in {x:1};");
    this.parser.next();
    var res = this.parser.parseVariableDeclarationList(false);
    ok(res instanceof ast.Expression, "this is Expression");
    equals(this.parser.current(),tokenize.TOKEN_TYPE.K_IN,"inまで到達してる");
    equals(this.parser._variables[0][0],"i","variable 'i' is registered");

    this.parser.init("var i=0,j=x in {x:1};");
    this.parser.next();
    var res = this.parser.parseVariableDeclarationList(true);
    ok(res instanceof ast.Expression, "this is Expression");
    equals(this.parser.current(),tokenize.TOKEN_TYPE.P_SEMICOLON,";まで到達してる");
    equals(this.parser._variables[0][0],"i","variable 'i' is registered");
    equals(this.parser._variables[0][1],"j","variable 'j' is registered");

    this.parser.init("var i=0,j=x in {x:1};");
    this.parser.next();
    var res = this.parser.parseVariableDeclarationList(false);
    ok(res instanceof ast.Expression, "this is Expression");
    equals(this.parser.current(),tokenize.TOKEN_TYPE.K_IN,"inまで到達してる");
    equals(this.parser._variables[0][0],"i","variable 'i' is registered");
    equals(this.parser._variables[0][1],"j","variable 'j' is registered");
  });
  test("parseVariableStatement",function(){
    this.parser.init("var i=0;");
    this.parser.next();
    var res = this.parser.parseVariableStatement();
    ok(res instanceof ast.ExpressionStatement, "this is ExpressionStatement");
    equals(this.parser.current(),tokenize.TOKEN_TYPE.EOS,"ソースの最後まで到達してる");
    equals(this.parser._variables[0][0],"i","variable 'i' is registered");

    this.parser.init("var i=0");
    this.parser.next();
    var res = this.parser.parseVariableStatement();
    ok(res instanceof ast.ExpressionStatement, "this is ExpressionStatement");
    equals(this.parser.current(),tokenize.TOKEN_TYPE.EOS,"ソースの最後まで到達してる");
    equals(this.parser._variables[0][0],"i","variable 'i' is registered");

    this.parser.init("var i=0\n");
    this.parser.next();
    var res = this.parser.parseVariableStatement();
    ok(res instanceof ast.ExpressionStatement, "this is ExpressionStatement");
    equals(this.parser.current(),tokenize.TOKEN_TYPE.EOS,"ソースの最後まで到達してる");
    equals(this.parser._variables[0][0],"i","variable 'i' is registered");

    this.parser.init("var i=x in {x:1};");
    this.parser.next();
    var res = this.parser.parseVariableStatement();
    ok(res instanceof ast.ExpressionStatement, "this is ExpressionStatement");
    equals(this.parser.current(),tokenize.TOKEN_TYPE.EOS,"ソースの最後まで到達してる");
    equals(this.parser._variables[0][0],"i","variable 'i' is registered");

    this.parser.init("var i=0,j=x in {x:1};");
    this.parser.next();
    var res = this.parser.parseVariableStatement();
    ok(res instanceof ast.ExpressionStatement, "this is ExpressionStatement");
    equals(this.parser.current(),tokenize.TOKEN_TYPE.EOS,"ソースの最後まで到達してる");
    equals(this.parser._variables[0][0],"i","variable 'i' is registered");
    equals(this.parser._variables[0][1],"j","variable 'j' is registered");
  });
  test("parseEmptyStatement",function(){
    this.parser.init(";");
    this.parser.next();
    var res = this.parser.parseEmptyStatement();
    ok(res instanceof ast.EmptyStatement, ";はEmptyStatement");
    equals(this.parser.current(),tokenize.TOKEN_TYPE.EOS,"ソースの最後まで到達してる");
  });
  test("parseLabelledOrExpressionStatement",function(){
    this.parser.init("{x:0};");
    this.parser.next();
    try{
      var res = this.parser.parseLabelledOrExpressionStatement([]);
      ok(false,"unreach here");
    }catch(e){
      ok(e instanceof parse.UnexpectedTokenError, "{で始まるのは式文じゃぬぇ");
    }

    this.parser.init("function(){x:0};");
    this.parser.next();
    try{
      var res = this.parser.parseLabelledOrExpressionStatement([]);
      ok(false,"unreach here");
    }catch(e){
      ok(e instanceof parse.UnexpectedTokenError, "functionで始まるのは式文じゃぬぇ");
    }

    this.parser.init("x;");
    this.parser.next();
    var res = this.parser.parseLabelledOrExpressionStatement([]);
    ok(res instanceof ast.ExpressionStatement, "x;はExpressionStatement");
    equals(this.parser.current(),tokenize.TOKEN_TYPE.EOS,"ソースの最後まで到達してる");
    ok(res.expression instanceof ast.Identifier,"x is Identifier");

    this.parser.init("x++;");
    this.parser.next();
    var res = this.parser.parseLabelledOrExpressionStatement([]);
    ok(res instanceof ast.ExpressionStatement, "x++;はExpressionStatement");
    ok(res.expression instanceof ast.PostfixOperation,"x is PostfixOperation");

    this.parser.init("x++;");
    this.parser.next();
    var res = this.parser.parseLabelledOrExpressionStatement([]);
    ok(res instanceof ast.ExpressionStatement, "x++;はExpressionStatement");
    ok(res.expression instanceof ast.PostfixOperation,"x is PostfixOperation");

    this.parser.init("x\nx++;");
    this.parser.next();
    res = this.parser.parseLabelledOrExpressionStatement([]);
    ok(res instanceof ast.ExpressionStatement, "xはExpressionStatement");
    equals(this.parser.current(),tokenize.TOKEN_TYPE.IDENTIFIER,"x++のxを指してる");
    ok(res.expression instanceof ast.Identifier,"x is Identifier");
    res = this.parser.parseLabelledOrExpressionStatement([]);
    ok(res instanceof ast.ExpressionStatement, "x++;はExpressionStatement");
    equals(this.parser.current(),tokenize.TOKEN_TYPE.EOS,"ソースの最後まで到達してる");
    ok(res.expression instanceof ast.PostfixOperation,"x is PostfixOperation");

    this.parser.init("++\nx;");
    this.parser.next();
    var res = this.parser.parseLabelledOrExpressionStatement([]);
    ok(res instanceof ast.ExpressionStatement, "++\nx;はExpressionStatement");
    equals(this.parser.current(),tokenize.TOKEN_TYPE.EOS,"ソースの最後まで到達してる");
    ok(res.expression instanceof ast.UnaryOperation,"x is UnaryOperation");

    this.parser.init("label:{}");
    this.parser.next();
    var res = this.parser.parseLabelledOrExpressionStatement([]);
    ok(res instanceof ast.BlockStatement, "label:{} is BlockStatement");
    equals(typeof res.label[0],"string", "label:; has string label 'label'");
    equals(res.label.length,1, "label:{} has 1 label");
    equals(res.label[0],"label", "label:{} has label 'label'");
    equals(this.parser.current(),tokenize.TOKEN_TYPE.EOS,"ソースの最後まで到達してる");

    this.parser.init("label2:{}");
    this.parser.next();
    var res = this.parser.parseLabelledOrExpressionStatement(["label1"]);
    ok(res instanceof ast.BlockStatement, "label:{} is BlockStatement");
    equals(res.label.length,2, "label:{} has 2 label");
    equals(res.label[0],"label1", "label:{} has label 'labe1'");
    equals(res.label[1],"label2", "label:{} has label 'labe2'");
    equals(this.parser.current(),tokenize.TOKEN_TYPE.EOS,"ソースの最後まで到達してる");

  });
  test("parseIfStatement",function(){
    this.parser.init("if(x)then_do();else else_do();");
    this.parser.next();
    var res = this.parser.parseIfStatement([]);
    ok(res instanceof ast.IfStatement, "if(x)then_do();else else_do(); is IfStatement");
    ok(res.condition instanceof ast.Expression, "condition is Expression");
    ok(res.thenStatement instanceof ast.Statement, "then is Statement");
    ok(res.elseStatement instanceof ast.Statement, "else is Statement");
    equals(this.parser.current(),tokenize.TOKEN_TYPE.EOS,"ソースの最後まで到達してる");

    this.parser.init("if(x)then_do();");
    this.parser.next();
    var res = this.parser.parseIfStatement([]);
    ok(res instanceof ast.IfStatement, "if(x)then_do(); is IfStatement");
    ok(res.thenStatement instanceof ast.Statement, "then is Statement");
    equals(res.elseStatement, null, "else is null");
    equals(this.parser.current(),tokenize.TOKEN_TYPE.EOS,"ソースの最後まで到達してる");

    this.parser.init("if(x)then_do()\n");
    this.parser.next();
    var res = this.parser.parseIfStatement([]);
    ok(res instanceof ast.IfStatement, "if(x)then_do(); is IfStatement");
    ok(res.thenStatement instanceof ast.Statement, "then is Statement");
    equals(res.elseStatement, null, "else is null");
    equals(this.parser.current(),tokenize.TOKEN_TYPE.EOS,"ソースの最後まで到達してる");

    this.parser.init("if(x)then_do()");
    this.parser.next();
    var res = this.parser.parseIfStatement([]);
    ok(res instanceof ast.IfStatement, "if(x)then_do(); is IfStatement");
    ok(res.thenStatement instanceof ast.Statement, "then is Statement");
    equals(res.elseStatement, null, "else is null");
    equals(this.parser.current(),tokenize.TOKEN_TYPE.EOS,"ソースの最後まで到達してる");

    this.parser.init("if(x){then_do()\nthen_do2()}else{else_do()}");
    this.parser.next();
    var res = this.parser.parseIfStatement([]);
    ok(res instanceof ast.IfStatement, "if(x)then_do();else else_do(); is IfStatement");
    ok(res.condition instanceof ast.Expression, "condition is Expression");
    ok(res.thenStatement instanceof ast.BlockStatement, "then is BlockStatement");
    ok(res.elseStatement instanceof ast.BlockStatement, "else is BlockStatement");
    equals(this.parser.current(),tokenize.TOKEN_TYPE.EOS,"ソースの最後まで到達してる");

    this.parser.init("if(x){then_do();}else if(y){else_do();}else{else2();}");
    this.parser.next();
    var res = this.parser.parseIfStatement([]);
    ok(res instanceof ast.IfStatement, "if(x)then_do();else else_do(); is IfStatement");
    ok(res.condition instanceof ast.Expression, "condition is Expression");
    ok(res.thenStatement instanceof ast.BlockStatement, "then is BlockStatement");
    ok(res.elseStatement instanceof ast.IfStatement, "else is IfStatement");
    ok(res.elseStatement.condition instanceof ast.Expression, "condition is Expression");
    ok(res.elseStatement.thenStatement instanceof ast.BlockStatement, "then is BlockStatement");
    ok(res.elseStatement.elseStatement instanceof ast.BlockStatement, "else is BlockStatement");
    equals(this.parser.current(),tokenize.TOKEN_TYPE.EOS,"ソースの最後まで到達してる");

    this.parser.init("if(x)then_do();");
    this.parser.next();
    var res = this.parser.parseIfStatement(["label1"]);
    ok(res instanceof ast.IfStatement, "if(x)then_do(); is IfStatement");
    ok(res.thenStatement instanceof ast.Statement, "then is Statement");
    equals(res.elseStatement, null, "else is null");
    equals(this.parser.current(),tokenize.TOKEN_TYPE.EOS,"ソースの最後まで到達してる");
    equals(res.label.length,1, "label:{} has 1 label");
    equals(res.label[0],"label1", "label:{} has label 'labe1'");

  });
  test("parseDoWhileStatement",function(){
    this.parser.init("do{exec();}while(i);");
    this.parser.next();
    var res = this.parser.parseDoWhileStatement([]);
    ok(res instanceof ast.DoWhileStatement, "this is DoWhileStatement");
    ok(res.expression instanceof ast.Identifier, "i is Identifier");
    ok(res.statement instanceof ast.Statement, "; is Statement");
    equals(this.parser.current(),tokenize.TOKEN_TYPE.EOS,"ソースの最後まで到達してる");

    this.parser.init("do{exec();}while(i);");
    this.parser.next();
    var res = this.parser.parseDoWhileStatement(["label1"]);
    ok(res instanceof ast.DoWhileStatement, "this is DoWhileStatement");
    ok(res.expression instanceof ast.Identifier, "i is Identifier");
    ok(res.statement instanceof ast.Statement, "; is Statement");
    equals(this.parser.current(),tokenize.TOKEN_TYPE.EOS,"ソースの最後まで到達してる");
    equals(res.label.length,1, "label:{} has 1 label");
    equals(res.label[0],"label1", "label:{} has label 'labe1'");
  });
  test("parseWhileStatement",function(){
    this.parser.init("while(i);");
    this.parser.next();
    var res = this.parser.parseWhileStatement([]);
    ok(res instanceof ast.WhileStatement, "this is WhileStatement");
    ok(res.expression instanceof ast.Identifier, "i is Identifier");
    ok(res.statement instanceof ast.Statement, "; is Statement");
    equals(this.parser.current(),tokenize.TOKEN_TYPE.EOS,"ソースの最後まで到達してる");

    this.parser.init("while(i);");
    this.parser.next();
    var res = this.parser.parseWhileStatement(["label1"]);
    ok(res instanceof ast.WhileStatement, "this is WhileStatement");
    ok(res.expression instanceof ast.Identifier, "i is Identifier");
    ok(res.statement instanceof ast.Statement, "; is Statement");
    equals(this.parser.current(),tokenize.TOKEN_TYPE.EOS,"ソースの最後まで到達してる");
    equals(res.label.length,1, "label:{} has 1 label");
    equals(res.label[0],"label1", "label:{} has label 'labe1'");
  });
  test("parseForStatement",function(){
    this.parser.init("for(i=0;i<10;i++){i;}");
    this.parser.next();
    var res = this.parser.parseForStatement([]);
    ok(res instanceof ast.ForStatement, "this is ForStatement");
    ok(res.init instanceof ast.Expression, "i=0 is Expression");
    ok(res.condition instanceof ast.Expression, "i<10 is Expression");
    ok(res.next instanceof ast.Expression, "i++ is Expression");
    ok(res.statement instanceof ast.Statement, "; is Statement");
    equals(this.parser.current(),tokenize.TOKEN_TYPE.EOS,"ソースの最後まで到達してる");

    this.parser.init("for(var i=0;i<10;i++){i;}");
    this.parser.next();
    var res = this.parser.parseForStatement([]);
    ok(res instanceof ast.ForStatement, "this is ForStatement");
    ok(res.init instanceof ast.Expression, "i=0 is Expression");
    ok(res.condition instanceof ast.Expression, "i<10 is Expression");
    ok(res.next instanceof ast.Expression, "i++ is Expression");
    ok(res.statement instanceof ast.Statement, "; is Statement");
    equals(this.parser.current(),tokenize.TOKEN_TYPE.EOS,"ソースの最後まで到達してる");
    equals(this.parser._variables[0][0],"i","variable 'i' is registered");

    this.parser.init("for(i in x){x[i];}");
    this.parser.next();
    var res = this.parser.parseForStatement([]);
    ok(res instanceof ast.ForInStatement, "this is ForInStatement");
    ok(res.key instanceof ast.Expression, "i is Expression");
    ok(res.enumerable instanceof ast.Expression, "x is Expression");
    ok(res.statement instanceof ast.Statement, "{x[i];} is Statement");
    equals(this.parser.current(),tokenize.TOKEN_TYPE.EOS,"ソースの最後まで到達してる");

    this.parser.init("for(var i=0 in x){x[i];}");
    this.parser.next();
    var res = this.parser.parseForStatement([]);
    ok(res instanceof ast.BlockStatement, "this is BlockStatement");
    ok(res.statements[0] instanceof ast.ExpressionStatement, "this[0] is ExpressionStatement");
    ok(res.statements[0].expression instanceof ast.Assignment, "this[0] is AssignmentExpression");
    ok(res.statements[1] instanceof ast.ForInStatement, "this[1] is ForInStatement");
    ok(res.statements[1].key instanceof ast.Expression, "i=0 is Expression");
    ok(res.statements[1].enumerable instanceof ast.Expression, "x is Expression");
    ok(res.statements[1].statement instanceof ast.Statement, "{x[i];} is Statement");
    equals(this.parser.current(),tokenize.TOKEN_TYPE.EOS,"ソースの最後まで到達してる");
    equals(this.parser._variables[0][0],"i","variable 'i' is registered");

    this.parser.init("for(i=0;i<10;i++){i;}");
    this.parser.next();
    var res = this.parser.parseForStatement(["label1"]);
    ok(res instanceof ast.ForStatement, "this is ForStatement");
    ok(res.init instanceof ast.Expression, "i=0 is Expression");
    ok(res.condition instanceof ast.Expression, "i<10 is Expression");
    ok(res.next instanceof ast.Expression, "i++ is Expression");
    ok(res.statement instanceof ast.Statement, "; is Statement");
    equals(this.parser.current(),tokenize.TOKEN_TYPE.EOS,"ソースの最後まで到達してる");
    equals(res.label.length,1, "label:{} has 1 label");
    equals(res.label[0],"label1", "label:{} has label 'labe1'");

    this.parser.init("for(i in x){x[i];}");
    this.parser.next();
    var res = this.parser.parseForStatement(["label1"]);
    ok(res instanceof ast.ForInStatement, "this is ForInStatement");
    ok(res.key instanceof ast.Expression, "i is Expression");
    ok(res.enumerable instanceof ast.Expression, "x is Expression");
    ok(res.statement instanceof ast.Statement, "{x[i];} is Statement");
    equals(this.parser.current(),tokenize.TOKEN_TYPE.EOS,"ソースの最後まで到達してる");
    equals(res.label.length,1, "label:{} has 1 label");
    equals(res.label[0],"label1", "label:{} has label 'labe1'");
  });
  test("parseContinueStatement",function(){
    this.parser.init("continue;");
    this.parser.next();
    var res = this.parser.parseContinueStatement();
    ok(res instanceof ast.ContinueStatement, "continue; is ContinueStatement");
    equals(res.label,null,"no label");
    equals(this.parser.current(),tokenize.TOKEN_TYPE.EOS,"ソースの最後まで到達してる");

    this.parser.init("continue label;");
    this.parser.next();
    var res = this.parser.parseContinueStatement();
    ok(res instanceof ast.ContinueStatement, "continue; is ContinueStatement");
    equals(res.label,"label","has 'label'");
    equals(this.parser.current(),tokenize.TOKEN_TYPE.EOS,"ソースの最後まで到達してる");

    this.parser.init("continue \n");
    this.parser.next();
    var res = this.parser.parseContinueStatement();
    ok(res instanceof ast.ContinueStatement, "continue; is ContinueStatement");
    equals(res.label,null,"no label.改行によってセミコロン挿入される。");
    equals(this.parser.current(),tokenize.TOKEN_TYPE.EOS,"ソースの最後まで到達してる");

    this.parser.init("continue label\n");
    this.parser.next();
    var res = this.parser.parseContinueStatement();
    ok(res instanceof ast.ContinueStatement, "continue; is ContinueStatement");
    equals(res.label,"label","has 'label'");
    equals(this.parser.current(),tokenize.TOKEN_TYPE.EOS,"ソースの最後まで到達してる");

    this.parser.init("continue \nlabel;");
    this.parser.next();
    var res = this.parser.parseContinueStatement();
    ok(res instanceof ast.ContinueStatement, "continue; is ContinueStatement");
    equals(res.label,null,"no label.改行によってセミコロン挿入される。");
    equals(this.parser.current(),tokenize.TOKEN_TYPE.IDENTIFIER,"改行の次の識別子まで到達してる");
  });
  test("parseBreakStatement",function(){
    this.parser.init("break;");
    this.parser.next();
    var res = this.parser.parseBreakStatement();
    ok(res instanceof ast.BreakStatement, "break; is BreakStatement");
    equals(res.label,null,"no label");
    equals(this.parser.current(),tokenize.TOKEN_TYPE.EOS,"ソースの最後まで到達してる");

    this.parser.init("break label;");
    this.parser.next();
    var res = this.parser.parseBreakStatement();
    ok(res instanceof ast.BreakStatement, "break; is BreakStatement");
    equals(res.label,"label","has 'label'");
    equals(this.parser.current(),tokenize.TOKEN_TYPE.EOS,"ソースの最後まで到達してる");

    this.parser.init("break \n");
    this.parser.next();
    var res = this.parser.parseBreakStatement();
    ok(res instanceof ast.BreakStatement, "break; is BreakStatement");
    equals(res.label,null,"no label.改行によってセミコロン挿入される。");
    equals(this.parser.current(),tokenize.TOKEN_TYPE.EOS,"ソースの最後まで到達してる");

    this.parser.init("break label\n");
    this.parser.next();
    var res = this.parser.parseBreakStatement();
    ok(res instanceof ast.BreakStatement, "break; is BreakStatement");
    equals(res.label,"label","has 'label'");
    equals(this.parser.current(),tokenize.TOKEN_TYPE.EOS,"ソースの最後まで到達してる");

    this.parser.init("break \nlabel;");
    this.parser.next();
    var res = this.parser.parseBreakStatement();
    ok(res instanceof ast.BreakStatement, "break; is BreakStatement");
    equals(res.label,null,"no label.改行によってセミコロン挿入される。");
    equals(this.parser.current(),tokenize.TOKEN_TYPE.IDENTIFIER,"改行の次の識別子まで到達してる");
  });
  test("parseReturnStatement",function(){
    this.parser.init("return;");
    this.parser.next();
    var res = this.parser.parseReturnStatement();
    ok(res instanceof ast.ReturnStatement, "return; is ReturnStatement");
    equals(res.expression,null,"no expression");
    equals(this.parser.current(),tokenize.TOKEN_TYPE.EOS,"ソースの最後まで到達してる");

    this.parser.init("return 10*x;");
    this.parser.next();
    var res = this.parser.parseReturnStatement();
    ok(res instanceof ast.ReturnStatement, "return; is ReturnStatement");
    ok(res.expression instanceof ast.BinaryOperation, "expression is BinaryOperation");
    equals(this.parser.current(),tokenize.TOKEN_TYPE.EOS,"ソースの最後まで到達してる");

    this.parser.init("return \n");
    this.parser.next();
    var res = this.parser.parseReturnStatement();
    ok(res instanceof ast.ReturnStatement, "return; is ReturnStatement");
    equals(res.expression,null,"no expression");
    equals(this.parser.current(),tokenize.TOKEN_TYPE.EOS,"ソースの最後まで到達してる");

    this.parser.init("return 10*x\n");
    this.parser.next();
    var res = this.parser.parseReturnStatement();
    ok(res instanceof ast.ReturnStatement, "return; is ReturnStatement");
    ok(res.expression instanceof ast.BinaryOperation, "expression is BinaryOperation");
    equals(this.parser.current(),tokenize.TOKEN_TYPE.EOS,"ソースの最後まで到達してる");

    this.parser.init("return \n10*x;");
    this.parser.next();
    var res = this.parser.parseReturnStatement();
    ok(res instanceof ast.ReturnStatement, "return; is ReturnStatement");
    equals(res.expression,null,"no expression");
    equals(this.parser.current(),tokenize.TOKEN_TYPE.NUMERIC_LITERAL,"改行の次の10まで到達してる");

    this.parser.init("return 10\n*x\n");
    this.parser.next();
    var res = this.parser.parseReturnStatement();
    ok(res instanceof ast.ReturnStatement, "return; is ReturnStatement");
    ok(res.expression instanceof ast.BinaryOperation, "expression is BinaryOperation");
    equals(this.parser.current(),tokenize.TOKEN_TYPE.EOS,"ソースの最後まで到達してる");
  });
  test("parseWithStatement",function(){
    this.parser.init("with(a)do_it();");
    this.parser.next();
    var res = this.parser.parseWithStatement([]);
    ok(res instanceof ast.WithStatement, "with(a)do_it(); is WithStatement");
    ok(res.expression instanceof ast.Expression,"a is expression");
    ok(res.statement instanceof ast.Statement,"do_it(); is statement");
    equals(this.parser.current(),tokenize.TOKEN_TYPE.EOS,"ソースの最後まで到達してる");

    this.parser.init("with({a:10,b:'test'}){do_it(b)\nonemoreset(a)}");
    this.parser.next();
    var res = this.parser.parseWithStatement([]);
    ok(res instanceof ast.WithStatement, "this is WithStatement");
    ok(res.expression instanceof ast.ObjectLiteral,"{a:10,b:'test'} is ObjectLiteral");
    ok(res.statement instanceof ast.BlockStatement,"{do_it(b)\nonemoreset(a)} is BlockStatement");
    equals(this.parser.current(),tokenize.TOKEN_TYPE.EOS,"ソースの最後まで到達してる");

    this.parser.init("with(a)do_it();");
    this.parser.next();
    var res = this.parser.parseWithStatement(["label1"]);
    ok(res instanceof ast.WithStatement, "with(a)do_it(); is WithStatement");
    ok(res.expression instanceof ast.Expression,"a is expression");
    ok(res.statement instanceof ast.Statement,"do_it(); is statement");
    equals(this.parser.current(),tokenize.TOKEN_TYPE.EOS,"ソースの最後まで到達してる");
    equals(res.label.length,1, "label:{} has 1 label");
    equals(res.label[0],"label1", "label:{} has label 'labe1'");
  });
  test("parseCaseClause",function(){
    this.parser.init("case 1:case 2:");
    this.parser.next();
    var res = this.parser.parseCaseClause(false);
    ok(res instanceof ast.CaseClause, "case 1: is CaseClause");
    ok(res.label instanceof ast.Literal, "label is Literal");
    ok(res.label.value.isNumber(), "label is Number");
    equals(res.label.value.getNumber(),1,"label is 1");
    equals(this.parser.current(),tokenize.TOKEN_TYPE.K_CASE,"次のcaseまで到達してる");

    this.parser.init("default:case 2:");
    this.parser.next();
    var res = this.parser.parseCaseClause(false);
    ok(res instanceof ast.CaseClause, "default: is CaseClause");
    equals(res.label,null,"label is null");
    equals(this.parser.current(),tokenize.TOKEN_TYPE.K_CASE,"次のcaseまで到達してる");

    this.parser.init("default:}");
    this.parser.next();
    var res = this.parser.parseCaseClause(false);
    ok(res instanceof ast.CaseClause, "default: is CaseClause");
    equals(res.label,null,"label is null");
    equals(this.parser.current(),tokenize.TOKEN_TYPE.P_BRACE_R,"}まで到達してる");

    this.parser.init("case 1:default:");
    this.parser.next();
    var res = this.parser.parseCaseClause(false);
    ok(res instanceof ast.CaseClause, "case 1: is CaseClause");
    ok(res.label instanceof ast.Literal, "label is Literal");
    ok(res.label.value.isNumber(), "label is Number");
    equals(res.label.value.getNumber(),1,"label is 1");
    equals(this.parser.current(),tokenize.TOKEN_TYPE.K_DEFAULT,"defaultまで到達してる");

    this.parser.init("default:case 2:");
    this.parser.next();
    try{
      var res = this.parser.parseCaseClause(true);
      ok(false,"cant reach here");
    }catch(e){
      ok(e instanceof parse.ParseError,"default has already seen");
    }
  });
  test("parseSwitchStatement",function(){
    this.parser.init("switch(i){}");
    this.parser.next();
    var res = this.parser.parseSwitchStatement([]);
    ok(res instanceof ast.SwitchStatement, "switch(i){} is SwitchStatement");
    ok(res.expression instanceof ast.Expression, "i is Expression");
    equals(res.caseClauses.length,0,"0 clause");
    equals(this.parser.current(),tokenize.TOKEN_TYPE.EOS,"EOSまで到達してる");

    this.parser.init("switch(i){case 1:}");
    this.parser.next();
    var res = this.parser.parseSwitchStatement([]);
    ok(res instanceof ast.SwitchStatement, "switch(i){case 1:} is SwitchStatement");
    ok(res.expression instanceof ast.Expression, "i is Expression");
    equals(res.caseClauses.length,1,"1 clause");
    equals(this.parser.current(),tokenize.TOKEN_TYPE.EOS,"EOSまで到達してる");

    this.parser.init("switch(i*2){case 1:do1();do2();break;case 2:do_3();default:do_df();}");
    this.parser.next();
    var res = this.parser.parseSwitchStatement([]);
    ok(res instanceof ast.SwitchStatement, "this is SwitchStatement");
    ok(res.expression instanceof ast.BinaryOperation, "i*2 is Expression(BinaryOperation)");
    equals(res.caseClauses.length,3,"3 clause");
    equals(this.parser.current(),tokenize.TOKEN_TYPE.EOS,"EOSまで到達してる");

    this.parser.init("switch(i){default:;default:;}");
    this.parser.next();
    try{
      var res = this.parser.parseSwitchStatement([]);
      ok(false,"cant reach here");
    }catch(e){
      ok(e instanceof parse.ParseError,"default has already seen");
    }

    this.parser.init("switch(i){}");
    this.parser.next();
    var res = this.parser.parseSwitchStatement(["label1"]);
    ok(res instanceof ast.SwitchStatement, "switch(i){} is SwitchStatement");
    ok(res.expression instanceof ast.Expression, "i is Expression");
    equals(res.caseClauses.length,0,"0 clause");
    equals(this.parser.current(),tokenize.TOKEN_TYPE.EOS,"EOSまで到達してる");
    equals(res.label.length,1, "label:{} has 1 label");
    equals(res.label[0],"label1", "label:{} has label 'labe1'");
  });
  test("parseThrowStatement",function(){
    this.parser.init("throw;");
    this.parser.next();
    var res = this.parser.parseThrowStatement();
    ok(res instanceof ast.ThrowStatement, "throw; is ThrowStatement");
    equals(res.expression,null,"no expression");
    equals(this.parser.current(),tokenize.TOKEN_TYPE.EOS,"ソースの最後まで到達してる");

    this.parser.init("throw 10*x;");
    this.parser.next();
    var res = this.parser.parseThrowStatement();
    ok(res instanceof ast.ThrowStatement, "throw; is ThrowStatement");
    ok(res.expression instanceof ast.BinaryOperation, "expression is BinaryOperation");
    equals(this.parser.current(),tokenize.TOKEN_TYPE.EOS,"ソースの最後まで到達してる");

    this.parser.init("throw \n");
    this.parser.next();
    var res = this.parser.parseThrowStatement();
    ok(res instanceof ast.ThrowStatement, "throw; is ThrowStatement");
    equals(res.expression,null,"no expression");
    equals(this.parser.current(),tokenize.TOKEN_TYPE.EOS,"ソースの最後まで到達してる");

    this.parser.init("throw 10*x\n");
    this.parser.next();
    var res = this.parser.parseThrowStatement();
    ok(res instanceof ast.ThrowStatement, "throw; is ThrowStatement");
    ok(res.expression instanceof ast.BinaryOperation, "expression is BinaryOperation");
    equals(this.parser.current(),tokenize.TOKEN_TYPE.EOS,"ソースの最後まで到達してる");

    this.parser.init("throw \n10*x;");
    this.parser.next();
    var res = this.parser.parseThrowStatement();
    ok(res instanceof ast.ThrowStatement, "throw; is ThrowStatement");
    equals(res.expression,null,"no expression");
    equals(this.parser.current(),tokenize.TOKEN_TYPE.NUMERIC_LITERAL,"改行の次の10まで到達してる");

    this.parser.init("throw 10\n*x\n");
    this.parser.next();
    var res = this.parser.parseThrowStatement();
    ok(res instanceof ast.ThrowStatement, "throw; is ThrowStatement");
    ok(res.expression instanceof ast.BinaryOperation, "expression is BinaryOperation");
    equals(this.parser.current(),tokenize.TOKEN_TYPE.EOS,"ソースの最後まで到達してる");
  });
  test("parseTryStatement",function(){
    this.parser.init("try{}catch(e){}");
    this.parser.next();
    var res = this.parser.parseTryStatement([]);
    ok(res instanceof ast.TryCatchStatement, "try{}catch(e){} is TryCatchStatement");
    ok(res.tryBlock instanceof ast.BlockStatement, "try{} is BlockStatement");
    ok(res.catchBlock instanceof ast.BlockStatement, "catch(e){} is BlockStatement");
    ok(res.exception instanceof ast.Identifier, "e is Identifier");
    equals(this.parser.current(),tokenize.TOKEN_TYPE.EOS,"ソースの最後まで到達してる");

    this.parser.init("try{}finally{}");
    this.parser.next();
    var res = this.parser.parseTryStatement([]);
    ok(res instanceof ast.TryFinallyStatement, "try{}finally{} is TryFinallyStatement");
    ok(res.tryBlock instanceof ast.BlockStatement, "try{} is BlockStatement");
    ok(res.finallyBlock instanceof ast.BlockStatement, "finally{} is BlockStatement");
    equals(this.parser.current(),tokenize.TOKEN_TYPE.EOS,"ソースの最後まで到達してる");

    this.parser.init("try{}catch(e){}finally{}");
    this.parser.next();
    var res = this.parser.parseTryStatement([]);
    ok(res instanceof ast.TryFinallyStatement, "try{}catch(e){}finally{} is TryFinallyStatement");
    ok(res.tryBlock instanceof ast.BlockStatement, "try{} is BlockStatement");
    ok(res.finallyBlock instanceof ast.BlockStatement, "finally{} is BlockStatement");
    equals(res.tryBlock.statements.length,1, "try{ } length is 1");
    ok(res.tryBlock.statements[0] instanceof ast.TryCatchStatement, "try{ inner} is TryCatchStatement");
    ok(res.tryBlock.statements[0].tryBlock instanceof ast.BlockStatement, "try{ inner try{}} is BlockStatement");
    ok(res.tryBlock.statements[0].catchBlock instanceof ast.BlockStatement, "try{ inner catch(e){}} is BlockStatement");
    ok(res.tryBlock.statements[0].exception instanceof ast.Identifier, "try{ inner e} is Identifier");
    equals(this.parser.current(),tokenize.TOKEN_TYPE.EOS,"ソースの最後まで到達してる");

    this.parser.init("try{}catch(e){}");
    this.parser.next();
    var res = this.parser.parseTryStatement(["label1"]);
    ok(res instanceof ast.BlockStatement, "wrapped by BlockStatement");
    equals(res.label.length,1, "label:{} has 1 label");
    equals(res.label[0],"label1", "label:{} has label 'labe1'");
    ok(res.statements[0] instanceof ast.TryCatchStatement, "try{}catch(e){} is TryCatchStatement");
    ok(res.statements[0].tryBlock instanceof ast.BlockStatement, "try{} is BlockStatement");
    ok(res.statements[0].catchBlock instanceof ast.BlockStatement, "catch(e){} is BlockStatement");
    ok(res.statements[0].exception instanceof ast.Identifier, "e is Identifier");
    equals(this.parser.current(),tokenize.TOKEN_TYPE.EOS,"ソースの最後まで到達してる");

    this.parser.init("try{}finally{}");
    this.parser.next();
    var res = this.parser.parseTryStatement(["label1"]);
    ok(res instanceof ast.BlockStatement, "wrapped by BlockStatement");
    equals(res.label.length,1, "label:{} has 1 label");
    equals(res.label[0],"label1", "label:{} has label 'labe1'");
    ok(res.statements[0] instanceof ast.TryFinallyStatement, "try{}finally{} is TryFinallyStatement");
    ok(res.statements[0].tryBlock instanceof ast.BlockStatement, "try{} is BlockStatement");
    ok(res.statements[0].finallyBlock instanceof ast.BlockStatement, "finally{} is BlockStatement");
    equals(this.parser.current(),tokenize.TOKEN_TYPE.EOS,"ソースの最後まで到達してる");

    this.parser.init("try{}catch(e){}finally{}");
    this.parser.next();
    var res = this.parser.parseTryStatement(["label1"]);
    ok(res instanceof ast.BlockStatement, "wrapped by BlockStatement");
    equals(res.label.length,1, "label:{} has 1 label");
    equals(res.label[0],"label1", "label:{} has label 'labe1'");
    ok(res.statements[0] instanceof ast.TryFinallyStatement, "try{}catch(e){}finally{} is TryFinallyStatement");
    ok(res.statements[0].tryBlock instanceof ast.BlockStatement, "try{} is BlockStatement");
    ok(res.statements[0].finallyBlock instanceof ast.BlockStatement, "finally{} is BlockStatement");
    equals(res.statements[0].tryBlock.statements.length,1, "try{ } length is 1");
    ok(res.statements[0].tryBlock.statements[0] instanceof ast.TryCatchStatement, "try{ inner} is TryCatchStatement");
    ok(res.statements[0].tryBlock.statements[0].tryBlock instanceof ast.BlockStatement, "try{ inner try{}} is BlockStatement");
    ok(res.statements[0].tryBlock.statements[0].catchBlock instanceof ast.BlockStatement, "try{ inner catch(e){}} is BlockStatement");
    ok(res.statements[0].tryBlock.statements[0].exception instanceof ast.Identifier, "try{ inner e} is Identifier");
    equals(this.parser.current(),tokenize.TOKEN_TYPE.EOS,"ソースの最後まで到達してる");
  });
  test("parseFunctionLiteral",function(){
    this.parser.init("function(){}");
    this.parser.expectNext(tokenize.TOKEN_TYPE.K_FUNCTION);
    this.parser.next();//consume function
    var res = this.parser.parseFunctionLiteral(null);
    ok(res instanceof ast.FunctionExpression, "this is FunctionLiteral");
    equals(res.name,null,"has no name");
    equals(res.argumentNames.length,0,"has no args");
    equals(res.variableNames.length,0,"has no local variables");
    equals(res.functionDeclares.length,0,"has no local functions");
    equals(res.statements.length,0,"has no statements");
    equals(this.parser.current(),tokenize.TOKEN_TYPE.EOS,"ソースの最後まで到達してる");

    this.parser.init("function a(x,y){var v,w=0;while(true){var b=0;} function f(v){return v*2;}}");
    this.parser.expectNext(tokenize.TOKEN_TYPE.K_FUNCTION);
    this.parser.next();//consume function
    var ident = this.parser.parseIdentifier();
    var res = this.parser.parseFunctionLiteral(ident.name);
    ok(res instanceof ast.FunctionExpression, "this is FunctionLiteral");
    equals(res.name,"a","has name 'a'");
    equals(res.argumentNames.length,2,"has 2 args");
    equals(res.variableNames.length,3,"has 3 local variables");
    equals(res.functionDeclares.length,1,"has 1 local function");
    equals(res.functionDeclares[0].name,"f"," local function name is 'f'");
    equals(res.statements.length,3,"has 3 statements");
    equals(this.parser.current(),tokenize.TOKEN_TYPE.EOS,"ソースの最後まで到達してる");
  });
  test("parseFunctionExpression",function(){
    this.parser.init("function(){}");
    this.parser.next();//consume function
    var res = this.parser.parseFunctionExpression();
    ok(res instanceof ast.FunctionExpression, "this is FunctionLiteral");
    equals(res.name,null,"has no name");
    equals(res.argumentNames.length,0,"has no args");
    equals(res.variableNames.length,0,"has no local variables");
    equals(res.statements.length,0,"has no statements");
    equals(this.parser.current(),tokenize.TOKEN_TYPE.EOS,"ソースの最後まで到達してる");

    this.parser.init("function a(x,y){var v,w=0;while(true){var b=0;}}");
    this.parser.next();//consume function
    var res = this.parser.parseFunctionExpression();
    ok(res instanceof ast.FunctionExpression, "this is FunctionLiteral");
    equals(res.name,"a","has name 'a'");
    equals(res.argumentNames.length,2,"has 2 args");
    equals(res.variableNames.length,3,"has 3 local variables");
    equals(res.statements.length,2,"has 2 statements");
    equals(this.parser.current(),tokenize.TOKEN_TYPE.EOS,"ソースの最後まで到達してる");
  });
  test("parseFunctionDeclaration",function(){
    this.parser.init("function a(x,y){var v,w=0;while(true){var b=0;}}");
    this.parser.next();//consume function
    var res = this.parser.parseFunctionDeclaration();
    ok(res instanceof ast.EmptyStatement, "res is EmptyStatement");
    equals(this.parser._functions[0].length,1,"has 1 local function");
    equals(this.parser._functions[0][0].func.name,null,"has no name");
    equals(this.parser._functions[0][0].func.argumentNames.length,2,"has 2 args");
    equals(this.parser._functions[0][0].func.variableNames.length,3,"has 3 local variables");
    equals(this.parser._functions[0][0].func.statements.length,2,"has 2 statements");
    equals(this.parser._functions[0][0].func.functionDeclares.length,0,"has no functions");
    equals(this.parser.current(),tokenize.TOKEN_TYPE.EOS,"ソースの最後まで到達してる");
  });
  test("parseProgram",function(){
    this.parser.init("function a(x,y){var v,w=0;while(true){var b=0;}}\nvar i=2,j=0;f(i,j);");
    var res =this.parser.parseProgram();
    ok(res instanceof ast.Program, "res is Program");
    equals(res.variableNames.length,2,"has 2 local variables");
    equals(res.functionDeclares.length,1,"has 1 local function");
    equals(res.functionDeclares[0].name,"a"," local function name is 'a'");
    equals(res.statements.length,3,"has 3 statements");
    equals(this.parser.current(),tokenize.TOKEN_TYPE.EOS,"ソースの最後まで到達してる");
  });
});
