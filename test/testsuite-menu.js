/**
 * @ignore
 */
$(function(){
  $('#testsuite-menu').append($('<li>').append($('<a/>').attr('href','?').text('test all')));
  var modules = {};
  QUnit.moduleDone = function(name){
    modules[name]=true;
  };
  QUnit.done = function(){
    $.each(modules,function(v){
      $('#testsuite-menu').append($('<li>').append($('<a/>').attr('href','?'+v).text('test '+v)));
    });
  };
});

