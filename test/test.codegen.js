/**
 * @fileOverview codegen.js unit test
 * @author ide
 * @requires qunit.js 
 * @requires jquery.js
*/
$(function(){
  var assert_implementme = true;
  module("codegen.js",{
    setup:function(){
      this.visitor = new codegen.AstVisitor();
      var self = this;
      this.init = function(){
        self.ib = new codegen.InstructionBuilder();
        self.ss = new codegen.SymbolSolver();
        self.visitor.init(self.ib,self.ss);
      };
    }
  });
  test("AstVisitor.visitArguments",function(){
    var args ;

    args = new ast.Arguments([]);
    this.init();
    this.visitor.visitArguments(args);
    equals(this.ib.instructions.length,0,"no args");

    args = new ast.Arguments([new ast.Literal(new jsobjects.JSNumber(1))]);
    this.init();
    this.visitor.visitArguments(args);
    equals(this.ib.instructions.length,1,"1 args");
    equals(this.ib.instructions[0].opecode,vm.opecode.loadi,"loadi");
    ok(this.ib.instructions[0].operand[0] instanceof jsobjects.JSNumber ,"loadi [JSNumber]");
    equals(this.ib.instructions[0].operand[0].getNumber(),1,"loadi 1");

    args = new ast.Arguments([new ast.Literal(new jsobjects.JSNumber(1)),new ast.Literal(new jsobjects.JSNumber(2))]);
    this.init();
    this.visitor.visitArguments(args);
    equals(this.ib.instructions.length,2,"2 args");
    equals(this.ib.instructions[0].opecode,vm.opecode.loadi,"loadi");
    ok(this.ib.instructions[0].operand[0] instanceof jsobjects.JSNumber ,"loadi [JSNumber]");
    equals(this.ib.instructions[0].operand[0].getNumber(),1,"loadi 1");
    equals(this.ib.instructions[1].opecode,vm.opecode.loadi,"loadi");
    ok(this.ib.instructions[1].operand[0] instanceof jsobjects.JSNumber ,"loadi [JSNumber]");
    equals(this.ib.instructions[1].operand[0].getNumber(),2,"loadi 2");
  });
  test("AstVisitor.visitArrayLiteral",function(){

    ok(assert_implementme,"implement me!!!!")
  });
  test("AstVisitor.visitAssignment",function(){
    var op1 = new ast.Identifier("x");
    var op2 = new ast.Literal(new jsobjects.JSNumber(10));
    ok(assert_implementme,"implement me!!!!")
  });
  test("AstVisitor.visitBinaryOperation",function(){
    var op1 = new ast.Literal(new jsobjects.JSNumber(1));
    var op2 = new ast.Literal(new jsobjects.JSNumber(10));
    var operator;

    operator = new ast.BinaryOperation(tokenize.TOKEN_TYPE.P_ADD,op1,op2);
    this.init();
    this.visitor.visitBinaryOperation(operator);
    equals(this.ib.instructions.length,3,"3 line");
    ok(this.ib.instructions[0].operand[0] instanceof jsobjects.JSNumber ,"loadi [JSNumber]");
    equals(this.ib.instructions[0].operand[0].getNumber(),1,"loadi 1");
    equals(this.ib.instructions[1].opecode,vm.opecode.loadi,"loadi");
    ok(this.ib.instructions[1].operand[0] instanceof jsobjects.JSNumber ,"loadi [JSNumber]");
    equals(this.ib.instructions[1].operand[0].getNumber(),10,"loadi 10");
    equals(this.ib.instructions[2].opecode,vm.opecode.add,"add");

    operator = new ast.BinaryOperation(tokenize.TOKEN_TYPE.P_SUB,op1,op2);
    this.init();
    this.visitor.visitBinaryOperation(operator);
    equals(this.ib.instructions.length,3,"3 line");
    ok(this.ib.instructions[0].operand[0] instanceof jsobjects.JSNumber ,"loadi [JSNumber]");
    equals(this.ib.instructions[0].operand[0].getNumber(),1,"loadi 1");
    equals(this.ib.instructions[1].opecode,vm.opecode.loadi,"loadi");
    ok(this.ib.instructions[1].operand[0] instanceof jsobjects.JSNumber ,"loadi [JSNumber]");
    equals(this.ib.instructions[1].operand[0].getNumber(),10,"loadi 10");
    equals(this.ib.instructions[2].opecode,vm.opecode.sub,"sub");

    operator = new ast.BinaryOperation(tokenize.TOKEN_TYPE.P_MUL,op1,op2);
    this.init();
    this.visitor.visitBinaryOperation(operator);
    equals(this.ib.instructions.length,3,"3 line");
    ok(this.ib.instructions[0].operand[0] instanceof jsobjects.JSNumber ,"loadi [JSNumber]");
    equals(this.ib.instructions[0].operand[0].getNumber(),1,"loadi 1");
    equals(this.ib.instructions[1].opecode,vm.opecode.loadi,"loadi");
    ok(this.ib.instructions[1].operand[0] instanceof jsobjects.JSNumber ,"loadi [JSNumber]");
    equals(this.ib.instructions[1].operand[0].getNumber(),10,"loadi 10");
    equals(this.ib.instructions[2].opecode,vm.opecode.mul,"mul");

    operator = new ast.BinaryOperation(tokenize.TOKEN_TYPE.P_MOD,op1,op2);
    this.init();
    this.visitor.visitBinaryOperation(operator);
    equals(this.ib.instructions.length,3,"3 line");
    ok(this.ib.instructions[0].operand[0] instanceof jsobjects.JSNumber ,"loadi [JSNumber]");
    equals(this.ib.instructions[0].operand[0].getNumber(),1,"loadi 1");
    equals(this.ib.instructions[1].opecode,vm.opecode.loadi,"loadi");
    ok(this.ib.instructions[1].operand[0] instanceof jsobjects.JSNumber ,"loadi [JSNumber]");
    equals(this.ib.instructions[1].operand[0].getNumber(),10,"loadi 10");
    equals(this.ib.instructions[2].opecode,vm.opecode.mod,"mod");

    operator = new ast.BinaryOperation(tokenize.TOKEN_TYPE.P_DIV,op1,op2);
    this.init();
    this.visitor.visitBinaryOperation(operator);
    equals(this.ib.instructions.length,3,"3 line");
    ok(this.ib.instructions[0].operand[0] instanceof jsobjects.JSNumber ,"loadi [JSNumber]");
    equals(this.ib.instructions[0].operand[0].getNumber(),1,"loadi 1");
    equals(this.ib.instructions[1].opecode,vm.opecode.loadi,"loadi");
    ok(this.ib.instructions[1].operand[0] instanceof jsobjects.JSNumber ,"loadi [JSNumber]");
    equals(this.ib.instructions[1].operand[0].getNumber(),10,"loadi 10");
    equals(this.ib.instructions[2].opecode,vm.opecode.div,"div");

    operator = new ast.BinaryOperation(tokenize.TOKEN_TYPE.P_AND,op1,op2);
    this.init();
    this.visitor.visitBinaryOperation(operator);
    equals(this.ib.instructions.length,3,"3 line");
    ok(this.ib.instructions[0].operand[0] instanceof jsobjects.JSNumber ,"loadi [JSNumber]");
    equals(this.ib.instructions[0].operand[0].getNumber(),1,"loadi 1");
    equals(this.ib.instructions[1].opecode,vm.opecode.loadi,"loadi");
    ok(this.ib.instructions[1].operand[0] instanceof jsobjects.JSNumber ,"loadi [JSNumber]");
    equals(this.ib.instructions[1].operand[0].getNumber(),10,"loadi 10");
    equals(this.ib.instructions[2].opecode,vm.opecode.bit_and,"bit_and");

    operator = new ast.BinaryOperation(tokenize.TOKEN_TYPE.P_OR,op1,op2);
    this.init();
    this.visitor.visitBinaryOperation(operator);
    equals(this.ib.instructions.length,3,"3 line");
    ok(this.ib.instructions[0].operand[0] instanceof jsobjects.JSNumber ,"loadi [JSNumber]");
    equals(this.ib.instructions[0].operand[0].getNumber(),1,"loadi 1");
    equals(this.ib.instructions[1].opecode,vm.opecode.loadi,"loadi");
    ok(this.ib.instructions[1].operand[0] instanceof jsobjects.JSNumber ,"loadi [JSNumber]");
    equals(this.ib.instructions[1].operand[0].getNumber(),10,"loadi 10");
    equals(this.ib.instructions[2].opecode,vm.opecode.bit_or,"bit_or");

    operator = new ast.BinaryOperation(tokenize.TOKEN_TYPE.P_XOR,op1,op2);
    this.init();
    this.visitor.visitBinaryOperation(operator);
    equals(this.ib.instructions.length,3,"3 line");
    ok(this.ib.instructions[0].operand[0] instanceof jsobjects.JSNumber ,"loadi [JSNumber]");
    equals(this.ib.instructions[0].operand[0].getNumber(),1,"loadi 1");
    equals(this.ib.instructions[1].opecode,vm.opecode.loadi,"loadi");
    ok(this.ib.instructions[1].operand[0] instanceof jsobjects.JSNumber ,"loadi [JSNumber]");
    equals(this.ib.instructions[1].operand[0].getNumber(),10,"loadi 10");
    equals(this.ib.instructions[2].opecode,vm.opecode.bit_xor,"bit_xor");

    operator = new ast.BinaryOperation(tokenize.TOKEN_TYPE.P_SHL,op1,op2);
    this.init();
    this.visitor.visitBinaryOperation(operator);
    equals(this.ib.instructions.length,3,"3 line");
    ok(this.ib.instructions[0].operand[0] instanceof jsobjects.JSNumber ,"loadi [JSNumber]");
    equals(this.ib.instructions[0].operand[0].getNumber(),1,"loadi 1");
    equals(this.ib.instructions[1].opecode,vm.opecode.loadi,"loadi");
    ok(this.ib.instructions[1].operand[0] instanceof jsobjects.JSNumber ,"loadi [JSNumber]");
    equals(this.ib.instructions[1].operand[0].getNumber(),10,"loadi 10");
    equals(this.ib.instructions[2].opecode,vm.opecode.bit_shl,"bit_shl");

    operator = new ast.BinaryOperation(tokenize.TOKEN_TYPE.P_SHR,op1,op2);
    this.init();
    this.visitor.visitBinaryOperation(operator);
    equals(this.ib.instructions.length,3,"3 line");
    ok(this.ib.instructions[0].operand[0] instanceof jsobjects.JSNumber ,"loadi [JSNumber]");
    equals(this.ib.instructions[0].operand[0].getNumber(),1,"loadi 1");
    equals(this.ib.instructions[1].opecode,vm.opecode.loadi,"loadi");
    ok(this.ib.instructions[1].operand[0] instanceof jsobjects.JSNumber ,"loadi [JSNumber]");
    equals(this.ib.instructions[1].operand[0].getNumber(),10,"loadi 10");
    equals(this.ib.instructions[2].opecode,vm.opecode.bit_shr,"bit_shr");

    operator = new ast.BinaryOperation(tokenize.TOKEN_TYPE.P_SHR_ARITH,op1,op2);
    this.init();
    this.visitor.visitBinaryOperation(operator);
    equals(this.ib.instructions.length,3,"3 line");
    ok(this.ib.instructions[0].operand[0] instanceof jsobjects.JSNumber ,"loadi [JSNumber]");
    equals(this.ib.instructions[0].operand[0].getNumber(),1,"loadi 1");
    equals(this.ib.instructions[1].opecode,vm.opecode.loadi,"loadi");
    ok(this.ib.instructions[1].operand[0] instanceof jsobjects.JSNumber ,"loadi [JSNumber]");
    equals(this.ib.instructions[1].operand[0].getNumber(),10,"loadi 10");
    equals(this.ib.instructions[2].opecode,vm.opecode.bit_shr_arith,"bit_shr_arith");

    operator = new ast.BinaryOperation(tokenize.TOKEN_TYPE.P_EQUAL,op1,op2);
    this.init();
    this.visitor.visitBinaryOperation(operator);
    equals(this.ib.instructions.length,3,"3 line");
    ok(this.ib.instructions[0].operand[0] instanceof jsobjects.JSNumber ,"loadi [JSNumber]");
    equals(this.ib.instructions[0].operand[0].getNumber(),1,"loadi 1");
    equals(this.ib.instructions[1].opecode,vm.opecode.loadi,"loadi");
    ok(this.ib.instructions[1].operand[0] instanceof jsobjects.JSNumber ,"loadi [JSNumber]");
    equals(this.ib.instructions[1].operand[0].getNumber(),10,"loadi 10");
    equals(this.ib.instructions[2].opecode,vm.opecode.eq,"eq");

    operator = new ast.BinaryOperation(tokenize.TOKEN_TYPE.P_EQAUL_STRICT,op1,op2);
    this.init();
    this.visitor.visitBinaryOperation(operator);
    equals(this.ib.instructions.length,3,"3 line");
    ok(this.ib.instructions[0].operand[0] instanceof jsobjects.JSNumber ,"loadi [JSNumber]");
    equals(this.ib.instructions[0].operand[0].getNumber(),1,"loadi 1");
    equals(this.ib.instructions[1].opecode,vm.opecode.loadi,"loadi");
    ok(this.ib.instructions[1].operand[0] instanceof jsobjects.JSNumber ,"loadi [JSNumber]");
    equals(this.ib.instructions[1].operand[0].getNumber(),10,"loadi 10");
    equals(this.ib.instructions[2].opecode,vm.opecode.eqs,"eqs");

    operator = new ast.BinaryOperation(tokenize.TOKEN_TYPE.P_NOT_EQUAL,op1,op2);
    this.init();
    this.visitor.visitBinaryOperation(operator);
    equals(this.ib.instructions.length,3,"3 line");
    ok(this.ib.instructions[0].operand[0] instanceof jsobjects.JSNumber ,"loadi [JSNumber]");
    equals(this.ib.instructions[0].operand[0].getNumber(),1,"loadi 1");
    equals(this.ib.instructions[1].opecode,vm.opecode.loadi,"loadi");
    ok(this.ib.instructions[1].operand[0] instanceof jsobjects.JSNumber ,"loadi [JSNumber]");
    equals(this.ib.instructions[1].operand[0].getNumber(),10,"loadi 10");
    equals(this.ib.instructions[2].opecode,vm.opecode.neq,"neq");

    operator = new ast.BinaryOperation(tokenize.TOKEN_TYPE.P_NOT_EQUAL_STRICT,op1,op2);
    this.init();
    this.visitor.visitBinaryOperation(operator);
    equals(this.ib.instructions.length,3,"3 line");
    ok(this.ib.instructions[0].operand[0] instanceof jsobjects.JSNumber ,"loadi [JSNumber]");
    equals(this.ib.instructions[0].operand[0].getNumber(),1,"loadi 1");
    equals(this.ib.instructions[1].opecode,vm.opecode.loadi,"loadi");
    ok(this.ib.instructions[1].operand[0] instanceof jsobjects.JSNumber ,"loadi [JSNumber]");
    equals(this.ib.instructions[1].operand[0].getNumber(),10,"loadi 10");
    equals(this.ib.instructions[2].opecode,vm.opecode.neqs,"neqs");

    operator = new ast.BinaryOperation(tokenize.TOKEN_TYPE.P_LT,op1,op2);
    this.init();
    this.visitor.visitBinaryOperation(operator);
    equals(this.ib.instructions.length,3,"3 line");
    ok(this.ib.instructions[0].operand[0] instanceof jsobjects.JSNumber ,"loadi [JSNumber]");
    equals(this.ib.instructions[0].operand[0].getNumber(),1,"loadi 1");
    equals(this.ib.instructions[1].opecode,vm.opecode.loadi,"loadi");
    ok(this.ib.instructions[1].operand[0] instanceof jsobjects.JSNumber ,"loadi [JSNumber]");
    equals(this.ib.instructions[1].operand[0].getNumber(),10,"loadi 10");
    equals(this.ib.instructions[2].opecode,vm.opecode.lt,"lt");

    operator = new ast.BinaryOperation(tokenize.TOKEN_TYPE.P_GT,op1,op2);
    this.init();
    this.visitor.visitBinaryOperation(operator);
    equals(this.ib.instructions.length,3,"3 line");
    ok(this.ib.instructions[0].operand[0] instanceof jsobjects.JSNumber ,"loadi [JSNumber]");
    equals(this.ib.instructions[0].operand[0].getNumber(),1,"loadi 1");
    equals(this.ib.instructions[1].opecode,vm.opecode.loadi,"loadi");
    ok(this.ib.instructions[1].operand[0] instanceof jsobjects.JSNumber ,"loadi [JSNumber]");
    equals(this.ib.instructions[1].operand[0].getNumber(),10,"loadi 10");
    equals(this.ib.instructions[2].opecode,vm.opecode.gt,"gt");

    operator = new ast.BinaryOperation(tokenize.TOKEN_TYPE.P_LEQ,op1,op2);
    this.init();
    this.visitor.visitBinaryOperation(operator);
    equals(this.ib.instructions.length,3,"3 line");
    ok(this.ib.instructions[0].operand[0] instanceof jsobjects.JSNumber ,"loadi [JSNumber]");
    equals(this.ib.instructions[0].operand[0].getNumber(),1,"loadi 1");
    equals(this.ib.instructions[1].opecode,vm.opecode.loadi,"loadi");
    ok(this.ib.instructions[1].operand[0] instanceof jsobjects.JSNumber ,"loadi [JSNumber]");
    equals(this.ib.instructions[1].operand[0].getNumber(),10,"loadi 10");
    equals(this.ib.instructions[2].opecode,vm.opecode.leq,"leq");

    operator = new ast.BinaryOperation(tokenize.TOKEN_TYPE.P_GEQ,op1,op2);
    this.init();
    this.visitor.visitBinaryOperation(operator);
    equals(this.ib.instructions.length,3,"3 line");
    ok(this.ib.instructions[0].operand[0] instanceof jsobjects.JSNumber ,"loadi [JSNumber]");
    equals(this.ib.instructions[0].operand[0].getNumber(),1,"loadi 1");
    equals(this.ib.instructions[1].opecode,vm.opecode.loadi,"loadi");
    ok(this.ib.instructions[1].operand[0] instanceof jsobjects.JSNumber ,"loadi [JSNumber]");
    equals(this.ib.instructions[1].operand[0].getNumber(),10,"loadi 10");
    equals(this.ib.instructions[2].opecode,vm.opecode.geq,"geq");

    operator = new ast.BinaryOperation(tokenize.TOKEN_TYPE.P_AND_LOGIC,op1,op2);
    this.init();
    this.visitor.visitBinaryOperation(operator);
    equals(this.ib.instructions.length,5,"5 line");
    ok(this.ib.instructions[0].operand[0] instanceof jsobjects.JSNumber ,"loadi [JSNumber]");
    equals(this.ib.instructions[0].operand[0].getNumber(),1,"loadi 1");
    equals(this.ib.instructions[1].opecode,vm.opecode.dup,"dup");
    equals(this.ib.instructions[2].opecode,vm.opecode.jump_if_false,"jump_if_false");
    equals(this.ib.instructions[2].operand[0],5,"jump_if_false to 5");
    equals(this.ib.instructions[3].opecode,vm.opecode.pop,"pop");
    equals(this.ib.instructions[4].opecode,vm.opecode.loadi,"loadi");
    ok(this.ib.instructions[4].operand[0] instanceof jsobjects.JSNumber ,"loadi [JSNumber]");
    equals(this.ib.instructions[4].operand[0].getNumber(),10,"loadi 10");

    operator = new ast.BinaryOperation(tokenize.TOKEN_TYPE.P_OR_LOGIC,op1,op2);
    this.init();
    this.visitor.visitBinaryOperation(operator);
    equals(this.ib.instructions.length,5,"5 line");
    ok(this.ib.instructions[0].operand[0] instanceof jsobjects.JSNumber ,"loadi [JSNumber]");
    equals(this.ib.instructions[0].operand[0].getNumber(),1,"loadi 1");
    equals(this.ib.instructions[1].opecode,vm.opecode.dup,"dup");
    equals(this.ib.instructions[2].opecode,vm.opecode.jump_if_true,"jump_if_true");
    equals(this.ib.instructions[2].operand[0],5,"jump_if_true to 5");
    equals(this.ib.instructions[3].opecode,vm.opecode.pop,"pop");
    equals(this.ib.instructions[4].opecode,vm.opecode.loadi,"loadi");
    ok(this.ib.instructions[4].operand[0] instanceof jsobjects.JSNumber ,"loadi [JSNumber]");
    equals(this.ib.instructions[4].operand[0].getNumber(),10,"loadi 10");
  });
  test("AstVisitor.visitBlockStatement",function(){
    ok(assert_implementme,"implement me!!!!")
  });
  test("AstVisitor.visitBreakStatement",function(){
    ok(assert_implementme,"implement me!!!!")
  });
  test("AstVisitor.visitCall",function(){
    ok(assert_implementme,"implement me!!!!")
  });
  test("AstVisitor.visitCallNew",function(){
    ok(assert_implementme,"implement me!!!!")
  });
  test("AstVisitor.visitCaseClause",function(){
    ok(assert_implementme,"implement me!!!!")
  });
  test("AstVisitor.visitConditional",function(){
    ok(assert_implementme,"implement me!!!!")
  });
  test("AstVisitor.visitContinueStatement",function(){
    ok(assert_implementme,"implement me!!!!")
  });
  test("AstVisitor.visitDoWhileStatement",function(){
    ok(assert_implementme,"implement me!!!!")
  });
  test("AstVisitor.visitEmptyStatement",function(){
    ok(assert_implementme,"implement me!!!!")
  });
  test("AstVisitor.visitExpressionStatement",function(){
    ok(assert_implementme,"implement me!!!!")
  });
  test("AstVisitor.visitForInStatement",function(){
    ok(assert_implementme,"implement me!!!!")
  });
  test("AstVisitor.visitForStatement",function(){
    ok(assert_implementme,"implement me!!!!")
  });
  test("AstVisitor.visitFunctionExpression",function(){
    ok(assert_implementme,"implement me!!!!")
  });
  test("AstVisitor.visitIdentifier",function(){
    ok(assert_implementme,"implement me!!!!")
  });
  test("AstVisitor.visitIfStatement",function(){
    ok(assert_implementme,"implement me!!!!")
  });
  test("AstVisitor.visitLiteral",function(){
    var lit ;

    lit = new ast.Literal(new jsobjects.JSNumber(1));
    this.init();
    this.visitor.visitLiteral(lit);
    equals(this.ib.instructions.length,1,"1 opecode");
    equals(this.ib.instructions[0].opecode,vm.opecode.loadi,"loadi");
    ok(this.ib.instructions[0].operand[0] instanceof jsobjects.JSNumber ,"loadi [JSNumber]");
    equals(this.ib.instructions[0].operand[0].getNumber(),1,"loadi 1");

    lit = new ast.Literal(new jsobjects.JSString("test"));
    this.init();
    this.visitor.visitLiteral(lit);
    equals(this.ib.instructions.length,1,"1 opecode");
    equals(this.ib.instructions[0].opecode,vm.opecode.loadi,"loadi");
    ok(this.ib.instructions[0].operand[0] instanceof jsobjects.JSString ,"loadi [JSString]");
    equals(this.ib.instructions[0].operand[0].getString(),"test","loadi 'test'");

    lit = new ast.Literal(new jsobjects.JSBoolean(true));
    this.init();
    this.visitor.visitLiteral(lit);
    equals(this.ib.instructions.length,1,"1 opecode");
    equals(this.ib.instructions[0].opecode,vm.opecode.loadi,"loadi");
    ok(this.ib.instructions[0].operand[0] instanceof jsobjects.JSBoolean ,"loadi [JSBoolean]");
    equals(this.ib.instructions[0].operand[0].isTrue(),true,"loadi true");

    lit = new ast.Literal(new jsobjects.JSUndefined());
    this.init();
    this.visitor.visitLiteral(lit);
    equals(this.ib.instructions.length,1,"1 opecode");
    equals(this.ib.instructions[0].opecode,vm.opecode.loadi,"loadi");
    equals(this.ib.instructions[0].operand[0].isUndefined(),true,"loadi undefined");

    lit = new ast.Literal(new jsobjects.JSNull());
    this.init();
    this.visitor.visitLiteral(lit);
    equals(this.ib.instructions.length,1,"1 opecode");
    equals(this.ib.instructions[0].opecode,vm.opecode.loadi,"loadi");
    equals(this.ib.instructions[0].operand[0].isNull(),true,"loadi null");
  });
  test("AstVisitor.visitObjectLiteral",function(){
    ok(assert_implementme,"implement me!!!!")
  });
  test("AstVisitor.visitPostfixOperation",function(){
    ok(assert_implementme,"implement me!!!!")
  });
  test("AstVisitor.visitProgram",function(){
    ok(assert_implementme,"implement me!!!!")
  });
  test("AstVisitor.visitPropertyAccessor",function(){
    ok(assert_implementme,"implement me!!!!")
  });
  test("AstVisitor.visitRegExpLiteral",function(){
    ok(assert_implementme,"implement me!!!!")
  });
  test("AstVisitor.visitReturnStatement",function(){
    ok(assert_implementme,"implement me!!!!")
  });
  test("AstVisitor.visitSwitchStatement",function(){
    ok(assert_implementme,"implement me!!!!")
  });
  test("AstVisitor.visitThis",function(){
    ok(assert_implementme,"implement me!!!!")
  });
  test("AstVisitor.visitThrowStatement",function(){
    ok(assert_implementme,"implement me!!!!")
  });
  test("AstVisitor.visitTryCatchStatement",function(){
    ok(assert_implementme,"implement me!!!!")
  });
  test("AstVisitor.visitTryFinallyStatement",function(){
    ok(assert_implementme,"implement me!!!!")
  });
  test("AstVisitor.visitUnaryOperation",function(){
    ok(assert_implementme,"implement me!!!!")
  });
  test("AstVisitor.visitWhileStatement",function(){
    ok(assert_implementme,"implement me!!!!")
  });
  test("AstVisitor.visitWithStatement",function(){
    ok(assert_implementme,"implement me!!!!")
  });
});
