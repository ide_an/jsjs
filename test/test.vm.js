/**
 * @fileOverview vm.js unit test
 * @author ide
 * @requires qunit.js 
 * @requires jquery.js
 */
$(function(){
  module("vm.js",{
    setup:function(){
      this.vm = new vm.Vm();
    }
  });
  test("load",function(){
    var code = [
      new vm.Instruction(vm.opecode.loadi,[new jsobjects.JSNumber(1)]),
      new vm.Instruction(vm.opecode.loadi,[new jsobjects.JSNumber(2)])
    ];
    this.vm.load(code);
    equals(this.vm.instructions,code,"load success");
    equals(this.vm.pc,0,"pc is 0 default")
    equals(this.vm.accumulatorStack.length,0,"acc is empty");
    equals(this.vm.callStack.length,1,"callStack has 1 entry");
    equals(this.vm.exceptionHandlerStack.length,0,"exceptionHandlerStack is empty");
  });
  test("step",function(){
    var res ;
    //this.vm.init();
    //res = this.vm.step(new vm.Instruction(vm.opecode.nop));
    //ok(res,"continue exceute:nop");
    //equals(this.vm.pc,1,"pc++");
    //equals(this.vm.accumulatorStack.length,0,"acc is empty");

    this.vm.init();
    res = this.vm.step(new vm.Instruction(vm.opecode.exit));
    ok(!res,"exceute end:exit");
    equals(this.vm.pc,0,"pc doesnt change")
    equals(this.vm.accumulatorStack.length,0,"acc is empty");

    this.vm.init();
    this.vm.accumulatorStack.push("test");
    equals(this.vm.accumulatorStack.length,1,"acc 1 entry");
    res = this.vm.step(new vm.Instruction(vm.opecode.pop));
    ok(res,"continue exceute:pop");
    equals(this.vm.pc,1,"pc++");
    equals(this.vm.accumulatorStack.length,0,"acc is empty");

    this.vm.init();
    res = this.vm.step(new vm.Instruction(vm.opecode.loadi,[new jsobjects.JSObject()]));
    equals(this.vm.pc,1,"pc++");
    equals(this.vm.accumulatorStack.length,1,"acc 1 entry");
    res = this.vm.step(new vm.Instruction(vm.opecode.dup));
    ok(res,"continue exceute:pop");
    equals(this.vm.pc,2,"pc++");
    equals(this.vm.accumulatorStack.length,2,"acc is 2 entry");
    equals(this.vm.accumulatorStack[0],this.vm.accumulatorStack[1],"[0]===[1]");

    this.vm.init();
    res = this.vm.step(new vm.Instruction(vm.opecode.loadi,[new jsobjects.JSObject()]));
    equals(this.vm.accumulatorStack.length,1,"acc 1 entry");
    res = this.vm.step(new vm.Instruction(vm.opecode.loadi,[new jsobjects.JSObject()]));
    equals(this.vm.accumulatorStack.length,2,"acc 2 entry");
    res = this.vm.step(new vm.Instruction(vm.opecode.dup2));
    ok(res,"continue exceute:dup2");
    equals(this.vm.pc,3,"pc++");
    equals(this.vm.accumulatorStack.length,4,"acc is 4 entry");
    equals(this.vm.accumulatorStack[0],this.vm.accumulatorStack[2],"[0]===[2]");
    equals(this.vm.accumulatorStack[1],this.vm.accumulatorStack[3],"[1]===[3]");

    this.vm.init();
    res = this.vm.step(new vm.Instruction(vm.opecode.loadi,[new jsobjects.JSNumber(128)]));
    ok(res,"continue exceute:loadi");
    equals(this.vm.pc,1,"pc++");
    equals(this.vm.accumulatorStack.length,1,"acc has 1 entry");
    equals(this.vm.accumulatorStack[0].getNumber(),128,"acc has 1 entry");

    this.vm.init();
    res = this.vm.step(new vm.Instruction(vm.opecode.add_scope,["i"]));
    equals(this.vm.pc,1,"pc++");
    res = this.vm.step(new vm.Instruction(vm.opecode.load,[0,"i"]));
    ok(res,"continue exceute:load");
    equals(this.vm.pc,2,"pc++");
    equals(this.vm.accumulatorStack.length,1,"acc has 1 entry");
    this.vm.step(new vm.Instruction(vm.opecode.remove_scope,[]));
    equals(this.vm.pc,3,"pc++:remove_scope");
    try{
      res = this.vm.step(new vm.Instruction(vm.opecode.load,[0,"i"]));
      ok(false,"unreacheable :remove_scope");
    }catch(e){
      ok(true,"cant access i:remove_scope");
    }
    this.vm.init();
    res = this.vm.step(new vm.Instruction(vm.opecode.add_scope,["i"]));
    equals(this.vm.pc,1,"pc++");
    this.vm.step(new vm.Instruction(vm.opecode.loadi,[new jsobjects.JSNumber(128)]));
    equals(this.vm.pc,2,"pc++");
    res = this.vm.step(new vm.Instruction(vm.opecode.store,[0,"i"]));
    ok(res,"continue exceute:store");
    equals(this.vm.pc,3,"pc++");
    equals(this.vm.accumulatorStack.length,0,"acc has no entry");
    res = this.vm.step(new vm.Instruction(vm.opecode.load,[0,"i"]));
    ok(res,"continue exceute:load");
    equals(this.vm.pc,4,"pc++");
    equals(this.vm.accumulatorStack.length,1,"acc has 1 entry");
    equals(this.vm.accumulatorStack[0].getNumber(),128,"acc is 128");

    this.vm.init();
    this.vm.step(new vm.Instruction(vm.opecode.loadi,[new jsobjects.JSObject()]));
    this.vm.step(new vm.Instruction(vm.opecode.dup));
    this.vm.step(new vm.Instruction(vm.opecode.loadi,[new jsobjects.JSString("a")]));
    this.vm.step(new vm.Instruction(vm.opecode.loadi,[new jsobjects.JSNumber(128)]));
    this.vm.step(new vm.Instruction(vm.opecode.set_prop));
    res = this.vm.step(new vm.Instruction(vm.opecode.add_scope_from_object));
    ok(res,"continue exceute:add_scope_from_object");
    equals(this.vm.pc,6,"pc++:add_scope_from_object");
    this.vm.step(new vm.Instruction(vm.opecode.load,[0,"a"]));
    equals(this.vm.accumulatorStack.length,1,"acc has 1 entry");
    equals(this.vm.accumulatorStack[0].getNumber(),128,"acc is 128");
    this.vm.step(new vm.Instruction(vm.opecode.pop));
    this.vm.step(new vm.Instruction(vm.opecode.load,[1,"a"]));
    equals(this.vm.accumulatorStack.length,1,"acc has 1 entry");
    equals(this.vm.accumulatorStack[0].getNumber(),128,"acc is 128");
    this.vm.step(new vm.Instruction(vm.opecode.remove_scope));
    try{
      res = this.vm.step(new vm.Instruction(vm.opecode.load,[0,"a"]));
      ok(false,"unreacheable :remove_scope");
    }catch(e){
      ok(true,"cant access i:remove_scope");
    }

    this.vm.init();
    try{
      res = this.vm.step(new vm.Instruction(vm.opecode.load,[0,"a"]));
      ok(false,"unreacheable :remove_scope");
    }catch(e){
      ok(true,"cant access i:remove_scope");
    }
    res = this.vm.step(new vm.Instruction(vm.opecode.add_global_scope,["a"]));
    ok(res,"continue exceute:add_global_scope");
    equals(this.vm.pc,1,"pc++:add_global_scope");
    res = this.vm.step(new vm.Instruction(vm.opecode.load,[0,"a"]));
    //console.log(this.vm.global_object.hasProperty("a"));
    ok(true,"enable to access i:add_global_scope");
    res = this.vm.step(new vm.Instruction(vm.opecode.pop));
    res = this.vm.step(new vm.Instruction(vm.opecode.loadi,[new jsobjects.JSNumber(10)]));
    res = this.vm.step(new vm.Instruction(vm.opecode.store,[0,"a"]));
    res = this.vm.step(new vm.Instruction(vm.opecode.add_scope,["a"]));
    res = this.vm.step(new vm.Instruction(vm.opecode.load,[0,"a"]));
    ok(this.vm.accumulatorStack[0].isUndefined(),"acc is undefined");

    this.vm.init();
    res = this.vm.step(new vm.Instruction(vm.opecode.load_this));
    ok(res,"continue exceute:load_this");
    equals(this.vm.pc,1,"pc++");
    equals(this.vm.accumulatorStack.length,1,"acc has 1 entry");
    //TODO: access variable test

    this.vm.init();
    this.vm.step(new vm.Instruction(vm.opecode.loadi,[new jsobjects.JSNumber(128)]));
    equals(this.vm.pc,1,"pc++");
    res = this.vm.step(new vm.Instruction(vm.opecode.store_this));
    ok(res,"continue exceute:store_this");
    equals(this.vm.pc,2,"pc++");
    equals(this.vm.accumulatorStack.length,0,"acc has no entry");
    //TODO: access variable test

    this.vm.init();
    var obj = new jsobjects.JSObject();
    obj.setProperty("length",new jsobjects.JSNumber(1));
    this.vm.step(new vm.Instruction(vm.opecode.loadi,[obj]));
    equals(this.vm.pc,1,"pc++");
    this.vm.step(new vm.Instruction(vm.opecode.loadi,[new jsobjects.JSString("length")]));
    equals(this.vm.pc,2,"pc++");
    res = this.vm.step(new vm.Instruction(vm.opecode.get_prop));
    ok(res,"continue exceute:get_prop");
    equals(this.vm.pc,3,"pc++");
    equals(this.vm.accumulatorStack.length,1,"acc has 1 entry");
    equals(this.vm.accumulatorStack[0].getNumber(),1,"acc is 1");

    this.vm.init();
    this.vm.step(new vm.Instruction(vm.opecode.loadi,[new jsobjects.JSObject()]));
    equals(this.vm.pc,1,"pc++");
    this.vm.step(new vm.Instruction(vm.opecode.dup));
    equals(this.vm.pc,2,"pc++");
    this.vm.step(new vm.Instruction(vm.opecode.loadi,[new jsobjects.JSString("a")]));
    equals(this.vm.pc,3,"pc++");
    this.vm.step(new vm.Instruction(vm.opecode.loadi,[new jsobjects.JSNumber(128)]));
    equals(this.vm.pc,4,"pc++");
    res = this.vm.step(new vm.Instruction(vm.opecode.set_prop));
    ok(res,"continue exceute:set_prop");
    equals(this.vm.pc,5,"pc++");
    equals(this.vm.accumulatorStack.length,1,"acc has 1 entry");
    this.vm.step(new vm.Instruction(vm.opecode.loadi,[new jsobjects.JSString("a")]));
    equals(this.vm.pc,6,"pc++");
    res = this.vm.step(new vm.Instruction(vm.opecode.get_prop));
    ok(res,"continue exceute:get_prop");
    equals(this.vm.pc,7,"pc++");
    equals(this.vm.accumulatorStack.length,1,"acc has 1 entry");
    equals(this.vm.accumulatorStack[0].getNumber(),128,"acc is 128");

    this.vm.init();
    res = this.vm.step(new vm.Instruction(vm.opecode.add_scope,["i"]));
    equals(this.vm.pc,1,"pc++");
    this.vm.step(new vm.Instruction(vm.opecode.loadi,[new jsobjects.JSNumber(128)]));
    equals(this.vm.pc,2,"pc++");
    res = this.vm.step(new vm.Instruction(vm.opecode.store,[0,"i"]));
    equals(this.vm.pc,3,"pc++");
    res = this.vm.step(new vm.Instruction(vm.opecode.inc,[0,"i"]));
    ok(res,"continue exceute:inc");
    equals(this.vm.pc,4,"pc++");
    equals(this.vm.accumulatorStack.length,1,"acc has 1 entry");
    ok(this.vm.accumulatorStack[0].isNumber(),"acc top is number");
    equals(this.vm.accumulatorStack[0].getNumber(),128,"acp top is 1t28");
    res = this.vm.step(new vm.Instruction(vm.opecode.load,[0,"i"]));//
    equals(this.vm.pc,5,"pc++");
    equals(this.vm.accumulatorStack.length,2,"acc has 2 entry");
    ok(this.vm.accumulatorStack[1].isNumber(),"acc top is number");
    equals(this.vm.accumulatorStack[1].getNumber(),129,"acp top is 129");

    this.vm.init();
    var obj = new jsobjects.JSObject();
    obj.setProperty("a",new jsobjects.JSNumber(1));
    this.vm.step(new vm.Instruction(vm.opecode.loadi,[obj]));
    equals(this.vm.pc,1,"pc++");
    this.vm.step(new vm.Instruction(vm.opecode.dup));
    equals(this.vm.pc,2,"pc++");
    this.vm.step(new vm.Instruction(vm.opecode.loadi,[new jsobjects.JSString("a")]));
    equals(this.vm.pc,3,"pc++");
    res = this.vm.step(new vm.Instruction(vm.opecode.inc_prop));
    ok(res,"continue exceute:inc_prop");
    equals(this.vm.pc,4,"pc++");
    equals(this.vm.accumulatorStack.length,2,"acc has 2 entry");
    ok(this.vm.accumulatorStack[1].isNumber(),"acc top is number");
    equals(this.vm.accumulatorStack[1].getNumber(),1,"acp top is 1");
    this.vm.step(new vm.Instruction(vm.opecode.pop));
    this.vm.step(new vm.Instruction(vm.opecode.loadi,[new jsobjects.JSString("a")]));
    this.vm.step(new vm.Instruction(vm.opecode.get_prop));
    equals(this.vm.accumulatorStack.length,1,"acc has 1 entry");
    ok(this.vm.accumulatorStack[0].isNumber(),"acc top is number");
    equals(this.vm.accumulatorStack[0].getNumber(),2,"acp top is 2");

    this.vm.init();
    res = this.vm.step(new vm.Instruction(vm.opecode.add_scope,["i"]));
    equals(this.vm.pc,1,"pc++");
    this.vm.step(new vm.Instruction(vm.opecode.loadi,[new jsobjects.JSNumber(128)]));
    equals(this.vm.pc,2,"pc++");
    res = this.vm.step(new vm.Instruction(vm.opecode.store,[0,"i"]));
    equals(this.vm.pc,3,"pc++");
    res = this.vm.step(new vm.Instruction(vm.opecode.dec,[0,"i"]));
    ok(res,"continue exceute:dec");
    equals(this.vm.pc,4,"pc++");
    equals(this.vm.accumulatorStack.length,1,"acc has 1 entry");
    ok(this.vm.accumulatorStack[0].isNumber(),"acc top is number");
    equals(this.vm.accumulatorStack[0].getNumber(),128,"acp top is 128");
    res = this.vm.step(new vm.Instruction(vm.opecode.load,[0,"i"]));//
    equals(this.vm.pc,5,"pc++");
    equals(this.vm.accumulatorStack.length,2,"acc has 2 entry");
    ok(this.vm.accumulatorStack[1].isNumber(),"acc top is number");
    equals(this.vm.accumulatorStack[1].getNumber(),127,"acp top is 127");

    this.vm.init();
    var obj = new jsobjects.JSObject();
    obj.setProperty("a",new jsobjects.JSNumber(1));
    this.vm.step(new vm.Instruction(vm.opecode.loadi,[obj]));
    equals(this.vm.pc,1,"pc++");
    this.vm.step(new vm.Instruction(vm.opecode.dup));
    equals(this.vm.pc,2,"pc++");
    this.vm.step(new vm.Instruction(vm.opecode.loadi,[new jsobjects.JSString("a")]));
    equals(this.vm.pc,3,"pc++");
    res = this.vm.step(new vm.Instruction(vm.opecode.dec_prop));
    ok(res,"continue exceute:dec_prop");
    equals(this.vm.pc,4,"pc++");
    equals(this.vm.accumulatorStack.length,2,"acc has 2 entry");
    ok(this.vm.accumulatorStack[1].isNumber(),"acc top is number");
    equals(this.vm.accumulatorStack[1].getNumber(),1,"acp top is 1");
    this.vm.step(new vm.Instruction(vm.opecode.pop));
    this.vm.step(new vm.Instruction(vm.opecode.loadi,[new jsobjects.JSString("a")]));
    this.vm.step(new vm.Instruction(vm.opecode.get_prop));
    equals(this.vm.accumulatorStack.length,1,"acc has 1 entry");
    ok(this.vm.accumulatorStack[0].isNumber(),"acc top is number");
    equals(this.vm.accumulatorStack[0].getNumber(),0,"acp top is 2");

    this.vm.init();
    res = this.vm.step(new vm.Instruction(vm.opecode.jump,[100]));
    ok(res,"continue exceute:jump");
    equals(this.vm.pc,100,"pc++");
    equals(this.vm.accumulatorStack.length,0,"acc has no entry");

    //true case
    this.vm.init();
    this.vm.step(new vm.Instruction(vm.opecode.loadi,[new jsobjects.JSBoolean(true)]));
    equals(this.vm.pc,1,"pc++");
    res = this.vm.step(new vm.Instruction(vm.opecode.jump_if_true,[100]));
    ok(res,"continue exceute:jump_if_true(true)");
    equals(this.vm.pc,100,"pc++");
    equals(this.vm.accumulatorStack.length,0,"acc has no entry");

    //false case
    this.vm.init();
    this.vm.step(new vm.Instruction(vm.opecode.loadi,[new jsobjects.JSBoolean(false)]));
    equals(this.vm.pc,1,"pc++");
    res = this.vm.step(new vm.Instruction(vm.opecode.jump_if_true,[100]));
    ok(res,"continue exceute:jump_if_true(false)");
    equals(this.vm.pc,2,"pc++");
    equals(this.vm.accumulatorStack.length,0,"acc has no entry");

    //true case
    this.vm.init();
    this.vm.step(new vm.Instruction(vm.opecode.loadi,[new jsobjects.JSBoolean(true)]));
    equals(this.vm.pc,1,"pc++");
    res = this.vm.step(new vm.Instruction(vm.opecode.jump_if_false,[100]));
    ok(res,"continue exceute:jump_if_false(true)");
    equals(this.vm.pc,2,"pc++");
    equals(this.vm.accumulatorStack.length,0,"acc has no entry");

    //false case
    this.vm.init();
    this.vm.step(new vm.Instruction(vm.opecode.loadi,[new jsobjects.JSBoolean(false)]));
    equals(this.vm.pc,1,"pc++");
    res = this.vm.step(new vm.Instruction(vm.opecode.jump_if_false,[100]));
    ok(res,"continue exceute:jump_if_false(false)");
    equals(this.vm.pc,100,"pc++");
    equals(this.vm.accumulatorStack.length,0,"acc has no entry");

    //true case
    this.vm.init();
    this.vm.step(new vm.Instruction(vm.opecode.loadi,[new jsobjects.JSBoolean(true)]));
    this.vm.step(new vm.Instruction(vm.opecode.loadi,[new jsobjects.JSBoolean(true)]));
    this.vm.step(new vm.Instruction(vm.opecode.loadi,[new jsobjects.JSBoolean(true)]));
    res = this.vm.step(new vm.Instruction(vm.opecode.jump_and_pop_if_equal,[100]));
    ok(res,"continue exceute:jump_and_pop_if_equal(true)");
    equals(this.vm.pc,100,"pc++");
    equals(this.vm.accumulatorStack.length,0,"acc has no entry");

    this.vm.init();
    this.vm.step(new vm.Instruction(vm.opecode.loadi,[new jsobjects.JSBoolean(true)]));
    this.vm.step(new vm.Instruction(vm.opecode.loadi,[new jsobjects.JSObject()]));
    this.vm.step(new vm.Instruction(vm.opecode.dup));
    res = this.vm.step(new vm.Instruction(vm.opecode.jump_and_pop_if_equal,[100]));
    ok(res,"continue exceute:jump_and_pop_if_equal(true)");
    equals(this.vm.pc,100,"pc++");
    equals(this.vm.accumulatorStack.length,0,"acc has no entry");

    //false case
    this.vm.init();
    this.vm.step(new vm.Instruction(vm.opecode.loadi,[new jsobjects.JSBoolean(true)]));
    this.vm.step(new vm.Instruction(vm.opecode.loadi,[new jsobjects.JSBoolean(false)]));
    this.vm.step(new vm.Instruction(vm.opecode.loadi,[new jsobjects.JSBoolean(true)]));
    res = this.vm.step(new vm.Instruction(vm.opecode.jump_and_pop_if_equal,[100]));
    ok(res,"continue exceute:jump_and_pop_if_equal(false)");
    equals(this.vm.pc,4,"pc++");
    equals(this.vm.accumulatorStack.length,1,"acc has 1 entry");

    this.vm.init();
    this.vm.step(new vm.Instruction(vm.opecode.loadi,[new jsobjects.JSBoolean(true)]));
    this.vm.step(new vm.Instruction(vm.opecode.loadi,[new jsobjects.JSObject()]));
    this.vm.step(new vm.Instruction(vm.opecode.loadi,[new jsobjects.JSObject()]));
    res = this.vm.step(new vm.Instruction(vm.opecode.jump_and_pop_if_equal,[100]));
    ok(res,"continue exceute:jump_and_pop_if_equal(false)");
    equals(this.vm.pc,4,"pc++");
    equals(this.vm.accumulatorStack.length,1,"acc has 1 entry");

    this.vm.init();
    res = this.vm.step(new vm.Instruction(vm.opecode.jump_subroutine,[100]));
    ok(res,"continue exceute:jump_subroutine");
    equals(this.vm.pc,100,"pc++");
    equals(this.vm.accumulatorStack.length,1,"acc has 1 entry");
    //TODO:スタックにある戻り値番地の検証

    this.vm.init();
    this.vm.global_object.setProperty("test",new jsobjects.JSNumber(123));
    this.vm.step(new vm.Instruction(vm.opecode.loadi,[new jsobjects.JSFunction(new jsobjects.JSFunctionDeclaration(100,["x"]),
                                                                               this.vm.callStack[0].scope)]));
    equals(this.vm.pc,1,"pc++");
    this.vm.step(new vm.Instruction(vm.opecode.loadi,[new jsobjects.JSNumber(100)]));
    equals(this.vm.pc,2,"pc++");
    res = this.vm.step(new vm.Instruction(vm.opecode.call,[1]));
    ok(res,"continue exceute:call(100)");
    equals(this.vm.pc,100,"pc++");
    equals(this.vm.accumulatorStack.length,0,"acc has no entry");
    equals(this.vm.callStack.length,2,"stack call stack");
    this.vm.step(new vm.Instruction(vm.opecode.load,[0,"x"]));
    equals(this.vm.accumulatorStack.length,1,"acc has 1 entry");
    ok(this.vm.accumulatorStack[0].isNumber(),"entry is Number");
    equals(this.vm.accumulatorStack[0].getNumber(),100,"entry is 100");
    this.vm.step(new vm.Instruction(vm.opecode.load,[1,"arguments"]));
    equals(this.vm.accumulatorStack.length,2,"acc has 2 entry:arguments");
    this.vm.step(new vm.Instruction(vm.opecode.loadi,[new jsobjects.JSString("0")]));
    equals(this.vm.accumulatorStack.length,3,"acc has 3 entry:arguments[0]");
    this.vm.step(new vm.Instruction(vm.opecode.get_prop));
    equals(this.vm.accumulatorStack.length,2,"acc has 2 entry");
    ok(this.vm.accumulatorStack[1].isNumber(),"entry is Number");
    equals(this.vm.accumulatorStack[1].getNumber(),100,"entry is 100");
    this.vm.step(new vm.Instruction(vm.opecode.pop));
    this.vm.step(new vm.Instruction(vm.opecode.load,[2,"test"]));
    equals(this.vm.accumulatorStack.length,2,"acc has 2 entry");
    ok(this.vm.accumulatorStack[1].isNumber(),"entry is Number");
    equals(this.vm.accumulatorStack[1].getNumber(),123,"entry is 123");
    this.vm.step(new vm.Instruction(vm.opecode.pop));
    /** arguments.callee recursive call */
    this.vm.step(new vm.Instruction(vm.opecode.load,[1,"arguments"]));
    this.vm.step(new vm.Instruction(vm.opecode.loadi,[new jsobjects.JSString("callee")]));
    this.vm.step(new vm.Instruction(vm.opecode.get_prop));
    equals(this.vm.accumulatorStack.length,2,"acc has 2 entry");
    ok(this.vm.accumulatorStack[1] instanceof jsobjects.JSFunction,"entry is function");
    this.vm.step(new vm.Instruction(vm.opecode.loadi,[new jsobjects.JSNumber(200)]));
    equals(this.vm.pc,111,"pc++");
    this.vm.step(new vm.Instruction(vm.opecode.call,[1]));
    this.vm.step(new vm.Instruction(vm.opecode.load,[0,"x"]));
    equals(this.vm.accumulatorStack[1].getNumber(),200,"entry is 200:recursive call(200)");
    /* returns */
    equals(this.vm.pc,101,"pc++");
    equals(this.vm.callStack.length,3,"stack call stack");
    this.vm.step(new vm.Instruction(vm.opecode.returns));;
    equals(this.vm.callStack.length,2,"stack call stack");
    equals(this.vm.pc,112,"pc++");
    equals(this.vm.accumulatorStack.length,2,"acc has 1 entry");
    ok(this.vm.accumulatorStack[1].isNumber(),"entry is Number");
    equals(this.vm.accumulatorStack[1].getNumber(),200,"entry is 200:returns");
    
    this.vm.init();
    this.vm.step(new vm.Instruction(vm.opecode.generate_func,[new jsobjects.JSFunctionDeclaration(100,["x"])]));
    equals(this.vm.pc,1,"pc++");
    this.vm.step(new vm.Instruction(vm.opecode.loadi,[new jsobjects.JSNumber(100)]));
    equals(this.vm.pc,2,"pc++");
    res = this.vm.step(new vm.Instruction(vm.opecode.call_new,[1]));
    ok(res,"continue exceute:call_new(100)");
    equals(this.vm.pc,100,"pc++");
    equals(this.vm.accumulatorStack.length,0,"acc has no entry");
    this.vm.step(new vm.Instruction(vm.opecode.load_this));
    equals(this.vm.accumulatorStack.length,1,"acc has 1 entry");
    this.vm.step(new vm.Instruction(vm.opecode.loadi,[new jsobjects.JSString("add")]));
    equals(this.vm.accumulatorStack.length,2,"acc has 2 entry");
    this.vm.step(new vm.Instruction(vm.opecode.loadi,[new jsobjects.JSString("I am member of this")]));
    equals(this.vm.accumulatorStack.length,3,"acc has 3 entry");
    this.vm.step(new vm.Instruction(vm.opecode.set_prop));
    this.vm.step(new vm.Instruction(vm.opecode.loadi,[new jsobjects.JSUndefined()]));
    this.vm.step(new vm.Instruction(vm.opecode.returns));// return;//
    equals(this.vm.accumulatorStack.length,1,"acc has 1 entry");
    this.vm.step(new vm.Instruction(vm.opecode.dup));
    this.vm.step(new vm.Instruction(vm.opecode.loadi,[new jsobjects.JSString("add")]));
    this.vm.step(new vm.Instruction(vm.opecode.get_prop));
    equals(this.vm.accumulatorStack.length,2,"acc has 2 entry");
    ok(this.vm.accumulatorStack[1].isString(),"entry is String");
    equals(this.vm.accumulatorStack[1].getString(),"I am member of this","entry is 'I am member of this':returns");
    
    this.vm.init();
    this.vm.global_object.setProperty("obj",new jsobjects.JSObject());
    this.vm.step(new vm.Instruction(vm.opecode.load,[0,"obj"]));
    this.vm.step(new vm.Instruction(vm.opecode.loadi,[new jsobjects.JSString("f")]));
    this.vm.step(new vm.Instruction(vm.opecode.generate_func,[new jsobjects.JSFunctionDeclaration(100,["x"])]));
    this.vm.step(new vm.Instruction(vm.opecode.set_prop));
    this.vm.step(new vm.Instruction(vm.opecode.load,[0,"obj"]));
    this.vm.step(new vm.Instruction(vm.opecode.dup));
    this.vm.step(new vm.Instruction(vm.opecode.loadi,[new jsobjects.JSString("f")]));
    this.vm.step(new vm.Instruction(vm.opecode.get_prop));
    equals(this.vm.accumulatorStack.length,2,"acc has 2 entry");
    ok(this.vm.accumulatorStack[0] instanceof jsobjects.JSObject,"entry 1 is Object");
    ok(this.vm.accumulatorStack[1] instanceof jsobjects.JSFunction,"entry 2 is Function");
    this.vm.step(new vm.Instruction(vm.opecode.loadi,[new jsobjects.JSNumber(100)]));
    res = this.vm.step(new vm.Instruction(vm.opecode.call_method,[1]));
    ok(res,"continue exceute:call_method  obj.f(100)");
    equals(this.vm.pc,100,"pc++");
    this.vm.step(new vm.Instruction(vm.opecode.load_this));
    this.vm.step(new vm.Instruction(vm.opecode.loadi,[new jsobjects.JSString("y")]));
    this.vm.step(new vm.Instruction(vm.opecode.load,[0,"x"]));
    this.vm.step(new vm.Instruction(vm.opecode.set_prop));//this.y = x
    this.vm.step(new vm.Instruction(vm.opecode.loadi,[new jsobjects.JSUndefined()]));
    this.vm.step(new vm.Instruction(vm.opecode.returns));// return;//
    this.vm.step(new vm.Instruction(vm.opecode.pop));//戻り値 undefinedを捨てる
    this.vm.step(new vm.Instruction(vm.opecode.load,[0,"obj"]));
    this.vm.step(new vm.Instruction(vm.opecode.loadi,[new jsobjects.JSString("y")]));
    this.vm.step(new vm.Instruction(vm.opecode.get_prop));
    equals(this.vm.accumulatorStack.length,1,"acc has 1 entry");
    ok(this.vm.accumulatorStack[0].isNumber(),"entry is Number");
    equals(this.vm.accumulatorStack[0].getNumber(),100,"entry is 100:obj.y==100");

    this.vm.init();
    this.vm.global_object.setProperty("test",new jsobjects.JSNumber(123));
    this.vm.step(new vm.Instruction(vm.opecode.add_scope,["i"]));
    this.vm.step(new vm.Instruction(vm.opecode.loadi,[new jsobjects.JSNumber(10)]));
    this.vm.step(new vm.Instruction(vm.opecode.store,[0,"i"]));
    this.vm.step(new vm.Instruction(vm.opecode.generate_func,[new jsobjects.JSFunctionDeclaration(100,["x"])]));
    equals(this.vm.pc,4,"pc++:generate_func");
    this.vm.step(new vm.Instruction(vm.opecode.store,[1,"test"]));
    this.vm.step(new vm.Instruction(vm.opecode.remove_scope,[]));
    this.vm.step(new vm.Instruction(vm.opecode.load,[0,"test"]));
    res = this.vm.step(new vm.Instruction(vm.opecode.call,[0]));
    this.vm.step(new vm.Instruction(vm.opecode.inc,[2,"i"]));
    ok(this.vm.accumulatorStack[0].isNumber(),"entry is Number");
    equals(this.vm.accumulatorStack[0].getNumber(),10,"entry is 10:generate_func:i++");
    this.vm.step(new vm.Instruction(vm.opecode.pop));
    this.vm.step(new vm.Instruction(vm.opecode.inc,[2,"i"]));
    ok(this.vm.accumulatorStack[0].isNumber(),"entry is Number");
    equals(this.vm.accumulatorStack[0].getNumber(),11,"entry is 11:generate_func:i++");
    this.vm.step(new vm.Instruction(vm.opecode.returns));;
    this.vm.step(new vm.Instruction(vm.opecode.load,[0,"test"]));
    this.vm.step(new vm.Instruction(vm.opecode.call,[0]));
    this.vm.step(new vm.Instruction(vm.opecode.load,[2,"i"]));
    ok(this.vm.accumulatorStack[1].isNumber(),"entry is Number");
    equals(this.vm.accumulatorStack[1].getNumber(),12,"entry is 12:generate_func:i++:closure");

    this.vm.init();
    this.vm.step(new vm.Instruction(vm.opecode.loadi,[new jsobjects.JSString("toString")]));
    equals(this.vm.pc,1,"pc++");
    this.vm.step(new vm.Instruction(vm.opecode.loadi,[new jsobjects.JSObject()]));
    equals(this.vm.pc,2,"pc++");
    res = this.vm.step(new vm.Instruction(vm.opecode.in_));
    ok(res,"continue exceute:in_");
    equals(this.vm.pc,3,"pc++");
    equals(this.vm.accumulatorStack.length,1,"acc has 1 entry");
    ok(!this.vm.accumulatorStack[0].isTrue(),"in fail");

    var obj =new jsobjects.JSObject();
    obj.setProperty("toString",new jsobjects.JSString("test"));
    this.vm.init();
    this.vm.step(new vm.Instruction(vm.opecode.loadi,[new jsobjects.JSString("toString")]));
    equals(this.vm.pc,1,"pc++");
    this.vm.step(new vm.Instruction(vm.opecode.loadi,[obj]));
    equals(this.vm.pc,2,"pc++");
    res = this.vm.step(new vm.Instruction(vm.opecode.in_));
    ok(res,"continue exceute:in_");
    equals(this.vm.pc,3,"pc++");
    equals(this.vm.accumulatorStack.length,1,"acc has 1 entry");
    ok(this.vm.accumulatorStack[0].isTrue(),"in success");

    this.vm.init();
    this.vm.global_object.setProperty("f",new jsobjects.JSUndefined());
    this.vm.global_object.setProperty("g",new jsobjects.JSObject());
    this.vm.global_object.getProperty("g").setProperty("x",new jsobjects.JSNumber(12));
    this.vm.step(new vm.Instruction(vm.opecode.generate_func,[new jsobjects.JSFunctionDeclaration(100,["x"])]));
    this.vm.step(new vm.Instruction(vm.opecode.store,[0,"f"]));
    this.vm.step(new vm.Instruction(vm.opecode.load,[0,"f"]));
    this.vm.step(new vm.Instruction(vm.opecode.loadi,[new jsobjects.JSString("prototype")]));
    this.vm.step(new vm.Instruction(vm.opecode.load,[0,"g"]));
    this.vm.step(new vm.Instruction(vm.opecode.set_prop));//f.prototype = g
    this.vm.step(new vm.Instruction(vm.opecode.load,[0,"f"]));
    this.vm.step(new vm.Instruction(vm.opecode.call_new,[0]));
    this.vm.step(new vm.Instruction(vm.opecode.loadi,[new jsobjects.JSUndefined()]));
    this.vm.step(new vm.Instruction(vm.opecode.returns));// return;//
    this.vm.step(new vm.Instruction(vm.opecode.dup));
    this.vm.step(new vm.Instruction(vm.opecode.load,[0,"f"]));
    equals(this.vm.pc,10,"pc++");
    res = this.vm.step(new vm.Instruction(vm.opecode.instanceof_));
    ok(res,"continue exceute:instanceof_");
    equals(this.vm.pc,11,"pc++");
    equals(this.vm.accumulatorStack.length,2,"acc has 2 entry");
    ok(this.vm.accumulatorStack[0].isTrue(),"instanceof success");
    this.vm.step(new vm.Instruction(vm.opecode.pop));// consume true
    this.vm.step(new vm.Instruction(vm.opecode.loadi,[new jsobjects.JSString("x")]));
    this.vm.step(new vm.Instruction(vm.opecode.get_prop));//inst.a 
    //console.log(this.vm.accumulatorStack);
    equals(this.vm.accumulatorStack[0].getNumber(),12,"inst.x === 12");

    this.vm.init();
    this.vm.step(new vm.Instruction(vm.opecode.loadi,[new jsobjects.JSNumber(2)]));
    equals(this.vm.pc,1,"pc++");
    this.vm.step(new vm.Instruction(vm.opecode.loadi,[new jsobjects.JSNumber(3)]));
    equals(this.vm.pc,2,"pc++");
    res = this.vm.step(new vm.Instruction(vm.opecode.add));
    ok(res,"continue exceute:add(num+num)");
    equals(this.vm.pc,3,"pc++");
    equals(this.vm.accumulatorStack.length,1,"acc has 1 entry");
    ok(this.vm.accumulatorStack[0].isNumber(),"res is Number");
    equals(this.vm.accumulatorStack[0].getNumber(),5,"res is 5");

    this.vm.init();
    this.vm.step(new vm.Instruction(vm.opecode.loadi,[new jsobjects.JSString("this is ")]));
    equals(this.vm.pc,1,"pc++");
    this.vm.step(new vm.Instruction(vm.opecode.loadi,[new jsobjects.JSNumber(3)]));
    equals(this.vm.pc,2,"pc++");
    res = this.vm.step(new vm.Instruction(vm.opecode.add));
    ok(res,"continue exceute:add(str+num)");
    equals(this.vm.pc,3,"pc++");
    equals(this.vm.accumulatorStack.length,1,"acc has 1 entry");
    ok(this.vm.accumulatorStack[0].isString(),"res is String");
    equals(this.vm.accumulatorStack[0].getString(),"this is 3","res is 'this is 3'");

    this.vm.init();
    this.vm.step(new vm.Instruction(vm.opecode.loadi,[new jsobjects.JSNumber(2)]));
    equals(this.vm.pc,1,"pc++");
    this.vm.step(new vm.Instruction(vm.opecode.loadi,[new jsobjects.JSNumber(3)]));
    equals(this.vm.pc,2,"pc++");
    res = this.vm.step(new vm.Instruction(vm.opecode.sub));
    ok(res,"continue exceute:sub(num-num)");
    equals(this.vm.pc,3,"pc++");
    equals(this.vm.accumulatorStack.length,1,"acc has 1 entry");
    ok(this.vm.accumulatorStack[0].isNumber(),"res is Number");
    equals(this.vm.accumulatorStack[0].getNumber(),-1,"res is -1");

    this.vm.init();
    this.vm.step(new vm.Instruction(vm.opecode.loadi,[new jsobjects.JSNumber(2)]));
    equals(this.vm.pc,1,"pc++");
    this.vm.step(new vm.Instruction(vm.opecode.loadi,[new jsobjects.JSNumber(3)]));
    equals(this.vm.pc,2,"pc++");
    res = this.vm.step(new vm.Instruction(vm.opecode.mul));
    ok(res,"continue exceute:mul(num*num)");
    equals(this.vm.pc,3,"pc++");
    equals(this.vm.accumulatorStack.length,1,"acc has 1 entry");
    ok(this.vm.accumulatorStack[0].isNumber(),"res is Number");
    equals(this.vm.accumulatorStack[0].getNumber(),6,"res is 6");

    this.vm.init();
    this.vm.step(new vm.Instruction(vm.opecode.loadi,[new jsobjects.JSNumber(6)]));
    equals(this.vm.pc,1,"pc++");
    this.vm.step(new vm.Instruction(vm.opecode.loadi,[new jsobjects.JSNumber(3)]));
    equals(this.vm.pc,2,"pc++");
    res = this.vm.step(new vm.Instruction(vm.opecode.div));
    ok(res,"continue exceute:div(num/num)");
    equals(this.vm.pc,3,"pc++");
    equals(this.vm.accumulatorStack.length,1,"acc has 1 entry");
    ok(this.vm.accumulatorStack[0].isNumber(),"res is Number");
    equals(this.vm.accumulatorStack[0].getNumber(),2,"res is 2");

    this.vm.init();
    this.vm.step(new vm.Instruction(vm.opecode.loadi,[new jsobjects.JSNumber(6)]));
    equals(this.vm.pc,1,"pc++");
    this.vm.step(new vm.Instruction(vm.opecode.loadi,[new jsobjects.JSNumber(5)]));
    equals(this.vm.pc,2,"pc++");
    res = this.vm.step(new vm.Instruction(vm.opecode.mod));
    ok(res,"continue exceute:mod(num%num)");
    equals(this.vm.pc,3,"pc++");
    equals(this.vm.accumulatorStack.length,1,"acc has 1 entry");
    ok(this.vm.accumulatorStack[0].isNumber(),"res is Number");
    equals(this.vm.accumulatorStack[0].getNumber(),1,"res is 1");

    this.vm.init();
    this.vm.step(new vm.Instruction(vm.opecode.loadi,[new jsobjects.JSNumber(0xffff0000)]));
    equals(this.vm.pc,1,"pc++");
    this.vm.step(new vm.Instruction(vm.opecode.loadi,[new jsobjects.JSNumber(0x0f0f0f0f)]));
    equals(this.vm.pc,2,"pc++");
    res = this.vm.step(new vm.Instruction(vm.opecode.bit_and));
    ok(res,"continue exceute:bit_and(num&num)");
    equals(this.vm.pc,3,"pc++");
    equals(this.vm.accumulatorStack.length,1,"acc has 1 entry");
    ok(this.vm.accumulatorStack[0].isNumber(),"res is Number");
    equals(this.vm.accumulatorStack[0].getNumber(),0xffff0000&0x0f0f0f0f,"res is ");

    this.vm.init();
    this.vm.step(new vm.Instruction(vm.opecode.loadi,[new jsobjects.JSNumber(0xffff0000)]));
    equals(this.vm.pc,1,"pc++");
    this.vm.step(new vm.Instruction(vm.opecode.loadi,[new jsobjects.JSNumber(0x0f0f0f0f)]));
    equals(this.vm.pc,2,"pc++");
    res = this.vm.step(new vm.Instruction(vm.opecode.bit_or));
    ok(res,"continue exceute:bit_or(num|num)");
    equals(this.vm.pc,3,"pc++");
    equals(this.vm.accumulatorStack.length,1,"acc has 1 entry");
    ok(this.vm.accumulatorStack[0].isNumber(),"res is Number");
    equals(this.vm.accumulatorStack[0].getNumber(),0xffff0000|0x0f0f0f0f,"res is ");

    this.vm.init();
    this.vm.step(new vm.Instruction(vm.opecode.loadi,[new jsobjects.JSNumber(0xffff0000)]));
    equals(this.vm.pc,1,"pc++");
    this.vm.step(new vm.Instruction(vm.opecode.loadi,[new jsobjects.JSNumber(0x0f0f0f0f)]));
    equals(this.vm.pc,2,"pc++");
    res = this.vm.step(new vm.Instruction(vm.opecode.bit_xor));
    ok(res,"continue exceute:bit_xor(num^num)");
    equals(this.vm.pc,3,"pc++");
    equals(this.vm.accumulatorStack.length,1,"acc has 1 entry");
    ok(this.vm.accumulatorStack[0].isNumber(),"res is Number");
    equals(this.vm.accumulatorStack[0].getNumber(),0xffff0000^0x0f0f0f0f,"res is ");

    this.vm.init();
    this.vm.step(new vm.Instruction(vm.opecode.loadi,[new jsobjects.JSNumber(0x0000ffff)]));
    equals(this.vm.pc,1,"pc++");
    this.vm.step(new vm.Instruction(vm.opecode.loadi,[new jsobjects.JSNumber(0x0f)]));
    equals(this.vm.pc,2,"pc++");
    res = this.vm.step(new vm.Instruction(vm.opecode.bit_shl));
    ok(res,"continue exceute:bit_shl(num<<num)");
    equals(this.vm.pc,3,"pc++");
    equals(this.vm.accumulatorStack.length,1,"acc has 1 entry");
    ok(this.vm.accumulatorStack[0].isNumber(),"res is Number");
    equals(this.vm.accumulatorStack[0].getNumber(),0x0000ffff<<0x0f,"res is ");

    this.vm.init();
    this.vm.step(new vm.Instruction(vm.opecode.loadi,[new jsobjects.JSNumber(0xffff0000)]));
    equals(this.vm.pc,1,"pc++");
    this.vm.step(new vm.Instruction(vm.opecode.loadi,[new jsobjects.JSNumber(0x0f)]));
    equals(this.vm.pc,2,"pc++");
    res = this.vm.step(new vm.Instruction(vm.opecode.bit_shr));
    ok(res,"continue exceute:bit_shr(num>>num)");
    equals(this.vm.pc,3,"pc++");
    equals(this.vm.accumulatorStack.length,1,"acc has 1 entry");
    ok(this.vm.accumulatorStack[0].isNumber(),"res is Number");
    equals(this.vm.accumulatorStack[0].getNumber(),0xffff0000>>0x0f,"res is ");

    this.vm.init();
    this.vm.step(new vm.Instruction(vm.opecode.loadi,[new jsobjects.JSNumber(0xffff0000)]));
    equals(this.vm.pc,1,"pc++");
    this.vm.step(new vm.Instruction(vm.opecode.loadi,[new jsobjects.JSNumber(0x0f)]));
    equals(this.vm.pc,2,"pc++");
    res = this.vm.step(new vm.Instruction(vm.opecode.bit_shr_arith));
    ok(res,"continue exceute:bit_shr_arith(num>>>num)");
    equals(this.vm.pc,3,"pc++");
    equals(this.vm.accumulatorStack.length,1,"acc has 1 entry");
    ok(this.vm.accumulatorStack[0].isNumber(),"res is Number");
    equals(this.vm.accumulatorStack[0].getNumber(),0xffff0000>>>0x0f,"res is ");

    this.vm.init();
    this.vm.step(new vm.Instruction(vm.opecode.loadi,[new jsobjects.JSNumber(6)]));
    equals(this.vm.pc,1,"pc++");
    this.vm.step(new vm.Instruction(vm.opecode.loadi,[new jsobjects.JSNumber(6)]));
    equals(this.vm.pc,2,"pc++");
    res = this.vm.step(new vm.Instruction(vm.opecode.eq));
    ok(res,"continue exceute:eq(num==num)");
    equals(this.vm.pc,3,"pc++");
    equals(this.vm.accumulatorStack.length,1,"acc has 1 entry");
    ok(this.vm.accumulatorStack[0].isTrue(),"eq");

    this.vm.init();
    this.vm.step(new vm.Instruction(vm.opecode.loadi,[new jsobjects.JSNumber(6)]));
    equals(this.vm.pc,1,"pc++");
    this.vm.step(new vm.Instruction(vm.opecode.loadi,[new jsobjects.JSString("6")]));
    equals(this.vm.pc,2,"pc++");
    res = this.vm.step(new vm.Instruction(vm.opecode.eq));
    ok(res,"continue exceute:eq(num==num)");
    equals(this.vm.pc,3,"pc++");
    equals(this.vm.accumulatorStack.length,1,"acc has 1 entry");
    ok(this.vm.accumulatorStack[0].isTrue(),"eq");

    this.vm.init();
    this.vm.step(new vm.Instruction(vm.opecode.loadi,[new jsobjects.JSNumber(6)]));
    equals(this.vm.pc,1,"pc++");
    this.vm.step(new vm.Instruction(vm.opecode.loadi,[new jsobjects.JSNumber(6)]));
    equals(this.vm.pc,2,"pc++");
    res = this.vm.step(new vm.Instruction(vm.opecode.eqs));
    ok(res,"continue exceute:eqs(num===num)");
    equals(this.vm.pc,3,"pc++");
    equals(this.vm.accumulatorStack.length,1,"acc has 1 entry");
    ok(this.vm.accumulatorStack[0].isTrue(),"eq");

    this.vm.init();
    this.vm.step(new vm.Instruction(vm.opecode.loadi,[new jsobjects.JSNumber(6)]));
    equals(this.vm.pc,1,"pc++");
    this.vm.step(new vm.Instruction(vm.opecode.loadi,[new jsobjects.JSString("6")]));
    equals(this.vm.pc,2,"pc++");
    res = this.vm.step(new vm.Instruction(vm.opecode.eqs));
    ok(res,"continue exceute:eqs(num===num)");
    equals(this.vm.pc,3,"pc++");
    equals(this.vm.accumulatorStack.length,1,"acc has 1 entry");
    ok(!this.vm.accumulatorStack[0].isTrue(),"eq");

    this.vm.init();
    this.vm.step(new vm.Instruction(vm.opecode.loadi,[new jsobjects.JSNumber(6)]));
    equals(this.vm.pc,1,"pc++");
    this.vm.step(new vm.Instruction(vm.opecode.loadi,[new jsobjects.JSNumber(5)]));
    equals(this.vm.pc,2,"pc++");
    res = this.vm.step(new vm.Instruction(vm.opecode.neq));
    ok(res,"continue exceute:neq(num!=num)");
    equals(this.vm.pc,3,"pc++");
    equals(this.vm.accumulatorStack.length,1,"acc has 1 entry");
    ok(this.vm.accumulatorStack[0].isTrue(),"neq");

    this.vm.init();
    this.vm.step(new vm.Instruction(vm.opecode.loadi,[new jsobjects.JSNumber(6)]));
    equals(this.vm.pc,1,"pc++");
    this.vm.step(new vm.Instruction(vm.opecode.loadi,[new jsobjects.JSString("6")]));
    equals(this.vm.pc,2,"pc++");
    res = this.vm.step(new vm.Instruction(vm.opecode.neq));
    ok(res,"continue exceute:neq(num!=num)");
    equals(this.vm.pc,3,"pc++");
    equals(this.vm.accumulatorStack.length,1,"acc has 1 entry");
    ok(!this.vm.accumulatorStack[0].isTrue(),"neq");

    this.vm.init();
    this.vm.step(new vm.Instruction(vm.opecode.loadi,[new jsobjects.JSNumber(6)]));
    equals(this.vm.pc,1,"pc++");
    this.vm.step(new vm.Instruction(vm.opecode.loadi,[new jsobjects.JSNumber(5)]));
    equals(this.vm.pc,2,"pc++");
    res = this.vm.step(new vm.Instruction(vm.opecode.neqs));
    ok(res,"continue exceute:neqs(num!==num)");
    equals(this.vm.pc,3,"pc++");
    equals(this.vm.accumulatorStack.length,1,"acc has 1 entry");
    ok(this.vm.accumulatorStack[0].isTrue(),"neqs");

    this.vm.init();
    this.vm.step(new vm.Instruction(vm.opecode.loadi,[new jsobjects.JSNumber(6)]));
    equals(this.vm.pc,1,"pc++");
    this.vm.step(new vm.Instruction(vm.opecode.loadi,[new jsobjects.JSString("6")]));
    equals(this.vm.pc,2,"pc++");
    res = this.vm.step(new vm.Instruction(vm.opecode.neqs));
    ok(res,"continue exceute:neqs(num!==num)");
    equals(this.vm.pc,3,"pc++");
    equals(this.vm.accumulatorStack.length,1,"acc has 1 entry");
    ok(this.vm.accumulatorStack[0].isTrue(),"neqs");

    this.vm.init();
    this.vm.step(new vm.Instruction(vm.opecode.loadi,[new jsobjects.JSNumber(4)]));
    equals(this.vm.pc,1,"pc++");
    this.vm.step(new vm.Instruction(vm.opecode.loadi,[new jsobjects.JSNumber(5)]));
    equals(this.vm.pc,2,"pc++");
    res = this.vm.step(new vm.Instruction(vm.opecode.lt));
    ok(res,"continue exceute:lt(num<num)");
    equals(this.vm.pc,3,"pc++");
    equals(this.vm.accumulatorStack.length,1,"acc has 1 entry");
    ok(this.vm.accumulatorStack[0].isTrue(),"lt");

    this.vm.init();
    this.vm.step(new vm.Instruction(vm.opecode.loadi,[new jsobjects.JSNumber(6)]));
    equals(this.vm.pc,1,"pc++");
    this.vm.step(new vm.Instruction(vm.opecode.loadi,[new jsobjects.JSNumber(5)]));
    equals(this.vm.pc,2,"pc++");
    res = this.vm.step(new vm.Instruction(vm.opecode.gt));
    ok(res,"continue exceute:gt(num>num)");
    equals(this.vm.pc,3,"pc++");
    equals(this.vm.accumulatorStack.length,1,"acc has 1 entry");
    ok(this.vm.accumulatorStack[0].isTrue(),"gt");

    this.vm.init();
    this.vm.step(new vm.Instruction(vm.opecode.loadi,[new jsobjects.JSNumber(4)]));
    equals(this.vm.pc,1,"pc++");
    this.vm.step(new vm.Instruction(vm.opecode.loadi,[new jsobjects.JSNumber(5)]));
    equals(this.vm.pc,2,"pc++");
    res = this.vm.step(new vm.Instruction(vm.opecode.leq));
    ok(res,"continue exceute:leq(num<=num)");
    equals(this.vm.pc,3,"pc++");
    equals(this.vm.accumulatorStack.length,1,"acc has 1 entry");
    ok(this.vm.accumulatorStack[0].isTrue(),"leq");

    this.vm.init();
    this.vm.step(new vm.Instruction(vm.opecode.loadi,[new jsobjects.JSNumber(6)]));
    equals(this.vm.pc,1,"pc++");
    this.vm.step(new vm.Instruction(vm.opecode.loadi,[new jsobjects.JSNumber(5)]));
    equals(this.vm.pc,2,"pc++");
    res = this.vm.step(new vm.Instruction(vm.opecode.geq));
    ok(res,"continue exceute:geq(num>=num)");
    equals(this.vm.pc,3,"pc++");
    equals(this.vm.accumulatorStack.length,1,"acc has 1 entry");
    ok(this.vm.accumulatorStack[0].isTrue(),"geq");

    this.vm.init();
    this.vm.step(new vm.Instruction(vm.opecode.loadi,[new jsobjects.JSNumber(4)]));
    equals(this.vm.pc,1,"pc++");
    this.vm.step(new vm.Instruction(vm.opecode.loadi,[new jsobjects.JSNumber(4)]));
    equals(this.vm.pc,2,"pc++");
    res = this.vm.step(new vm.Instruction(vm.opecode.leq));
    ok(res,"continue exceute:leq(num<=num)");
    equals(this.vm.pc,3,"pc++");
    equals(this.vm.accumulatorStack.length,1,"acc has 1 entry");
    ok(this.vm.accumulatorStack[0].isTrue(),"leq");

    this.vm.init();
    this.vm.step(new vm.Instruction(vm.opecode.loadi,[new jsobjects.JSNumber(6)]));
    equals(this.vm.pc,1,"pc++");
    this.vm.step(new vm.Instruction(vm.opecode.loadi,[new jsobjects.JSNumber(5)]));
    equals(this.vm.pc,2,"pc++");
    res = this.vm.step(new vm.Instruction(vm.opecode.geq));
    ok(res,"continue exceute:geq(num>=num)");
    equals(this.vm.pc,3,"pc++");
    equals(this.vm.accumulatorStack.length,1,"acc has 1 entry");
    ok(this.vm.accumulatorStack[0].isTrue(),"geq");

    this.vm.init();
    var obj = new jsobjects.JSObject();
    obj.setProperty("length",new jsobjects.JSNumber(1));
    this.vm.step(new vm.Instruction(vm.opecode.loadi,[obj]));
    equals(this.vm.pc,1,"pc++");
    this.vm.step(new vm.Instruction(vm.opecode.dup));
    equals(this.vm.pc,2,"pc++");
    this.vm.step(new vm.Instruction(vm.opecode.loadi,[new jsobjects.JSString("length")]));
    equals(this.vm.pc,3,"pc++");
    res = this.vm.step(new vm.Instruction(vm.opecode.del_prop));
    ok(res,"continue exceute:del_prop");
    equals(this.vm.pc,4,"pc++");
    equals(this.vm.accumulatorStack.length,2,"acc has 2 entry");
    ok(this.vm.accumulatorStack[0].isTrue(),"delete success");
    this.vm.step(new vm.Instruction(vm.opecode.pop));
    this.vm.step(new vm.Instruction(vm.opecode.loadi,[new jsobjects.JSString("length")]));
    this.vm.step(new vm.Instruction(vm.opecode.get_prop));
    equals(this.vm.accumulatorStack.length,1,"acc has 1 entry");
    ok(this.vm.accumulatorStack[0].isUndefined(),"delete success");

    this.vm.init();
    this.vm.global_object.setProperty("test",new jsobjects.JSNumber(123));
    this.vm.step(new vm.Instruction(vm.opecode.load,[0,"test"]));//
    equals(this.vm.accumulatorStack.length,1,"load from global object");
    equals(this.vm.accumulatorStack[0].getNumber(),123,"loaded value is 123");
    this.vm.step(new vm.Instruction(vm.opecode.del_var,[0,"test"]));
    try{
      this.vm.step(new vm.Instruction(vm.opecode.load,[0,"test"]));//
      ok(false,"cant reach here");
    }catch(e){
      ok(true,"reference erro occur");
    }
    
    this.vm.init();
    this.vm.step(new vm.Instruction(vm.opecode.loadi,[new jsobjects.JSString("6")]));
    equals(this.vm.pc,1,"pc++");
    res = this.vm.step(new vm.Instruction(vm.opecode.mul_plus));
    ok(res,"continue exceute:mul_plus");
    equals(this.vm.pc,2,"pc++");
    equals(this.vm.accumulatorStack.length,1,"acc has 1 entry");
    equals(this.vm.accumulatorStack[0].getNumber(),6,"res is 6");

    this.vm.init();
    this.vm.step(new vm.Instruction(vm.opecode.loadi,[new jsobjects.JSString("6")]));
    equals(this.vm.pc,1,"pc++");
    res = this.vm.step(new vm.Instruction(vm.opecode.mul_minus));
    ok(res,"continue exceute:mul_minus");
    equals(this.vm.pc,2,"pc++");
    equals(this.vm.accumulatorStack.length,1,"acc has 1 entry");
    equals(this.vm.accumulatorStack[0].getNumber(),-6,"res is 6");

    this.vm.init();
    this.vm.step(new vm.Instruction(vm.opecode.loadi,[new jsobjects.JSNumber(0xff)]));
    equals(this.vm.pc,1,"pc++");
    res = this.vm.step(new vm.Instruction(vm.opecode.comp));
    ok(res,"continue exceute:comp");
    equals(this.vm.pc,2,"pc++");
    equals(this.vm.accumulatorStack.length,1,"acc has 1 entry");
    equals(this.vm.accumulatorStack[0].getNumber(),~0xff,"res is ~0xff");

    this.vm.init();
    this.vm.step(new vm.Instruction(vm.opecode.loadi,[new jsobjects.JSBoolean(false)]));
    equals(this.vm.pc,1,"pc++");
    res = this.vm.step(new vm.Instruction(vm.opecode.not));
    ok(res,"continue exceute:not");
    equals(this.vm.pc,2,"pc++");
    equals(this.vm.accumulatorStack.length,1,"acc has 1 entry");
    ok(this.vm.accumulatorStack[0].isTrue(),"res is true");

    this.vm.init();
    this.vm.step(new vm.Instruction(vm.opecode.loadi,[new jsobjects.JSNumber(6)]));
    equals(this.vm.pc,1,"pc++");
    res = this.vm.step(new vm.Instruction(vm.opecode.sub_1));
    ok(res,"continue exceute:sub_1");
    equals(this.vm.pc,2,"pc++");
    equals(this.vm.accumulatorStack.length,1,"acc has 1 entry");
    equals(this.vm.accumulatorStack[0].getNumber(),5,"res is 5");

    this.vm.init();
    this.vm.step(new vm.Instruction(vm.opecode.loadi,[new jsobjects.JSNumber(6)]));
    equals(this.vm.pc,1,"pc++");
    res = this.vm.step(new vm.Instruction(vm.opecode.add_1));
    ok(res,"continue exceute:add_1");
    equals(this.vm.pc,2,"pc++");
    equals(this.vm.accumulatorStack.length,1,"acc has 1 entry");
    equals(this.vm.accumulatorStack[0].getNumber(),7,"res is 7");

    //get_iterator
    this.vm.init();
    this.vm.step(new vm.Instruction(vm.opecode.loadi,[new jsobjects.JSObject()]));
    equals(this.vm.pc,1,"pc++");
    res = this.vm.step(new vm.Instruction(vm.opecode.get_iterator,[]));
    ok(res,"continue exceute:get_iterator");
    equals(this.vm.pc,2,"pc++");
    equals(this.vm.accumulatorStack.length,1,"acc has 1 entry");
    ok(this.vm.accumulatorStack[0] instanceof jsobjects.JSIterator,"res is JSIterator");
    ok(!this.vm.accumulatorStack[0].hasNext(),"res has no next");

    this.vm.init();
    this.vm.step(new vm.Instruction(vm.opecode.loadi,[new jsobjects.JSObject()]));//loadi obj
    this.vm.step(new vm.Instruction(vm.opecode.dup,[]));//dup
    this.vm.step(new vm.Instruction(vm.opecode.loadi,[new jsobjects.JSString("key")]));//loadi key
    this.vm.step(new vm.Instruction(vm.opecode.loadi,[new jsobjects.JSNumber(128)]));//loadi value 
    this.vm.step(new vm.Instruction(vm.opecode.set_prop,[]));//set_prop
    this.vm.step(new vm.Instruction(vm.opecode.get_iterator,[]));//get_iterator
    equals(this.vm.accumulatorStack.length,1,"acc has 1 entry:get_iterator case 2");
    ok(this.vm.accumulatorStack[0] instanceof jsobjects.JSIterator,"res is JSIterator");
    ok(this.vm.accumulatorStack[0].hasNext(),"res has next");
    equals(this.vm.accumulatorStack[0].getNext(),"key","next key is 'key'");

    //has_next get_next
    this.vm.init();
    this.vm.step(new vm.Instruction(vm.opecode.loadi,[new jsobjects.JSObject()]));//loadi obj
    this.vm.step(new vm.Instruction(vm.opecode.dup,[]));//dup
    this.vm.step(new vm.Instruction(vm.opecode.loadi,[new jsobjects.JSString("key")]));//loadi key
    this.vm.step(new vm.Instruction(vm.opecode.loadi,[new jsobjects.JSNumber(128)]));//loadi value 
    this.vm.step(new vm.Instruction(vm.opecode.set_prop,[]));//set_prop
    this.vm.step(new vm.Instruction(vm.opecode.get_iterator,[]));//get_iterator
    this.vm.step(new vm.Instruction(vm.opecode.dup,[]));//dup
    equals(this.vm.accumulatorStack.length,2,"acc has 2 entry: has_next");
    res = this.vm.step(new vm.Instruction(vm.opecode.has_next,[]));
    ok(res,"continue exceute:has_next");
    equals(this.vm.pc,8,"pc++");
    equals(this.vm.accumulatorStack.length,2,"acc has 2 entry: has_next");
    ok(this.vm.accumulatorStack[1].isTrue(),"res is true");
    this.vm.step(new vm.Instruction(vm.opecode.pop,[]));//pop
    equals(this.vm.accumulatorStack.length,1,"acc has 1 entry: get_next");

    res = this.vm.step(new vm.Instruction(vm.opecode.get_next,[]));
    ok(res,"continue exceute:get_next");
    equals(this.vm.pc,10,"pc++");
    equals(this.vm.accumulatorStack.length,1,"acc has 1 entry: get_next");
    ok(this.vm.accumulatorStack[0].isString(),"res is string");
    equals(this.vm.accumulatorStack[0].getString(),"key","res is string 'key'");

    //rot3
    this.vm.init();
    this.vm.step(new vm.Instruction(vm.opecode.loadi,[new jsobjects.JSNumber(1)]));//loadi value 
    this.vm.step(new vm.Instruction(vm.opecode.loadi,[new jsobjects.JSNumber(2)]));//loadi value 
    this.vm.step(new vm.Instruction(vm.opecode.loadi,[new jsobjects.JSNumber(3)]));//loadi value 
    equals(this.vm.pc,3,"pc++");
    res = this.vm.step(new vm.Instruction(vm.opecode.rot3,[]));
    ok(res,"continue exceute:rot3");
    equals(this.vm.pc,4,"pc++");
    equals(this.vm.accumulatorStack.length,3,"acc has 3 entry: rot3");
    equals(this.vm.accumulatorStack[0].getNumber(),2,"res is 2");
    equals(this.vm.accumulatorStack[1].getNumber(),3,"res is 3");
    equals(this.vm.accumulatorStack[2].getNumber(),1,"res is 1");

    //TODO: implement enum_keys
    //this.vm.init();
    //this.vm.step(new vm.Instruction(vm.opecode.loadi,[new jsobjects.JSNumber(6)]));
    //equals(this.vm.pc,1,"pc++");
    //res = this.vm.step(new vm.Instruction(vm.opecode.del));
    //ok(res,"continue exceute:del");
    //equals(this.vm.pc,2,"pc++");
    //equals(this.vm.accumulatorStack.length,1,"acc has 1 entry");

    //TODO: implement array_pop 
    //this.vm.init();
    //this.vm.step(new vm.Instruction(vm.opecode.loadi,[new jsobjects.JSNumber(6)]));
    //equals(this.vm.pc,1,"pc++");
    //res = this.vm.step(new vm.Instruction(vm.opecode.del));
    //ok(res,"continue exceute:del");
    //equals(this.vm.pc,2,"pc++");
    //equals(this.vm.accumulatorStack.length,1,"acc has 1 entry");

    //TODO: implement throw_
    //this.vm.init();
    //this.vm.step(new vm.Instruction(vm.opecode.loadi,[new jsobjects.JSNumber(6)]));
    //equals(this.vm.pc,1,"pc++");
    //res = this.vm.step(new vm.Instruction(vm.opecode.del));
    //ok(res,"continue exceute:del");
    //equals(this.vm.pc,2,"pc++");
    //equals(this.vm.accumulatorStack.length,1,"acc has 1 entry");

  });
  test("exec",function(){
    var self = this;
    var init = function(code) {
      self.vm.init();
      self.vm.load(code);
    };

    init([new vm.Instruction(vm.opecode.loadi,[new jsobjects.JSNumber(1)])]);
  });
});

