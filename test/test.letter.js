/**
 * @fileOverview letter.js unit test
 * @author ide
 * @requires qunit.js 
 * @requires jquery.js
 */
$(function(){
  module("letter.js",{
    setup:function(){
      this.letter_Lu = ["A","\u00C0","\u0100","\u0391","\u03E2","\u0401","\u0531","\u10A0","\u1E00","\u1F08","\uFF21"];
      this.letter_Ll = ["a","\u00DF","\u0101","\u03B1","\u03E3","\u0451","\u0561","\u10D0","\u1E01","\u1F00","\uFF41"];
    }
  });
  test("isWhitespace",function(){
    ok(letter.isWhitespace("\u0009"),"tab is whitespace");
    ok(letter.isWhitespace("\u000B"),"vertical tab is whitespace");
    ok(letter.isWhitespace("\u000C"),"form feed is whitespace");
    ok(letter.isWhitespace("\u0020"),"space is whitespace");
    ok(letter.isWhitespace("\u00A0"),"no break space is whitespace");
    var letter_Zs = [
      "\u0020","\u00A0","\u2000","\u2001","\u2002","\u2003","\u2004","\u2005",
      "\u2006","\u2007","\u2008","\u2009","\u200A","\u200B","\u3000"
    ];
    letter_Zs.forEach(function(v){
      ok(letter.isWhitespace(v),v+" is whitespace");
    });
  });
  test("isLineTerminator",function(){
    ok(letter.isLineTerminator("\u000A"),"LF is line terminator");
    ok(letter.isLineTerminator("\u000D"),"CR is line terminator");
    ok(letter.isLineTerminator("\u2028"),"LS is line terminator");
    ok(letter.isLineTerminator("\u2029"),"PS is line terminator");
  });
  test("isHexDigit",function(){
    for(var i=0;i<15;i++){
      ok(letter.isHexDigit(i.toString(16)),i.toString(16)+" is hex digit");
    }
    var str = "ABCDEF";
    for(var i=0;i<str.length;i++){
      ok(letter.isHexDigit(str.charAt(i)),str.charAt(i)+" is hex digit");
    }
  });
  test("isOctalDigit",function(){
    for(var i=0;i<7;i++){
      ok(letter.isOctalDigit(i.toString(8)),i.toString(8)+" is octal digit");
    }
  });
  test("isUnicodeLu",function(){
    expect(this.letter_Lu.length + this.letter_Ll.length);
    this.letter_Lu.forEach(function(v){
      ok(letter.isUnicodeLu(v),v+" is unicode Lu");
    });
    this.letter_Ll.forEach(function(v){
      ok(!letter.isUnicodeLu(v),v+" is not unicode Lu");
    });
  });
  test("isUnicodeLl",function(){
    expect(this.letter_Lu.length + this.letter_Ll.length);
    this.letter_Ll.forEach(function(v){
      ok(letter.isUnicodeLl(v),v+" is unicode Ll");
    });
    this.letter_Lu.forEach(function(v){
      ok(!letter.isUnicodeLl(v),v+" is not unicode Ll");
    });
  });
  test("isUnicodeLt",function(){
    var letter_Lt = ["\u01C5","\u01C8","\u01CB","\u01F2"];
    expect(letter_Lt.length);
    letter_Lt.forEach(function(v){
      ok(letter.isUnicodeLt(v),v+" is unicode Lt");
    });
  });
  test("isUnicodeLm",function(){
    var letter_Lm = [
      "\u02B0","\u02D0","\u02E0","\u037A","\u0559","\u0640","\u06E5","\u0E46","\u0EC6",
      "\u3005","\u3031","\u309B","\u30FC","\uFF70","\uFF9E"
    ];
    expect(letter_Lm.length);
    letter_Lm.forEach(function(v){
      ok(letter.isUnicodeLm(v),v+" is unicode Lm");
    });
  });
  test("isUnicodeLo",function(){
    var letter_Lo = [
      "\u01AA","\u03F3","\u04C0","\u05D0","\u0621","\u0905","\u0985","\u0A05","\u0A85",
      "\u0B85","\u0C05","\u0C85","\u0D05","\u0E01","\u0E81","\u0F40","\u1100","\u2135",
      "\u2136","\u2137","\u2138","\u3041","\u30A1","\u3105","\u3131","\u4E00","\u9FA5",
      "\uAC00","\uD7A3","\uF900","\uFA2D","\uFB1F","\uFB50","\uFBEA","\uFF66","\uFFA0"
    ];
    expect(letter_Lo.length);
    letter_Lo.forEach(function(v){
      ok(letter.isUnicodeLo(v),v+" is unicode Lo");
    });
  });
  test("isUnicodeNl",function(){
    var letter_Nl = [
      "\u2160","\u2170","\u2180","\u3007","\u3021"
    ];
    expect(letter_Nl.length);
    letter_Nl.forEach(function(v){
      ok(letter.isUnicodeNl(v),v+" is unicode Nl");
    });
  });
  test("isUnicodeMn",function(){
    var letter_Mn = [
      "\u0300","\u0591","\u064B","\u0901","\u0981","\u0A02","\u0A81","\u0B01","\u0B82",
      "\u0C3E","\u0CBF","\u0D41","\u0E31","\u0EB1","\u0F18","\u20D0","\u302A","\u302E",
      "\u3099","\uFB1E","\uFE20"
    ];
    expect(letter_Mn.length);
    letter_Mn.forEach(function(v){
      ok(letter.isUnicodeMn(v),v+" is unicode Mn");
    });
  });
  test("isUnicodeMc",function(){
    var letter_Mc = [
      "\u0903","\u0982","\u0A3E","\u0A83","\u0B02","\u0B83","\u0C01","\u0C82","\u0D02","\u0F3E"
    ];
    expect(letter_Mc.length);
    letter_Mc.forEach(function(v){
      ok(letter.isUnicodeMc(v),v+" is unicode Mc");
    });
  });
  test("isUnicodeNd",function(){
    var letter_Nd = [
      "\u0030","\u0660","\u06F0","\u0966","\u09E6","\u0A66","\u0AE6","\u0B66","\u0BE7","\u0C66",
      "\u0CE6","\u0D66","\u0E50","\u0ED0","\u0F20","\uFF10"
    ];
    expect(letter_Nd.length);
    letter_Nd.forEach(function(v){
      ok(letter.isUnicodeNd(v),v+" is unicode Nd");
    });
  });
  test("isUnicodePc",function(){
    var letter_Pc = [
      "\u005F","\u203F","\u2040","\uFE33","\uFE4D","\uFE4E","\uFE4F","\uFF3F"
    ];
    expect(letter_Pc.length);
    letter_Pc.forEach(function(v){
      ok(letter.isUnicodePc(v),v+" is unicode Pc");
    });
  });
  test("isUnicodeZs",function(){
    var letter_Zs = [
      "\u0020","\u00A0","\u2000","\u2001","\u2002","\u2003","\u2004","\u2005",
      "\u2006","\u2007","\u2008","\u2009","\u200A","\u200B","\u3000"
    ];
    expect(letter_Zs.length);
    letter_Zs.forEach(function(v){
      ok(letter.isUnicodeZs(v),v+" is unicode Zs");
    });
  });
  test("isUnicodeCombinationMark",function(){
    var letter_Mn = [
      "\u0300","\u0591","\u064B","\u0901","\u0981","\u0A02","\u0A81","\u0B01","\u0B82",
      "\u0C3E","\u0CBF","\u0D41","\u0E31","\u0EB1","\u0F18","\u20D0","\u302A","\u302E",
      "\u3099","\uFB1E","\uFE20"
    ];
    var letter_Mc = [
      "\u0903","\u0982","\u0A3E","\u0A83","\u0B02","\u0B83","\u0C01","\u0C82","\u0D02","\u0F3E"
    ];
    expect(letter_Mn.length+letter_Mc.length);
    letter_Mn.forEach(function(v){
      ok(letter.isUnicodeMn(v),v+" is unicode combination");
    });
    letter_Mc.forEach(function(v){
      ok(letter.isUnicodeMc(v),v+" is unicode combination");
    });
  });
  test("isIdentifierStart",function(){
    var str = "xyz";
    ok(letter.isIdentifierStart("$"),"$ is identifier start");
    ok(letter.isIdentifierStart("_"),"_ is identifier start");
    ok(letter.isIdentifierStart("\\uAAAA".charAt(0)),"\\uAAAA is identifier start");
    for(var i=0;i<str.length;i++){
      ok(letter.isIdentifierStart(str.charAt(i)),str.charAt(i)+" is Identifier start");
      ok(letter.isIdentifierStart(str.toUpperCase().charAt(i)),str.toUpperCase().charAt(i)+" is Identifier start");
    }
  });
  test("isIdentifierPart",function(){
    var str = "xyz0123";
    ok(letter.isIdentifierPart("$"),"$ is identifier part");
    ok(letter.isIdentifierPart("_"),"_ is identifier part");
    ok(letter.isIdentifierPart("\\uAAAA".charAt(0)),"\\uAAAA is identifier part");
    for(var i=0;i<str.length;i++){
      ok(letter.isIdentifierPart(str.charAt(i)),str.charAt(i)+" is Identifier part");
      ok(letter.isIdentifierPart(str.toUpperCase().charAt(i)),str.toUpperCase().charAt(i)+" is Identifier part");
    }
  });
  test("isDecimalDigit",function(){
    var str = "0123456789";
    for(var i=0;i<str.length;i++){
      ok(letter.isDecimalDigit(str.charAt(i)),str.charAt(i)+" is decimal digit");
    }
    var str2 = "abcdefg";
    for(var i=0;i<str2.length;i++){
      ok(!letter.isDecimalDigit(str2.charAt(i)),str2.charAt(i)+" is not decimal digit");
    }
  });
  test("isNonZeroDecimalDigit",function(){
    var str = "123456789";
    for(var i=0;i<str.length;i++){
      ok(letter.isNonZeroDecimalDigit(str.charAt(i)),str.charAt(i)+" is nonzero decimal digit");
    }
    var str2 = "0abcdefg";
    for(var i=0;i<str2.length;i++){
      ok(!letter.isNonZeroDecimalDigit(str2.charAt(i)),str2.charAt(i)+" is not nonzero decimal digit");
    }
  });
  test("isAlphabetLower",function(){
    var str = "abcdefghijklmnopqrstuvwxyz";
    for(var i=0;i<str.length;i++){
      ok(letter.isAlphabetLower(str.charAt(i)),str.charAt(i)+" is lower case alp");
    }
    var str2 = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
    for(var i=0;i<str2.length;i++){
      ok(!letter.isAlphabetLower(str2.charAt(i)),str2.charAt(i)+" is not lower case alp");
    }
  });
  test("isAlphabetUpper",function(){
    var str = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
    for(var i=0;i<str.length;i++){
      ok(letter.isAlphabetUpper(str.charAt(i)),str.charAt(i)+" is upper case alp");
    }
    var str2 = "abcdefghijklmnopqrstuvwxyz";
    for(var i=0;i<str2.length;i++){
      ok(!letter.isAlphabetUpper(str2.charAt(i)),str2.charAt(i)+" is not upper case alp");
    }
  });
});
