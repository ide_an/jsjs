/**
 * @fileOverview tokenize.js unit test
 * @author ide
 * @requires qunit.js 
 * @requires jquery.js
*/
jQuery(function(){
  module("tokenize.js:Tokenizer",{
    setup:function(){
      this.tokenizer = new tokenize.Tokenizer();
    }
  });
  test("scanStringLiteral ",function(){
    this.tokenizer.init('"heyheywow"');
    this.tokenizer._current_token = new tokenize.TokenInfo(this.tokenizer._current_position);
    equals(this.tokenizer.scanStringLiteral(),tokenize.TOKEN_TYPE.STRING_LITERAL,"単純なダブルクオート");
    equals(this.tokenizer.getLiteral(),"heyheywow","単純なダブルクオートリテラル")

    this.tokenizer.init('"マルチバイトだが大丈夫か？"');
    this.tokenizer._current_token = new tokenize.TokenInfo(this.tokenizer._current_position);
    equals(this.tokenizer.scanStringLiteral(),tokenize.TOKEN_TYPE.STRING_LITERAL,"マルチバイトなダブルクオート");
    equals(this.tokenizer.getLiteral(),"マルチバイトだが大丈夫か？","マルチバイトなダブルクオート");

    this.tokenizer.init("'heyheywow'");
    this.tokenizer._current_token = new tokenize.TokenInfo(this.tokenizer._current_position);
    equals(this.tokenizer.scanStringLiteral(),tokenize.TOKEN_TYPE.STRING_LITERAL,"単純なシングルクオート");
    equals(this.tokenizer.getLiteral(),"heyheywow","単純なシングルクオート");

    this.tokenizer.init("'マルチバイトだが大丈夫か？'");
    this.tokenizer._current_token = new tokenize.TokenInfo(this.tokenizer._current_position);
    equals(this.tokenizer.scanStringLiteral(),tokenize.TOKEN_TYPE.STRING_LITERAL,"マルチバイトなシングルクオート");
    equals(this.tokenizer.getLiteral(),"マルチバイトだが大丈夫か？","マルチバイトなシングルクオート");

    this.tokenizer.init("'test\\'escape quote'");
    this.tokenizer._current_token = new tokenize.TokenInfo(this.tokenizer._current_position);
    equals(this.tokenizer.scanStringLiteral(),tokenize.TOKEN_TYPE.STRING_LITERAL,"\\'エスケープ");
    equals(this.tokenizer.getLiteral(),"test'escape quote","\\'エスケープ");

    this.tokenizer.init('"test\\"escape quote"');
    this.tokenizer._current_token = new tokenize.TokenInfo(this.tokenizer._current_position);
    equals(this.tokenizer.scanStringLiteral(),tokenize.TOKEN_TYPE.STRING_LITERAL,"\\\"エスケープ");
    equals(this.tokenizer.getLiteral(),'test"escape quote',"\\\"エスケープ");

    this.tokenizer.init('"\\u3042 is ok?"');
    this.tokenizer._current_token = new tokenize.TokenInfo(this.tokenizer._current_position);
    equals(this.tokenizer.scanStringLiteral(),tokenize.TOKEN_TYPE.STRING_LITERAL,"unicode エスケープ");
    equals(this.tokenizer.getLiteral(),'あ is ok?',"unicode エスケープ");

    this.tokenizer.init('"\\x61 is ok?"');
    this.tokenizer._current_token = new tokenize.TokenInfo(this.tokenizer._current_position);
    equals(this.tokenizer.scanStringLiteral(),tokenize.TOKEN_TYPE.STRING_LITERAL,"hex エスケープ");
    equals(this.tokenizer.getLiteral(),'a is ok?',"hex エスケープ");

    this.tokenizer.init('"\\0 is ok?"');
    this.tokenizer._current_token = new tokenize.TokenInfo(this.tokenizer._current_position);
    equals(this.tokenizer.scanStringLiteral(),tokenize.TOKEN_TYPE.STRING_LITERAL,"\\0エスケープ");
    equals(this.tokenizer.getLiteral(),'\0 is ok?',"\\0エスケープ");

    this.tokenizer.init('"\\60 is ok?"');
    this.tokenizer._current_token = new tokenize.TokenInfo(this.tokenizer._current_position);
    equals(this.tokenizer.scanStringLiteral(),tokenize.TOKEN_TYPE.STRING_LITERAL,"octalエスケープ");
    equals(this.tokenizer.getLiteral(),'\60 is ok?',"octalエスケープ");

    this.tokenizer.init("''");
    this.tokenizer._current_token = new tokenize.TokenInfo(this.tokenizer._current_position);
    equals(this.tokenizer.scanStringLiteral(),tokenize.TOKEN_TYPE.STRING_LITERAL,"空文字列''");
    equals(this.tokenizer.getLiteral(),'',"空文字列''");

    this.tokenizer.init('""');
    this.tokenizer._current_token = new tokenize.TokenInfo(this.tokenizer._current_position);
    equals(this.tokenizer.scanStringLiteral(),tokenize.TOKEN_TYPE.STRING_LITERAL,"空文字列\"\"");
    equals(this.tokenizer.getLiteral(),'',"空文字列\"\"");

    this.tokenizer.init('"\\b"');
    this.tokenizer._current_token = new tokenize.TokenInfo(this.tokenizer._current_position);
    equals(this.tokenizer.scanStringLiteral(),tokenize.TOKEN_TYPE.STRING_LITERAL,"\\bエスケープ");
    equals(this.tokenizer.getLiteral(),'\b',"\\bエスケープ");

    this.tokenizer.init('"\\t"');
    this.tokenizer._current_token = new tokenize.TokenInfo(this.tokenizer._current_position);
    equals(this.tokenizer.scanStringLiteral(),tokenize.TOKEN_TYPE.STRING_LITERAL,"\\tエスケープ");
    equals(this.tokenizer.getLiteral(),'\t',"\\tエスケープ");

    this.tokenizer.init('"\\n"');
    this.tokenizer._current_token = new tokenize.TokenInfo(this.tokenizer._current_position);
    equals(this.tokenizer.scanStringLiteral(),tokenize.TOKEN_TYPE.STRING_LITERAL,"\\nエスケープ");
    equals(this.tokenizer.getLiteral(),'\n',"\\nエスケープ");

    this.tokenizer.init('"\\v"');
    this.tokenizer._current_token = new tokenize.TokenInfo(this.tokenizer._current_position);
    equals(this.tokenizer.scanStringLiteral(),tokenize.TOKEN_TYPE.STRING_LITERAL,"\\vエスケープ");
    equals(this.tokenizer.getLiteral(),'\v',"\\vエスケープ");

    this.tokenizer.init('"\\f"');
    this.tokenizer._current_token = new tokenize.TokenInfo(this.tokenizer._current_position);
    equals(this.tokenizer.scanStringLiteral(),tokenize.TOKEN_TYPE.STRING_LITERAL,"\\fエスケープ");
    equals(this.tokenizer.getLiteral(),'\f',"\\fエスケープ");

    this.tokenizer.init('"\\r"');
    this.tokenizer._current_token = new tokenize.TokenInfo(this.tokenizer._current_position);
    equals(this.tokenizer.scanStringLiteral(),tokenize.TOKEN_TYPE.STRING_LITERAL,"\\rエスケープ");
    equals(this.tokenizer.getLiteral(),'\r',"\\rエスケープ");

    this.tokenizer.init('"\\\\"');
    this.tokenizer._current_token = new tokenize.TokenInfo(this.tokenizer._current_position);
    equals(this.tokenizer.scanStringLiteral(),tokenize.TOKEN_TYPE.STRING_LITERAL,"\\\\エスケープ");
    equals(this.tokenizer.getLiteral(),'\\',"\\\\エスケープ");

    this.tokenizer.init('"\\alpha"');
    this.tokenizer._current_token = new tokenize.TokenInfo(this.tokenizer._current_position);
    equals(this.tokenizer.scanStringLiteral(),tokenize.TOKEN_TYPE.STRING_LITERAL,"non-escape-character");
    equals(this.tokenizer.getLiteral(),'alpha',"non-escape-character");

    this.tokenizer.init('"alp\nha"');
    this.tokenizer._current_token = new tokenize.TokenInfo(this.tokenizer._current_position);
    equals(this.tokenizer.scanStringLiteral(),tokenize.TOKEN_TYPE.ILLEGAL_TOKEN,"途中の改行はILEGAL");

    this.tokenizer.init('"alpha');
    this.tokenizer._current_token = new tokenize.TokenInfo(this.tokenizer._current_position);
    equals(this.tokenizer.scanStringLiteral(),tokenize.TOKEN_TYPE.ILLEGAL_TOKEN,"クオートで終了しないならILLEGAL");
  });
  test("scanHexDigits",function(){
    this.tokenizer.init('3042');
    equals(this.tokenizer.scanHexDigits(4),0x3042,"length = 4");

    this.tokenizer.init('61');
    equals(this.tokenizer.scanHexDigits(2),0x61,"length = 2");
  });
  test("scanOctalDigits",function(){
    this.tokenizer.init('61');
    equals(this.tokenizer.scanOctalDigits(2),061,"length <= 2");
  });
  test("scanNumericLiteral",function(){
    //decimal
    this.tokenizer.init("0");
    this.tokenizer._current_token = new tokenize.TokenInfo(this.tokenizer._current_position);
    equals(this.tokenizer.scanNumericLiteral(false),tokenize.TOKEN_TYPE.NUMERIC_LITERAL,"0");
    equals(this.tokenizer.getLiteral(),'0',"0");

    this.tokenizer.init("1");
    this.tokenizer._current_token = new tokenize.TokenInfo(this.tokenizer._current_position);
    equals(this.tokenizer.scanNumericLiteral(false),tokenize.TOKEN_TYPE.NUMERIC_LITERAL,"1");
    equals(this.tokenizer.getLiteral(),'1');

    this.tokenizer.init("10");
    this.tokenizer._current_token = new tokenize.TokenInfo(this.tokenizer._current_position);
    equals(this.tokenizer.scanNumericLiteral(false),tokenize.TOKEN_TYPE.NUMERIC_LITERAL,"10");
    equals(this.tokenizer.getLiteral(),'10');

    this.tokenizer.init("1.");
    this.tokenizer._current_token = new tokenize.TokenInfo(this.tokenizer._current_position);
    equals(this.tokenizer.scanNumericLiteral(false),tokenize.TOKEN_TYPE.NUMERIC_LITERAL,"1.");
    equals(this.tokenizer.getLiteral(),'1.');

    this.tokenizer.init("0.1");
    this.tokenizer._current_token = new tokenize.TokenInfo(this.tokenizer._current_position);
    equals(this.tokenizer.scanNumericLiteral(false),tokenize.TOKEN_TYPE.NUMERIC_LITERAL,"0.1");
    equals(this.tokenizer.getLiteral(),'0.1');

    this.tokenizer.init(".1");
    this.tokenizer._current_token = new tokenize.TokenInfo(this.tokenizer._current_position);
    this.tokenizer.advance();
    equals(this.tokenizer.scanNumericLiteral(true),tokenize.TOKEN_TYPE.NUMERIC_LITERAL,".1");
    equals(this.tokenizer.getLiteral(),'.1');

    this.tokenizer.init("0.123");
    this.tokenizer._current_token = new tokenize.TokenInfo(this.tokenizer._current_position);
    equals(this.tokenizer.scanNumericLiteral(false),tokenize.TOKEN_TYPE.NUMERIC_LITERAL,"0.123");
    equals(this.tokenizer.getLiteral(),'0.123');

    this.tokenizer.init(".123");
    this.tokenizer._current_token = new tokenize.TokenInfo(this.tokenizer._current_position);
    this.tokenizer.advance();
    equals(this.tokenizer.scanNumericLiteral(true),tokenize.TOKEN_TYPE.NUMERIC_LITERAL,".123");
    equals(this.tokenizer.getLiteral(),'.123');
    //digits exp
    this.tokenizer.init("10e2");
    this.tokenizer._current_token = new tokenize.TokenInfo(this.tokenizer._current_position);
    equals(this.tokenizer.scanNumericLiteral(false),tokenize.TOKEN_TYPE.NUMERIC_LITERAL,"10e2");
    equals(this.tokenizer.getLiteral(),'10e2');

    this.tokenizer.init("10E2");
    this.tokenizer._current_token = new tokenize.TokenInfo(this.tokenizer._current_position);
    equals(this.tokenizer.scanNumericLiteral(false),tokenize.TOKEN_TYPE.NUMERIC_LITERAL,"10E2");
    equals(this.tokenizer.getLiteral(),'10E2');

    this.tokenizer.init("10e+2");
    this.tokenizer._current_token = new tokenize.TokenInfo(this.tokenizer._current_position);
    equals(this.tokenizer.scanNumericLiteral(false),tokenize.TOKEN_TYPE.NUMERIC_LITERAL,"10e+2");
    equals(this.tokenizer.getLiteral(),'10e+2');

    this.tokenizer.init("10E+2");
    this.tokenizer._current_token = new tokenize.TokenInfo(this.tokenizer._current_position);
    equals(this.tokenizer.scanNumericLiteral(false),tokenize.TOKEN_TYPE.NUMERIC_LITERAL,"10E+2");
    equals(this.tokenizer.getLiteral(),'10E+2');

    this.tokenizer.init("10e-2");
    this.tokenizer._current_token = new tokenize.TokenInfo(this.tokenizer._current_position);
    equals(this.tokenizer.scanNumericLiteral(false),tokenize.TOKEN_TYPE.NUMERIC_LITERAL,"10e-2");
    equals(this.tokenizer.getLiteral(),'10e-2');

    this.tokenizer.init("10E-2");
    this.tokenizer._current_token = new tokenize.TokenInfo(this.tokenizer._current_position);
    equals(this.tokenizer.scanNumericLiteral(false),tokenize.TOKEN_TYPE.NUMERIC_LITERAL,"10E-2");
    equals(this.tokenizer.getLiteral(),'10E-2');
    //digits.digits exp
    this.tokenizer.init("1.3e2");
    this.tokenizer._current_token = new tokenize.TokenInfo(this.tokenizer._current_position);
    equals(this.tokenizer.scanNumericLiteral(false),tokenize.TOKEN_TYPE.NUMERIC_LITERAL,"1.3e2");
    equals(this.tokenizer.getLiteral(),'1.3e2');

    this.tokenizer.init("1.3E2");
    this.tokenizer._current_token = new tokenize.TokenInfo(this.tokenizer._current_position);
    equals(this.tokenizer.scanNumericLiteral(false),tokenize.TOKEN_TYPE.NUMERIC_LITERAL,"1.3E2");
    equals(this.tokenizer.getLiteral(),'1.3E2');

    this.tokenizer.init("1.3e+2");
    this.tokenizer._current_token = new tokenize.TokenInfo(this.tokenizer._current_position);
    equals(this.tokenizer.scanNumericLiteral(false),tokenize.TOKEN_TYPE.NUMERIC_LITERAL,"1.3e+2");
    equals(this.tokenizer.getLiteral(),'1.3e+2');

    this.tokenizer.init("1.3E+2");
    this.tokenizer._current_token = new tokenize.TokenInfo(this.tokenizer._current_position);
    equals(this.tokenizer.scanNumericLiteral(false),tokenize.TOKEN_TYPE.NUMERIC_LITERAL,"1.3E+2");
    equals(this.tokenizer.getLiteral(),'1.3E+2');

    this.tokenizer.init("1.3e-2");
    this.tokenizer._current_token = new tokenize.TokenInfo(this.tokenizer._current_position);
    equals(this.tokenizer.scanNumericLiteral(false),tokenize.TOKEN_TYPE.NUMERIC_LITERAL,"1.3e-2");
    equals(this.tokenizer.getLiteral(),'1.3e-2');

    this.tokenizer.init("1.3E-2");
    this.tokenizer._current_token = new tokenize.TokenInfo(this.tokenizer._current_position);
    equals(this.tokenizer.scanNumericLiteral(false),tokenize.TOKEN_TYPE.NUMERIC_LITERAL,"1.3E-2");
    equals(this.tokenizer.getLiteral(),'1.3E-2');
    //.digits expt
    this.tokenizer.init(".3e2");
    this.tokenizer._current_token = new tokenize.TokenInfo(this.tokenizer._current_position);
    this.tokenizer.advance();
    equals(this.tokenizer.scanNumericLiteral(true),tokenize.TOKEN_TYPE.NUMERIC_LITERAL,".3e2");
    equals(this.tokenizer.getLiteral(),'.3e2');

    this.tokenizer.init(".3E2");
    this.tokenizer._current_token = new tokenize.TokenInfo(this.tokenizer._current_position);
    this.tokenizer.advance();
    equals(this.tokenizer.scanNumericLiteral(true),tokenize.TOKEN_TYPE.NUMERIC_LITERAL,".3E2");
    equals(this.tokenizer.getLiteral(),'.3E2');

    this.tokenizer.init(".3e+2");
    this.tokenizer._current_token = new tokenize.TokenInfo(this.tokenizer._current_position);
    this.tokenizer.advance();
    equals(this.tokenizer.scanNumericLiteral(true),tokenize.TOKEN_TYPE.NUMERIC_LITERAL,".3e+2");
    equals(this.tokenizer.getLiteral(),'.3e+2');

    this.tokenizer.init(".3E+2");
    this.tokenizer._current_token = new tokenize.TokenInfo(this.tokenizer._current_position);
    this.tokenizer.advance();
    equals(this.tokenizer.scanNumericLiteral(true),tokenize.TOKEN_TYPE.NUMERIC_LITERAL,".3E+2");
    equals(this.tokenizer.getLiteral(),'.3E+2');

    this.tokenizer.init(".3e-2");
    this.tokenizer._current_token = new tokenize.TokenInfo(this.tokenizer._current_position);
    this.tokenizer.advance();
    equals(this.tokenizer.scanNumericLiteral(true),tokenize.TOKEN_TYPE.NUMERIC_LITERAL,".3e-2");
    equals(this.tokenizer.getLiteral(),'.3e-2');

    this.tokenizer.init(".3E-2");
    this.tokenizer._current_token = new tokenize.TokenInfo(this.tokenizer._current_position);
    this.tokenizer.advance();
    equals(this.tokenizer.scanNumericLiteral(true),tokenize.TOKEN_TYPE.NUMERIC_LITERAL,".3E-2");
    equals(this.tokenizer.getLiteral(),'.3E-2');
    //hex
    this.tokenizer.init("0xa");
    this.tokenizer._current_token = new tokenize.TokenInfo(this.tokenizer._current_position);
    equals(this.tokenizer.scanNumericLiteral(false),tokenize.TOKEN_TYPE.NUMERIC_LITERAL,"0xa");
    equals(this.tokenizer.getLiteral(),'0xa');

    this.tokenizer.init("0xA");
    this.tokenizer._current_token = new tokenize.TokenInfo(this.tokenizer._current_position);
    equals(this.tokenizer.scanNumericLiteral(false),tokenize.TOKEN_TYPE.NUMERIC_LITERAL,"0xA");
    equals(this.tokenizer.getLiteral(),'0xA');

    this.tokenizer.init("0xab");
    this.tokenizer._current_token = new tokenize.TokenInfo(this.tokenizer._current_position);
    equals(this.tokenizer.scanNumericLiteral(false),tokenize.TOKEN_TYPE.NUMERIC_LITERAL,"0xab");
    equals(this.tokenizer.getLiteral(),'0xab');

    this.tokenizer.init("0xAB");
    this.tokenizer._current_token = new tokenize.TokenInfo(this.tokenizer._current_position);
    equals(this.tokenizer.scanNumericLiteral(false),tokenize.TOKEN_TYPE.NUMERIC_LITERAL,"0xAB");
    equals(this.tokenizer.getLiteral(),'0xAB');

    this.tokenizer.init("123p");
    this.tokenizer._current_token = new tokenize.TokenInfo(this.tokenizer._current_position);
    equals(this.tokenizer.scanNumericLiteral(false), tokenize.TOKEN_TYPE.ILLEGAL_TOKEN,"123pはイリーガル");
  });

  test("scanIdentifier",function(){
    //keywords and true ,fale ,null
    this.tokenizer.init("break");
    this.tokenizer._current_token = new tokenize.TokenInfo(this.tokenizer._current_position);
    equals(this.tokenizer.scanIdentifier(), tokenize.TOKEN_TYPE.K_BREAK,"break");

    this.tokenizer.init("case");
    this.tokenizer._current_token = new tokenize.TokenInfo(this.tokenizer._current_position);
    equals(this.tokenizer.scanIdentifier(), tokenize.TOKEN_TYPE.K_CASE,"case");

    this.tokenizer.init("catch");
    this.tokenizer._current_token = new tokenize.TokenInfo(this.tokenizer._current_position);
    equals(this.tokenizer.scanIdentifier(), tokenize.TOKEN_TYPE.K_CATCH,"catch");

    this.tokenizer.init("continue");
    this.tokenizer._current_token = new tokenize.TokenInfo(this.tokenizer._current_position);
    equals(this.tokenizer.scanIdentifier(), tokenize.TOKEN_TYPE.K_CONTINUE,"continue");

    this.tokenizer.init("default");
    this.tokenizer._current_token = new tokenize.TokenInfo(this.tokenizer._current_position);
    equals(this.tokenizer.scanIdentifier(), tokenize.TOKEN_TYPE.K_DEFAULT,"default");

    this.tokenizer.init("delete");
    this.tokenizer._current_token = new tokenize.TokenInfo(this.tokenizer._current_position);
    equals(this.tokenizer.scanIdentifier(), tokenize.TOKEN_TYPE.K_DELETE,"delete");

    this.tokenizer.init("do");
    this.tokenizer._current_token = new tokenize.TokenInfo(this.tokenizer._current_position);
    equals(this.tokenizer.scanIdentifier(), tokenize.TOKEN_TYPE.K_DO,"do");

    this.tokenizer.init("else");
    this.tokenizer._current_token = new tokenize.TokenInfo(this.tokenizer._current_position);
    equals(this.tokenizer.scanIdentifier(), tokenize.TOKEN_TYPE.K_ELSE,"else");

    this.tokenizer.init("false");
    this.tokenizer._current_token = new tokenize.TokenInfo(this.tokenizer._current_position);
    equals(this.tokenizer.scanIdentifier(), tokenize.TOKEN_TYPE.FALSE_LITERAL,"false");

    this.tokenizer.init("finally");
    this.tokenizer._current_token = new tokenize.TokenInfo(this.tokenizer._current_position);
    equals(this.tokenizer.scanIdentifier(), tokenize.TOKEN_TYPE.K_FINALLY,"finally");

    this.tokenizer.init("for");
    this.tokenizer._current_token = new tokenize.TokenInfo(this.tokenizer._current_position);
    equals(this.tokenizer.scanIdentifier(), tokenize.TOKEN_TYPE.K_FOR,"for");

    this.tokenizer.init("function");
    this.tokenizer._current_token = new tokenize.TokenInfo(this.tokenizer._current_position);
    equals(this.tokenizer.scanIdentifier(), tokenize.TOKEN_TYPE.K_FUNCTION,"function");

    this.tokenizer.init("if");
    this.tokenizer._current_token = new tokenize.TokenInfo(this.tokenizer._current_position);
    equals(this.tokenizer.scanIdentifier(), tokenize.TOKEN_TYPE.K_IF,"if");

    this.tokenizer.init("in");
    this.tokenizer._current_token = new tokenize.TokenInfo(this.tokenizer._current_position);
    equals(this.tokenizer.scanIdentifier(), tokenize.TOKEN_TYPE.K_IN,"in");

    this.tokenizer.init("instanceof");
    this.tokenizer._current_token = new tokenize.TokenInfo(this.tokenizer._current_position);
    equals(this.tokenizer.scanIdentifier(), tokenize.TOKEN_TYPE.K_INSTANCEOF,"instanceof");

    this.tokenizer.init("new");
    this.tokenizer._current_token = new tokenize.TokenInfo(this.tokenizer._current_position);
    equals(this.tokenizer.scanIdentifier(), tokenize.TOKEN_TYPE.K_NEW,"new");

    this.tokenizer.init("null");
    this.tokenizer._current_token = new tokenize.TokenInfo(this.tokenizer._current_position);
    equals(this.tokenizer.scanIdentifier(), tokenize.TOKEN_TYPE.NULL_LITERAL,"null");

    this.tokenizer.init("return");
    this.tokenizer._current_token = new tokenize.TokenInfo(this.tokenizer._current_position);
    equals(this.tokenizer.scanIdentifier(), tokenize.TOKEN_TYPE.K_RETURN,"return");

    this.tokenizer.init("switch");
    this.tokenizer._current_token = new tokenize.TokenInfo(this.tokenizer._current_position);
    equals(this.tokenizer.scanIdentifier(), tokenize.TOKEN_TYPE.K_SWITCH,"switch");

    this.tokenizer.init("this");
    this.tokenizer._current_token = new tokenize.TokenInfo(this.tokenizer._current_position);
    equals(this.tokenizer.scanIdentifier(), tokenize.TOKEN_TYPE.K_THIS,"this");

    this.tokenizer.init("throw");
    this.tokenizer._current_token = new tokenize.TokenInfo(this.tokenizer._current_position);
    equals(this.tokenizer.scanIdentifier(), tokenize.TOKEN_TYPE.K_THROW,"throw");

    this.tokenizer.init("true");
    this.tokenizer._current_token = new tokenize.TokenInfo(this.tokenizer._current_position);
    equals(this.tokenizer.scanIdentifier(), tokenize.TOKEN_TYPE.TRUE_LITERAL,"true");

    this.tokenizer.init("try");
    this.tokenizer._current_token = new tokenize.TokenInfo(this.tokenizer._current_position);
    equals(this.tokenizer.scanIdentifier(), tokenize.TOKEN_TYPE.K_TRY,"try");

    this.tokenizer.init("typeof");
    this.tokenizer._current_token = new tokenize.TokenInfo(this.tokenizer._current_position);
    equals(this.tokenizer.scanIdentifier(), tokenize.TOKEN_TYPE.K_TYPEOF,"typeof");

    this.tokenizer.init("var");
    this.tokenizer._current_token = new tokenize.TokenInfo(this.tokenizer._current_position);
    equals(this.tokenizer.scanIdentifier(), tokenize.TOKEN_TYPE.K_VAR,"var");

    this.tokenizer.init("void");
    this.tokenizer._current_token = new tokenize.TokenInfo(this.tokenizer._current_position);
    equals(this.tokenizer.scanIdentifier(), tokenize.TOKEN_TYPE.K_VOID,"void");

    this.tokenizer.init("while");
    this.tokenizer._current_token = new tokenize.TokenInfo(this.tokenizer._current_position);
    equals(this.tokenizer.scanIdentifier(), tokenize.TOKEN_TYPE.K_WHILE,"while");

    this.tokenizer.init("with");
    this.tokenizer._current_token = new tokenize.TokenInfo(this.tokenizer._current_position);
    equals(this.tokenizer.scanIdentifier(), tokenize.TOKEN_TYPE.K_WITH,"with");
    //identifier
    this.tokenizer.init("x");
    this.tokenizer._current_token = new tokenize.TokenInfo(this.tokenizer._current_position);
    equals(this.tokenizer.scanIdentifier(), tokenize.TOKEN_TYPE.IDENTIFIER,"x is identifier");
    equals(this.tokenizer.getLiteral(), "x","name:x");

    this.tokenizer.init("fo");
    this.tokenizer._current_token = new tokenize.TokenInfo(this.tokenizer._current_position);
    equals(this.tokenizer.scanIdentifier(), tokenize.TOKEN_TYPE.IDENTIFIER,"vx is identifier");
    equals(this.tokenizer.getLiteral(), "fo","name:fo");

    this.tokenizer.init("variable");
    this.tokenizer._current_token = new tokenize.TokenInfo(this.tokenizer._current_position);
    equals(this.tokenizer.scanIdentifier(), tokenize.TOKEN_TYPE.IDENTIFIER,"variable is identifier");
    equals(this.tokenizer.getLiteral(), "variable","name:variable");

    this.tokenizer.init("\\u0076ar");
    this.tokenizer._current_token = new tokenize.TokenInfo(this.tokenizer._current_position);
    equals(this.tokenizer.scanIdentifier(), tokenize.TOKEN_TYPE.IDENTIFIER,"\\u0076ar is identifier");
    equals(this.tokenizer.getLiteral(), "var","name:var");

    this.tokenizer.init("v\\u0061riable");
    this.tokenizer._current_token = new tokenize.TokenInfo(this.tokenizer._current_position);
    equals(this.tokenizer.scanIdentifier(), tokenize.TOKEN_TYPE.IDENTIFIER,"v\\u0061riable is identifier");
    equals(this.tokenizer.getLiteral(), "variable","name:variable");
  });
  test("scanRegularExpressionBody",function(){
    this.tokenizer.init("/test/");
    this.tokenizer.advance();
    this.tokenizer._current_token = new tokenize.TokenInfo(this.tokenizer._current_position);
    ok(this.tokenizer.scanRegularExpressionBody(false),"basic RegExp /test/");
    equals(this.tokenizer.getLiteral(),"test","literal is 'test'");

    this.tokenizer.init("/t\\dest/");
    this.tokenizer.advance();
    this.tokenizer._current_token = new tokenize.TokenInfo(this.tokenizer._current_position);
    ok(this.tokenizer.scanRegularExpressionBody(false),"RegExp with escape /t\\dest/");
    equals(this.tokenizer.getLiteral(),"t\\dest","literal is 't\\dest'");

    this.tokenizer.init("/t[a-z/]est/");
    this.tokenizer.advance();
    this.tokenizer._current_token = new tokenize.TokenInfo(this.tokenizer._current_position);
    ok(this.tokenizer.scanRegularExpressionBody(false),"RegExp including / in character class /t[a-z/]est/");
    equals(this.tokenizer.getLiteral(),"t[a-z/]est","literal is 't[a-z/]est'");

    this.tokenizer.init("/=test/");
    this.tokenizer.advance();
    this.tokenizer.advance();
    this.tokenizer._current_token = new tokenize.TokenInfo(this.tokenizer._current_position);
    ok(this.tokenizer.scanRegularExpressionBody(true),"RegExp start with '=' /=test/");
    equals(this.tokenizer.getLiteral(),"=test","literal is '=test'");

    this.tokenizer.init("/t\nest/");
    this.tokenizer.advance();
    this.tokenizer._current_token = new tokenize.TokenInfo(this.tokenizer._current_position);
    ok(!this.tokenizer.scanRegularExpressionBody(false),"途中の改行はILLEGAL");

    this.tokenizer.init("/t");
    this.tokenizer.advance();
    this.tokenizer._current_token = new tokenize.TokenInfo(this.tokenizer._current_position);
    ok(!this.tokenizer.scanRegularExpressionBody(false),"途中でソースコードが切れてたらILLEGAL");
  });
  test("scanRegularExpressionFlag",function(){
    this.tokenizer.init("");
    this.tokenizer._current_token = new tokenize.TokenInfo(this.tokenizer._current_position);
    ok(this.tokenizer.scanRegularExpressionFlag(),"フラグ指定なし");
    equals(this.tokenizer.getLiteral(),"","literal is ''");

    this.tokenizer.init("gi");
    this.tokenizer._current_token = new tokenize.TokenInfo(this.tokenizer._current_position);
    ok(this.tokenizer.scanRegularExpressionFlag(),"フラグ gi");
    equals(this.tokenizer.getLiteral(),"gi","literal is 'gi'");
  });
  test("skipSingleLineComment",function(){
    this.tokenizer.init("//test\nA");
    this.tokenizer.advance();
    equals(this.tokenizer.skipSingleLineComment(), tokenize.TOKEN_TYPE.WHITESPACE,"コメントは空白扱い");
    equals(this.tokenizer._c0,"\n","skip to end of comment(comment dont include line-terminator)");

    this.tokenizer.init("//test");
    this.tokenizer.advance();
    equals(this.tokenizer.skipSingleLineComment(), tokenize.TOKEN_TYPE.WHITESPACE,"コメントは空白扱い");
    equals(this.tokenizer._c0,tokenize.TOKEN_TYPE.EOS,"skip to end of comment(EndOfSource)");
  });
  test("skipMultiLineComment",function(){
    this.tokenizer.init("/*test*/A");
    this.tokenizer.advance();
    equals(this.tokenizer.skipMultiLineComment(), tokenize.TOKEN_TYPE.WHITESPACE,"コメントは空白扱い");
    equals(this.tokenizer._c0," ","スペースとして扱う。");
    ok(!this.tokenizer._hasLineTerminator,"改行してない");

    this.tokenizer.init("/*te\nst*/A");
    this.tokenizer.advance();
    equals(this.tokenizer.skipMultiLineComment(), tokenize.TOKEN_TYPE.WHITESPACE,"コメントは空白扱い");
    equals(this.tokenizer._c0,"\n","改行として扱う。");
    ok(this.tokenizer._hasLineTerminator,"改行している");
  });
  test("tokenize",function(){
    //punctuators
    this.tokenizer.init("{");
    equals(this.tokenizer.tokenize(),tokenize.TOKEN_TYPE.P_BRACE_L,"punctuator:no-space:BRACE_L");

    this.tokenizer.init("}");
    equals(this.tokenizer.tokenize(),tokenize.TOKEN_TYPE.P_BRACE_R,"punctuator:no-space:BRACE_R");

    this.tokenizer.init("(");
    equals(this.tokenizer.tokenize(),tokenize.TOKEN_TYPE.P_PAREN_L,"punctuator:no-space:PAREN_L");

    this.tokenizer.init(")");
    equals(this.tokenizer.tokenize(),tokenize.TOKEN_TYPE.P_PAREN_R,"punctuator:no-space:PAREN_R");

    this.tokenizer.init("[");
    equals(this.tokenizer.tokenize(),tokenize.TOKEN_TYPE.P_BRACKET_L,"punctuator:no-space:BRACKET_L");

    this.tokenizer.init("]");
    equals(this.tokenizer.tokenize(),tokenize.TOKEN_TYPE.P_BRACKET_R,"punctuator:no-space:BRACKET_R");

    this.tokenizer.init(".");
    equals(this.tokenizer.tokenize(),tokenize.TOKEN_TYPE.P_PERIOD,"punctuator:no-space:PERIOD");

    this.tokenizer.init(";");
    equals(this.tokenizer.tokenize(),tokenize.TOKEN_TYPE.P_SEMICOLON,"punctuator:no-space:SEMICOLON");

    this.tokenizer.init(",");
    equals(this.tokenizer.tokenize(),tokenize.TOKEN_TYPE.P_COMMA,"punctuator:no-space:COMMA");

    this.tokenizer.init("<")
    equals(this.tokenizer.tokenize(),tokenize.TOKEN_TYPE.P_LT,"punctuator:no-space:LT");

    this.tokenizer.init(">");
    equals(this.tokenizer.tokenize(),tokenize.TOKEN_TYPE.P_GT,"punctuator:no-space:GT");

    this.tokenizer.init("<=")
    equals(this.tokenizer.tokenize(),tokenize.TOKEN_TYPE.P_LEQ,"punctuator:no-space:LEQ");

    this.tokenizer.init(">=");
    equals(this.tokenizer.tokenize(),tokenize.TOKEN_TYPE.P_GEQ,"punctuator:no-space:GEQ");

    this.tokenizer.init("==")
    equals(this.tokenizer.tokenize(),tokenize.TOKEN_TYPE.P_EQUAL,"punctuator:no-space:EQUAL");

    this.tokenizer.init("!=");
    equals(this.tokenizer.tokenize(),tokenize.TOKEN_TYPE.P_NOT_EQUAL,"punctuator:no-space:NOT_EQUAL");

    this.tokenizer.init("===")
    equals(this.tokenizer.tokenize(),tokenize.TOKEN_TYPE.P_EQAUL_STRICT,"punctuator:no-space:EQAUL_STRICT");

    this.tokenizer.init("!==");
    equals(this.tokenizer.tokenize(),tokenize.TOKEN_TYPE.P_NOT_EQUAL_STRICT,"punctuator:no-space:NOT_EQUAL_STRICT");

    this.tokenizer.init("+")
    equals(this.tokenizer.tokenize(),tokenize.TOKEN_TYPE.P_ADD,"punctuator:no-space:ADD");

    this.tokenizer.init("-");
    equals(this.tokenizer.tokenize(),tokenize.TOKEN_TYPE.P_SUB,"punctuator:no-space:SUB");

    this.tokenizer.init("*")
    equals(this.tokenizer.tokenize(),tokenize.TOKEN_TYPE.P_MUL,"punctuator:no-space:MUL");

    this.tokenizer.init("%");
    equals(this.tokenizer.tokenize(),tokenize.TOKEN_TYPE.P_MOD,"punctuator:no-space:MOD");

    this.tokenizer.init("/");
    equals(this.tokenizer.tokenize(),tokenize.TOKEN_TYPE.P_DIV,"punctuator:no-space:DIV");

    this.tokenizer.init("++")
    equals(this.tokenizer.tokenize(),tokenize.TOKEN_TYPE.P_INC,"punctuator:no-space:INC");

    this.tokenizer.init("--");
    equals(this.tokenizer.tokenize(),tokenize.TOKEN_TYPE.P_DEC,"punctuator:no-space:DEC");

    this.tokenizer.init("<<")
    equals(this.tokenizer.tokenize(),tokenize.TOKEN_TYPE.P_SHL,"punctuator:no-space:SHL");

    this.tokenizer.init(">>");
    equals(this.tokenizer.tokenize(),tokenize.TOKEN_TYPE.P_SHR_ARITH,"punctuator:no-space:SHR_ARITH");

    this.tokenizer.init(">>>");
    equals(this.tokenizer.tokenize(),tokenize.TOKEN_TYPE.P_SHR,"punctuator:no-space:SHR");

    this.tokenizer.init("&")
    equals(this.tokenizer.tokenize(),tokenize.TOKEN_TYPE.P_AND,"punctuator:no-space:AND");

    this.tokenizer.init("|")
    equals(this.tokenizer.tokenize(),tokenize.TOKEN_TYPE.P_OR,"punctuator:no-space:OR");

    this.tokenizer.init("^")
    equals(this.tokenizer.tokenize(),tokenize.TOKEN_TYPE.P_XOR,"punctuator:no-space:XOR");

    this.tokenizer.init("!")
    equals(this.tokenizer.tokenize(),tokenize.TOKEN_TYPE.P_NOT,"punctuator:no-space:NOT");

    this.tokenizer.init("~")
    equals(this.tokenizer.tokenize(),tokenize.TOKEN_TYPE.P_COMP,"punctuator:no-space:COMP");

    this.tokenizer.init("&&")
    equals(this.tokenizer.tokenize(),tokenize.TOKEN_TYPE.P_AND_LOGIC,"punctuator:no-space:AND_LOGIC");

    this.tokenizer.init("||")
    equals(this.tokenizer.tokenize(),tokenize.TOKEN_TYPE.P_OR_LOGIC,"punctuator:no-space:OR_LOGIC");

    this.tokenizer.init("?")
    equals(this.tokenizer.tokenize(),tokenize.TOKEN_TYPE.P_COND,"punctuator:no-space:COND");

    this.tokenizer.init(":")
    equals(this.tokenizer.tokenize(),tokenize.TOKEN_TYPE.P_COLON,"punctuator:no-space:COLON");

    this.tokenizer.init("=")
    equals(this.tokenizer.tokenize(),tokenize.TOKEN_TYPE.P_ASSIGN,"punctuator:no-space:ASSIGN");

    this.tokenizer.init("+=")
    equals(this.tokenizer.tokenize(),tokenize.TOKEN_TYPE.P_ASSIGN_ADD,"punctuator:no-space:ASSIGN_ADD");

    this.tokenizer.init("-=")
    equals(this.tokenizer.tokenize(),tokenize.TOKEN_TYPE.P_ASSIGN_SUB,"punctuator:no-space:ASSIGN_SUB");

    this.tokenizer.init("*=")
    equals(this.tokenizer.tokenize(),tokenize.TOKEN_TYPE.P_ASSIGN_MUL,"punctuator:no-space:ASSIGN_MUL");

    this.tokenizer.init("%=")
    equals(this.tokenizer.tokenize(),tokenize.TOKEN_TYPE.P_ASSIGN_MOD,"punctuator:no-space:ASSIGN_MOD");

    this.tokenizer.init("/=")
    equals(this.tokenizer.tokenize(),tokenize.TOKEN_TYPE.P_ASSIGN_DIV,"punctuator:no-space:ASSIGN_DIV");

    this.tokenizer.init("<<=")
    equals(this.tokenizer.tokenize(),tokenize.TOKEN_TYPE.P_ASSIGN_SHL,"punctuator:no-space:ASSIGN_SHL");

    this.tokenizer.init(">>=")
    equals(this.tokenizer.tokenize(),tokenize.TOKEN_TYPE.P_ASSIGN_SHR_ARITH,"punctuator:no-space:ASSIGN_SHR_ARITH");

    this.tokenizer.init(">>>=")
    equals(this.tokenizer.tokenize(),tokenize.TOKEN_TYPE.P_ASSIGN_SHR,"punctuator:no-space:ASSIGN_SHR");

    this.tokenizer.init("&=")
    equals(this.tokenizer.tokenize(),tokenize.TOKEN_TYPE.P_ASSIGN_AND,"punctuator:no-space:ASSIGN_AND");

    this.tokenizer.init("|=")
    equals(this.tokenizer.tokenize(),tokenize.TOKEN_TYPE.P_ASSIGN_OR,"punctuator:no-space:ASSIGN_OR");

    this.tokenizer.init("^=")
    equals(this.tokenizer.tokenize(),tokenize.TOKEN_TYPE.P_ASSIGN_XOR,"punctuator:no-space:ASSIGN_XOR");
    //keywords
    this.tokenizer.init("break");
    equals(this.tokenizer.tokenize(),tokenize.TOKEN_TYPE.K_BREAK,"keyword:no-space:K_BREAK");

    this.tokenizer.init("case");
    equals(this.tokenizer.tokenize(),tokenize.TOKEN_TYPE.K_CASE,"keyword:no-space:K_CASE");

    this.tokenizer.init("catch");
    equals(this.tokenizer.tokenize(),tokenize.TOKEN_TYPE.K_CATCH,"keyword:no-space:K_CATCH");

    this.tokenizer.init("continue");
    equals(this.tokenizer.tokenize(),tokenize.TOKEN_TYPE.K_CONTINUE,"keyword:no-space:K_CONTINUE");

    this.tokenizer.init("default");
    equals(this.tokenizer.tokenize(),tokenize.TOKEN_TYPE.K_DEFAULT,"keyword:no-space:K_DEFAULT");

    this.tokenizer.init("delete");
    equals(this.tokenizer.tokenize(),tokenize.TOKEN_TYPE.K_DELETE,"keyword:no-space:K_DELETE");

    this.tokenizer.init("do");
    equals(this.tokenizer.tokenize(),tokenize.TOKEN_TYPE.K_DO,"keyword:no-space:K_DO");

    this.tokenizer.init("finally");
    equals(this.tokenizer.tokenize(),tokenize.TOKEN_TYPE.K_FINALLY,"keyword:no-space:K_FINALLY");

    this.tokenizer.init("for");
    equals(this.tokenizer.tokenize(),tokenize.TOKEN_TYPE.K_FOR,"keyword:no-space:K_FOR");

    this.tokenizer.init("function");
    equals(this.tokenizer.tokenize(),tokenize.TOKEN_TYPE.K_FUNCTION,"keyword:no-space:K_FUNCTION");

    this.tokenizer.init("if");
    equals(this.tokenizer.tokenize(),tokenize.TOKEN_TYPE.K_IF,"keyword:no-space:K_IF");

    this.tokenizer.init("in");
    equals(this.tokenizer.tokenize(),tokenize.TOKEN_TYPE.K_IN,"keyword:no-space:K_IN");

    this.tokenizer.init("instanceof");
    equals(this.tokenizer.tokenize(),tokenize.TOKEN_TYPE.K_INSTANCEOF,"keyword:no-space:K_INSTANCEOF");

    this.tokenizer.init("new");
    equals(this.tokenizer.tokenize(),tokenize.TOKEN_TYPE.K_NEW,"keyword:no-space:K_NEW");

    this.tokenizer.init("return");
    equals(this.tokenizer.tokenize(),tokenize.TOKEN_TYPE.K_RETURN,"keyword:no-space:K_RETURN");

    this.tokenizer.init("switch");
    equals(this.tokenizer.tokenize(),tokenize.TOKEN_TYPE.K_SWITCH,"keyword:no-space:K_SWITCH");

    this.tokenizer.init("this");
    equals(this.tokenizer.tokenize(),tokenize.TOKEN_TYPE.K_THIS,"keyword:no-space:K_THIS");

    this.tokenizer.init("throw");
    equals(this.tokenizer.tokenize(),tokenize.TOKEN_TYPE.K_THROW,"keyword:no-space:K_THROW");

    this.tokenizer.init("try");
    equals(this.tokenizer.tokenize(),tokenize.TOKEN_TYPE.K_TRY,"keyword:no-space:K_TRY");

    this.tokenizer.init("typeof");
    equals(this.tokenizer.tokenize(),tokenize.TOKEN_TYPE.K_TYPEOF,"keyword:no-space:K_TYPEOF");

    this.tokenizer.init("var");
    equals(this.tokenizer.tokenize(),tokenize.TOKEN_TYPE.K_VAR,"keyword:no-space:K_VAR");

    this.tokenizer.init("void");
    equals(this.tokenizer.tokenize(),tokenize.TOKEN_TYPE.K_VOID,"keyword:no-space:K_VOID");

    this.tokenizer.init("while");
    equals(this.tokenizer.tokenize(),tokenize.TOKEN_TYPE.K_WHILE,"keyword:no-space:K_WHILE");

    this.tokenizer.init("with");
    equals(this.tokenizer.tokenize(),tokenize.TOKEN_TYPE.K_WITH,"keyword:no-space:K_WITH");
    //identifier
    this.tokenizer.init("variable");
    equals(this.tokenizer.tokenize(),tokenize.TOKEN_TYPE.IDENTIFIER,"identifier:no-space:IDENTIFIER");
    equals(this.tokenizer.getLiteral(),"variable","identifier name is 'variable'");
    //string literal
    this.tokenizer.init("'string'");
    equals(this.tokenizer.tokenize(),tokenize.TOKEN_TYPE.STRING_LITERAL,"string:no-space:STRING_LITERAL(doubl quote)");
    equals(this.tokenizer.getLiteral(),"string","string vakue is 'string'");

    this.tokenizer.init('"string"');
    equals(this.tokenizer.tokenize(),tokenize.TOKEN_TYPE.STRING_LITERAL,"string:no-space:STRING_LITERAL(single quote)");
    equals(this.tokenizer.getLiteral(),"string","string vakue is 'string'");
    //number literal
    this.tokenizer.init('20e3');
    equals(this.tokenizer.tokenize(),tokenize.TOKEN_TYPE.NUMERIC_LITERAL,"number:no-space:NUMERIC_LITERAL");
    equals(this.tokenizer.getLiteral(),"20e3","number vakue is '20e3'");
    this.tokenizer.init('2.0e3');
    equals(this.tokenizer.tokenize(),tokenize.TOKEN_TYPE.NUMERIC_LITERAL,"number:no-space:NUMERIC_LITERAL");
    equals(this.tokenizer.getLiteral(),"2.0e3","number vakue is '2.0e3'");
    this.tokenizer.init('.03');
    equals(this.tokenizer.tokenize(),tokenize.TOKEN_TYPE.NUMERIC_LITERAL,"number:no-space:NUMERIC_LITERAL");
    equals(this.tokenizer.getLiteral(),".03","number vakue is '.03'");
    //boolean literal
    this.tokenizer.init('true');
    equals(this.tokenizer.tokenize(),tokenize.TOKEN_TYPE.TRUE_LITERAL,"boolean:no-space:TRUE_LITERAL");

    this.tokenizer.init('false');
    equals(this.tokenizer.tokenize(),tokenize.TOKEN_TYPE.FALSE_LITERAL,"boolean:no-space:FALSE_LITERAL");
    //null literal
    this.tokenizer.init('null');
    equals(this.tokenizer.tokenize(),tokenize.TOKEN_TYPE.NULL_LITERAL,"null:no-space:NULL_LITERAL");
    //comment
    this.tokenizer.init('/*test*/');
    equals(this.tokenizer.tokenize(),tokenize.TOKEN_TYPE.EOS,"multi line comment:no-space:EOS");

    this.tokenizer.init('//test');
    equals(this.tokenizer.tokenize(),tokenize.TOKEN_TYPE.EOS,"single line comment:no-space:EOS");
    
    this.tokenizer.init('/*test*/;');
    equals(this.tokenizer.tokenize(),tokenize.TOKEN_TYPE.P_SEMICOLON,"multi line comment:no-space:SEMICOLON");

    this.tokenizer.init('//test\n;');
    equals(this.tokenizer.tokenize(),tokenize.TOKEN_TYPE.P_SEMICOLON,"single line comment:no-space:SEMICOLON");
    //complex
    this.tokenizer.init("(^^)");
    equals(this.tokenizer.tokenize(),tokenize.TOKEN_TYPE.P_PAREN_L,"complex:no-space:(");
    equals(this.tokenizer.tokenize(),tokenize.TOKEN_TYPE.P_XOR,"complex:no-space:^");
    equals(this.tokenizer.tokenize(),tokenize.TOKEN_TYPE.P_XOR,"complex:no-space:^");
    equals(this.tokenizer.tokenize(),tokenize.TOKEN_TYPE.P_PAREN_R,"complex:no-space:)");
    equals(this.tokenizer.tokenize(),tokenize.TOKEN_TYPE.EOS,"complex:no-space:end");
    this.tokenizer.init("( ^ ^ )");
    equals(this.tokenizer.tokenize(),tokenize.TOKEN_TYPE.P_PAREN_L,"complex:space:(");
    equals(this.tokenizer.tokenize(),tokenize.TOKEN_TYPE.P_XOR,"complex:space:^");
    equals(this.tokenizer.tokenize(),tokenize.TOKEN_TYPE.P_XOR,"complex:space:^");
    equals(this.tokenizer.tokenize(),tokenize.TOKEN_TYPE.P_PAREN_R,"complex:space:)");
    equals(this.tokenizer.tokenize(),tokenize.TOKEN_TYPE.EOS,"complex:space:end");
  });
  module("tokenize.js:KeywordMatcher");
  test("addChar",function(){
    var matcher = new tokenize.KeywordMatcher();
    matcher.addChar("a");
    equals(matcher.getTokenType(),tokenize.TOKEN_TYPE.IDENTIFIER,"a is identifier");

    matcher = new tokenize.KeywordMatcher();
    matcher.addChar("b");
    matcher.addChar("r");
    matcher.addChar("e");
    matcher.addChar("a");
    matcher.addChar("k");
    equals(matcher.getTokenType(),tokenize.TOKEN_TYPE.K_BREAK,"break");

    matcher = new tokenize.KeywordMatcher();
    matcher.addChar("c");
    matcher.addChar("a");
    matcher.addChar("s");
    matcher.addChar("e");
    equals(matcher.getTokenType(),tokenize.TOKEN_TYPE.K_CASE,"case");

    matcher = new tokenize.KeywordMatcher();
    matcher.addChar("c");
    matcher.addChar("a");
    matcher.addChar("t");
    matcher.addChar("c");
    matcher.addChar("h");
    equals(matcher.getTokenType(),tokenize.TOKEN_TYPE.K_CATCH,"catch");

    matcher = new tokenize.KeywordMatcher();
    matcher.addChar("c");
    matcher.addChar("o");
    matcher.addChar("n");
    matcher.addChar("t");
    matcher.addChar("i");
    matcher.addChar("n");
    matcher.addChar("u");
    matcher.addChar("e");
    equals(matcher.getTokenType(),tokenize.TOKEN_TYPE.K_CONTINUE,"continue");

    matcher = new tokenize.KeywordMatcher();
    matcher.addChar("d");
    matcher.addChar("e");
    matcher.addChar("f");
    matcher.addChar("a");
    matcher.addChar("u");
    matcher.addChar("l");
    matcher.addChar("t");
    equals(matcher.getTokenType(),tokenize.TOKEN_TYPE.K_DEFAULT,"default");

    matcher = new tokenize.KeywordMatcher();
    matcher.addChar("d");
    matcher.addChar("e");
    matcher.addChar("l");
    matcher.addChar("e");
    matcher.addChar("t");
    matcher.addChar("e");
    equals(matcher.getTokenType(),tokenize.TOKEN_TYPE.K_DELETE,"delete");

    matcher = new tokenize.KeywordMatcher();
    matcher.addChar("d");
    matcher.addChar("o");
    equals(matcher.getTokenType(),tokenize.TOKEN_TYPE.K_DO,"do");

    matcher = new tokenize.KeywordMatcher();
    matcher.addChar("e");
    matcher.addChar("l");
    matcher.addChar("s");
    matcher.addChar("e");
    equals(matcher.getTokenType(),tokenize.TOKEN_TYPE.K_ELSE,"else");

    matcher = new tokenize.KeywordMatcher();
    matcher.addChar("f");
    matcher.addChar("a");
    matcher.addChar("l");
    matcher.addChar("s");
    matcher.addChar("e");
    equals(matcher.getTokenType(),tokenize.TOKEN_TYPE.FALSE_LITERAL,"false");

    matcher = new tokenize.KeywordMatcher();
    matcher.addChar("f");
    matcher.addChar("i");
    matcher.addChar("n");
    matcher.addChar("a");
    matcher.addChar("l");
    matcher.addChar("l");
    matcher.addChar("y");
    equals(matcher.getTokenType(),tokenize.TOKEN_TYPE.K_FINALLY,"finally");

    matcher = new tokenize.KeywordMatcher();
    matcher.addChar("f");
    matcher.addChar("o");
    matcher.addChar("r");
    equals(matcher.getTokenType(),tokenize.TOKEN_TYPE.K_FOR,"for");

    matcher = new tokenize.KeywordMatcher();
    matcher.addChar("f");
    matcher.addChar("u");
    matcher.addChar("n");
    matcher.addChar("c");
    matcher.addChar("t");
    matcher.addChar("i");
    matcher.addChar("o");
    matcher.addChar("n");
    equals(matcher.getTokenType(),tokenize.TOKEN_TYPE.K_FUNCTION,"function");

    matcher = new tokenize.KeywordMatcher();
    matcher.addChar("i");
    matcher.addChar("f");
    equals(matcher.getTokenType(),tokenize.TOKEN_TYPE.K_IF,"if");

    matcher = new tokenize.KeywordMatcher();
    matcher.addChar("i");
    matcher.addChar("n");
    equals(matcher.getTokenType(),tokenize.TOKEN_TYPE.K_IN,"in");

    matcher = new tokenize.KeywordMatcher();
    matcher.addChar("i");
    matcher.addChar("n");
    matcher.addChar("s");
    matcher.addChar("t");
    matcher.addChar("a");
    matcher.addChar("n");
    matcher.addChar("c");
    matcher.addChar("e");
    matcher.addChar("o");
    matcher.addChar("f");
    equals(matcher.getTokenType(),tokenize.TOKEN_TYPE.K_INSTANCEOF,"instanceof");

    matcher = new tokenize.KeywordMatcher();
    matcher.addChar("n");
    matcher.addChar("e");
    matcher.addChar("w");
    equals(matcher.getTokenType(),tokenize.TOKEN_TYPE.K_NEW,"new");

    matcher = new tokenize.KeywordMatcher();
    matcher.addChar("n");
    matcher.addChar("u");
    matcher.addChar("l");
    matcher.addChar("l");
    equals(matcher.getTokenType(),tokenize.TOKEN_TYPE.NULL_LITERAL,"null");

    matcher = new tokenize.KeywordMatcher();
    matcher.addChar("r");
    matcher.addChar("e");
    matcher.addChar("t");
    matcher.addChar("u");
    matcher.addChar("r");
    matcher.addChar("n");
    equals(matcher.getTokenType(),tokenize.TOKEN_TYPE.K_RETURN,"return");

    matcher = new tokenize.KeywordMatcher();
    matcher.addChar("s");
    matcher.addChar("w");
    matcher.addChar("i");
    matcher.addChar("t");
    matcher.addChar("c");
    matcher.addChar("h");
    equals(matcher.getTokenType(),tokenize.TOKEN_TYPE.K_SWITCH,"switch");

    matcher = new tokenize.KeywordMatcher();
    matcher.addChar("t");
    matcher.addChar("h");
    matcher.addChar("i");
    matcher.addChar("s");
    equals(matcher.getTokenType(),tokenize.TOKEN_TYPE.K_THIS,"this");

    matcher = new tokenize.KeywordMatcher();
    matcher.addChar("t");
    matcher.addChar("h");
    matcher.addChar("r");
    matcher.addChar("o");
    matcher.addChar("w");
    equals(matcher.getTokenType(),tokenize.TOKEN_TYPE.K_THROW,"throw");

    matcher = new tokenize.KeywordMatcher();
    matcher.addChar("t");
    matcher.addChar("r");
    matcher.addChar("u");
    matcher.addChar("e");
    equals(matcher.getTokenType(),tokenize.TOKEN_TYPE.TRUE_LITERAL,"true");

    matcher = new tokenize.KeywordMatcher();
    matcher.addChar("t");
    matcher.addChar("r");
    matcher.addChar("y");
    equals(matcher.getTokenType(),tokenize.TOKEN_TYPE.K_TRY,"try");

    matcher = new tokenize.KeywordMatcher();
    matcher.addChar("t");
    matcher.addChar("y");
    matcher.addChar("p");
    matcher.addChar("e");
    matcher.addChar("o");
    matcher.addChar("f");
    equals(matcher.getTokenType(),tokenize.TOKEN_TYPE.K_TYPEOF,"typeof");

    matcher = new tokenize.KeywordMatcher();
    matcher.addChar("v");
    matcher.addChar("a");
    matcher.addChar("r");
    equals(matcher.getTokenType(),tokenize.TOKEN_TYPE.K_VAR,"var");

    matcher = new tokenize.KeywordMatcher();
    matcher.addChar("v");
    matcher.addChar("o");
    matcher.addChar("i");
    matcher.addChar("d");
    equals(matcher.getTokenType(),tokenize.TOKEN_TYPE.K_VOID,"void");

    matcher = new tokenize.KeywordMatcher();
    matcher.addChar("w");
    matcher.addChar("h");
    matcher.addChar("i");
    matcher.addChar("l");
    matcher.addChar("e");
    equals(matcher.getTokenType(),tokenize.TOKEN_TYPE.K_WHILE,"while");

    matcher = new tokenize.KeywordMatcher();
    matcher.addChar("w");
    matcher.addChar("i");
    matcher.addChar("t");
    matcher.addChar("h");
    equals(matcher.getTokenType(),tokenize.TOKEN_TYPE.K_WITH,"with");

    matcher = new tokenize.KeywordMatcher();
    matcher.addChar("w");
    matcher.addChar("i");
    matcher.addChar("t");
    matcher.addChar("h");
    matcher.addChar("h");
    matcher.addChar("d");
    matcher.addChar("r");
    matcher.addChar("a");
    matcher.addChar("w");
    equals(matcher.getTokenType(),tokenize.TOKEN_TYPE.IDENTIFIER,"withdrawは予約後ではなく識別子");
  });
  test("fail",function(){
    matcher = new tokenize.KeywordMatcher();
    matcher.addChar("t");
    matcher.addChar("h");
    matcher.fail();
    matcher.addChar("i");
    matcher.addChar("s");
    equals(matcher.getTokenType(),tokenize.TOKEN_TYPE.IDENTIFIER,"this is keyword,but fail() cause unmatch keyword");
  });
});
