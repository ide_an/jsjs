/**
 * @fileOverview JavaScriptコードのトークン解析を行うコード群
 * @author ide
 * @requires letter.js
 */
/**
 * JavaScript Tokenizer  container
 * @namespace 
 */
var tokenize =  {};
(function(){
  /**
   * @constant token type
   */
  tokenize.TOKEN_TYPE = {
    //keywords
    K_BREAK:0,            // break
    K_CASE:1,             // case
    K_CATCH:2,            // catch
    K_CONTINUE:3,         // continue
    K_DEFAULT:4,          // default
    K_DELETE:5,           // delete
    K_DO:6,               // do
    K_ELSE:7,             // else
    K_FINALLY:8,          // finally
    K_FOR:9,              // for
    K_FUNCTION:10,        // function
    K_IF:11,              // if
    K_IN:12,              // in
    K_INSTANCEOF:13,      // instanceof
    K_NEW:14,             // new
    K_RETURN:15,          // return
    K_SWITCH:16,          // switch
    K_THIS:17,            // this
    K_THROW:18,           // throw
    K_TRY:19,             // try
    K_TYPEOF:20,          // typeof
    K_VAR:21,             // var
    K_VOID:22,            // void
    K_WHILE:23,           // while
    K_WITH:24,            // with
    //future reserved keywords
    KR_ABSTRACT:25,       // abstract
    KR_BOOLEAN:26,        // boolean
    KR_BYTE:27,           // byte
    KR_CHAR:28,           // char
    KR_CLASS:29,          // class
    KR_CONST:30,          // const
    KR_DEBUGGER:31,       // debugger
    KR_DOUBLE:32,         // double
    KR_ENUM:33,           // enum
    KR_EXPORT:34,         // export
    KR_EXTENDS:35,        // extends
    KR_FINAL:36,          // final
    KR_FLOAT:37,          // float
    KR_GOTO:38,           // goto
    KR_IMPLEMENTS:39,     // implements
    KR_IMPORT:40,         // import
    KR_INT:41,            // int
    KR_INTERFACE:42,      // interface
    KR_LONG:43,           // long
    KR_NATIVE:44,         // native
    KR_PACKAGE:45,        // package
    KR_PRIVATE:46,        // private
    KR_PROTECTED:47,      // protected
    KR_PUBLIC:48,         // public
    KR_SHORT:49,          // short
    KR_STATIC:50,         // static
    KR_SUPER:51,          // super
    KR_SYNCHRONIZED:52,   // synchronized
    KR_THROWS:53,         // throws
    KR_TRANSIENT:54,      // transient
    KR_VOLATILE:55,       // volatile
    //punctuators
    P_BRACE_L:56,         // {
    P_BRACE_R:57,         // }
    P_PAREN_L:58,         // (
    P_PAREN_R:59,         // )
    P_BRACKET_L:60,       // [
    P_BRACKET_R:61,       // ]
    P_PERIOD:62,          // .
    P_SEMICOLON:63,       // ;
    P_COMMA:64,           // ,
    P_LT:65,              // <
    P_GT:66,              // >
    P_LEQ:67,             // <=
    P_GEQ:68,             // >=
    P_EQUAL:69,           // ==
    P_NOT_EQUAL:70,       // !=
    P_EQAUL_STRICT:71,    // ===
    P_NOT_EQUAL_STRICT:72,// !==
    P_ADD:73,             // +
    P_SUB:74,             // -
    P_MUL:75,             // *
    P_MOD:76,             // %
    P_DIV: 109,           // /
    P_INC:77,             // ++
    P_DEC:78,             // --
    P_SHL:79,             // <<
    P_SHR:80,             // >>>
    P_SHR_ARITH:81,       // >>
    P_AND:82,             // &
    P_OR:83,              // |
    P_XOR:84,             // ^
    P_NOT:85,             // !
    P_COMP:86,            // ~
    P_AND_LOGIC:87,       // &&
    P_OR_LOGIC:88,        // ||
    P_COND:89,            // ?
    P_COLON:90,           // :
    P_ASSIGN:91,          // =
    P_ASSIGN_ADD:92,      // +=
    P_ASSIGN_SUB:93,      // -=
    P_ASSIGN_MUL:94,      // *=
    P_ASSIGN_MOD:95,      // %=
    P_ASSIGN_DIV: 110,    // /=
    P_ASSIGN_SHL:96,      // <<=
    P_ASSIGN_SHR:97,      // >>>=
    P_ASSIGN_SHR_ARITH:98,// >>=
    P_ASSIGN_AND:99,      // &=
    P_ASSIGN_OR:100,      // |=
    P_ASSIGN_XOR:101,     // ^=
    //other token         //
    IDENTIFIER:102,        //
    NUMERIC_LITERAL: 103, //
    STRING_LITERAL: 104,  //
    //REGEXP_LITERAL: 105,  //
    NULL_LITERAL: 106,    //
    TRUE_LITERAL: 107,
    FALSE_LITERAL: 108,
    //illegal token
    ILLEGAL_TOKEN: 111,
    EOS: 112,
    //etc
    WHITESPACE: 113//,
    //LINE_TERMINATOR: 113
  };
  var TOKEN = tokenize.TOKEN_TYPE;//internal use
  /**
   * @class Keyword matching for JavaScript keywords
   */
  tokenize.KeywordMatcher = function(){
    this._str = "";
    this._token = TOKEN.IDENTIFIER;
    this._state = this.STATES.initial;
    this._keyword = "";
    this._keyword_token = TOKEN.ILLEGAL_TOKEN;
    this._counter = 0;
  };
  /** @constant states of this._state */
  tokenize.KeywordMatcher.prototype.STATES = {
    initial: 0,
    unmatch: 1,
    keywordPrefix: 2,
    keywordMatched: 3,
    C:4,
    D:5,
    F:6,
    I:7,
    N:8,
    T:9,
    V:10,
    W:11,
    CA:12,
    DE:13,
    IN:14,
    TH:15,
    TR:16
  };
  var STATES = tokenize.KeywordMatcher.prototype.STATES;//internal use
  tokenize.KeywordMatcher.prototype.FIRST_STATES = {
    a:{keyword:null,    state:STATES.unmatch,       token:TOKEN.ILLEGAL_TOKEN },
    b:{keyword:"break", state:STATES.keywordPrefix, token:TOKEN.K_BREAK       },
    c:{keyword:null,    state:STATES.C,             token:TOKEN.ILLEGAL_TOKEN },
    d:{keyword:null,    state:STATES.D,             token:TOKEN.ILLEGAL_TOKEN },
    e:{keyword:"else",  state:STATES.keywordPrefix, token:TOKEN.K_ELSE        },
    f:{keyword:null,    state:STATES.F,             token:TOKEN.ILLEGAL_TOKEN },
    g:{keyword:null,    state:STATES.unmatch,       token:TOKEN.ILLEGAL_TOKEN },
    h:{keyword:null,    state:STATES.unmatch,       token:TOKEN.ILLEGAL_TOKEN },
    i:{keyword:null,    state:STATES.I,             token:TOKEN.ILLEGAL_TOKEN },
    j:{keyword:null,    state:STATES.unmatch,       token:TOKEN.ILLEGAL_TOKEN },
    k:{keyword:null,    state:STATES.unmatch,       token:TOKEN.ILLEGAL_TOKEN },
    l:{keyword:null,    state:STATES.unmatch,       token:TOKEN.ILLEGAL_TOKEN },
    m:{keyword:null,    state:STATES.unmatch,       token:TOKEN.ILLEGAL_TOKEN },
    n:{keyword:null,    state:STATES.N,             token:TOKEN.ILLEGAL_TOKEN },
    o:{keyword:null,    state:STATES.unmatch,       token:TOKEN.ILLEGAL_TOKEN },
    p:{keyword:null,    state:STATES.unmatch,       token:TOKEN.ILLEGAL_TOKEN },
    q:{keyword:null,    state:STATES.unmatch,       token:TOKEN.ILLEGAL_TOKEN },
    r:{keyword:"return",state:STATES.keywordPrefix, token:TOKEN.K_RETURN      },
    s:{keyword:"switch",state:STATES.keywordPrefix, token:TOKEN.K_SWITCH      },
    t:{keyword:null,    state:STATES.T,             token:TOKEN.ILLEGAL_TOKEN },
    u:{keyword:null,    state:STATES.unmatch,       token:TOKEN.ILLEGAL_TOKEN },
    v:{keyword:null,    state:STATES.V,             token:TOKEN.ILLEGAL_TOKEN },
    w:{keyword:null,    state:STATES.W,             token:TOKEN.ILLEGAL_TOKEN },
    x:{keyword:null,    state:STATES.unmatch,       token:TOKEN.ILLEGAL_TOKEN },
    y:{keyword:null,    state:STATES.unmatch,       token:TOKEN.ILLEGAL_TOKEN },
    z:{keyword:null,    state:STATES.unmatch,       token:TOKEN.ILLEGAL_TOKEN } 
  };
  /**
   * @param {String} keyword
   * @param {Number} counter
   * @param {tokenize.TOKEN_TYPE} keywordToken
   */
  tokenize.KeywordMatcher.prototype.matchKeywordStart = function(keyword,counter,keywordToken){
    this._state = this.STATES.keywordPrefix;
    this._keyword = keyword;
    this._keyword_token = keywordToken;
    this._counter = counter;
  };
  /**
   * @param {tokenize.TOKEN_TYPE} keywordToken
   */
  tokenize.KeywordMatcher.prototype.matchToken = function(keywordToken){
    this._state = this.STATES.keywordMatched;
    this._token = keywordToken;
  };
  tokenize.KeywordMatcher.prototype.addChar = function(/** String */c){
    if(this._state !== this.STATES.unmatch){
      switch(this._state){
        case this.STATES.initial:
          if(c in this.FIRST_STATES){
            var firstState = this.FIRST_STATES[c];
            this._state = firstState.state;
            if(firstState.state===this.STATES.keywordPrefix){
              this._keyword = firstState.keyword;
              this._keyword_token = firstState.token;
              this._counter = 1;
            }
            return;
          }
          break;
        case this.STATES.keywordPrefix:
          if(c===this._keyword.charAt(this._counter)){
            this._counter++;
            if(this._counter===this._keyword.length){
              this._state = this.STATES.keywordMatched;
              this._token = this._keyword_token;
            }
            return;
          }
          //
          break;
        case this.STATES.keywordMatched:
          this._token = TOKEN.IDENTIFIER;
          break;
        case this.STATES.C:
          if(c==="a"){
            this._state = this.STATES.CA;
            return;
          }
          if(c==="o"){
            this.matchKeywordStart("continue",2,TOKEN.K_CONTINUE);
            return;
          }
          break;
        case this.STATES.CA:
          if(c==="s"){
            this.matchKeywordStart("case",3,TOKEN.K_CASE);
            return;
          }
          if(c==="t"){
            this.matchKeywordStart("catch",3,TOKEN.K_CATCH);
            return;
          }
          break;
        case this.STATES.D:
          if(c==="e"){
            this._state = this.STATES.DE;
            return;
          }
          if(c==="o"){
            this.matchToken(TOKEN.K_DO);
            return;
          }
          break;
        case this.STATES.DE:
          if(c==="f"){
            this.matchKeywordStart("default",3,TOKEN.K_DEFAULT);
            return;
          }
          if(c==="l"){
            this.matchKeywordStart("delete",3,TOKEN.K_DELETE);
            return;
          }
          break;
        case this.STATES.F:
          if(c==="a"){
            this.matchKeywordStart("false",2,TOKEN.FALSE_LITERAL);
            return;
          }
          if(c==="i"){
            this.matchKeywordStart("finally",2,TOKEN.K_FINALLY);
            return;
          }
          if(c==="o"){
            this.matchKeywordStart("for",2,TOKEN.K_FOR);
            return;
          }
          if(c==="u"){
            this.matchKeywordStart("function",2,TOKEN.K_FUNCTION);
            return;
          }
          break;
        case this.STATES.I:
          if(c==="f"){
            this.matchToken(TOKEN.K_IF);
            return;
          }
          if(c==="n"){
            this._token = TOKEN.K_IN;
            this._state = this.STATES.IN;
            return;
          }
          break;
        case this.STATES.IN:
          if(c==="s"){
            this.matchKeywordStart("instanceof",3,TOKEN.K_INSTANCEOF);
            return;
          }else{
            this._token = TOKEN.IDENTIFIER;
          }
          break;
        case this.STATES.N:
          if(c==="e"){
            this.matchKeywordStart("new",2,TOKEN.K_NEW);
            return;
          }
          if(c==="u"){
            this.matchKeywordStart("null",2,TOKEN.NULL_LITERAL);
            return;
          }
          break;
        case this.STATES.T:
          if(c==="h"){
            this._state = this.STATES.TH;
            return;
          }
          if(c==="r"){
            this._state = this.STATES.TR;
            return;
          }
          if(c==="y"){
            this.matchKeywordStart("typeof",2,TOKEN.K_TYPEOF);
            return;
          }
          break;
        case this.STATES.TH:
          if(c==="i"){
            this.matchKeywordStart("this",3,TOKEN.K_THIS);
            return;
          }
          if(c==="r"){
            this.matchKeywordStart("throw",3,TOKEN.K_THROW);
            return;
          }
          break;
        case this.STATES.TR:
          if(c==="u"){
            this.matchKeywordStart("true",3,TOKEN.TRUE_LITERAL);
            return;
          }
          if(c==="y"){
            this.matchToken(TOKEN.K_TRY);
            return;
          }
          break;
        case this.STATES.V:
          if(c==="a"){
            this.matchKeywordStart("var",2,TOKEN.K_VAR);
            return;
          }
          if(c==="o"){
            this.matchKeywordStart("void",2,TOKEN.K_VOID);
            return;
          }
          break;
        case this.STATES.W:
          if(c==="h"){
            this.matchKeywordStart("while",2,TOKEN.K_WHILE);
            return;
          }
          if(c==="i"){
            this.matchKeywordStart("with",2,TOKEN.K_WITH);
            return;
          }
          break;
        default:
          throw new Error("unreachable case@KeywordMatcher.addChar:state="+this._state+";char="+c);
      }
      this._state = this.STATES.unmatch;
    }
  };
  /**
   * Set unmatch current keyword
   */
  tokenize.KeywordMatcher.prototype.fail = function(){
    this._state = this.STATES.unmatch;
    this._token = TOKEN.IDENTIFIER;
  };
  /**
   * @return {tokenize.TOKEN_TYPE} token type of current keyword
   */
  tokenize.KeywordMatcher.prototype.getTokenType = function(){
    return this._token;
  };
  /**
   * @class Tokenize JavaScript code
   */
  tokenize.Tokenizer = function(){
    this._src = "";
    this._current_position = 0;
    this._current_token = null;
    this._c0 = "";
    this._hasLineTerminator = false;
  };
  /**
   * 初期化
   * @param {String} source source code to scan
   * @param {Number} [startAt=0] start scan at
   */
  tokenize.Tokenizer.prototype.init = function(source,startAt){
    this._src =source 
    this._current_position = startAt || 0;
    this._current_token = null;
    this._c0 = this._src.charAt(this._current_position); 
    this._hasLineTerminator = false;
  };
  /**
   * トークナイズざます。
   * @return {tokenize.TOKEN_TYPE} トークン種 
   */
  tokenize.Tokenizer.prototype.tokenize = function(){
    return this.scanJavaScript();
  };
  /**
   * 今指してるトークン種を返す
   * @return {tokenize.TOKEN_TYPE} 現在指しているトークン種
   */
  tokenize.Tokenizer.prototype.current = function(){
    return this._current_token._type;
  };
  /**
   * Scan JavaScript code 
   * @return {tokenize.TOKEN_TYPE} 取得したトークン種
   */
  tokenize.Tokenizer.prototype.scanJavaScript = function(){
    var that = this;
    var select = function(isChar,then_token,else_token){
      that.advance();
      if(that._c0===isChar){
        token.type(then_token);
        that.advance();
      }else{
        token.type(else_token);
      }
    };
    var selectOne = function(then_token){
      token.type(then_token);
      that.advance();
    };
    var token = this._current_token = new tokenize.TokenInfo(this.getPosition());
    this._hasLineTerminator = false;//でおｋ？
    do{
      token.start(this.getPosition());
      if(this._c0===TOKEN.EOS){
        token.type(TOKEN.EOS); 
        return TOKEN.EOS;
      }
      switch(this._c0){
        case "{": selectOne(TOKEN.P_BRACE_L); break;
        case "}": selectOne(TOKEN.P_BRACE_R); break;
        case "(": selectOne(TOKEN.P_PAREN_L); break;
        case ")": selectOne(TOKEN.P_PAREN_R); break;
        case "[": selectOne(TOKEN.P_BRACKET_L); break;
        case "]": selectOne(TOKEN.P_BRACKET_R); break;
        case ".": 
          this.advance();
          if(letter.isDecimalDigit(this._c0)){// number format like .01
            token.type(this.scanNumericLiteral(true));
          }else{
            token.type(TOKEN.P_PERIOD); 
          }
          break;
        case ";": selectOne(TOKEN.P_SEMICOLON); break;
        case ",": selectOne(TOKEN.P_COMMA); break;
        case "<":// < << <= <<=
          this.advance();
          if(this._c0==="<"){// << <<=
            select("=",TOKEN.P_ASSIGN_SHL,TOKEN.P_SHL);
          }else if(this._c0==="="){// <=
            selectOne(TOKEN.P_LEQ);
          }else{// <
            token.type(TOKEN.P_LT);
          }
          break;
        case ">":// > >> >>> >= >>= >>>=
          this.advance();
          if(this._c0===">"){// >> >>> >>= >>>=
            this.advance();
            if(this._c0===">"){// >>> >>>=
              select("=",TOKEN.P_ASSIGN_SHR,TOKEN.P_SHR);
            }else if(this._c0==="="){// >>=
              selectOne(TOKEN.P_ASSIGN_SHR_ARITH);
            }else{// >>
              token.type(TOKEN.P_SHR_ARITH);
            }
          }else if(this._c0==="="){// >=
            selectOne(TOKEN.P_GEQ);
          }else{// >
            token.type(TOKEN.P_GT);
          }
          break;
        case "=":// = == ===
          this.advance();
          if(this._c0==="="){// == ===
            select("=",TOKEN.P_EQAUL_STRICT,TOKEN.P_EQUAL);
          }else{// =
            token.type(TOKEN.P_ASSIGN);
          }
          break;
        case "!":// ! != !==
          this.advance();
          if(this._c0==="="){// != !==
            select("=",TOKEN.P_NOT_EQUAL_STRICT,TOKEN.P_NOT_EQUAL);
          }else{// !
            token.type(TOKEN.P_NOT);
          }
          break;
        case "+": // + ++ +=
          this.advance();
          if(this._c0==="+"){// ++
            selectOne(TOKEN.P_INC);
          }else if(this._c0==="="){// +=
            selectOne(TOKEN.P_ASSIGN_ADD);
          }else{// +
            token.type(TOKEN.P_ADD);
          }
          break;
        case "-": // - -- -=
          this.advance();
          if(this._c0==="-"){// --
            selectOne(TOKEN.P_DEC);
          }else if(this._c0==="="){// -=
            selectOne(TOKEN.P_ASSIGN_SUB);
          }else{// -
            token.type(TOKEN.P_SUB);
          }
          break;
        case "*":// * *=
          select("=",TOKEN.P_ASSIGN_MUL,TOKEN.P_MUL);
          break;
        case "%":// % %=
          select("=",TOKEN.P_ASSIGN_MOD,TOKEN.P_MOD);
          break;
        case "/":// / /= /**/ //
          this.advance();
          if(this._c0==="/"){// //
            token.type(this.skipSingleLineComment());
          }else if(this._c0==="*"){// /* */
            token.type(this.skipMultiLineComment());
          }else if(this._c0==="="){// /=
            selectOne(TOKEN.P_ASSIGN_DIV);
          }else{// /
            token.type(TOKEN.P_DIV);
          }
          break;
        case "&": // & && &=
          this.advance();
          if(this._c0==="&"){// &&
            selectOne(TOKEN.P_AND_LOGIC);
          }else if(this._c0==="="){// &=
            selectOne(TOKEN.P_ASSIGN_AND);
          }else{// &
            token.type(TOKEN.P_AND);
          }
          break;
        case "|": // | || |=
          this.advance();
          if(this._c0==="|"){// ||
            selectOne(TOKEN.P_OR_LOGIC);
          }else if(this._c0==="="){// |=
            selectOne(TOKEN.P_ASSIGN_OR);
          }else{// |
            token.type(TOKEN.P_OR);
          }
          break;
        case "^":// ^ ^=
          select("=",TOKEN.P_ASSIGN_XOR,TOKEN.P_XOR);
          break;
        case "~": selectOne(TOKEN.P_COMP); break;
        case "?": selectOne(TOKEN.P_COND); break;
        case ":": selectOne(TOKEN.P_COLON); break;
        case "'"://fall through
        case '"':
          token.type(this.scanStringLiteral());
          break;
        default:
          if(letter.isWhitespace(this._c0)){
            token.type(TOKEN.WHITESPACE);
            this.advance();
          }else if(letter.isLineTerminator(this._c0)){
            this._hasLineTerminator = true;
            token.type(TOKEN.WHITESPACE);
            this.advance();
          }else if(letter.isIdentifierStart(this._c0)){
            token.type(this.scanIdentifier());
          }else if(letter.isDecimalDigit(this._c0)){
            token.type(this.scanNumericLiteral(false));
          }else{
            this.advance();
          }
      }
      if(token._type===TOKEN.ILLEGAL_TOKEN){
        throw new Error(this._c0+":"+token._startOffset);
      }
      //TODO: ILLEGAL_TOKEN handling
    }while(token._type===TOKEN.WHITESPACE);//空白飛ばす
    token.end(this.getPosition());
    return token._type;
  };
  /**
   * Skip single line comment
   * @return {tokenize.TOKEN_TYPE}
   */
  tokenize.Tokenizer.prototype.skipSingleLineComment = function(){
    if(this._c0 !== "/"){
      throw new Error("行コメントの2文字目は/なはず:"+this._c0);
    }
    this.advance();// consume 2nd /
    while(this._c0 !== TOKEN.EOS && !letter.isLineTerminator(this._c0)){
      this.advance();
    }
    return TOKEN.WHITESPACE;
  };
  /**
   * Skip mult line comment
   * @return {tokenize.TOKEN_TYPE}
   */
  tokenize.Tokenizer.prototype.skipMultiLineComment = function(){
    if(this._c0 !== "*"){
      throw new Error("複数行コメントの2文字目は*なはず:"+this._c0);
    }
    var c;
    var hasLineTerminator = false;
    this.advance();//consume *
    while(this._c0 !== TOKEN.EOS){
      var c = this._c0
      this.advance();
      if(c==="*" && this._c0==="/"){
        this._c0 = " ";
        if(hasLineTerminator){
          this._hasLineTerminator = true;
          this._c0 = "\n";
        }
        return TOKEN.WHITESPACE;
      }
      if(letter.isLineTerminator(this._c0)){//ECMA 262 3th sec7.4 コメント中に改行を含むならlineTerminatorとして扱う。
        hasLineTerminator = true;
      }
    }
    return TOKEN.ILLEGAL_TOKEN;
  };
  /**
   * @return {tokenize.TOKEN_TYPE}
   */
  tokenize.Tokenizer.prototype.scanStringLiteral = function(){
    if(this._c0 !== "\"" && this._c0 !== "'"){
      throw new Error("文字列リテラルは\"か'で始まるべき:this._c0="+this._c0);
    }
    var quote = this._c0;
    this.advance();//quoteそのものは拾わない。
    while(this._c0 !== quote && this._c0 !== TOKEN.EOS && !letter.isLineTerminator(this._c0)){
      var c = this._c0;
      var res;
      this.advance();
      if(c === "\\"){
        switch(this._c0){
          case "'":
          case '"':
          case '/':
          case '\\':
            this.addChar(this._c0);
            this.advance();
            break;
          case 'b':
            this.addChar("\b");
            this.advance();
            break;
          case 't':
            this.addChar("\t");
            this.advance();
            break;
          case 'n':
            this.addChar("\n");
            this.advance();
            break;
          case 'v':
            this.addChar("\v");
            this.advance();
            break;
          case 'f':
            this.addChar("\f");
            this.advance();
            break;
          case 'r':
            this.addChar("\r");
            this.advance();
            break;
          case 'u':
            this.advance();
            res = this.scanHexDigits(4);
            if(res===null){
              return TOKEN.ILLEGAL_TOKEN;
            }
            this.addChar(String.fromCharCode(res));
            break;
          case 'x':
            this.advance();
            res = this.scanHexDigits(2);
            if(res===null){
              return TOKEN.ILLEGAL_TOKEN;
            }
            this.addChar(String.fromCharCode(res));
            break;
          case '0':
          case '1':
          case '2':
          case '3':
          case '4':
          case '5':
          case '6':
          case '7':
            this.addChar(String.fromCharCode(this.scanOctalDigits(2)));
            break;
          case TOKEN.EOS:
            return TOKEN.ILLEGAL_TOKEN;
            break;
          default:
            if(letter.isLineTerminator(this._c0)){
              return TOKEN.ILLEGAL_TOKEN;
            }else{
              this.addChar(this._c0);
              this.advance();
            }
        }
      }else{
        this.addChar(c);
      }
    }
    if(this._c0 !== quote){
      return TOKEN.ILLEGAL_TOKEN;
    }
    this._current_token.end(this.getPosition());
    this.advance();//quoteそのものは拾わない。
    return TOKEN.STRING_LITERAL;
  };
  /**
   * @param {Boolean} hasPeriod Is start with period format(like ".10") ?
   * @return {tokenize.TOKEN_TYPE}
   */
  tokenize.Tokenizer.prototype.scanNumericLiteral = function(hasPeriod){
    var AXIS = {
      decimal: 0,
      hex: 1
    };
    var axis;
    if(hasPeriod){
      this.addChar(".");
    }
    var c = this._c0;
    this.advance();
    //ISSUE: should implement octal NUMERIC_LITERAL ? ecma262 dont require...
    if(c==="0" && this._c0==="x"){//hex
      axis = AXIS.hex;
      this.addChar(c);
      this.addChar(this._c0);
    }else{
      axis = AXIS.decimal;
    }

    switch(axis){
      case AXIS.hex:
        this.advance();
        var hexLen = 0;
        while(letter.isHexDigit(this._c0)){
          this.addChar(this._c0);
          this.advance();
          hexLen++;
        }
        if(hexLen<1){
          return TOKEN.ILLEGAL_TOKEN;
        }
        break;
      case AXIS.decimal:
        //整数部
        if(letter.isNonZeroDecimalDigit(c)){
          this.addChar(c);
          while(letter.isDecimalDigit(this._c0)){
            this.addChar(this._c0);
            this.advance();
          }
        }else if(c==="0"){
          this.addChar(c);
        }
        if(!hasPeriod && this._c0==="."){
          hasPeriod = true;
          this.addChar(this._c0);
          this.advance();
        }
        //小数部
        if(hasPeriod){
          while(letter.isDecimalDigit(this._c0)){
            this.addChar(this._c0);
            this.advance();
          }
        }
        //指数部
        if(this._c0 === "e" || this._c0 === "E"){
          this.addChar(this._c0);
          this.advance();
          if(this._c0 === "+" || this._c0 === "-"){//sign
            this.addChar(this._c0);
            this.advance();
          }
          while(letter.isDecimalDigit(this._c0)){
            this.addChar(this._c0);
            this.advance();
          }
        }
        break;
      default:
        return TOKEN.ILLEGAL_TOKEN;
    }
    if(letter.isIdentifierStart(this._c0)){
      return TOKEN.ILLEGAL_TOKEN;
    }
    return TOKEN.NUMERIC_LITERAL;
  };
  /**
   * @return {tokenize.TOKEN_TYPE}
   */
  tokenize.Tokenizer.prototype.scanIdentifier = function(){
    var matcher = new tokenize.KeywordMatcher();
    var charCode = null;
    var c = this._c0;
    if(!letter.isIdentifierStart(this._c0)){
      throw new Error("invalid start characer of identifier:"+this._c0);
    }
    this.advance();
    if(c==="\\"){
      matcher.fail();
      if(this._c0==="u"){// unicode escape
        this.advance();
        charCode = this.scanHexDigits(4);
        if(charCode===null){
          return TOKEN.ILLEGAL_TOKEN;
        }
        c = String.fromCharCode(charCode);
      }else{// ILLEGAL_TOKEN
        return TOKEN.ILLEGAL_TOKEN;
      }
    }
    this.addChar(c);
    matcher.addChar(c);
    while(letter.isIdentifierPart(this._c0)){
      c = this._c0;
      this.advance();
      if(c==="\\"){
        matcher.fail();
        if(this._c0==="u"){// unicode escape
          this.advance();
          charCode = this.scanHexDigits(4);
          if(charCode===null){
            return TOKEN.ILLEGAL_TOKEN;
          }
          c = String.fromCharCode(charCode);
        }else{// ILLEGAL_TOKEN
          return TOKEN.ILLEGAL_TOKEN;
        }
      }
      this.addChar(c);
      matcher.addChar(c);
    }
    return matcher.getTokenType();
  };
  /**
   * @return {Boolean} Is parsing success? 
   */
  tokenize.Tokenizer.prototype.scanRegularExpressionBody = function(/** Boolean */afterEqual){
    var inCharacterClass = false;
    this._current_token = new tokenize.TokenInfo(this.getPosition() - afterEqual ? 2 : 1);
    this._current_token.end(this._current_token.start+1);
    if(afterEqual){
      this.addChar("=");
    }
    while(this._c0 !== "/" || inCharacterClass){
      if(letter.isLineTerminator(this._c0) || this._c0 === TOKEN.EOS){
        return false;
      }
      if(this._c0==="\\"){//escaped characer
        this.addChar("\\");
        this.advance();
        if(letter.isLineTerminator(this._c0) || this._c0 === TOKEN.EOS){
          return false;
        }
        this.addChar(this._c0);
      }else{
        if(this._c0==="["){
          inCharacterClass = true;
        }else if(this._c0==="]"){
          inCharacterClass = false;
        }
        this.addChar(this._c0);
      }
      this.advance();
    }
    this.advance();// consume /
    return true;
  };
  /**
   * @return {Boolean} Is parsing success? 
   */
  tokenize.Tokenizer.prototype.scanRegularExpressionFlag = function(){
    this._current_token = new tokenize.TokenInfo(this.getPosition());
    while(letter.isIdentifierPart(this._c0)){//フラグ回収
      this.addChar(this._c0);
      this.advance();
    }
    this._current_token.end(this.getPosition());
    return true;
  };
  tokenize.Tokenizer.prototype.scanHexDigits = function(len){
    var text = "";
    var i = 0;
    var c;
    while(i<len){
      c = this._c0;
      if(letter.isHexDigit(c)){
        text += c;
      }else{
        return null;
      }
      this.advance();
      i++;
    }
    return parseInt(text,16);
  };
  tokenize.Tokenizer.prototype.scanOctalDigits = function(len){
    var text = "";
    var i = 0;
    var c;
    while(i<len){
      c = this._c0;
      if(letter.isOctalDigit(c)){
        text += c;
      }else{
        break;
      }
      this.advance();
      i++;
    }
    return parseInt(text,8);
  };
  /**
   * 次の文字にポインタを進める
   */
  tokenize.Tokenizer.prototype.advance = function(){
    this._current_position++;
    if(this._current_position >= this._src.length){
      this._c0 = TOKEN.EOS;
    }else{
      this._c0 = this._src.charAt(this._current_position);
    }
  };
  /**
   * get current source pointer
   */
  tokenize.Tokenizer.prototype.getPosition = function(){
    return this._current_position;
  };
  /**
   * リテラルに1文字追加
   */
  tokenize.Tokenizer.prototype.addChar = function(/** String */c){
    this._current_token.addChar(c);
  };
  /**
   * リテラル取得
   */
  tokenize.Tokenizer.prototype.getLiteral = function(){
    return this._current_token._literalString;
  };

  /**
   * @class TokenInfo 各トークンの情報を表す
   * @param {Number} [start=0] トークン開始位置
   */
  tokenize.TokenInfo = function(start){
    /** 
     * @type tokenize.TOKEN_TYPE 
     * @description トークンの種類 
     * */
    this._type = TOKEN.ILLEGAL_TOKEN;
    /**  
     * @type Number
     * @description トークン開始位置 
     * */
    this._startOffset = start || 0;
    /** 
     * @type Number 
     * @description トークン終了位置 
     * */
    this._endOffset = 0;
    /**  
     * @type String 
     * @description リテラル文字列 
     * */
    this._literalString = "";
  };
  /**
   * トークン開始位置指定 
   */
  tokenize.TokenInfo.prototype.start = function(/** Number */start){
    this._startOffset = start;
  };
  /**
   * トークン終了位置指定 
   */
  tokenize.TokenInfo.prototype.end = function(/** Number */end){
    this._endOffset = end;
  };
  /**
   * リテラルに1文字追加 
   */
  tokenize.TokenInfo.prototype.addChar = function(/** String */c){
    this._literalString += c;
  };
  /**
   * トークン種類指定 
   */
  tokenize.TokenInfo.prototype.type = function(/** tokenize.TOKEN_TYPE */type){
    this._type = type;
  };
})();
